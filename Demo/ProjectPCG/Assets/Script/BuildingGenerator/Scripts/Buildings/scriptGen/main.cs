using System;
using System.Collections.Generic;

namespace ScriptGen {

class Program {

    static void Main(string[] args)
    {	
		ScriptGenerator scriptGen = new ScriptGenerator();
	
		List<string> rooms = scriptGen.getScriptRooms(BaseShape.L_SHAPE);
	
		foreach(string room in rooms) {
			Console.WriteLine(room + "#HAI\n");
		}
	
    }

};


}