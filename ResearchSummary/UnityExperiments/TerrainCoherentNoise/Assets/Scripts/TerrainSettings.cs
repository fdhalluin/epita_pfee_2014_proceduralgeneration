﻿using UnityEngine;
using System.Collections;
using CoherentNoise;
using CoherentNoise.Generation;
using CoherentNoise.Generation.Displacement;
using CoherentNoise.Generation.Fractal;
using CoherentNoise.Generation.Modification;
using CoherentNoise.Generation.Patterns;


/*
 *  Settings for the terrain
 * 	Generator is the noise for heights
 *	width of the terrain
 *	length of the terrain
 */

public class TerrainSettings 
{
	private Generator m_Generator;
	private int width;
	private int length;

	public TerrainSettings()
	{
		width = 1000;
		length = 1000;

		var desert = new Gain(
			new RidgeNoise(23478568)
			{
			OctaveCount = 8
		} * 0.6f, 0.4f);
		
		m_Generator = desert;
	}

	public Generator getGenerator()
	{
		return m_Generator;
	}

	public void setGenerator(Generator generator)
	{
		m_Generator = generator;
	}

	public int getWidth()
	{
		return width;
	}

	public void setWidth(int value)
	{
		width = value;
	}

	public int getLength()
	{
		return length;
	}
	
	public void setLength(int value)
	{
		length = value;
	}

}