
Room_18:
size 14 15
height 2
build Room_18 50 50
addRoom NORTH Door_19 RANDOM
onTop Room_20 2 6
displacement 0 0.3 -0.2 
windows NORTH 0 3 3 SQUARE
windows SOUTH 0 1 2 SQUARE
windows WEST 0 3 3 SQUARE
windows EAST 0 1 1 SQUARE
windows NORTH 1 2 1 SQUARE
windows SOUTH 1 1 0 SQUARE
windows WEST 1 3 3 SQUARE
windows EAST 1 3 0 SQUARE
roof 0 SLOPE 0.9 0.3

Room_20:
size 7 5
height 3
onTop Room_21 1 1
displacement 0 -0.2 -0.2 -0.2 
windows NORTH 0 2 0 SQUARE
windows SOUTH 0 2 0 SQUARE
windows WEST 0 3 1 SQUARE
windows EAST 0 2 1 SQUARE
windows NORTH 1 1 3 SQUARE
windows SOUTH 1 2 1 SQUARE
windows WEST 1 2 3 SQUARE
windows EAST 1 2 1 SQUARE
windows NORTH 2 2 1 SQUARE
windows SOUTH 2 2 0 SQUARE
windows WEST 2 3 3 SQUARE
windows EAST 2 3 2 SQUARE
roof 0 SLOPE 0.9 0.3

Room_21:
size 2 2
height 1
displacement 0 0 
windows NORTH 0 2 3 SQUARE
windows SOUTH 0 1 0 SQUARE
windows WEST 0 2 0 SQUARE
windows EAST 0 1 0 SQUARE
roof 1 POINT 0.4







Door_19:
size 3 1
height 1
addWindow NORTH 0 1 DOOR
displacement 0 0 
roof 1 SLOPE 0.7 0.4 NORTH


