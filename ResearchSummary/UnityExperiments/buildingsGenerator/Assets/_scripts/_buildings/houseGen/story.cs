using UnityEngine;
using System;
using System.Collections.Generic;

namespace HouseGen {

public enum Direction {NONE=0, NORTH=1, SOUTH=2, WEST=3, EAST=4};

public class Story {
int[,] plan;
int[,] roomPlan;
string name;
int width, height;
List<Rectangle> rooms;
Story previous = null;
Story next = null;

public Story(int w, int h, string n) {
	width = w;
	height = h;
	name = n;
	plan = new int[w, h];
	roomPlan = new int[w, h];
	rooms = new List<Rectangle>();
}

public Story(Story p, string n) : this(p.width, p.height, n) {
previous = p;
p.next = this;
width = p.width;
height = p.height;
}

public int[,] getPlan() {return plan;}
public int[,] getRoomPlan() {return roomPlan;}
public List<Rectangle> getRooms() {return rooms;}

public int getHeight() {return height;}
public int getWidth() {return width;}

public Story getPrevious() {return previous;}
public Story getNext() {return next;}

public string getName(){return name;}

public void print(bool rPlan = false) {
	Debug.Log("--" + name + "--");
	string line = "";
	for (int j = 0; j < height; ++j) {
		for(int i = 0; i < width; ++i) {	
			if (!rPlan) {
				line += (plan[i, j]==0?"0":""+plan[i, j]);
			}
			else {
				line += (roomPlan[i, j]==0?"0":""+roomPlan[i, j]);
			}
		}
		line += "\n";
	}
	Debug.Log(line + "\n");
}

public void writeRooms() {
	foreach (Rectangle room in rooms) {
		writeRoom(room);		
	}
}

public void writeRoom(Rectangle room) {
	for(int i = 0; i < room.w-1; ++i) {
				plan[i + room.x, room.y] = (int)Direction.NORTH;
				plan[room.x + room.w - i-1, room.y+room.h-1] = (int)Direction.SOUTH;
				}
			for(int j = 1; j < room.h; ++j) {
				plan[room.x, room.y + j] = (int)Direction.WEST;
				plan[room.x + room.w -1, room.y+room.h-1 - j] = (int)Direction.EAST;
				}
}

public void writeRoomPlan(Rectangle room) {
	for (int i = 0; i < room.w; ++i){
		for (int j = 0; j < room.h; ++j) {
			roomPlan[room.x + i, room.y + j] = room.roomId;
		}
	}
}

public Rectangle addRoom(Rectangle room) {
	for (int i = 0; i < 2; ++i) {
		int valX = room.x + i*(room.w-1);
		int valY = room.y + i*(room.h-1);
		
		if (valX < 0 || valX >= width) {
			Console.WriteLine("Bad Width");
			return null;
		}
		if (valY < 0 || valY >= height) {
			Console.WriteLine("Bad Height");
			return null;
		}
		
	}

	rooms.Add(room);
	room.roomId = rooms.Count;
	room.floor = this;
	writeRoom(room);
	writeRoomPlan(room);
	
	return room;
}

public void growRoomsFromPrevious() {
	foreach (Rectangle room in previous.getRooms()) {
		if (room.roomHeight > 0 && room.next == null) {
			Rectangle newRoom = new Rectangle(room);
			
			if (room.parentSide != Direction.NONE && room.parent.roomHeight > 0) {
				newRoom.parent = rooms[room.parent.next.roomId-1];
				if (room.parentSide != null) {
					newRoom.parentSide = room.parentSide;
					newRoom.parent.getSide(Util.reverse(newRoom.parentSide)).Add(newRoom);		
				}
			}
			
			addRoom(newRoom);
			
		}
	}
}

public Story generateNextStory(string n) {
	Story nextStory = new Story(this, n);
	nextStory.growRoomsFromPrevious();
	return nextStory;
}

public Story getStoryAtHeight(int height) {
		if (height <= 0) {
			return this;
		}
		if (next == null) {
			return generateNextStory("").getStoryAtHeight(height-1);
		}
		
		return next.getStoryAtHeight(height-1);		
	}

}

public class Rectangle {
	public int x, y, w, h;
	public int roomId;
	public enum Type {SIMPLE=0, TOWER=1};
	public enum RoofType {FLAT=0, POINT=1, LINEAR=2};
	public enum PositionType {RANDOM=0, COMPLETE_MATCH=1, CENTER=2, FIRST_MATCH=3};
	
	public Rectangle parent;
	public Direction parentSide; // on which side of this room its parent is
	public Story floor;
	Dictionary<Direction, List<Rectangle>> sides;
	Dictionary<Direction, List<Vector3>> windows;
	Dictionary<Direction, WindowShape> windowShapes;
	public Type roomType;
	List<Rectangle> towerList; // a tower is stored as North/South * 10 + West/East;
	public float roofOffset;
	public int roomHeight; // how tall the room will finally be
	public RoofType roofType;
	public List<Rectangle> onTop;

	public static int wallHeight = 2;
	
	public Rectangle previous = null;
	public Rectangle next = null;
	
	public float baseDispl = 0f;
	public float topDispl = 0f;
	
	public List<float> displacements;
	
	public Rectangle(int xs, int ys, int ws, int hs) {
	x = xs;
	y = ys;
	w = ws;
	h = hs;
	
	sides = new Dictionary<Direction, List<Rectangle>>();
	sides[Direction.NORTH] = new List<Rectangle>();
	sides[Direction.SOUTH] = new List<Rectangle>();
	sides[Direction.WEST] = new List<Rectangle>();
	sides[Direction.EAST] = new List<Rectangle>();

	parentSide = Direction.NONE;
	roomType = Type.SIMPLE;

	roofOffset = ((float)Util.rand.Next(80, 220) / 100)*(wallHeight);
	roomHeight = 0;
	roofType = RoofType.FLAT;
	
	windows = new Dictionary<Direction, List<Vector3>>();
	windows[Direction.NORTH] = new List<Vector3>();
	windows[Direction.SOUTH] = new List<Vector3>();
	windows[Direction.WEST] = new List<Vector3>();
	windows[Direction.EAST] = new List<Vector3>();
	
	windowShapes = new Dictionary<Direction, WindowShape>();
	windowShapes[Direction.NORTH] = WindowShape.SQUARE;
	windowShapes[Direction.SOUTH] = WindowShape.SQUARE;
	windowShapes[Direction.WEST] = WindowShape.SQUARE;
	windowShapes[Direction.EAST] = WindowShape.SQUARE;
	
	towerList = new List<Rectangle>();
	
	onTop = new List<Rectangle>();
	
	displacements = new List<float> {0, 0};
	
	}
	
	public Rectangle(int xs, int ys, int ws, int hs, Type t) : this(xs, ys, ws, hs) {
		roomType = t;
	}

	public Rectangle(int xs, int ys, int ws, int hs, int rH) : this(xs, ys, ws, hs) {
		roomHeight = rH;
		
		while (displacements.Count <= rH+1) {
			displacements.Add(displacements[displacements.Count-1]);
		}
	}

	public Rectangle(Rectangle previousRoom) : this(previousRoom.x, previousRoom.y, previousRoom.w, previousRoom.h, previousRoom.roomHeight-1){
		roofOffset = previousRoom.roofOffset;
		roofType = previousRoom.roofType;
		
		previousRoom.next = this;
		this.previous = previousRoom;
		this.setDisplacements(previousRoom.displacements.GetRange(1, previousRoom.displacements.Count-1));
		
	}
	
	public List<Rectangle> getSide(Direction d) {
		return sides[d];
	}
	
	public List<Vector3> getWindows(Direction d) {
		return windows[d];
	}
	
	public WindowShape getWindowShape(Direction d) {
		return windowShapes[d];
	}
	
	public void setWindowShape(Direction d, WindowShape shape) {
		windowShapes[d] = shape;
	}
	
	public Rectangle addRectangleToSide(Direction side, int rectangleSize, int minLen = 3, int maxLen = 10, PositionType positionType = PositionType.RANDOM, bool doNotAdd = false) {
		// Debug.Log(positionType);
		Rectangle ans = new Rectangle(0, 0, 0, 0);
		Rectangle room = this;
		int dirX = 0, dirY = 0;
		
		// Console.WriteLine("Side {0}, Size {1}", side, rectangleSize);
				
		// Set the search directions
		switch(side) {
			case Direction.NORTH: dirY = -1; break;
			case Direction.SOUTH: dirY = 1; break;
			case Direction.WEST: dirX = -1; break;
			case Direction.EAST: dirX = 1; break;
		}
		
		// Console.WriteLine("dirX {0} dirY {1}", dirX, dirY);
		
		// Determine the search range
		int start=0, end=0;
		switch (side) {
			case Direction.NORTH:
			case Direction.SOUTH:
				start = room.x - rectangleSize*(positionType==PositionType.COMPLETE_MATCH?0:1) + 1;
				end = room.x + room.w - 1 - rectangleSize*(positionType==PositionType.COMPLETE_MATCH?1:0);
			break;
			
			case Direction.WEST:
			case Direction.EAST:
				start = room.y - rectangleSize*(positionType==PositionType.COMPLETE_MATCH?0:1) + 1;
				end = room.y + room.h - 1 - rectangleSize*(positionType==PositionType.COMPLETE_MATCH?1:0);
			break;
		}
		
		start = Math.Max(start, 0);
		
		// Console.WriteLine("Search range {0}-{1}", start, end);
		
		// Search an area to place the new room
		int bestInd = -1, bestLen = -1;
		bool stopSearch = false;
		for (int ind = start; ind <= end; ++ind) {
			if (stopSearch) {
					break;
			}
			// Console.WriteLine("# Checking index {0}", ind);
			int x = 0, y = 0;
			// Determine where will the new room be placed on the designated side
			switch (side) {
			case Direction.NORTH:
			case Direction.SOUTH:
				x = ind; y = room.y + (1 - ((int)side)%2)*room.h;
			break;
			case Direction.WEST:
			case Direction.EAST:
				x = room.x + (1 - ((int)side)%2)*room.w; y = ind;
			break;
			}

			// Search the area in front of the side to determine whether the room can be placed here
			for (int len = 1; len <= maxLen; ++len) {
				int sum = 0; // if non zero, the search encountered an already-used area;
				if (stopSearch) {
					break;
				}
				// Console.WriteLine("- Checking length {0}", len);
				
				if ((int)side >= (int)Direction.WEST) {
					// Horizontal Search
					int sX = 0;
					if (dirX == 1) sX = x+(len-1)*dirX;
					if (dirX == -1) sX = x+(len)*dirX;
					
					if (y+rectangleSize > floor.getHeight()) {
						// Console.WriteLine("Reached map border");
						break;
					}
					
					if (sX < 0 || sX >= floor.getWidth()) {
						// Console.WriteLine("Reached map border");
						break;
					}
					
					// Console.WriteLine("\tX {0}", sX);

					for (int sY = y; sY < y+rectangleSize; ++sY) {
						// Console.WriteLine("\t\tY {0}", sY);
						sum += floor.getPlan()[sX, sY];
					}				
					
					// Console.WriteLine("Sum is {0}", sum);
				
					if (sum != 0) {
						// Console.WriteLine("Blocked!");
						break;
					}
					else {
						if (len > bestLen
						|| (len == bestLen && Util.rand.Next(100) > 85 && !(positionType == PositionType.FIRST_MATCH))
						|| (len >= bestLen && (positionType == PositionType.CENTER))) {
							bestInd = ind;
							bestLen = len;
							if (y+rectangleSize/2 >= this.y + this.h/2){
								stopSearch = true;
							}
						}
					}
				}
				else {
					// Vertical Search
					int sY = 0;
					if (dirY == 1) sY = y+(len-1)*dirY;
					if (dirY == -1) sY = y+(len)*dirY;
					
					if (x+rectangleSize > floor.getWidth()) {
						// Console.WriteLine("Reached map border");
						break;
					}
					
					if (sY < 0 || sY >= floor.getHeight()) {
						// Console.WriteLine("Reached map border");
						break;
					}
					
					// Console.WriteLine("\tY {0}", sY);

					for (int sX = x; sX < Math.Min(x+rectangleSize, floor.getWidth()-1); ++sX) {
						// Console.WriteLine("\t\tX {0}", sX);
						sum += floor.getPlan()[sX, sY];
					}				
					
					// Console.WriteLine("Sum is {0}", sum);
				
					if (sum != 0) {
						// Console.WriteLine("Blocked!");
						break;
					}
					else {
						if (len > bestLen
							|| ((len == bestLen && Util.rand.Next(100) > 85 && !(positionType == PositionType.FIRST_MATCH)))
							|| (len >= bestLen && (positionType == PositionType.CENTER))) {
							bestInd = ind;
							bestLen = len;
							if (x+rectangleSize/2 >= this.x + this.w/2){
								stopSearch = true;
							}
						}
					}
				}
				
				
			}
				
		}
		
		// Console.WriteLine("Best ind is {0} with length {1}", bestInd, bestLen);

		// Horizontal Construction
		if ((int)side >= (int)Direction.WEST) {
			int xF = room.x + (dirX==1?room.w:0) + (dirX==-1?-bestLen:0);
			int yF = bestInd;
			ans = new Rectangle(xF, yF, bestLen, rectangleSize, Math.Max(roomHeight-1, 0));

				if (!(ans.y < room.y) && !(ans.y+ans.h > room.y + room.h)) {
					float ansMiddle = ans.y + (float)ans.h/2;
					ansMiddle -= this.y;
					float roofOffsetPerc = 1- (((float)Math.Abs(room.h/2 - ansMiddle))/(room.h/2));
					ans.roofOffset = roofOffset*(roofOffsetPerc-0.01f); // not exactly the same to avoid z-fighting
					// Debug.Log(roofOffset);
					// Debug.Log(roofOffsetPerc);
					// Debug.Log(ans.roofOffset);
				}
		}
		else {
			int yF = room.y + (dirY==1?room.h:0) + (dirY==-1?-bestLen:0);
			int xF = bestInd;
			ans = new Rectangle(xF, yF, rectangleSize, bestLen, Math.Max(roomHeight-1, 0));

				if (!(ans.x < room.x) && !(ans.x+ans.w > room.x + room.w)) {
					float ansMiddle = ans.x + (float)ans.w/2;
					ansMiddle -= this.x;
					float roofOffsetPerc = 1- (((float)Math.Abs(room.w/2 - ansMiddle))/(room.w/2));
					ans.roofOffset = roofOffset*(roofOffsetPerc-0.01f); // not exactly the same to avoid z-fighting
					// Debug.Log(roofOffset);
					// Debug.Log(roofOffsetPerc);
					// Debug.Log(ans.roofOffset);
				}

		}
		
		if (!doNotAdd) {
			addGeneratedRoomToSide(side, ans);
		}
		
		return ans;
	}
	
	// DO NOT USE
	public Rectangle addTower(Direction vertical, Direction horizontal, int radius, int height = 4) {
		Rectangle ans = new Rectangle(-1, -1, -1, -1, height);
		int xT = x - radius ;
		int yT = y - radius ;
		if (vertical == Direction.SOUTH) yT += h-1;
		if (horizontal == Direction.EAST) xT += w-1;
		
		// Check if there is enough space around the corner to add the tower
		bool valid = true;
		int[,] roomPlan = floor.getRoomPlan();
		for (int i = 0; i < 1+2*radius; ++i) {
			for (int j = 0; j < 1+2*radius; ++j) {
				if (roomPlan[xT+i, yT+j] != 0 && roomPlan[xT+i, yT+j] != this.roomId) {
					// The tower cannot overlap a room other than the one it is added to
					valid = false;
					break;
				}
			}
			if (!valid) break;
		}
		
		if (valid) {
			ans.x = xT;
			ans.y = yT;
			ans.w = 1 + 2*radius;
			ans.h = 1 + 2*radius;
			
			// Save this room in the tower list
			int towerIndex = ((int)vertical)*10 + (int)horizontal;
			while (towerList.Count <= towerIndex) {
				towerList.Add(null);
			}
			towerList.Insert(towerIndex, ans);
			
			//ans.parent = this;
			ans.parentSide = Direction.NONE;
			ans.roofType = RoofType.POINT;
			ans.roofOffset = 4;
			floor.addRoom(ans);
		}

		return ans;
	}
	
	public void addPillars(Direction side, int dist, int spacing) {
		int[,] plan = floor.getPlan();
		int[,] roomPlan = floor.getRoomPlan();
		if ((int)side >= (int)Direction.WEST) {
			// Vertical line of pillars
			int xS = x;
			if (side == Direction.WEST) xS -= dist;
			else xS += w + dist - 1;
			
			int spaceCount = spacing/2;
			for (int yS = y; yS < y+h; ++yS) {
				if (plan[xS, yS] != 0 || roomPlan[xS, yS] != 0) {
					spaceCount = spacing/2;
				}
				else {
					if (spaceCount == 0) {
						plan[xS, yS] = (int)side + 4;
						spaceCount = spacing;
					}
					else {
						--spaceCount;
					}
				}
			}
			
		}
		else {
			// Horizontal line of pillars
			int yS = y;
			if (side == Direction.NORTH) yS -= dist;
			else yS += h + dist - 1;
			
			int spaceCount = spacing/2;
			for (int xS = x; xS < x+w; ++xS) {
				if (plan[xS, yS] != 0 || roomPlan[xS, yS] != 0) {
					spaceCount = spacing/2;
				}
				else {
					if (spaceCount == 0) {
						plan[xS, yS] = (int)side + 4;
						spaceCount = spacing;
					}
					else {
						--spaceCount;
					}
				}
			}
		}
	}
	
	// size is the size of the window itself
	public void addWindows(Direction side, int size, int spacing, WindowShape shape = WindowShape.SQUARE) {
		setWindowShape(side, shape);
		int[,] plan = floor.getPlan();
		int[,] roomPlan = floor.getRoomPlan();
		int inward, outward;
		if ((int)side >= (int)Direction.WEST) {
			// Vertical line of windows
			outward = (side==Direction.EAST?1:-1);
			inward = -outward;
			int xS = x;
			if (side == Direction.EAST) xS += w - 1;
			
			int spaceCount = 0;
			// reduced interval so as to not put windows right on the corner of the wall
			for (int yS = y+1; yS < y+h-1; ++yS) {
				bool placeWindow = true;
				for (int sizeInd = 0; sizeInd < size; ++sizeInd) {
					if (yS+sizeInd >= y+h-1 || roomPlan[xS, yS+sizeInd] != roomId || (xS+outward >= 0 && xS+outward < floor.getWidth() && roomPlan[xS+outward, yS+sizeInd] != 0) ) {
						spaceCount = spacing/2;
						placeWindow = false;
						break;
					}
				}
				
				if (placeWindow) {
					if (spaceCount == 0) {
						for (int sizeInd = 0; sizeInd < size; ++sizeInd) {
							plan[xS, yS+sizeInd] = (int)side + 4;
						}
						windows[side].Add(new Vector3(xS + (side==Direction.EAST?1:0), size, yS));
						spaceCount = spacing;
						yS += size-1;
					}
					else {
						--spaceCount;
					}
				}
				
			}
			
		}
		else {
			outward = (side==Direction.SOUTH?1:-1);
			inward = -outward;
			int yS = y;
			if (side == Direction.SOUTH) yS += h - 1;
			
			int spaceCount = 0;
			// reduced interval so as to not put windows right on the corner of the wall
			for (int xS = x+1; xS < x+w-1; ++xS) {
				bool placeWindow = true;
				for (int sizeInd = 0; sizeInd < size; ++sizeInd) {
					if (xS+sizeInd >= x+w-1 || roomPlan[xS+sizeInd, yS] != roomId || (yS+outward >= 0 && yS+outward < floor.getHeight() && roomPlan[xS+sizeInd, yS+outward] != 0) ) {
						spaceCount = spacing/2;
						placeWindow = false;
						break;
					}
				}
				
				if (placeWindow) {
					if (spaceCount == 0) {
						for (int sizeInd = 0; sizeInd < size; ++sizeInd) {
							plan[xS+sizeInd, yS] = (int)side + 4;
						}
						windows[side].Add(new Vector3(xS, size, yS + (side==Direction.SOUTH?1:0)));
						spaceCount = spacing;
						xS += size-1;
					}
					else {
						--spaceCount;
					}
				}
				
			}
		}
	}

	public Rectangle getRoomAtStory(int story) {
		if (story == 0 || next == null) {
			
			return this;
		}
		
		return next.getRoomAtStory(story-1);		
	}
	
	public Rectangle getTower(Direction vertical, Direction horizontal) {
		int towerIndex = ((int)vertical)*10 + (int)horizontal;
		if (towerIndex >= towerList.Count){
			return null;
		}
		
		return towerList[towerIndex];
	}
	
	public void addGeneratedRoomToSide(Direction side, Rectangle room) {
			room.parent = this;
			room.parentSide = Util.reverse(side);
			sides[side].Add(room);		
			floor.addRoom(room);
	}

	public Dictionary<Direction, List<Rectangle>> getSides() {
		return sides;
	}
	
	public List<Rectangle> getTowerList() {
		return towerList;
	}
	
	public List<Rectangle> getOnTop() {
		return onTop;
	}
	
	public int getRoomsOnSidesCount() {
		int northCount = sides[Direction.NORTH].Count;
		int southCount = sides[Direction.SOUTH].Count;
		int westCount = sides[Direction.WEST].Count;
		int eastCount = sides[Direction.EAST].Count;
		
		return northCount + southCount + westCount + eastCount;
	}
	
	public void setDisplacements(List<float> f) {
		displacements = f;
				
		while (displacements.Count <= roomHeight+1) {
			displacements.Add(0);
		}
		
		baseDispl = displacements[0];
		topDispl = displacements[1];
	}
	
}

}
