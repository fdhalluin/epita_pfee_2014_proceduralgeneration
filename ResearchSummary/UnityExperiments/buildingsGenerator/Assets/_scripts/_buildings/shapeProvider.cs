using UnityEngine;
using System.Collections.Generic;
using HouseGen;
using System;

namespace HouseGen {

public enum WindowShape {SQUARE=0, CIRCLE=1, SEMICIRCULAR=2, DOOR=3, CANNON=4};
public enum PropType {BALCONEY=0};

public class Shape {

public List<Vector2> points;
public Vector2 gravityCenter;
public Vector2 center; // where to put the center of the window, as a percentage of the available space
public List<Prop> props;
public float winDepth = 0.2f;

public Shape() {
	points = new List<Vector2>();
	props = new List<Prop>();
}

}

public class ShapeProvider {

static Dictionary<WindowShape, Shape> shapes = new Dictionary<WindowShape, Shape>();

public static Shape getShape(WindowShape shape) {
if (shapes.ContainsKey(shape)) {
	return shapes[shape];
}

Shape ans = new Shape();

switch(shape) {
	case WindowShape.SQUARE:
		float margin = -0.5f;
		ans.points.Add(new Vector2(margin, 1-margin));
		ans.points.Add(new Vector2(1-margin, 1-margin));
		ans.points.Add(new Vector2(1-margin, margin));
		ans.points.Add(new Vector2(margin, margin));
		ans.center.x = 0.5f;
		ans.center.y = 0.5f;
		//ans.props.Add(PropProvider.getProp(PropType.BALCONEY));
		break;
	case WindowShape.CIRCLE:
		float circleRadius = 1.2f;
		int circlePointsNum = 16;
		for (int i = circlePointsNum-1; i >= 0; --i) {
			float theta = (float)i*2*Mathf.PI / circlePointsNum;
			ans.points.Add(new Vector2(circleRadius*Mathf.Cos(theta), circleRadius*Mathf.Sin(theta)));
		}
		ans.center.x = 0.5f;
		ans.center.y = 0.0f;
		ans.winDepth *= 4;
		break;
	case WindowShape.SEMICIRCULAR:
		float semiRadius = 0.4f;
		int semiCirclePointsNum = 16;
		for (int i = semiCirclePointsNum/2; i >= 0; --i) {
			float theta = (float)i*2*Mathf.PI / semiCirclePointsNum;
			ans.points.Add(new Vector2(semiRadius*Mathf.Cos(theta), semiRadius*Mathf.Sin(theta)));
		}
		ans.points.Add(new Vector2(ans.points[ans.points.Count-1].x, -0.8f));

		ans.points.Add(new Vector2(ans.points[0].x, -0.8f));

		
		ans.center.x = 0.5f;
		ans.center.y = 0.7f;
		break;
	case WindowShape.DOOR:
		ans.points.Add(new Vector2(0.1f, 0.9f));
		ans.points.Add(new Vector2(0.9f, 0.9f));
		ans.points.Add(new Vector2(0.9f, -0.5f));
		ans.points.Add(new Vector2(0.1f, -0.5f));
		ans.center.x = 0.5f;
		ans.center.y = 0.5f;
		break;
	case WindowShape.CANNON:
		float cannonRadius = 0.7f;
		int cannonPointsNum = 24;
		for (int i = cannonPointsNum-1; i >= 0; --i) {
			float theta = (float)i*2*Mathf.PI / cannonPointsNum;
			ans.points.Add(new Vector2(cannonRadius*Mathf.Cos(theta), cannonRadius*Mathf.Sin(theta)));
		}
		ans.center.x = 0.5f;
		ans.center.y = 0.6f;
		ans.winDepth = 4;
		break;
}

for (int i = 0; i < ans.points.Count; ++i) {
		ans.gravityCenter += ans.points[i];
}
ans.gravityCenter /= ans.points.Count;	

shapes.Add(shape, ans);

return ans;
}

}

public class Prop {
	public List<Vector3> points;
	public List<List<int>> faces;
	public Vector3 center;
	
	string folder = "Assets/_scripts/_buildings/props/";
	
	public Prop() {
		points = new List<Vector3>();
		faces = new List<List<int>>();
		center = new Vector3(0, 0, 0);
	}
	
	public Prop(string filename) : this() {
		//string input = System.IO.File.ReadAllText(folder + filename + ".off");
		//buildFromOFFFile(input);
		//Debug.Log("Got prop with " + points.Count + " vertices and " + faces.Count + " faces");
	}
	
	public void buildFromOFFFile(string input) {
		points.Clear();
		faces.Clear();
		string[] lines = input.Split("\n".ToCharArray());
		
		// The first line contains the magic word "OFF" and is thus ignored
		string[] dimensions = lines[1].Split(" ".ToCharArray());
		
		int verticesNum = Int32.Parse(dimensions[0]);
		int facesNum = Int32.Parse(dimensions[1]);
		
		Debug.Log("verticesNum " + verticesNum + " facesNum " + facesNum);
		
		for (int i = 0; i < verticesNum; ++i) {
			string[] coords = lines[2+i].Split(" ".ToCharArray());
			points.Add(new Vector3(Single.Parse(coords[0]), Single.Parse(coords[1]), Single.Parse(coords[2])));
		}
		
		for (int i = 0; i < facesNum; ++i) {
			string[] faceVertices = lines[2+verticesNum+i].Split(" ".ToCharArray());
			List<int> face = new List<int>();
			int pointsNum = Int32.Parse(faceVertices[0]);
			for (int j = 1; j <= pointsNum; ++j) {
				face.Add(Int32.Parse(faceVertices[j]));
			}
			faces.Add(face);
		}
		
	}
	
}

public class PropProvider {

static Dictionary<PropType, Prop> props = new Dictionary<PropType, Prop>();

public static Prop getProp(PropType propType) {
if (props.ContainsKey(propType)) {
	return props[propType];
}

Prop ans = null;

float horizontalScale = 1f;
float verticalScale = 1f;

switch (propType) {
	case PropType.BALCONEY:
		ans = new Prop("cube");
		float balconeyMargin = 0.1f;
		
		horizontalScale = 1f;
		verticalScale = 0.4f;
		
		ans.points.Add(new Vector3(0, 0, 0)); // Unused points, to help with the 1-indexed points
		ans.points.Add(new Vector3(0, 1, 0)); // 1
		ans.points.Add(new Vector3(1, 1, 0)); // 2
		ans.points.Add(new Vector3(0, 0, 0)); // 3
		ans.points.Add(new Vector3(1, 0, 0)); // 4
		ans.points.Add(new Vector3(0, 0, 1)); // 5
		ans.points.Add(new Vector3(1, 0, 1)); // 6
		ans.points.Add(new Vector3(0, 1, 1)); // 7
		ans.points.Add(new Vector3(1, 1, 1)); // 8
		
		ans.points.Add(new Vector3(balconeyMargin, 1, balconeyMargin)); //9
		ans.points.Add(new Vector3(1-balconeyMargin, 1, balconeyMargin)); //10
		ans.points.Add(new Vector3(1-balconeyMargin, 1, 1)); //11
		ans.points.Add(new Vector3(balconeyMargin, 1, 1)); //12
		
		ans.points.Add(new Vector3(1-balconeyMargin, 0.25f, 1)); //13
		ans.points.Add(new Vector3(balconeyMargin, 0.25f, 1)); //14
		ans.points.Add(new Vector3(balconeyMargin, 0.25f, balconeyMargin)); //15
		ans.points.Add(new Vector3(1-balconeyMargin, 0.25f, balconeyMargin)); //16
		
		ans.faces.Add(new List<int> {1, 3, 4, 2});
		ans.faces.Add(new List<int> {8, 2, 4, 6});
		ans.faces.Add(new List<int> {1, 7, 5, 3});
		ans.faces.Add(new List<int> {4, 3, 5, 6});
		
		ans.faces.Add(new List<int> {12, 7, 1, 9});
		ans.faces.Add(new List<int> {10, 9, 1, 2});
		ans.faces.Add(new List<int> {8, 11, 10, 2});
		
		ans.faces.Add(new List<int> {12, 9, 15, 14});
		ans.faces.Add(new List<int> {9, 10, 16, 15});
		ans.faces.Add(new List<int> {10, 11, 13, 16});
		ans.faces.Add(new List<int> {13, 14, 15, 16});
		
		ans.center = new Vector3(0.5f, 0.2f, 0);
		break;
}

for(int i = 0; i < ans.points.Count; ++i) {
	ans.points[i] = new Vector3(ans.points[i].x*horizontalScale, ans.points[i].y*verticalScale, ans.points[i].z);
}

return ans;
}

}

}