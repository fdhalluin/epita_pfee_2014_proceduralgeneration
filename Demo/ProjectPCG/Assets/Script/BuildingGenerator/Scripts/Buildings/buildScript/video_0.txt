
Room_22:
size ?(5, 8) ?(4, 7)
height 2
build Room_22 50 50
addRoom NORTH Door_23 RANDOM
onTop Room_24 0 0
displacement 0 0.4 0.9 
windows NORTH 0 1 1 SQUARE
windows SOUTH 0 3 3 SQUARE
windows WEST 0 3 3 SQUARE
windows EAST 0 1 3 SQUARE
windows NORTH 1 1 3 BALCONEY
windows SOUTH 1 3 3 SQUARE
windows WEST 1 3 2 SQUARE
windows EAST 1 1 3 SQUARE
roof 0 SLOPE 0.9 0.3

Room_24:
size ?(2, 5) ?(2, 5)
height 1
onTop Room_25 1 1
displacement 0 -0.1 
windows NORTH 0 2 3 SQUARE
windows SOUTH 0 2 1 SQUARE
windows WEST 0 1 2 SQUARE
windows EAST 0 3 3 SQUARE
roof 0 SLOPE 0.9 0.3

Room_25:
size ?(2, 5) ?(2, 5)
height 1
displacement 0 0.8 
windows NORTH 0 3 2 SQUARE
windows SOUTH 0 1 1 SQUARE
windows WEST 0 2 2 SQUARE
windows EAST 0 2 3 SQUARE
roof 1 POINT 0.4







Door_23:
size ?(3, 6) ?(1, 4)
height 1
addWindow NORTH 0 1 DOOR
displacement 
roof 0 SLOPE 0.7 0.4 NORTH


