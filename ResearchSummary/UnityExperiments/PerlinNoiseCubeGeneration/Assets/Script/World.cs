using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World {
	public int mSizeX = 75;
	public int mSizeY = 15;
	public int mSizeZ = 75;
	public int[,,] mBlocksArray;
	
	public void Init() {
		mBlocksArray = new int[mSizeX, mSizeY, mSizeZ];
	}
}
