using System.Collections.Generic;
using System;
using UnityEngine;
using System.Text.RegularExpressions;

namespace HouseGen {

// The name of this class might be a little misleading. The Parser will only
// half-parse the script, effectively placing random numbers where they should be
// and organizing the statements in a easy-to-process structure for the houseBuilder.
// The actual value and statements will be split into tokens and parsed
// during the building phase.
class Parser {

// CommandType values
//
// DEFINITION: defines the current room (size, height, room)
// ROOM: defines the rooms sharing one side with the current room (addRoom, addTower)
// WINDOWS: defines the windows of the current room (windows)
// TOP: defines room put on the top of this one (onTop)
// SINGLE_WINDOW: defined windows added by hand to the building (addWindow)
// PILLARS: defines the pillars of the current room (pillars)
// PROP: defines the props added to the building
public enum CommandType {DEFINITION=0, ROOM=1, WINDOWS=2, TOP=3, SINGLE_WINDOW=4, PILLARS=5, PROP=6};

// Those two strings are the identifiers of specific rooms, that are not to be built
// but rather used as containers for global values and commands of the building.
// As a # symbol will start a comment for the rest of the line, no actual user-built
// or user-generated room can use that name.
public static string roomsToBuild = "#TOBUILD#";
public static string globalRoom = "#GLOBAL#";

public Parser() {}

// This method allows for the use of random integers in a script
// Before trying to parse a line, each string of characters
// of the form ?(A, B) will be replaced by a random number
// chosen between A and B.
private string handleRandom(string script) {
	Regex rx = new Regex("\\?\\((\\d*),[ ]*(\\d*)\\)");

	MatchCollection matches = rx.Matches(script);

	if (matches.Count <= 0) return script;

	Match m = matches[0];
	GroupCollection gc = m.Groups;
	int result = Util.rand.Next(Int32.Parse (gc[1].Value), Int32.Parse (gc[2].Value)+1);
	string ans = script.Substring(0, m.Index) + result + script.Substring(m.Index + m.Length, script.Length - m.Index - m.Length);

	return handleRandom(ans);

}

// This is the method that will parse the script and return all its rooms
// and building instructions in a easy-to-process structure.
public Dictionary<string, RoomDescription> parse(string input) {
	// The input is first splitted into lines
	string[] lines = input.Split("\n".ToCharArray());

	RoomDescription currentRoom = new RoomDescription(roomsToBuild);

	Dictionary<string, RoomDescription> building = new Dictionary<string, RoomDescription>();

	building.Add(globalRoom, new RoomDescription(globalRoom));
	building.Add(currentRoom.name, currentRoom);

	int propCount = 0;

	foreach (string rawLine in lines) {
		string line = handleRandom(rawLine.Trim());

		// You can add comment to a script by starting a line with a #
		if (line.IndexOf('#') > 0) {
			line = line.Substring(0, line.IndexOf('#'));
			line = line.Trim();
		}
		
		// Ignore empty lines
		if (line.Length < 2) continue;
		
		// We will only care about the first token, which is the command given to the generator
		string[] tokens = line.Split(' ');
		
		int index = -1;
		
		switch (tokens[0]) {
			// Those commands define properties of the room
			case "size":
			case "height":
			case "roof":
			case "displacement":
				index = currentRoom.commandsCount[(int)CommandType.DEFINITION];
				currentRoom.properties[tokens[0]] = line;
				++currentRoom.commandsCount[(int)CommandType.DEFINITION];
				break;
			
			// Those commands define other rooms to be added on the sides of the current room
			case "addRoom":
			case "addTower":
			case "@ddRoom":
				index = currentRoom.commandsCount[(int)CommandType.ROOM];
				currentRoom.properties[CommandType.ROOM + "_" + index] = line;
				++currentRoom.commandsCount[(int)CommandType.ROOM];
				break;
			
			// This command defines a bulk-adding of windows
			case "windows":
				index = currentRoom.commandsCount[(int)CommandType.WINDOWS];
				currentRoom.properties[CommandType.WINDOWS + "_" + index] = line;
				++currentRoom.commandsCount[(int)CommandType.WINDOWS];
				break;
			
			// This command adds a room on top of the current one
			case "onTop":
				index = currentRoom.commandsCount[(int)CommandType.TOP];
				currentRoom.properties[CommandType.TOP + "_" + index] = line;
				++currentRoom.commandsCount[(int)CommandType.TOP];
				break;
			
			// This is a global statement to consider a certain room as part of the floor level
			// If no build statement is specified, no room will actually be written
			case "build":
				building[roomsToBuild].properties.Add(building[roomsToBuild].properties.Keys.Count + "", line);
				break;
			
			// This commands add a single window to the room
			case "addWindow":
				index = currentRoom.commandsCount[(int)CommandType.SINGLE_WINDOW];
				currentRoom.properties[CommandType.SINGLE_WINDOW + "_" + index] = line;
				++currentRoom.commandsCount[(int)CommandType.SINGLE_WINDOW];
				break;
			
			// This command defines a bulk-adding of windows
			case "pillars":
				index = currentRoom.commandsCount[(int)CommandType.PILLARS];
				currentRoom.properties[CommandType.PILLARS + "_" + index] = line;
				++currentRoom.commandsCount[(int)CommandType.PILLARS];
				break;
			
			// This command is a global property of the building.
			// It defines the rotation angle of the building once it has been built.
			case "rotation":
				building[globalRoom].properties.Add("rotation", line);
				break;

			// Those commands are global properties of the building and add props to the building
			// as children GameObjects instatiated from a prefab.
			case "addProp":
			case "@ddProp":
					building[globalRoom].properties.Add("prop_" + propCount++, line);
				break;
			
			// This command is a global property of the building and moves its anchor point
			// after the mesh has been constructed
			case "moveCenter":
				building[globalRoom].properties.Add("moveCenter", line);
				break;
			// By default, if the command ends with a colon, it signals the start of the description
			// of a new room, which name is everything that is paced before the colon.
			default:
				if (tokens[0][tokens[0].Length-1] == ':') {
					currentRoom = new RoomDescription(tokens[0].Substring(0, tokens[0].Length-1));
					building.Add(currentRoom.name, currentRoom);
				}
				break;
		}


	}
	
	// This property will always be called, so it should be added if it is not present already.
	if (!building[globalRoom].properties.ContainsKey("rotation")) {
				building[globalRoom].properties.Add("rotation", "rotation 0");
	}
	
	// For each room that will actually be built, we check if it has its displacement values set.
	// If not, we add a statement with zeroed displacement values for each storey of the room.
	foreach (string key in building.Keys) {
		if (key.Equals(roomsToBuild)) continue;
		
		if (!building[key].properties.ContainsKey("displacement")) {
			string displ = "displacement ";
			int height = Int32.Parse(building[key].properties["height"].Split(' ')[1]);
			for (int i = 0; i <= height; ++i) {
				displ += "0 ";
			}
			displ = displ.Substring(0, displ.Length-1);
			building[key].properties["displacement"] = displ;
		}
		
	}
	
	// TODO: detect and remove infinitely-recursive rooms
	// Note: infinitely-recursive rooms can only happen as part of
	// a manual modification of the script and cannot be genrated by the
	// script generator.
	
	return building;
}

}

// This class contains all the statements related to a room in a easy-to-access manner.
// During the actual building phase, it also contains references to all rooms instantiated
// from itself.
class RoomDescription {

	public Dictionary<string, string> properties;
	public string name;
	public int[] commandsCount = new int[Enum.GetNames(typeof(Parser.CommandType)).Length];
	public List<Rectangle> rooms;
	public List<Rectangle> roomsBuffer;
	
	public RoomDescription(string n) {
		name = n;
		properties = new Dictionary<string, string>();
		rooms = new List<Rectangle>();
		roomsBuffer = new List<Rectangle>();
		
		if (!n.Equals(Parser.roomsToBuild)) {
			properties["roof"] = "roof 0 FLAT";
			properties["height"] = "height 1";
		}
	}
	
	public void unloadRoomBuffer() {
		rooms.AddRange(roomsBuffer);
		roomsBuffer.Clear();
	}
	
}

}
