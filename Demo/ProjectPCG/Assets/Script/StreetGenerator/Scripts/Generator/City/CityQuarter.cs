using UnityEngine;
using System.Collections.Generic;
using LotGeneration;
using graph;

public class CityQuarter {
	public List<CityNode> mNodes = new List<CityNode>();
	public Vector3 mPosition;
	public VoronoiPolygon mVoroPoly = new VoronoiPolygon();
	public VoronoiPolygon mCenterPoly = new VoronoiPolygon();
	public List<VoronoiPolygon> mStreetLotPoly = new List<VoronoiPolygon>();
	public List<VoronoiPolygon> mCenterLotPoly = new List<VoronoiPolygon>();
	// Should not be used anymore
	public LotMesh mLots;
	public bool mIsField = false;
	public float mSize = 0;
	
	public CityQuarter(Vector3 iPosition) {
		mPosition = iPosition;
	}
	
	public CityQuarter() {}
}
