﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

public class CityEdge {
	public CityNode mFirstNode { get; internal set; }
	public CityNode mSecondNode { get; internal set; }
	public GameObject mRootStreet { get; internal set; }
	public Vector3 mPosition { get; internal set; }

	public CityEdge(CityNode iFirstNode, CityNode iSecondNode) {
		mFirstNode = iFirstNode;
		mSecondNode = iSecondNode;
		mPosition = (mFirstNode.mPosition + mSecondNode.mPosition) / 2;
	}
}

public class CityNode {
	public Vector3 mPosition { get; internal set; }
	public GameObject mRootStreet { get; internal set; }
	public List<CityNode> mAdjacentNodes = new List<CityNode>();

	public CityNode(float iX, float iY, float iZ) {
		mPosition = new Vector3(iX, iY, iZ);
	}

	public CityNode(Vector3 iVector) {
		mPosition = iVector;
	}

	public void addAdjacentNode(CityNode iNode) {
		mAdjacentNodes.Add(iNode);
	}
}

public class CityGenerator : MonoBehaviour {
	private StreetGenerator mStreetGenerator = new StreetGenerator();

	private List<GameObject> mMainStreets = new List<GameObject>();
	private List<CityNode> mOutsideNodes = new List<CityNode>();
	private List<CityNode> mNodes = new List<CityNode>();
	private List<CityEdge> mEdges = new List<CityEdge>();
	private float mOriginalX = 20f;
	private float mOriginalZ = 20f;

	private Graph mGraph = new Graph();
	private CartoNode[,] mMap;
	private Vector3 mCityCenter = new Vector3(0, 0, 0);

	private Settings mSettings = new Settings();

	private double[] mXTab;
	private double[] mYTab;

	private GameObject mRootMainRoads;
	private GameObject mRootQuarters;

	private float mMinXObject = 9999.0f;
	private float mMaxXObject = 0.0f;
	private float mMinZObject = 9999.0f;
	private float mMaxZObject = 0.0f;

	private int mGrowthStep = 1;


	/* Behavior methods */

	void Start() {
		mRootMainRoads = new GameObject();
		mRootMainRoads.name = "Roads";

		mRootQuarters = new GameObject();
		mRootQuarters.name = "Quarters";
		
		GenerateCity(mSettings);
	}

	void Update() {}

	/* Public methods */

	/*
	 * Entrypoint of the generation
	 */ 
	public void GenerateCity(Settings iSettings) {
		mSettings = iSettings;

		CreateNodes();
		CreateCityStructure();

		RemoveUselessNodesEdges();

		CreateEdges();
		CreateOutsideNodes();

		GetVillageCenter();
		GetVillageCoordinates();
	}

	/*
	 * Delete all generated gameobject and reinit data
	 */ 
	public void ClearAll() {
		foreach (GameObject lObject in mMainStreets) {
			Destroy(lObject);
		}
		
		foreach (CityEdge lCityEdge in mEdges) {
			Destroy(lCityEdge.mRootStreet);
		}
		
		foreach (CityNode lCityNode in mNodes) {
			Destroy(lCityNode.mRootStreet);
		}
		
		mMinXObject = 9999.0f;
		mMaxXObject = 0.0f;
		mMinZObject = 9999.0f;
		mMaxZObject = 0.0f;
		mGrowthStep = 1;
		
		mStreetGenerator.ClearAll();
		mEdges.Clear();
		mNodes.Clear();
		mOutsideNodes.Clear();
		mGraph.mNodes.Clear();
	}

	/*
	 * Increase the size of the village
	 * Relies on the GrowthStep parameter
	 */ 
	public void Growth() {
		mGrowthStep++;
		if (mGrowthStep == 2) {
			ProcessGrowthStepTwo();
		} else if (mGrowthStep == 3) {
			ProcessGrowthStepThree();
		} else if (mGrowthStep == 4) {
			ProcessGrowthStepFour();
		}  else if (mGrowthStep >= 5) {
			ProcessGrowthStepFive();
		}
	}
	
	/* Private methods */

	/*
	 * Place node randomly on the map
	 * The placement relies on many rules
	 */
	private void CreateNodes() {
		mXTab = new double[mSettings.mNbNodes];
		mYTab = new double[mSettings.mNbNodes];

		float lPreviousX = mOriginalX;
		float lPreviousZ = mOriginalZ;
		float lPreviousY = GetY(lPreviousX, lPreviousZ) + 1;
		Vector3 lPreviousPosition = new Vector3(lPreviousX, lPreviousY, lPreviousZ);
		
		for (int i = 0; i < mSettings.mNbNodes; ++i) {
			float lX = 0f;
			float lZ = 0f;
			float lY = 0f;
			bool lCanPlaced = false;
			do {
				int j = 0;
				if (i > 0) {
					foreach (CityNode lPlacedNode in mNodes) {
						do {
							lX = Random.Range(200f, mSettings.mExpansion);
							lZ = Random.Range(200f, mSettings.mExpansion);
							lY = GetY(lX, lZ) + 1;
						} while (Vector3.Distance(lPlacedNode.mPosition, new Vector3(lX, lY, lZ)) < mSettings.mMinDistanceNodes
						         || Vector3.Distance(lPlacedNode.mPosition, new Vector3(lX, lY, lZ)) > mSettings.mMaxDistanceNodes
						         || Mathf.Abs(lPreviousY - lY) > 30);
							j++;
					}
					if (j == mNodes.Count) {
						lCanPlaced = true;
					}
				} else {
					lX = Random.Range(200f, mSettings.mExpansion);
					lZ = Random.Range(200f, mSettings.mExpansion);
					lY = GetY(lX, lZ) + 1;
					lCanPlaced = true;
				}
			} while (!lCanPlaced);

			lPreviousPosition = new Vector3(lX, lY, lZ);
			CityNode lCityNode = new CityNode(lPreviousPosition);

			mXTab[i] = lX;
			mYTab[i] = lZ;

			mNodes.Add(lCityNode);
		}
	}

	/*
	 * Generation of the Voronoi diagram
	 * Creation of edges between nodes of the diagram
	 */ 
	private void CreateCityStructure() {
		Voronoi2.Voronoi lVoronoi = new Voronoi2.Voronoi(0);
		List<Voronoi2.GraphEdge> lGraphEdges = lVoronoi.generateVoronoi(mXTab, mYTab, 0f, 400f, 0f, 350f);
		mNodes.Clear();
		
		foreach (Voronoi2.GraphEdge lEdge in lGraphEdges) {
			float lX1 = (float)lEdge.x1 + 250f;
			float lY1 = (float)lEdge.y1 + 250f;
			float lX2 = (float)lEdge.x2 + 250f;
			float lY2 = (float)lEdge.y2 + 250f;
			
			float lDistance = Vector3.Distance(new Vector3(lX1, 0, lY1), new Vector3(lX2, 0, lY2));
			
			if (lDistance > 4 && lDistance < 100) {
				Vector3 lPos1 = new Vector3(lX1, GetY(lX1, lY1) + 0.5f, lY1);
				Vector3 lPos2 = new Vector3(lX2, GetY(lX2, lY2) + 0.5f, lY2);
				
				bool lFindFirst = false;
				bool lFindSecond = false;
				CityNode lFirstNode = new CityNode(lPos1);
				CityNode lSecondNode = new CityNode(lPos2);
				
				if (mNodes.Count != 0) {
					foreach (CityNode lCityNode in mNodes) {
						if (lCityNode.mPosition == lPos1) {
							UpdateEdges(lCityNode, lSecondNode);
							lFindFirst = true;
							break;
						}
					}
					
					if (!lFindFirst) {
						mNodes.Add(lFirstNode);
						UpdateEdges(lFirstNode, lSecondNode);
					}
					
					foreach (CityNode lCityNode in mNodes) {
						if (lCityNode.mPosition == lPos2) {
							UpdateEdges(lFirstNode, lCityNode);
							lFindSecond = true;
							break;
						}
					}
					
					if (!lFindSecond) {
						mNodes.Add(lSecondNode);
						UpdateEdges(lFirstNode, lSecondNode);
					}
					
				} else {
					mNodes.Add(lFirstNode);
					mNodes.Add(lSecondNode);
					UpdateEdges(lFirstNode, lSecondNode);
				}
			}
		}
	}

	/*
	 * Remove unuse nodes 
	 */ 
	private void RemoveUselessNodesEdges() {
		List<CityEdge> lToRemove = new List<CityEdge>();
		foreach (CityEdge lCityEdge in mEdges) {
			if (lCityEdge.mFirstNode.mAdjacentNodes.Count == 1 && lCityEdge.mSecondNode.mAdjacentNodes.Count <= 2 ) {
				mNodes.Remove(lCityEdge.mFirstNode);
				mNodes.Remove(lCityEdge.mSecondNode);
				lCityEdge.mFirstNode.mAdjacentNodes.Remove(lCityEdge.mSecondNode);
				lCityEdge.mSecondNode.mAdjacentNodes.Remove(lCityEdge.mFirstNode);
				lToRemove.Add(lCityEdge);
			}
		}
		
		foreach (CityEdge lCityEdge in lToRemove) {
			mEdges.Remove(lCityEdge);
		}
	}

	/*
	 * Creation of main roads thanks to Voronoi edges
	 */ 
	private void CreateEdges() {
		foreach (CityEdge lCityEdge in mEdges) {
			CreateRoad(lCityEdge.mFirstNode.mPosition, lCityEdge.mSecondNode.mPosition, 3, 10, 0.32f);
		}
	}

	private void UpdateEdges(CityNode iFirstNode, CityNode iSecondNode) {
		if (!iFirstNode.mAdjacentNodes.Contains(iSecondNode)) {
			iFirstNode.mAdjacentNodes.Add(iSecondNode);
		}

		if (!iSecondNode.mAdjacentNodes.Contains(iFirstNode)) {
			iSecondNode.mAdjacentNodes.Add(iFirstNode);
		}
		
		bool lFind = false;
		
		foreach (CityEdge lOtherCityEdge in mEdges) {
			if ((iFirstNode.mPosition == lOtherCityEdge.mSecondNode.mPosition
			     && iSecondNode.mPosition == lOtherCityEdge.mFirstNode.mPosition)
			    || (iFirstNode.mPosition == lOtherCityEdge.mFirstNode.mPosition
			    && iSecondNode.mPosition == lOtherCityEdge.mSecondNode.mPosition)) {
				lFind = true;
				break;
			}
		}
		
		if (!lFind) {
			CityEdge lCityEdge = new CityEdge(iFirstNode, iSecondNode);
			mEdges.Add(lCityEdge);
		}
	}

	/*
	 * Placement of (hardcoded) outside nodes
	 */ 
	private void CreateOutsideNodes() {
		CreateGraph();
		
		Vector3 lPos1 = new Vector3(20, 0, 20);
		LinkOutsideNode(new Vector3(lPos1.x, GetY(lPos1), lPos1.z));
		
		Vector3 lPos2 = new Vector3(Terrain.activeTerrain.terrainData.size.x - 1, 0, Terrain.activeTerrain.terrainData.size.z / 2.8f);
		LinkOutsideNode(new Vector3(lPos2.x, GetY(lPos2), lPos2.z));

		Vector3 lPos3 = new Vector3(Terrain.activeTerrain.terrainData.size.x / 1.5f, 0, Terrain.activeTerrain.terrainData.size.z - 1);
		LinkOutsideNode(new Vector3(lPos3.x, GetY(lPos3), lPos3.z));
	}


	/*
	 * Link the iPosition to the closest node of the city
	 * Based on a Dijkstra algorithm
	 */ 
	private void LinkOutsideNode(Vector3 iPosition) {
		CityNode lClosestNode = null;
		float lClosestDistance = 99999f;
		
		CityNode lNode = new CityNode(iPosition);
		mOutsideNodes.Add(lNode);
		foreach (CityNode lCityNode in mNodes) {
			float lDistance = Vector3.Distance(lCityNode.mPosition, iPosition);
			if (lDistance < lClosestDistance) {
				lClosestDistance = lDistance;
				lClosestNode = lCityNode;
			}
		}
		mNodes.Add(lNode);
		lNode.addAdjacentNode(lClosestNode);
		lClosestNode.addAdjacentNode(lNode);

		List<CartoNode> lPath = mGraph.getDijkstra(mMap[(int)lNode.mPosition.x / mSettings.mStep - 1, (int)lNode.mPosition.z / mSettings.mStep - 1], 
		                                      mMap[(int)lClosestNode.mPosition.x / mSettings.mStep - 1, (int)lClosestNode.mPosition.z / mSettings.mStep - 1]);
		Queue<CartoNode> lQueue = new Queue<CartoNode>(lPath);
		CartoNode lCurrentNode = mMap[(int)lNode.mPosition.x / mSettings.mStep, (int)lNode.mPosition.z / mSettings.mStep];
		Vector3 lCurrentPosition = lCurrentNode.mPosition;
		Vector3 lNextPosition = new Vector3();
		
		while (lQueue.Count != 0) {
			CartoNode lNextNode = lQueue.Dequeue();
			lNextPosition = lNextNode.mPosition;
			
			CreateRoad(lCurrentPosition, lNextPosition, 4, 2, 0.5f);
			
			lCurrentNode = lNextNode;
			lCurrentPosition = lNextPosition;
		}
	}

	/*
	 * Generate a road from a iRef position to the iTarget position
	 */ 
	private void CreateRoad(Vector3 iRef, Vector3 iTarget, int iMaxVertex, float iThreshold, float iWidth) {
		iMaxVertex = iMaxVertex <= 2 ? 2 : iMaxVertex;

		GameObject lMainStreet = new GameObject();
		lMainStreet.transform.parent = mRootMainRoads.transform;
		lMainStreet.name = "Road";
		lMainStreet.transform.position = (iRef + iTarget) / 2;
		mMainStreets.Add(lMainStreet);
		
		lMainStreet.AddComponent<LineRenderer>();
		LineRenderer lRenderer = lMainStreet.GetComponent<LineRenderer>();
		lRenderer.material = new Material(Shader.Find("Particles/Additive"));
		lRenderer.SetWidth(iWidth, iWidth);
		lRenderer.SetVertexCount(iMaxVertex);

		lMainStreet.AddComponent<BoxCollider>();
		BoxCollider lCollider = lMainStreet.GetComponent<BoxCollider>();
		lCollider.transform.position = (iRef + iTarget) / 2;
		lCollider.size = new Vector3(12, 12, 12);
		lCollider.isTrigger = false;

		lRenderer.SetPosition(0, iRef);
		
		float lX = iRef.x;
		float lY = iRef.y;
		float lZ = iRef.z;
		Vector3 lOldPosition = new Vector3(lX, lY, lZ);
		
		for (int j = 1; j < iMaxVertex - 1; j++) {
			float lTmpX = lX;
			float lTmpY = lY;
			float lTmpZ = lZ;
			do {
				lX = Random.Range(lTmpX, iTarget.x);
				lZ = Random.Range(lTmpZ, iTarget.z);
				lY = Terrain.activeTerrain.SampleHeight(new Vector3(lX, 0, lZ)) + 1;
			} while ((lY + lTmpY) / 2 < GetY((lOldPosition + new Vector3(lX, lY, lZ)) / 2)
			         || (lY + lTmpY) / 2 > GetY((lOldPosition + new Vector3(lX, lY, lZ)) / 2) + 2
			         || Vector3.Distance(lOldPosition, new Vector3(lX, lY, lZ)) > iThreshold);
			
			Vector3 lPosition = new Vector3(lX, lY, lZ);
			lRenderer.SetPosition(j, lPosition);
			lOldPosition = lPosition;
		}
		lRenderer.SetPosition(iMaxVertex - 1, iTarget);
	}


	/*
	 * Create a graph for pathfinding algorithm
	 * This graph is based on the terrain, not on the city
	 */ 
	private void CreateGraph() {
		mMap = new CartoNode[(int)Terrain.activeTerrain.terrainData.size.x / mSettings.mStep, (int)Terrain.activeTerrain.terrainData.size.z / mSettings.mStep];
		int i = 0;
		int j = 0;
		float lMinY = 999999f;
		for (int x = 0; x < Terrain.activeTerrain.terrainData.size.x && i < Terrain.activeTerrain.terrainData.size.x / mSettings.mStep; x += mSettings.mStep) {
			for (int z = 0; z < Terrain.activeTerrain.terrainData.size.z && j < Terrain.activeTerrain.terrainData.size.z / mSettings.mStep; z += mSettings.mStep) {
				float y = GetY(x, z);
				if (y < lMinY) {
					lMinY = y;
				}

				mMap[i, j] = new CartoNode(new Vector3(x, y, z), NodeKind.empty);
				j++;
			}
			i++;
			j = 0;
		}
		
		for (int x = 0; x < Terrain.activeTerrain.terrainData.size.x / mSettings.mStep; x++) {
			for (int z = 0; z < Terrain.activeTerrain.terrainData.size.z / mSettings.mStep; z++) {
				CartoNode lCurrentNode = mMap[x, z];
				if (x > 0) {
					lCurrentNode.addAdjacentNode(mMap[x - 1, z], mMap[x - 1, z].mPosition.y == lMinY ? 0 : (int)mMap[x - 1, z].mPosition.y * (int)mMap[x - 1, z].mPosition.y);
				}
				
				if (x < (int) Terrain.activeTerrain.terrainData.size.x / mSettings.mStep - 1) {
					lCurrentNode.addAdjacentNode(mMap[x + 1, z], mMap[x + 1, z].mPosition.y == lMinY ? 0 : (int)mMap[x + 1, z].mPosition.y * (int)mMap[x + 1, z].mPosition.y);
				}
				
				if (z > 0) {
					lCurrentNode.addAdjacentNode(mMap[x, z - 1], mMap[x, z - 1].mPosition.y == lMinY ? 0 : (int)mMap[x, z - 1].mPosition.y * (int)mMap[x, z - 1].mPosition.y);
				}
				
				if (z < (int) Terrain.activeTerrain.terrainData.size.z / mSettings.mStep - 1)	 {
					lCurrentNode.addAdjacentNode(mMap[x, z + 1], mMap[x, z + 1].mPosition.y == lMinY ? 0 : (int)mMap[x, z + 1].mPosition.y * (int)mMap[x, z + 1].mPosition.y);
				}
				mGraph.addNode(lCurrentNode);
			}
		}
	}

	private void CreateStreets() {
		foreach (CityNode lCityNode in mNodes) {
			if (lCityNode.mAdjacentNodes.Count < 2 && !mOutsideNodes.Contains(lCityNode)) {
				if (lCityNode.mPosition.x < mCityCenter.x && lCityNode.mPosition.z < mCityCenter.z) {
					mStreetGenerator.Generate(lCityNode.mPosition, StreetDirection.down);
				} else if (lCityNode.mPosition.x < mCityCenter.x && lCityNode.mPosition.z > mCityCenter.z) {
					mStreetGenerator.Generate(lCityNode.mPosition, StreetDirection.left);
				} else if (lCityNode.mPosition.x > mCityCenter.x && lCityNode.mPosition.z < mCityCenter.z) {
					mStreetGenerator.Generate(lCityNode.mPosition, StreetDirection.right);
				} else if (lCityNode.mPosition.x > mCityCenter.x && lCityNode.mPosition.z > mCityCenter.z) {
					mStreetGenerator.Generate(lCityNode.mPosition, StreetDirection.up);
				}
			}
		}
	}
		
	/*
	 * Get the center position of the village
	 */ 
	private void GetVillageCenter() {
		foreach (CityEdge lCityEdge in mEdges) {
			mCityCenter += lCityEdge.mPosition;
		}
		mCityCenter /= mEdges.Count;
	}

	/*
	 * Get the minX, maxX, minZ and maxZ coordonates of the village
	 */ 
	private void GetVillageCoordinates() {
		foreach (CityNode lCityNode in mNodes) {
			if (!mOutsideNodes.Contains(lCityNode)) {
				if (lCityNode.mPosition.x < mMinXObject) {
					mMinXObject = lCityNode.mPosition.x;
				}

				if (lCityNode.mPosition.x > mMaxXObject) {
					mMaxXObject = lCityNode.mPosition.x;
				}

				if (lCityNode.mPosition.z < mMinZObject) {
					mMinZObject = lCityNode.mPosition.z;
				}

				if (lCityNode.mPosition.z > mMaxZObject) {
					mMaxZObject = lCityNode.mPosition.z;
				}
			}
		}

		mStreetGenerator.mMaxXObject = mMaxXObject;
		mStreetGenerator.mMinXObject = mMinXObject;
		mStreetGenerator.mMaxZObject = mMaxZObject;
		mStreetGenerator.mMinZObject = mMinZObject;
	}

	/*
	 * Get the Y value of the iX, iZ position
	 */ 
	private float GetY(float iX, float iZ) {
		return Terrain.activeTerrain.SampleHeight(new Vector3(iX, 0, iZ));
	}

	/*
	 * Get the Y value of the iVector position
	 * The iVector.y value doesn't matter
	 */ 
	private float GetY(Vector3 iVector) {
		return Terrain.activeTerrain.SampleHeight(iVector);
	}


	/*
	 * Steps about the growth of the village to a city
	 */

	private void ProcessGrowthStepTwo() {
		Vector3 lPositionDown = new Vector3(mMinXObject, GetY(mMinXObject, mMinZObject), mMinZObject);
		mStreetGenerator.Generate(lPositionDown, StreetDirection.left);
		LinkOutsideNode(lPositionDown);

		Vector3 lPositionLeft = new Vector3(mMinXObject, GetY(mMinXObject, mMaxZObject), mMaxZObject);
		mStreetGenerator.Generate(lPositionLeft, StreetDirection.down);
		LinkOutsideNode(lPositionLeft);

		Vector3 lPositionRight = new Vector3(mMaxXObject, GetY(mMaxXObject, mMinZObject), mMinZObject);
		mStreetGenerator.Generate(lPositionRight, StreetDirection.right);
		LinkOutsideNode(lPositionRight);

		Vector3 lPositionUp = new Vector3(mMaxXObject, GetY(mMaxXObject, mMaxZObject), mMaxZObject);
		mStreetGenerator.Generate(lPositionUp, StreetDirection.up);
		LinkOutsideNode(lPositionUp);
	}

	private void ProcessGrowthStepThree() {
		Vector3 lPositionDown = new Vector3(mMinXObject, GetY(mMinXObject, mMinZObject), mMinZObject);
		Vector3 lPositionLeft = new Vector3(mMinXObject, GetY(mMinXObject, mMaxZObject), mMaxZObject);
		Vector3 lPositionRight = new Vector3(mMaxXObject, GetY(mMaxXObject, mMinZObject), mMinZObject);
		Vector3 lPositionUp = new Vector3(mMaxXObject, GetY(mMaxXObject, mMaxZObject), mMaxZObject);

		mStreetGenerator.Generate((lPositionDown + lPositionLeft) / 2, StreetDirection.down);
		LinkOutsideNode((lPositionDown + lPositionLeft) / 2);
		
		mStreetGenerator.Generate((lPositionUp + lPositionLeft) / 2, StreetDirection.down);
		LinkOutsideNode((lPositionUp + lPositionLeft) / 2);
		
		mStreetGenerator.Generate((lPositionRight + lPositionUp) / 2, StreetDirection.up);
		LinkOutsideNode((lPositionRight + lPositionUp) / 2);
		
		mStreetGenerator.Generate((lPositionRight + lPositionDown) / 2, StreetDirection.right);
		LinkOutsideNode((lPositionRight + lPositionDown) / 2);
	}

	private void ProcessGrowthStepFour() {
		mStreetGenerator.Increase();
		for (int i = 0; i < 4; ++i) {
			float lX = 0f;
			float lZ = 0f;
			
			if (Random.Range(0f, 1f) > 0.5f) {
				lX = Random.Range(mMaxXObject + 70f, mMaxXObject + 120f);
			} else {
				lX = Random.Range(mMinXObject - 120f, mMinXObject - 70f);
			}
			
			if (Random.Range(0f, 1f) > 0.5f) {
				lZ = Random.Range(mMaxZObject + 70f, mMaxZObject + 120f);
			} else {
				lZ = Random.Range(mMinZObject - 120f, mMinZObject - 70f);
			}
			
			Vector3 lPosition = new Vector3(lX, Terrain.activeTerrain.SampleHeight(new Vector3(lX, 0, lZ)), lZ);
			LinkOutsideNode(lPosition);
			
			if (lX < mCityCenter.x && lZ < mCityCenter.z) {
				mStreetGenerator.Generate(lPosition, StreetDirection.down);
			} else if (lX < mCityCenter.x && lZ > mCityCenter.z) {
				mStreetGenerator.Generate(lPosition, StreetDirection.left);
			} else if (lX > mCityCenter.x && lZ < mCityCenter.z) {
				mStreetGenerator.Generate(lPosition, StreetDirection.right);
			} else if (lX > mCityCenter.x && lZ > mCityCenter.z) {
				mStreetGenerator.Generate(lPosition, StreetDirection.up);
			}
		}
	}

	private void ProcessGrowthStepFive() {
		mStreetGenerator.Increase();
	}
}