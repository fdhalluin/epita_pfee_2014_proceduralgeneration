var http = require('http');
var fs = require('fs');
var async = require('async');

var chest = [];
var wordChest = [];

function getConceptResponse(uri, callback) {
var options = {
host: 'conceptnet5.media.mit.edu',
path:'/data/5.2/' + uri
};

httpParser = function(response) {
var ind = chest.length;

(function(i){
chest[ind] = '';
response.on('data', function(chunk){chest[ind] += chunk;});
response.on('end', function(){

try{
callback(JSON.parse(chest[ind]));
}
catch (err){
console.log(err);
fs.writeFile('error.txt', chest[ind], function(err){
process.exit();
});


}
})})(ind);

};

http.request(options, httpParser).end();
}

function getWordResponse(word, callback) {
var options = {
host: 'wordnetweb.princeton.edu',
path:'/perl/webwn?o2=&o0=1&o8=1&o1=1&o7=&o5=&o9=&o6=&o3=&o4=&s=' + word
};

httpParser = function(response) {
var ind = wordChest.length;

(function(i){
wordChest[ind] = '';
response.on('data', function(chunk){wordChest[ind] += chunk;});
response.on('end', function(){

callback(wordChest[ind]);


}
);})(ind);

};

http.request(options, httpParser).end();
}

function getWordNetPage(requestURI, callback) {
var options = {
host: 'wordnetweb.princeton.edu',
path:'/perl/'+ requestURI
};

httpParser = function(response) {
var ind = wordChest.length;

(function(i){
wordChest[ind] = '';
response.on('data', function(chunk){wordChest[ind] += chunk;});
response.on('end', function(){

callback(wordChest[ind]);


}
);})(ind);

};

http.request(options, httpParser).end();
}

var myResult = [];

function getLinkedEntities(location, category) {
getConceptResponse('search?rel=/r/RelatedTo&end=/c/en/' + location, function(data){
var uris = [];

for (var ind in data.edges){
uris.push(data.edges[ind].startLemmas);
//console.log(data.edges[ind].startLemmas);
};

for (var ind in uris) {
var entity = uris[ind];
(function(ent){

try{
isA(ent, category, myResult);
}
catch(err) {
console.log('ERROR: could not determine personness for ' + ent);
console.log('>> ' + err);
}

})(entity);
}

});

return myResult;
}

function getLink(linkType, html) {
var regexp = new RegExp('<a href="([^"]*)">' + linkType + '<\/a>');

var matched = regexp.exec(html);

if (matched && matched.length > 0) return matched[1];
return null;
}

function isA(entity, type, myResult) {
console.log('Is a ' + entity + ' a ' + type + ' ?');
getWordResponse(entity, function(data){

getWordNetPage(getLink('S:', data), function(data1){

getWordNetPage(getLink('inherited hypernym', data1), function(data2){
if (getLink(type, data2)) {
console.log(entity + ' is a ' + type);
myResult.push(entity);
}
else {
console.log(entity + ' is not a ' + type);
}

});
});
});
return myResult;
}

//isA('knight', 'person');

// getLinkedEntities('castle', 'person')
function getSyncLinkedEntities(location, category, callBack) {
getConceptResponse('search?rel=/r/RelatedTo&end=/c/en/' + location, function(data){
var uris = [];

for (var ind in data.edges){
uris.push(data.edges[ind].startLemmas);
//console.log(data.edges[ind].startLemmas);
};

async.map(uris, function(uri){return isA(uri, 'person', []);}, callBack);
});

return myResult;
}

getSyncLinkedEntities('castle', 'person', function(err, linked){
console.log(linked);
});
