using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface IIndexedObject {
	int Index { get; set; }
}

public class ANode : IComparer<ANode>, IIndexedObject {

	public static readonly ANode Comparer = new ANode(new Vector3());
	public int Index { get; set; }

	public int mMinDistance = 9999999;
	public ANode mPrievousNode = null;

	public Dictionary<ANode, int> mAdjacentNodes;
	public Vector3 mPosition;

	protected ANode() {}

	protected ANode(Vector3 iPosition) {
		mPosition = iPosition;
	}

	public void addAdjacentNode(ANode iNode, int iWeight) {
		mAdjacentNodes.Add(iNode, iWeight);
	}
	
	public Dictionary<ANode, int> getAdjacentNodes() {
		return mAdjacentNodes;
	}
	
	public int Compare(ANode x, ANode y) {
		if (x.mMinDistance < y.mMinDistance)
			return -1;
		else if (x.mMinDistance > y.mMinDistance)
			return 1;
		return 0;
	}
}

