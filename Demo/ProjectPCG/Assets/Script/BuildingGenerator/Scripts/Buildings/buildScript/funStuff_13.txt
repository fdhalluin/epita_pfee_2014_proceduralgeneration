
Room_28:
size 9 4
height 2
build Room_28 50 50
addRoom NORTH Door_29 RANDOM
onTop Room_30 2 0
displacement 0 0.7 0.3 
windows NORTH 0 1 0 SQUARE
windows SOUTH 0 3 3 SQUARE
windows WEST 0 1 2 SQUARE
windows EAST 0 3 1 SQUARE
windows NORTH 1 3 1 SQUARE
windows SOUTH 1 2 0 SQUARE
windows WEST 1 1 2 SQUARE
windows EAST 1 1 2 SQUARE
roof 0 SLOPE 0.9 0.3

Room_30:
size 3 2
height 2
displacement 0 0.3 0.8 
windows NORTH 0 3 3 SQUARE
windows SOUTH 0 2 2 SQUARE
windows WEST 0 1 2 SQUARE
windows EAST 0 1 2 SQUARE
windows NORTH 1 1 1 SQUARE
windows SOUTH 1 1 3 SQUARE
windows WEST 1 3 3 SQUARE
windows EAST 1 3 3 SQUARE
roof 1 POINT 0.4





Door_29:
size 3 1
height 1
addWindow NORTH 0 1 DOOR
displacement 0 0 
roof 0 SLOPE 0.7 0.4 NORTH


