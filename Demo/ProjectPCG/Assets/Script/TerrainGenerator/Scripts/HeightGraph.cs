﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class HeightNode : IComparer<HeightNode>, IIndexedObject {
	
	public static readonly HeightNode Comparer = new HeightNode(new int(),new int(),new float());
	
	private Dictionary<HeightNode, float> mAdjacentNodes;
	public float mMinDistance = 90000;
	public HeightNode mPrievousNode = null;
	public int mX { get; internal set; }
	public int mZ { get; internal set; }
	public int Index { get; set; }
	public float mHeight { get; set; }
	
	public HeightNode(int x, int z, float y) {
		mAdjacentNodes = new Dictionary<HeightNode, float>();
		mX = x;
		mZ = z;
		mHeight = y;
		mMinDistance = 90000 + y;
	}
	
	public void addAdjacentNode(HeightNode iNode, float iWeight) {
		mAdjacentNodes.Add(iNode, iWeight);
	}
	
	public Dictionary<HeightNode, float> getAdjacentNodes() {
		return mAdjacentNodes;
	}
	
	public int Compare(HeightNode x, HeightNode y) {
		if (x.mMinDistance < y.mMinDistance)
			return -1;
		else if (x.mMinDistance > y.mMinDistance)
			return 1;
		return 0;
	}
}

public class HeightGraph  {
	public List<HeightNode> mNodes { get; set; }
	
	public HeightGraph() {
		mNodes = new List<HeightNode>();
	}
	
	public void addNode(HeightNode iNode) {
		mNodes.Add(iNode);
	}
	
	public List<HeightNode> getDijkstra(HeightNode iSourceNode, HeightNode iDestinationNode) {
		reset();
		computePaths(iSourceNode);
		return shortestPathTo(iSourceNode, iDestinationNode);
	}
	
	private void computePaths(HeightNode iSourceNode) {
		iSourceNode.mMinDistance = 0;
		PriorityQueue<HeightNode> lNodeQueue = new PriorityQueue<HeightNode>(HeightNode.Comparer);
		lNodeQueue.Push(iSourceNode);
		
		while (lNodeQueue.Count != 0) {
			HeightNode lCurrentNode = lNodeQueue.Pop();
			foreach (KeyValuePair<HeightNode, float> lEntry in lCurrentNode.getAdjacentNodes()) {
				HeightNode lAdjacentNode = lEntry.Key;
				float lWeight = lEntry.Value;
				float lDistanceNode = lCurrentNode.mMinDistance + lWeight;
				if (lDistanceNode < lAdjacentNode.mMinDistance ) {
					lNodeQueue.Remove(lAdjacentNode);
					lAdjacentNode.mMinDistance = lDistanceNode;
					lAdjacentNode.mPrievousNode = lCurrentNode;
					lNodeQueue.Push(lAdjacentNode);
				}
			}
		}
	}
	
	private List<HeightNode> shortestPathTo(HeightNode iSourceNode, HeightNode iDestinationNode) {
		List<HeightNode> lPath = new List<HeightNode>();
		for (HeightNode lNode = iDestinationNode; lNode != null; lNode = lNode.mPrievousNode) {
			lPath.Add(lNode);
		}
		lPath.Reverse();
		return lPath;
	}
	
	private void reset() {
		foreach (HeightNode lNode in mNodes) {
			lNode.mMinDistance = 90000;
			lNode.mPrievousNode = null;
		}
	}
}
