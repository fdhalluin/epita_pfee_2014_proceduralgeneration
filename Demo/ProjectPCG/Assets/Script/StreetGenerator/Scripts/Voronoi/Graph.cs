using System;
using System.Collections.Generic;

namespace graph
{
	public class Graph
	{
		internal HashSet<graph.Point> vertices;

		internal HashSet<graph.Edge> edges;

		public Graph()
		{
			vertices = new HashSet<graph.Point>();
			edges = new HashSet<graph.Edge>();
		}

		public virtual void addVertex(graph.Point p)
		{
			if (vertices.Contains(p))
			{
				return;
			}
			vertices.Add(p);
		}

		public virtual void addEdge(graph.Point first, graph.Point second)
		{
			if (first == null || second == null)
			{
				return;
			}
			addVertex(first);
			addVertex(second);
			graph.Edge e = new graph.Edge(first, second);
			if (edges.Contains(e))
			{
				return;
			}
			edges.Add(e);
		}

		public virtual HashSet<graph.Point> getNeighbours(graph.Point p)
		{
			HashSet<graph.Point> ans = new HashSet<graph.Point>();
			if (!vertices.Contains(p))
			{
				// I will not need this line
				return ans;
			}
			foreach (graph.Edge e in edges)
			{
				if (e.getFirst() == p)
				{
					ans.Add(e.getSecond());
				}
				if (e.getSecond() == p)
				{
					ans.Add(e.getFirst());
				}
			}
			return ans;
		}

		public virtual HashSet<graph.Edge> getEdges(graph.Point p)
		{
			HashSet<graph.Edge> ans = new HashSet<graph.Edge>();
			foreach (graph.Edge e in edges)
			{
				if (e.getFirst() == p || e.getSecond() == p)
				{
					ans.Add(e);
				}
			}
			return ans;
		}

		public virtual HashSet<graph.Point> getVertices()
		{
			return vertices;
		}

		public virtual HashSet<graph.Edge> getEdges()
		{
			return edges;
		}

		public virtual void addEdge(graph.Edge e)
		{
			addEdge(e.first, e.second);
			//addEdge((graph.Point)e.first.clone(), (graph.Point)e.second.clone());
		}
	}
}
