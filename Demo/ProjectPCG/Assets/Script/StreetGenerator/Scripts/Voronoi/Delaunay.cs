using System;
using System.Collections.Generic;

namespace graph
{
	public class Delaunay : graph.IGraph
	{
		internal HashSet<graph.Triangle> triangles;

		internal graph.Voronoi v;

		internal bool vUpdate;

		public Delaunay(graph.Triangle t) : base()
		{
			triangles = new HashSet<graph.Triangle>();
			addTriangle(t);
			v = new graph.Voronoi();
			vUpdate = false;
		}

		public Delaunay() : this(new graph.Triangle(new graph.Point(-10000, -10000), new 
			graph.Point(10000, -10000), new graph.Point(0, 10000)))
		{
		}

		private void addTriangle(graph.Triangle t)
		{
			triangles.Add(t);
		}

		private void updateNeighbours(graph.Triangle t)
		{
			HashSet<graph.Edge> edges = t.getEdges();
			foreach (graph.Triangle n in triangles)
			{
				foreach (graph.Edge e in edges)
				{
					if (n != t && n.hasEdge(e))
					{
						t.addNeighbour(n);
						n.addNeighbour(t);
					}
				}
			}
		}

		private void removeTriangle(graph.Triangle t)
		{
			triangles.Remove(t);
			foreach (graph.Triangle n in t.getNeighbours())
			{
				n.removeNeighbour(t);
			}
		}

		public virtual void addPointToGraph(graph.Point p)
		{
			graph.Triangle container = null;
			vUpdate = true;
			foreach (graph.Triangle t in triangles)
			{
				if (t.contains(p))
				{
					container = t;
					break;
				}
			}
			if (container == null)
			{
				//System.out.println("NOT IN ORIGINAL ZONE");
				return;
			}
			HashSet<graph.Triangle> cavity = getCavity(container, p);
			HashSet<graph.Edge> border = new HashSet<graph.Edge>();
			foreach (graph.Triangle t_1 in cavity)
			{
				removeTriangle(t_1);
				foreach (graph.Edge e in t_1.getEdges())
				{
					bool add = true;
					foreach (graph.Edge m in border)
					{
						if (m.equals(e))
						{
							border.Remove(m);
							add = false;
							break;
						}
					}
					if (add)
					{
						border.Add(e);
					}
				}
			}
			//System.out.println("BOUNDARY " + border.size());
			HashSet<graph.Triangle> newTriangles = new HashSet<graph.Triangle
				>();
			foreach (graph.Edge e_1 in border)
			{
				newTriangles.Add(new graph.Triangle(p, e_1));
			}
			foreach (graph.Triangle t_2 in newTriangles)
			{
				addTriangle(t_2);
			}
			foreach (graph.Triangle t_3 in newTriangles)
			{
				updateNeighbours(t_3);
			}
		}

		//System.out.println("TRIANGLES " + triangles.size());
		//System.out.println();
		public virtual HashSet<graph.Triangle> getTriangles()
		{
			return triangles;
		}

		public virtual HashSet<graph.Triangle> getTriangles(graph.Triangle t)
		{
			HashSet<graph.Triangle> ans = new HashSet<graph.Triangle>();
			foreach (graph.Triangle tr in triangles)
			{
				foreach (graph.Edge e in t.getEdges())
				{
					if (tr.hasEdge(e))
					{
						ans.Add(tr);
					}
				}
			}
			return ans;
		}

		public virtual HashSet<graph.Triangle> getCavity(graph.Triangle t, graph.Point
			 p)
		{
			HashSet<graph.Triangle> ans = new HashSet<graph.Triangle>();
			Queue<graph.Triangle> toVisit = new System.Collections.Generic.Queue
				<graph.Triangle>();
			HashSet<graph.Triangle> visited = new HashSet<graph.Triangle>
				();
			toVisit.Enqueue(t);
			while (toVisit.Count > 0)
			{
				graph.Triangle pop = toVisit.Dequeue();
				visited.Add(pop);
				if (pop.isInCircumcircle(p))
				{
					ans.Add(pop);
					foreach (graph.Triangle tr in triangles)
					{
						foreach (graph.Edge e in pop.getEdges())
						{
							if (tr.hasEdge(e) && !visited.Contains(tr))
							{
								toVisit.Enqueue(tr);
							}
						}
					}
				}
			}
			//System.out.println("CAVITY " + ans.size());
			return (ans);
		}

		public virtual HashSet<graph.Point> getVertices()
		{
			HashSet<graph.Point> ans = new HashSet<graph.Point>();
			foreach (graph.Triangle t in triangles)
			{
				foreach (graph.Point point in t.getPoints()) {
					ans.Add (point);
				}
			}
			return ans;
		}

		public virtual HashSet<graph.Edge> getEdges()
		{
			HashSet<graph.Edge> ans = new HashSet<graph.Edge>();
			foreach (graph.Triangle t in triangles)
			{
				foreach (graph.Edge edge in t.getEdges()) {
					ans.Add (edge);
				}
			}
			return ans;
		}

		public virtual HashSet<graph.Edge> getVoronoiEdges()
		{
			HashSet<graph.Edge> ans = new HashSet<graph.Edge>();
			foreach (graph.Triangle t in triangles)
			{
				foreach (graph.Triangle tr in getTriangles(t))
				{
					ans.Add(new graph.Edge(t.getCircumCenter(), tr.getCircumCenter()));
				}
			}
			return ans;
		}

		public virtual HashSet<graph.Point> getVoronoiVertices()
		{
			HashSet<graph.Point> ans = new HashSet<graph.Point>();
			foreach (graph.Triangle t in triangles)
			{
				ans.Add(t.getCircumCenter());
			}
			return ans;
		}

		public virtual graph.VoronoiPolygon getPolygon(graph.Point p, graph.Triangle t)
		{
			graph.VoronoiPolygon ans = new graph.VoronoiPolygon();
			ans.setCenter(p);
//			HashSet<graph.Edge> edges = new HashSet<graph.Edge>();
			HashSet<graph.Triangle> visited = new HashSet<graph.Triangle>
				();
			Queue<graph.Triangle> toVisit = new System.Collections.Generic.Queue
				<graph.Triangle>();
			graph.Triangle pop = null;
			graph.Point cc = t.getCircumCenter();
			ans.addPoint(cc);
			toVisit.Enqueue(t);
			while (toVisit.Count > 0)
			{
				pop = toVisit.Dequeue();
				visited.Add(pop);
				foreach (graph.Triangle n in pop.getNeighbours())
				{
					if (n.hasPoint(p) && !visited.Contains(n))
					{
						cc = n.getCircumCenter();
						ans.addPoint(cc);
						toVisit.Enqueue(n);
						break;
					}
				}
			}
			return ans;
		}

		public virtual graph.Voronoi getVoronoi()
		{
			if (vUpdate)
			{
				HashSet<graph.VoronoiPolygon> polygons = new HashSet<graph.VoronoiPolygon
					>();
//				graph.VoronoiPolygon temp = null;
//				graph.Point circumcenter = null;
				bool exit = false;
				//System.out.println("VERTICES SIZE " + getVertices().size());
				foreach (graph.Point p in getVertices())
				{
					foreach (graph.Triangle t in triangles)
					{
						exit = false;
						foreach (graph.Point p2 in t.getPoints())
						{
							if (p2.Equals(p))
							{
								polygons.Add(getPolygon(p, t));
								exit = true;
								break;
							}
						}
						if (exit)
						{
							break;
						}
					}
				}
				v.setEdges(getVoronoiEdges());
				v.setVertices(getVoronoiVertices());
				v.update(polygons);
				vUpdate = false;
			}
			return v;
		}

		public virtual void llyodRelaxation(int times)
		{
			for (int i = 0; i < times; ++i)
			{
				getVoronoi();
				HashSet<graph.Point> np = new HashSet<graph.Point>();
				foreach (graph.VoronoiPolygon p in v.polygons)
				{
					//np.Add(new Point(p.getCentroid().x * 2, p.getCentroid().y * 2));
					np.Add(p.getCentroid());
				}
				triangles.Clear();
				triangles.Add(new graph.Triangle(new graph.Point(-10000, -10000), new graph.Point
					(10000, -10000), new graph.Point(0, 10000)));
				foreach (graph.Point p_1 in np)
				{
					addPointToGraph(p_1);
				}
			}
			getVoronoi();
		}

		public virtual void llyodRelaxation()
		{
			llyodRelaxation(1);
		}
	}
}
