using System;
using System.Collections.Generic;

namespace graph
{
	public class Voronoi : graph.IGraph
	{
		internal HashSet<graph.VoronoiPolygon> polygons;

		internal HashSet<graph.Point> vertices;

		internal HashSet<graph.Edge> edges;

		public virtual HashSet<graph.Point> getVertices()
		{
			return vertices;
		}

		public virtual HashSet<graph.Point> getNeighbours(graph.Point p)
		{
			HashSet<graph.Point> ans = new HashSet<graph.Point>();
			foreach (graph.Edge e in edges)
			{
				if (e.first.Equals(p))
				{
					ans.Add(e.second);
				}
				else
				{
					if (e.second.Equals(p))
					{
						ans.Add(e.first);
					}
				}
			}
			return ans;
		}

		public virtual void setVertices(HashSet<graph.Point> vertices)
		{
			this.vertices = vertices;
		}

		public virtual HashSet<graph.Edge> getEdges()
		{
			return edges;
		}

		public virtual void setEdges(HashSet<graph.Edge> edges)
		{
			this.edges = edges;
		}

		public Voronoi()
		{
		}

		public virtual HashSet<graph.VoronoiPolygon> getPolygons()
		{
			return polygons;
		}

		public virtual void update(HashSet<graph.VoronoiPolygon> polygons)
		{
			this.polygons = polygons;
		}

		public virtual HashSet<graph.VoronoiPolygon> getNeighbours(graph.VoronoiPolygon
			 p)
		{
			return p.getNeighbours();
		}


	}
}
