using System;
using System.Collections.Generic;
using UnityEngine;
using HouseGen;

namespace ScriptGen {

public enum BaseShape {L_SHAPE = 0, RECT=1};

// This class contains all information needed to write a room in script form
// It has the same expressive power as a Rectangle, except it can be translated
// to script form more easily.
public class Room {
	public int width;
	public int length;
	int height;
	public int offsetX;
	public int offsetY;
	List<string> statements;
	public string name;
	public List<Room> onTops;
	List<int> displacements;
	public string specifiedRoof = null;

	// Different parameters which restrains the room's construction
	public bool noDisplacement = false;
	public bool heightSet = false;
	public bool noWindows = false;
	public bool noOnTop = false;

	public Room parent = null;

	public static int roomCount = 0;
	
	public void setHeight(int h) {
		if (heightSet) return;
		height = h;
		heightSet = true;
	}

	public Room(string n, int w, int l) {
		name = n;
		width = w;
		length = l;
		statements = new List<string>();
		onTops = new List<Room>();
		height = 1;
		displacements = new List<int>();
		++roomCount;
	}
	
	// This add a statement to the room, to be copied with it when written in script form
	// This allow for easy customization of the script language without disturbing the Room class too much
	public void addStatement(string statement) {
		statements.Add(statement);
	}
	
	// This method converts the Room to a string which can then be parsed by the parser and built into a GameObject
	public string getScript() {
		string script = "\n" + name + ":\n";
		int maxWidth = width + 3;
		int maxLength = length + 3;

		script += "size ?(" + width + ", " + maxWidth + ") ?("
				+ length + ", " + maxLength + ")" + "\n";
		script += "height " + height + "\n";

		foreach (string statement in statements) {
			script += statement + "\n";
		}

		string onTopRooms = "";

		foreach (Room onTopRoom in onTops) {
			script += "onTop " + onTopRoom.name + " " + onTopRoom.offsetX + " " + onTopRoom.offsetY + "\n";
			onTopRooms += onTopRoom.getScript() + "\n";
		}

		script += getDisplacementAsString();
		script += getWindowsString();
		script += getRoof();
		script += "\n" + onTopRooms + "\n";

		return script;
	}

	// This method generates a displacement list to make the walls into slopes
	// It tries its best to disallow half-floating rooms, but sometimes fail.
	public void generateDisplacement() {
		int displ = 0;

		if (parent != null && !noDisplacement) {
			// Minimal margin available. Takes into account the offset used to position the room.
			int marginWithOffset = 0;
			int minOffset = Mathf.Min(offsetX,offsetY);
			int minOtherSide = Mathf.Min (parent.width - offsetX - this.width, parent.length - offsetY - this.length);
			marginWithOffset = Mathf.Min (minOffset, minOtherSide);

			displ = -marginWithOffset;
		}

		for(int i = 0; i <= height; ++i) {
			if (noDisplacement) {
				displacements.Add(0);
				continue;
			}

			displ = Util.rand.Next(displ-6, displ+6);
			if (-displ > Mathf.Min (width, length)/2) {
				if (displacements.Count > 0) {
					displ = displacements[displacements.Count-1];
				}
				else {
					displ = 0;
				}
			}
			displacements.Add(displ);

		}

		displacements[0] = 0;
	}

	// Write the displacement list in script form
	string getDisplacementAsString() { /*Debug.Log ("VER_2.0");*/
		string ans = "displacement ";

		foreach(int displ in displacements) {
			ans += ((float)displ/10) + " ";
		}

		ans += "\n";

		return ans;
	}

	// Get the displacement value of the top layer
	public float getTopDispl() {
		return displacements[displacements.Count-1];
	}

	// Get a string specifying which kind of roof the building will get
	public string getRoof() {
		if (specifiedRoof != null) return specifiedRoof; 
		string ans = "roof ";

		if (onTops.Count == 0) {
			if (Util.rand.Next(0, 10) <= 3) {
				ans += "1 POINT 0.4";
			}
			else {
				ans += "1.2 SLOPE 0.7 0.2";
			}
		}
		else {
			ans += "0 SLOPE 0.9 0.3";
		}

		return ans;
	}

	// Adds a statement to build another room to a specific side of this one
	public void addRoom(Room room, string direction, string matchType) {
		addStatement("addRoom " + direction + " " + room.name + " " + matchType);
	}

	// This method will return a string containing all statements needed to place windows
	// on the wall of this building
	public string getWindowsString() {
		if (noWindows) return "";
		List<string> windows = new List<string>();
		string ans = "";
		Direction[] sides = {Direction.NORTH, Direction.SOUTH, Direction.WEST, Direction.EAST};

		for(int i = 0; i < height; ++i) {
			foreach (Direction side in sides) {
				string w = getStoreyWindowsString(i,side);

				if (w != null) {
					windows.Add(w);
				}
			}
		}

		foreach(string w in windows) {
			ans += w + "\n";
		}

		return ans;
	}

	// This method will generate a windows bulk-add statement for a specific side
	// of the given layer of the room.
	public string getStoreyWindowsString(int storey, Direction side) {
		string ans = "windows ";
		int winSize = 1 + Util.rand.Next(0, 3);
		int winSpacing = Util.rand.Next(1, 4);
		WindowShape winStyle = WindowShape.SQUARE;
		if (height > 1 && storey == height-1 && Util.rand.Next(0, 9)  < 3) {
			winStyle = WindowShape.BALCONEY;
		}

		ans += side + " " + storey + " " + winSize + " " + winSpacing + " " + winStyle;

		return ans;
	}

}

// This class is used to procedurally generate buildings as scripts that can then be manually
// tweaked by a user and parsed by the houseBuilder to generate a GameObject.
public class ScriptGenerator {

	// These integers are the 4 parameters influencing the script creation process
	// The maximum height of the building, in storeys
	public int height = 6;
	// How many rooms should be use at mos to cover the height of the building (the name is misleading)
	public int maxStorey = 3;
	// The maximum footprint of the building at floor level
	public int maxWidth = 20;
	public int maxHeight = 20;
	
	// A handy shortcut to get a random number from the Util.rand RNG
	private int getRand(int min, int max) {
		return Util.rand.Next(min ,max);
	}
	
	// This method gives a standard room representing the door/main entrance of a building
	// We try to place it on each building that requires one
	private Room getDoorRoom() {
			Room door = new Room("Door_" + Room.roomCount, 3, 1);
			door.specifiedRoof = "roof 0 SLOPE 0.7 0.4 NORTH";
			door.addStatement("addWindow NORTH 0 1 DOOR");
			door.noDisplacement = true;
			door.setHeight(1);
			door.noWindows = true;
			door.noOnTop = true;

			return door;
	}
	
	// Unused method that should have been able to give us L-shaped room as a set of two connected rooms.
	private Room getL_SHAPE(List<Room> ans) {
		int mainBranchWidth = getRand(4, maxWidth/2);
		int mainBranchHeight = getRand(9, maxHeight/2);
		
		int secondBranchWidth = getRand(8, maxWidth-mainBranchWidth);
		int secondBranchHeight = getRand(4, maxHeight/2);
		
			Room mainBranch = new Room("Branch1_"+ Room.roomCount, mainBranchWidth, mainBranchHeight);
			mainBranch.addStatement("addRoom " + "WEST" + " " + "Branch2_"+ Room.roomCount + " " + "FIRST_MATCH");
				
			Room secondBranch = new Room("Branch2_"+ Room.roomCount, secondBranchWidth, secondBranchHeight);
		
		if (ans != null) {
			ans.Add(mainBranch);
			ans.Add(secondBranch);
		}
		
		return mainBranch;
	}
	
	// This method returns a rectangular room of a given size
	private Room getRECT(List<Room> ans, int maxW = 2, int maxL = 2, bool baseRoom = false) {
		if (maxW < 2) {
				maxW = 2;
			}

		if (maxL < 2) {
				maxL = 2;
			}

		int w = getRand(2, maxW);
		int l = getRand(2, maxL);
		
		// We try to get a baseRoom as big as possible, to allow for more rooms on top of it
		if (baseRoom) {
			w = getRand ((int)(2*maxW/3), maxW);
			l = getRand ((int)(2*maxL/3), maxL);
		}
		
		Room room = new Room("Room_" + Room.roomCount, w, l);
		
		if (ans != null) {
			ans.Add(room);
		}
		
		return room;
	}
	
	// Given a shape for the room at floor level, this method returns a list of all rooms that will
	// cosntruct the whole building in the end.
	public List<string> getScriptRooms(BaseShape baseShape) {
		List<Room> ans = new List<Room>();
		
		switch (baseShape) {
			case BaseShape.L_SHAPE:
				getL_SHAPE(ans);
				break;
			case BaseShape.RECT:
				getRECT(ans, maxWidth, maxHeight, true);
				break;
		}

			ans[0].addStatement("build " + ans[0].name + " 50 50");
			Room door = getDoorRoom();
			ans.Add (door);
			// The NORTH direction is always considered to be the front of the building, hence why
			// we ry to add a door to it.
			ans[0].addRoom(door, "NORTH", "RANDOM");
	
		return expandHeight(ans);
	}
	
	// This method is the one called from the exterior, and gives us a whole new script
	// from nothing more than the shape of the base room.
	public string getScript(BaseShape baseShape) {
		string ans = "";
		List<string> rooms = getScriptRooms(baseShape);

		foreach(string room in rooms) {
			ans += room + "\n";
		}

		return ans;
	}
	
	// Given a list of rooms at floor level, this method will grow them
	// and add other rooms on top of it so as to meet the requirements of the parameters.
	public List<string> expandHeight(List<Room> rooms) {
		List<string> ans = new List<string>();
		
		foreach(Room room in rooms) {
				if (room.noOnTop) {
					ans.Add (room.getScript());
					continue;
				}
				// Determine how many storeys will be generated and their respective heights
				Queue<int> storeys = new Queue<int>();
				int heightLeft = height;
				int storeyCount = 0;
				while (heightLeft > 0 && storeyCount < maxStorey) {
					int newStorey = getRand(1, heightLeft);
					storeys.Enqueue(newStorey);
					heightLeft -= newStorey;
					++storeyCount;
				}
			
				room.setHeight(storeys.Dequeue() + heightLeft);
				room.generateDisplacement();

				Queue<Room> toProcess = new Queue<Room>();			
				toProcess.Enqueue(room);
				Queue<Room> nextRound = new Queue<Room>();


				while (storeys.Count > 0) {
					int currHeight = storeys.Dequeue();
					if (currHeight == 0) {
						if (storeys.Count <= 0) break;

						currHeight = 1;
						storeys.Enqueue(storeys.Dequeue() - 1);
					}

					while (toProcess.Count > 0) {
						Room currRoom = toProcess.Dequeue();
						Room topRoom = null;

						// How many units are added/remove because of the displacement of the last storey 
						int unitDiff = 0;
						if (currRoom.getTopDispl() < 0) {
							unitDiff = (int)-Mathf.Ceil (Mathf.Abs(currRoom.getTopDispl()/10));
						}
						else {
							unitDiff = (int)-Mathf.Floor (Mathf.Abs(currRoom.getTopDispl()/10));
						}

						int effectiveWidth = Mathf.Min (currRoom.width, currRoom.width + unitDiff);
						int effectiveLength = Mathf.Min (currRoom.length, currRoom.length + unitDiff);

						topRoom = getRECT(currRoom.onTops, effectiveWidth-2, effectiveLength-2);

						if (topRoom == null) continue;

						topRoom.offsetX = -unitDiff;
						topRoom.offsetY = -unitDiff;

						if (effectiveWidth-topRoom.width >=2) {
							topRoom.offsetX += getRand(0, effectiveWidth-topRoom.width-2);
						}

						if (effectiveLength-topRoom.length >=2) {
							topRoom.offsetY += getRand(0, effectiveLength-topRoom.length-2);
						}

						topRoom.parent = currRoom;
						topRoom.setHeight(currHeight);
						topRoom.generateDisplacement();
						nextRound.Enqueue(topRoom);
					}

					toProcess = nextRound;
					nextRound = new Queue<Room>();
				}
				// Concatenate storeys to the base room
				ans.Add (room.getScript());
		}	
		return ans;
	}
}

}

