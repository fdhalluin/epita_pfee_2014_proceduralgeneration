-= buildingsGenerator =-

* TL;DR
	WASD+Mouse to move around the house (at fixed height).
	Escape to toggle the script editor on and off.
	There is a commented script at the end of the README,
	most of the cool stuff is described in it.

* Goal of this experiment
	To conceive and implement a generator capable of generating various buildings,
	from houses to skyscrapers.

* How was this achieved ?
	- Messy code (but surprisingly manageable), with a pending rewriting.
	- The rooms are first generated on a grid according to the commands input, then
	  converted to a single 3D mesh.
	- Colors are applied thanks to a texture.
	- Windows are created from a 2D connected shape defined as a list of points in a 2D
	  plane, and can easily be added (through C# code).
	- All the movement code is imported from standard Unity assets.
	
* Grammar for the script language
	
	Everything behind a # sign is ignored until the next line. This is useful to add comments.
	
	DECLARATION
	The first thing to do is to declare a room. If you want to name you
	room RoomName, put that name on a line, followed by a colon.
	You can delcare multiple rooms in a single script, and reference rooms
	described later in the script.
	
	RoomName:
	
	Once you declared a room, you must build it using the build command. It takes 3 parameters:
		- The name of your room
		- X and Z coordinates
	The default area is a 200 by 200 grid (measured in Unity unit).
	You can build multiple room, and you can also build the same room multiple times.
	NOTE: those rooms are place without checking for other rooms. Intersecting rooms will result
		  in undefined behaviour.
	
	build RoomName 32 16

	
	
	COMMANDS
	Each command only apply to the last declared room.
	
	The size command is the only command that is mandatory, as it indicates the width
	and length of your room. Width and length are measured in Unity unit.
	
	size 6 4
	
	
	The height command allows you to specify how many storey your room will span.
	Each storey is currently 2 unity unit tall.
	By default, a room is 1 storey tall.
	
	height 5
	
	
	The roof command specifies the roof properties. It takes 2 arguments:
		- The roof offset from the wall (think of it as the vertical distance betwen the
		  highest wall and the higest pioint of the roof)
		- The roof type (FLAT, POINT, LINEAR)
	If a room with a LINEAR roof is added to a room with a LINEAR roof, the roofs may connect
	if the two rooms are the same height.
	By default, a room has a FLAT roof.
	
	roof 1 POINT
	
	
	The windows command is used to put windows on a specific storey of a room. It takes 5 parameters:
		- The face of the room to be covered with windows (NORTH, SOUTH, WEST or EAST)
		- The targeted storey (0-indexed ; please make sure that the storey you target actually exist before compiling :) )
		- The size needed for a window to appear, in Unity unit.
		- The size between two consecutive window spaces, in Unity unit.
		- The type of window to use (SQUARE, CIRCLE, SEMICIRCULAR, DOOR)
		
	windows EAST 2 1 0 SQUARE
	
	
	The addRoom command is used to stick a room to the side of another one. It takes 3 parameters:
		- The side of the room to which the new room will be added (NORTH, SOUTH, WEST or EAST)
		- The name of the room to add (this is the name you gave to the room when you declared it)
		- The criteria used to place the room. It can be one of 4 options
			* RANDOM : will place the room differently each time the script is compiled, as long
					   as the two rooms are adjacent by at least one square.
			* COMPLETE_MATCH : will place the new room so that it is entirely touching the side
			* CENTER : will do its best to place the new room at the center of the side
			* FIRST_MATCH : will place the room as soon as it finds an available space
	If the room cannot be placed because of pre-existing structures, it will not be placed and you
	will NOT receive a warning.
	
	addRoom SOUTH treehouse CENTER
	
	
	The addTower command places a tower at one of the corners of the room. It takes 4 arguments:
		- The vertical component of the corner (NORTH or SOUTH)
		- The horizontal component of the corner (WEST or EAST)
		- The radius of the tower (The tower will be a (2*radius + 1)square centered on the corner)
		- The height of the tower.
	Towers always have a roof of the POINT type. This cannot currently be changed.
	Once again, please make sure to input the directions in the correct order (vertical, horizontal).
	NOTE: currently, it is impossible to add windows or rooms to a tower, making it mostly useless.
	
	addTower NORTH EAST 1 7
	
	
	The onTop command allows you to stack a room on another. It takes 3 arguments:
		- The name of the room to stack up (this is the name you gave to the room when you declared it)
		- A width-axis offset from the current room
		- A length-axis offset from the current room
	
	onTop finalTower 3 4
	
	
* Commented script example

build mainRoom 2 4 # this statement will create a room from
				   # the mainRoom model at coordinates X=2
				   # and Z=4

mainRoom: # starts the declaration of a room named mainRoom
size 15 8 # this room is 15 unit wide and 8 unit long
height 3 # it is 3 storey high, which is 6 unit

windows NORTH 2 9 1 CIRCLE # Circular windows are added to the north side of the room,
						   # on the second storey. Each window requires 9 units to be placed,
						   # and there must be 1 unit between each of these 9-units spaces.
						   
windows NORTH 0 3 0 DOOR # Door-like windows are added to the north side of the room,
						 # on the ground level. Each window requires 3 units to be placed,
						 # and no space is required between two window spaces.
						 
addRoom SOUTH nef CENTER # This adds a room named nef (described later) on the south side
						 # of the room named mainRoom, and tries to center it.

roof 3 LINEAR # The room named mainRoom has a 3-high roof of the LINEAR type

onTop nef 2 2 # This adds a room named nef on top of the room named mainRoom, at a
			  # 2 offset in both X and Z coordinates
			  
addTower NORTH EAST 2 5 # This adds a tower to the north-east corner of the room. The tower
						# will be a 2*2+1=5 square centered on the north-east corner, and will
						# span 5 storeys.

nef: # starts the declaration of a room named mainRoom
size 5 5 # this room is 5 unit wide and 5 unit long
height 4 # it is 3 storey high, which is 6 unit
roof 1 POINT # The room named mainRoom has a 3-high roof of the LINEAR type
windows EAST 1 1 0 DOOR  # Door-like windows are added to the east side of the room,
						 # on the first storey. Each window requires 1 unit to be placed,
						 # and no space is required between two window spaces.
	
	

		
		