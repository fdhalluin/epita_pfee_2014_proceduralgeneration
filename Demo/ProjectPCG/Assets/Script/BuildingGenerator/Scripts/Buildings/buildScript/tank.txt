# Design document, used as a model
# ?(a, b) will be replaced by a random integer between a and b (WHEN IMPLEMENTED)

build mainRoom 4 4 # build RoomModel XOffset YOffset

mainRoom:
size 16 12
height 3
displacement 0 0 0 -2 
onTop cannon 2 3
windows WEST 3 5 0 SQUARE
windows NORTH 0 2 2 CIRCLE
windows NORTH 1 4 3 SQUARE
windows SOUTH 0 2 2 CIRCLE
windows SOUTH 1 4 3 SQUARE

cannon:
size 5 6
height 1
displacement 0 -1
windows WEST 1 2 0 CANNON

