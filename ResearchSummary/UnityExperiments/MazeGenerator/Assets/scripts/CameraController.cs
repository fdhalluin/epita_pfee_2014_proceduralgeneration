﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject objectToFollow;
	private Vector3 offset;

	// Use this for initialization
	void Start () {
		offset = new Vector3(0, 10.5f, 0);
	}
	
	void LateUpdate () {
		transform.position = objectToFollow.transform.position + offset;
	}
}
