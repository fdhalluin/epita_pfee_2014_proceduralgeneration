using UnityEngine;
using System.Collections.Generic;
using graph;

public class CityNode : ANode {
	public int mID = -1;
	public bool mIsConnect = false;
	public Point mPoint;

	public CityNode(Vector3 iPosition) {
		mAdjacentNodes = new Dictionary<ANode, int>();
		mPosition = iPosition;
	}
	
	public CityNode(float iX, float iY, float iZ) {
		mAdjacentNodes = new Dictionary<ANode, int>();
		mPosition = new Vector3(iX, iY, iZ);
	}
}