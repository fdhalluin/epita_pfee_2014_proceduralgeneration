using System.Collections.Generic;
using System;
using UnityEngine;

namespace HouseGen {

// DEFINITION: defines the current room (size, height, room)
// ROOM: defines the rooms sharing one side with the current room (addRoom, addTower)
// WINDOWS: defines the windows of the current room (windows)
// TOP: defines room put on the top of this one (onTop)

class Parser {

public enum CommandType {DEFINITION=0, ROOM=1, WINDOWS=2, TOP=3};

public static string roomsToBuild = "#TOBUILD#";

public Parser() {

}

public Dictionary<string, RoomDescription> parse(string input) {

	string[] lines = input.Split("\n".ToCharArray());

	RoomDescription currentRoom = new RoomDescription(roomsToBuild);

	Dictionary<string, RoomDescription> building = new Dictionary<string, RoomDescription>();

	building.Add(currentRoom.name, currentRoom);

	foreach (string rawLine in lines) {
		string line = rawLine.Trim();
		if (line.IndexOf('#') > 0) {
			line = line.Substring(0, line.IndexOf('#'));
			line = line.Trim();
		}
			
		if (line.Length < 2) continue;
		
		string[] tokens = line.Split(' ');
		
		int index = -1;
		
		switch (tokens[0]) {
			case "size":
			case "height":
			case "roof":
			case "displacement":
				index = currentRoom.commandsCount[(int)CommandType.DEFINITION];
				currentRoom.properties[tokens[0]] = line;
				++currentRoom.commandsCount[(int)CommandType.DEFINITION];
				break;
			
			case "addRoom":
			case "addTower":
				index = currentRoom.commandsCount[(int)CommandType.ROOM];
				currentRoom.properties[CommandType.ROOM + "_" + index] = line;
				++currentRoom.commandsCount[(int)CommandType.ROOM];
				break;
			
			case "windows":
				index = currentRoom.commandsCount[(int)CommandType.WINDOWS];
				currentRoom.properties[CommandType.WINDOWS + "_" + index] = line;
				++currentRoom.commandsCount[(int)CommandType.WINDOWS];
				break;
				
			case "onTop":
				index = currentRoom.commandsCount[(int)CommandType.TOP];
				currentRoom.properties[CommandType.TOP + "_" + index] = line;
				++currentRoom.commandsCount[(int)CommandType.TOP];
				break;
				
			case "build":
				building[roomsToBuild].properties.Add(building[roomsToBuild].properties.Keys.Count + "", line);
				break;
			
			default:
				if (tokens[0][tokens[0].Length-1] == ':') {
					currentRoom = new RoomDescription(tokens[0].Substring(0, tokens[0].Length-1));
					building.Add(currentRoom.name, currentRoom);
				}
				break;
		}


	}

	foreach (string key in building.Keys) {
		if (key.Equals(roomsToBuild)) continue;
		
		if (!building[key].properties.ContainsKey("displacement")) {
			string displ = "displacement ";
			int height = Int32.Parse(building[key].properties["height"].Split(' ')[1]);
			for (int i = 0; i <= height; ++i) {
				displ += "0 ";
			}
			displ = displ.Substring(0, displ.Length-1);
			building[key].properties["displacement"] = displ;
			
			Debug.Log("GENERATED " + displ);
		}
		
	}
	
	// TODO: detect and remove infinitely-recursive rooms
	
	return building;
}

}

class RoomDescription {

	public Dictionary<string, string> properties;
	public string name;
	public int[] commandsCount = new int[Enum.GetNames(typeof(Parser.CommandType)).Length];
	public List<Rectangle> rooms;
	public List<Rectangle> roomsBuffer;
	
	public RoomDescription(string n) {
		name = n;
		properties = new Dictionary<string, string>();
		rooms = new List<Rectangle>();
		roomsBuffer = new List<Rectangle>();
		
		if (!n.Equals(Parser.roomsToBuild)) {
			properties["roof"] = "roof 0 FLAT";
			properties["height"] = "height 1";
		}
	}
	
	public void unloadRoomBuffer() {
		rooms.AddRange(roomsBuffer);
		roomsBuffer.Clear();
	}
	
}

}
