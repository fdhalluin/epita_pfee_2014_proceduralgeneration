using UnityEngine;
using System;
using System.Collections.Generic;

namespace HouseGen {

// An omnipresent enuim used to represent a direction
public enum Direction {NONE=0, NORTH=1, SOUTH=2, WEST=3, EAST=4};

// The Story class represent one layer of the house. It contains at least
// a layer of each rooms that reaches its height level.
public class Story {
// This is a plan representing all walls and their direction, used when placing windows
int[,] plan;
// This is a plan representing all space occupied by a room, a pillar or a prop
int[,] roomPlan;
// The name of the storey (used only for debugging purposes)
string name;
// The dimensions of the storey
int width, height;
// A list of all Rectangle objects representing the rooms belonging to this level
List<Rectangle> rooms;
// References to the next and previous layer, set to null when there is none
Story previous = null;
Story next = null;

public Story(int w, int h, string n) {
	width = w;
	height = h;
	name = n;
	plan = new int[w, h];
	roomPlan = new int[w, h];
	rooms = new List<Rectangle>();
}

public Story(Story p, string n) : this(p.width, p.height, n) {
previous = p;
p.next = this;
width = p.width;
height = p.height;
}

// Lots of getter method
public int[,] getPlan() {return plan;}
public int[,] getRoomPlan() {return roomPlan;}
public List<Rectangle> getRooms() {return rooms;}

public int getHeight() {return height;}
public int getWidth() {return width;}

public Story getPrevious() {return previous;}
public Story getNext() {return next;}

public string getName(){return name;}

// This is a debug method used to print one of the plans of the layer to the console's output
public void print(bool rPlan = false) {
	Debug.Log("--" + name + "--");
	string line = "";
	for (int j = 0; j < height; ++j) {
		for(int i = 0; i < width; ++i) {	
			if (!rPlan) {
				line += (plan[i, j]==0?"0":""+plan[i, j]);
			}
			else {
				line += (roomPlan[i, j]==0?"0":""+roomPlan[i, j]);
			}
		}
		line += "\n";
	}
	Debug.Log(line + "\n");
}

// This method iterates on all the rooms of this layer to print them on its plan
public void writeRooms() {
	foreach (Rectangle room in rooms) {
		writeRoom(room);		
	}
}

// This method writes the walls of a room on the layer's plan
public void writeRoom(Rectangle room) {
	for(int i = 0; i < room.w-1; ++i) {
				plan[i + room.x, room.y] = (int)Direction.NORTH;
				plan[room.x + room.w - i-1, room.y+room.h-1] = (int)Direction.SOUTH;
				}
			for(int j = 1; j < room.h; ++j) {
				plan[room.x, room.y + j] = (int)Direction.WEST;
				plan[room.x + room.w -1, room.y+room.h-1 - j] = (int)Direction.EAST;
				}
}

// This method writes the whole room on the layer's roomPlan
public void writeRoomPlan(Rectangle room) {
	for (int i = 0; i < room.w; ++i){
		for (int j = 0; j < room.h; ++j) {
			roomPlan[room.x + i, room.y + j] = room.roomId;
		}
	}
}

// This method adds a room to the layer without checking for collisions with existing rooms
public Rectangle addRoom(Rectangle room) {
	for (int i = 0; i < 2; ++i) {
		int valX = room.x + i*(room.w-1);
		int valY = room.y + i*(room.h-1);
		
		if (valX < 0 || valX >= width) {
			Console.WriteLine("Bad Width");
			return null;
		}
		if (valY < 0 || valY >= height) {
			Console.WriteLine("Bad Height");
			return null;
		}
		
	}

	rooms.Add(room);
	room.roomId = rooms.Count;
	room.floor = this;
	writeRoom(room);
	writeRoomPlan(room);
	
	return room;
}

// This method will check all the rooms from the previous layer to check if they need to be
// grown to their maximum height, and create the next layer of theses room on the current storey
public void growRoomsFromPrevious() {
	foreach (Rectangle room in previous.getRooms()) {
		if (room.roomHeight > 0 && room.next == null) {
			Rectangle newRoom = new Rectangle(room);
			
			if (room.parentSide != Direction.NONE && room.parent.roomHeight > 0) {
				newRoom.parent = rooms[room.parent.next.roomId-1];
				if (room.parentSide != null) {
					newRoom.parentSide = room.parentSide;
					newRoom.parent.getSide(Util.reverse(newRoom.parentSide)).Add(newRoom);		
				}
			}
			
			addRoom(newRoom);
			
		}
	}
}

// This is a utility method to automatically generate the next storey and grow the rooms of the
// current storey to the next one.
public Story generateNextStory(string n) {
	Story nextStory = new Story(this, n);
	nextStory.growRoomsFromPrevious();
	return nextStory;
}

// A recursive getter to get a storey at a specific height
public Story getStoryAtHeight(int height) {
		if (height <= 0) {
			return this;
		}
		if (next == null) {
			return generateNextStory("").getStoryAtHeight(height-1);
		}
		
		return next.getStoryAtHeight(height-1);		
	}

}

}
