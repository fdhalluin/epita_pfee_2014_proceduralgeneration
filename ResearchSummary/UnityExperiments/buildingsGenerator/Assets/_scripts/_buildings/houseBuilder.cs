﻿using UnityEngine;
using System.Collections.Generic;
using HouseGen;
using System;


public partial class houseBuilder : MonoBehaviour {

	public Material baseMaterial;

	List<Vector3> vertices;
	List<int> triangles;
	List<Vector2> UVValues;
	Dictionary<string, Vector3> points;

	int totalColors = 5;
	int wallHeight = Rectangle.wallHeight;

	string nwd = "nwd";
	string ned = "ned";
	string sed = "sed";
	string swd = "swd";
	string nwu = "nwu";
	string neu = "neu";
	string seu = "seu";
	string swu = "swu";
	
	string top = "top";
	string wtop = "wtop";
	string etop = "etop";
	string ntop = "ntop";
	string stop = "stop";
	
	// Use this for initialization
	void Start () {
		vertices = new List<Vector3> ();
		triangles = new List<int> ();
		UVValues = new List<Vector2> ();
		points = new Dictionary<string, Vector3> ();
		initFromScript(System.IO.File.ReadAllText("Assets/_scripts/_buildings/buildScript/" + "church" + ".txt"));
		//init ();
	}

	void init2() {
		Story floor = new HouseGen.Story(40, 20, "Floor");
		
		Rectangle r = new Rectangle(12, 6, 6, 8);
		floor.addRoom(r);
		Rectangle r1 = r.addRectangleToSide(Direction.NORTH, 5, 3, 4);
		Rectangle r2 = r1.addRectangleToSide(Direction.EAST, 5, 1, 4);

		//		Rectangle r1 = r.addRectangleToSide(Direction.WEST, 4, 1, 3);
		//		Rectangle r1P5 = r.addRectangleToSide(Direction.NORTH, 12, 1, 3);
		//		Rectangle r2 = r.addRectangleToSide(Direction.EAST, 8, 1, 9, true);
		
		GameObject house = buildHouse (floor);
		house.transform.Translate (-20f, 0, 0);
	}

	void init() {
		Story floor = new HouseGen.Story(40, 20, "Floor");
		
		Rectangle r = new Rectangle(8, 4, 8, 12, 1);
		floor.addRoom(r);
		//Rectangle r1 = r.addRectangleToSide(Direction.EAST, 4, 7, 10);
		r.addTower(Direction.NORTH, Direction.EAST, 2);
		r.addTower(Direction.NORTH, Direction.WEST, 2);
		r.addTower(Direction.SOUTH, Direction.EAST, 2);
		r.addTower(Direction.SOUTH, Direction.WEST, 2);
		
//		Rectangle r1 = r.addRectangleToSide(Direction.WEST, 4, 1, 3);
//		Rectangle r1P5 = r.addRectangleToSide(Direction.NORTH, 12, 1, 3);
//		Rectangle r2 = r.addRectangleToSide(Direction.EAST, 8, 1, 9, true);
		
		floor.generateNextStory("Story 1").generateNextStory("Story 2").generateNextStory("Story 3");		

		// r.addWindows(Direction.WEST, 2, 2, WindowShape.SEMICIRCULAR);
		// r.next.addWindows(Direction.WEST, 2, 2);

		floor.print();
		GameObject house = buildHouse (floor);
	}

	void initNotreDame() {
		Story floor = new Story(40, 40, "Floor #0");
		
		Rectangle r = new Rectangle(15, 15, 15, 8, 2);
		floor.addRoom(r);
		Rectangle nef = r.addRectangleToSide(Direction.SOUTH, 7, 7, 14, Rectangle.PositionType.CENTER);
		r.addTower(Direction.NORTH, Direction.WEST, 2);
		r.addTower(Direction.NORTH, Direction.EAST, 2);
		
		floor.generateNextStory("Floor #1")
			 .generateNextStory("Floor #2")
			 .generateNextStory("Floor #3")
			 .generateNextStory("Floor #4");
		
		r.getRoomAtStory(2)
		 .addWindows(Direction.NORTH, 9, 1, WindowShape.CIRCLE);
		r.getRoomAtStory(0)
		 .addWindows(Direction.NORTH, 3, 0, WindowShape.DOOR);
		
		r.getRoomAtStory(2)
		 .addWindows(Direction.EAST, 4, 0, WindowShape.CIRCLE);
		r.getRoomAtStory(2)
		 .addWindows(Direction.WEST, 4, 0, WindowShape.CIRCLE);
		 
		nef.getRoomAtStory(1)
		 .addWindows(Direction.EAST, 3, 1, WindowShape.SEMICIRCULAR);
		nef.getRoomAtStory(1)
		 .addWindows(Direction.WEST, 3, 1, WindowShape.SEMICIRCULAR);
		nef.getRoomAtStory(1)
		 .addWindows(Direction.SOUTH, 5, 0, WindowShape.CIRCLE);
		 
		foreach(Direction side in Enum.GetValues(typeof(Direction))) {
			if (side == Direction.NONE) continue;
			r.getTower(Direction.NORTH, Direction.WEST)
			 .getRoomAtStory(4)
			 .addWindows(side, 3, 0, WindowShape.SEMICIRCULAR);
			r.getTower(Direction.NORTH, Direction.EAST)
			 .getRoomAtStory(4)
			 .addWindows(side, 3, 0, WindowShape.SEMICIRCULAR);		
		}			
		
		foreach(Direction side in Enum.GetValues(typeof(Direction))) {
			if (side == Direction.NONE || side == Direction.EAST || side == Direction.SOUTH) continue;
			
			// WEST TOWER
			r.getTower(Direction.NORTH, Direction.WEST)
			 .getRoomAtStory(3)
			 .addWindows(side, 1, 1, WindowShape.SEMICIRCULAR);
			r.getTower(Direction.NORTH, Direction.WEST)
			 .getRoomAtStory(1)
			 .addWindows(side, 1, 0, WindowShape.SQUARE);
		 
			// EAST TOWER
			r.getTower(Direction.NORTH, Direction.EAST)
			 .getRoomAtStory(3)
			 .addWindows( (side == Direction.NORTH?side:Util.reverse(side)), 1, 1, WindowShape.SEMICIRCULAR);
			r.getTower(Direction.NORTH, Direction.EAST)
			 .getRoomAtStory(1)
			 .addWindows( (side == Direction.NORTH?side:Util.reverse(side)), 1, 0, WindowShape.SQUARE);
				
		}

		
		floor.print();
		
		buildHouse(floor);		
	}

	void addTriangle(List<int> l, int p1, int p2, int p3) {
		l.Add (p1);
		l.Add (p2);
		l.Add (p3);
	}

	void addTriangle(string pointName1, string pointName2, string pointName3, int color) {
		// We add + 0.01f to avoid a strange behaviour with the UVMapping
		int point1 = vertices.Count;
		vertices.Add (points[pointName1]);
		UVValues.Add (new Vector2((float)color / totalColors + 0.01f, 0.5f));
		int point2 = vertices.Count;
		vertices.Add (points[pointName2]);
		UVValues.Add (new Vector2((float)color / totalColors + 0.01f, 0.5f));
		int point3 = vertices.Count;
		vertices.Add (points[pointName3]);
		UVValues.Add (new Vector2((float)color / totalColors + 0.01f, 0.5f));

		triangles.Add (point1);
		triangles.Add (point2);
		triangles.Add (point3);
	}

	void addQuadrilateral(string pointName1, string pointName2, string pointName3, string pointName4, int color) {
		addTriangle (pointName2, pointName1, pointName4, color);
		addTriangle (pointName2, pointName4, pointName3, color);
	}

	void addQuadrilateral(List<int> l, int nw, int ne, int se, int sw) {
		l.Add (ne);
		l.Add (nw);
		l.Add (sw);

		l.Add (ne);
		l.Add (sw);
		l.Add (se);
	}

	void colorPoint(List<Vector2> uv, int color) {
		uv.Add (new Vector2(16*color + 8, 8));
	}

	// The level char tells us if we are modifying the down or up points of the room
	void expandPoints(char level, float d, Dictionary<string, Vector3> points) {
		string nw = "nw";
		string ne = "ne";
		string se = "se";
		string sw = "sw";
		points[nw+level] = new Vector3(points[nw+level].x-d, points[nw+level].y, points[nw+level].z-d);
		points[ne+level] = new Vector3(points[ne+level].x+d, points[ne+level].y, points[ne+level].z-d);
		points[se+level] = new Vector3(points[se+level].x+d, points[se+level].y, points[se+level].z+d);
		points[sw+level] = new Vector3(points[sw+level].x-d, points[sw+level].y, points[sw+level].z+d);
	}
	
	void buildRoom(Rectangle room, int story = 0) {
		points.Clear();

		// Naming convention:
		// - First two letters are the cardinal direction
		// - Last letter indicate if the point is on the floor (Down)
		//   or in the air (Up)
		float storyOffset = story*wallHeight;
		points.Add(nwd, new Vector3(room.x, storyOffset, room.y));
		points.Add(ned, new Vector3(room.x+room.w, storyOffset, room.y));
		points.Add(sed, new Vector3(room.x+room.w, storyOffset, room.y+room.h));
		points.Add(swd, new Vector3(room.x, storyOffset, room.y+room.h));
		points.Add(nwu, new Vector3(room.x, storyOffset + wallHeight, room.y));
		points.Add(neu, new Vector3(room.x+room.w, storyOffset + wallHeight, room.y));
		points.Add(seu, new Vector3(room.x+room.w, storyOffset + wallHeight, room.y+room.h));
		points.Add(swu, new Vector3(room.x, storyOffset + wallHeight, room.y+room.h));
		expandPoints('d', room.baseDispl, points);
		expandPoints('u', room.topDispl, points);
		
		// Then we describe the faces, with two triangles each.
		addQuadrilateral(neu, nwu, nwd, ned, 0);
		addQuadrilateral(seu, neu, ned, sed, 0);
		addQuadrilateral(swu, seu, sed, swd, 0);
		addQuadrilateral(nwu, swu, swd, nwd, 0);

		if (room.roomHeight == 0) {
			buildRoof(room, storyOffset);
		}
		else {
			buildRoof(room, storyOffset, true);
		}

		buildWindows(room, story);
		
	}
	
	void buildRoof(Rectangle room, float story = 0, bool forceFlat = false) {
		// Roof
		// bool flatTop = (Util.rand.Next(100) < 30);
		// bool pointTop = !flatTop && (Util.rand.Next(100) < 20);
		// bool linearTop = !flatTop && !pointTop;
		
		bool flatTop = room.roofType == Rectangle.RoofType.FLAT;
		bool pointTop = room.roofType == Rectangle.RoofType.POINT;
		bool linearTop = room.roofType == Rectangle.RoofType.LINEAR;

		if (flatTop || forceFlat) {
			addQuadrilateral(nwu, neu, seu, swu, 2);			
		}

		if (pointTop && !forceFlat) {
			
			points.Add(top, new Vector3(room.x+(float)room.w/2, story + wallHeight + room.roofOffset, room.y+(float)room.h/2));
			addTriangle(neu, nwu, top, 2);
			addTriangle(neu, top, seu, 2);
			addTriangle(seu, top, swu, 2);
			addTriangle(top, nwu, swu, 2);
		}

		if (linearTop && !forceFlat) {
			if (room.w > room.h) {
				float parentHalfOffset = 0;
				if (room.parent != null && room.parent.roomHeight == 0) {
					parentHalfOffset = room.parent.x + (float)room.parent.w/2 - room.x;
					if (room.parentSide == Direction.EAST) parentHalfOffset -= room.w;
					if (room.y < room.parent.y || room.y+room.h > room.parent.y + room.parent.h) parentHalfOffset = 0;
				}

				points.Add(wtop, new Vector3(room.x + (room.parentSide == Direction.WEST?parentHalfOffset:0), story + wallHeight + room.roofOffset, room.y+(float)room.h/2));
				points.Add(etop, new Vector3(room.x + room.w + (room.parentSide == Direction.EAST?parentHalfOffset:0), story + wallHeight + room.roofOffset, room.y+(float)room.h/2));

				addTriangle(wtop, nwu, swu, 1);
				addTriangle(etop, seu, neu, 1);

				addQuadrilateral(etop, wtop, nwu, neu, 2);
				addQuadrilateral(wtop, etop, seu, swu, 2);
				
			}
			else {
				float parentHalfOffset = 0;
				if (room.parent != null && room.parent.roomHeight == 0) {
					parentHalfOffset = room.parent.y + (float)room.parent.h/2 - room.y;
					if (room.parentSide == Direction.SOUTH) parentHalfOffset -= room.h;
					if (room.x < room.parent.x || room.x+room.w > room.parent.x + room.parent.w) parentHalfOffset = 0;
				}

				points.Add(ntop, new Vector3(room.x+(float)room.w/2, story + wallHeight + room.roofOffset, room.y + (room.parentSide == Direction.NORTH?parentHalfOffset:0)));
				points.Add(stop, new Vector3(room.x+(float)room.w/2, story + wallHeight + room.roofOffset, room.y+room.h + (room.parentSide == Direction.SOUTH?parentHalfOffset:0)));


				addTriangle(ntop, neu, nwu, 1);
				addTriangle(stop, swu, seu, 1);

				addQuadrilateral(ntop, stop, swu, nwu, 2);
				addQuadrilateral(stop, ntop, neu, seu, 2);
			}
		}
	
	}
	
	Vector3 getNewCoordinate(Vector3 point, Vector4 plane, Axis axis) {
		if (plane[(int)axis] == 0) return point;
		Vector3 ans = new Vector3(point.x, point.y, point.z);
		
		switch (axis) {
			case Axis.X:
				ans.x = -(point.y*plane.y + point.z*plane.z + plane.w )/plane.x;
				break;
			case Axis.Y:
				ans.y = -(point.x*plane.x + point.z*plane.z + plane.w )/plane.y;
				break;
			case Axis.Z:
				ans.z = -(point.y*plane.y + point.x*plane.x + plane.w )/plane.z;
				break;
		}
		
		// Debug.Log(point + " -> " + ans);
		
		return ans;		
	}
	
	Vector4 computePlaneFromSide(Rectangle room, Direction side, int story = 0) {
		Vector3[] p = new Vector3[3];
		float storyOffset = story*wallHeight;
		Dictionary<string, Vector3> roomPoints = new Dictionary<string, Vector3>();
		roomPoints.Add(nwd, new Vector3(room.x, storyOffset, room.y));
		roomPoints.Add(ned, new Vector3(room.x+room.w, storyOffset, room.y));
		roomPoints.Add(sed, new Vector3(room.x+room.w, storyOffset, room.y+room.h));
		roomPoints.Add(swd, new Vector3(room.x, storyOffset, room.y+room.h));
		roomPoints.Add(nwu, new Vector3(room.x, storyOffset + wallHeight, room.y));
		roomPoints.Add(neu, new Vector3(room.x+room.w, storyOffset + wallHeight, room.y));
		roomPoints.Add(seu, new Vector3(room.x+room.w, storyOffset + wallHeight, room.y+room.h));
		roomPoints.Add(swu, new Vector3(room.x, storyOffset + wallHeight, room.y+room.h));
		expandPoints('d', room.baseDispl, roomPoints);
		expandPoints('u', room.topDispl, roomPoints);
		
		switch (side) {
			case Direction.NORTH:
				p[0] = roomPoints[neu];
				p[1] = roomPoints[nwu];
				p[2] = roomPoints[nwd];
				break;
			case Direction.SOUTH:
				p[0] = roomPoints[swu];
				p[1] = roomPoints[seu];
				p[2] = roomPoints[sed];
				break;
			case Direction.WEST:
				p[0] = roomPoints[nwu];
				p[1] = roomPoints[swu];
				p[2] = roomPoints[swd];
				break;
			case Direction.EAST:
				p[0] = roomPoints[seu];
				p[1] = roomPoints[neu];
				p[2] = roomPoints[ned];
				break;
		}
		
		// Debug.Log("Plane " + Util.computePlane(p[0], p[1], p[2]));
		
		return Util.computePlane(p[0], p[1], p[2]);
	}
	
	void buildWindows(Rectangle room, int story = 0) {
		points.Clear();
		foreach(Direction side in Enum.GetValues(typeof(Direction))) {
			if (side == Direction.NONE) continue;
			float angle = Util.computeAngle(new Vector2(room.topDispl, wallHeight), new Vector2(room.baseDispl, 0));
			if (angle != 0)angle += (float) (Math.PI/2);
			if (side == Direction.NORTH ||side == Direction.WEST) angle *= -1;
			Vector4 plane = computePlaneFromSide(room, side, story);
			
			foreach (Vector3 center in room.getWindows(side)) {
				// By default, all generated windows are facing WEST
				Shape windowShape = ShapeProvider.getShape(room.getWindowShape(side));
									
				if (side == Direction.EAST || side == Direction.NORTH) {
					windowShape.points.Reverse();
				}
				
				float size = center.y; // This is a dirty payload ...
				
				Vector3 newCenter = new Vector3(center.x, 0, center.z);
				
				if (side == Direction.NORTH || side == Direction.SOUTH) {
					newCenter.y += (wallHeight*(windowShape.center.y+story)) - windowShape.gravityCenter.y;
					newCenter = getNewCoordinate(newCenter, plane, Axis.Z);
					newCenter.x += (windowShape.center.x*size) - windowShape.gravityCenter.x;
					newCenter.z += windowShape.winDepth*(((int)side)%2 == 0?1:-1);
				}
				else {
					newCenter.y += (wallHeight*(windowShape.center.y+story)) - windowShape.gravityCenter.y;
					newCenter = getNewCoordinate(newCenter, plane, Axis.X);
					newCenter.z += (windowShape.center.x*size) - windowShape.gravityCenter.x;
					newCenter.x += windowShape.winDepth*(((int)side)%2 == 0?1:-1);
				}
				
				
				
				buildWindowAroundCenter(newCenter, windowShape, side, angle, plane);

				if (side == Direction.EAST || side == Direction.NORTH) {
					windowShape.points.Reverse();
				}
				
			}
		}
	}
	
	void rotatePoints(Vector3 center, Axis axis, double angle) {
		Dictionary<string, Vector3> newPoints = new Dictionary<string, Vector3>();
		
		foreach(string key in points.Keys) {
			newPoints[key] = Util.rotatePoint(points[key], center, angle, axis);
		}
		
		points.Clear();
		foreach(string key in newPoints.Keys) points[key] = newPoints[key];
	}
	
	void markPoint(Vector3 p, string id) {
		points.Add(id+1, p + new Vector3(0, 0, 0));
		points.Add(id+2, p + new Vector3(0.1f, 1, 0));
		points.Add(id+3, p + new Vector3(-0.1f, 1, 0));
		addTriangle(id+1, id+2, id+3, 2);
		addTriangle(id+2, id+1, id+3, 2);
	}
	
	// The direction indicates where is the outside from the window's point of view
	void buildWindowAroundCenter(Vector3 windowPos, Shape shape, Direction side, float angle, Vector4 plane) {
		points.Clear();
		string inr = "InnerRing";
		string our = "OuterRing";
		string bor = "BackwardOuterRing";
		string bir = "BackwardInnerRing";
		float depth = shape.winDepth*(side == Direction.EAST || side == Direction.SOUTH?-1:1);
		float darkPos = 0.8f;
		Axis axis = (side == Direction.NORTH || side == Direction.SOUTH)?Axis.X:Axis.Z;
		
		// Compute the center of all the points of the shape
		Vector3 windowGravityCenter = new Vector3(0, 0, 0);
		for (int i = 0; i < shape.points.Count; ++i) {
			Vector3 point = new Vector3(windowPos.x, windowPos.y + shape.points[i].y, windowPos.z + shape.points[i].x);
			if (side == Direction.NORTH || side == Direction.SOUTH) {
				point = new Vector3(windowPos.x + shape.points[i].x, windowPos.y + shape.points[i].y, windowPos.z);
			}
			
			windowGravityCenter += point;
		}
		windowGravityCenter /= shape.points.Count;
		
		// Generate all the rings
		for (int i = 0; i < shape.points.Count; ++i) {
			Vector3 point = new Vector3(windowPos.x, windowPos.y + shape.points[i].y, windowPos.z + shape.points[i].x);
			Vector3 outerPoint = Util.getPointByPercentage(point, windowGravityCenter, -0.2f);
			Vector3 backwardOuterPoint = new Vector3(outerPoint.x + depth, outerPoint.y, outerPoint.z);
			Vector3 backwardInnerPoint = new Vector3(point.x + depth*darkPos, point.y, point.z);
			
			if (side == Direction.NORTH || side == Direction.SOUTH) {
				point = new Vector3(windowPos.x + shape.points[i].x, windowPos.y + shape.points[i].y, windowPos.z);
				outerPoint = Util.getPointByPercentage(point, windowGravityCenter, -0.2f);
				backwardOuterPoint = new Vector3(outerPoint.x, outerPoint.y, outerPoint.z + depth);
				backwardInnerPoint = new Vector3(point.x, point.y, point.z + depth*darkPos);
			}
			
			points.Add(inr + i, point);
			points.Add(our + i, outerPoint);
			points.Add(bor + i, backwardOuterPoint);
			points.Add(bir + i, backwardInnerPoint);
		}
		
		// Place all the faces from the computed rings
		string cdk = "cdk";
		Vector3 centerDark = new Vector3(windowGravityCenter.x + depth*darkPos, windowGravityCenter.y, windowGravityCenter.z);
		if (side == Direction.NORTH || side == Direction.SOUTH) {
			centerDark = new Vector3(windowGravityCenter.x, windowGravityCenter.y, windowGravityCenter.z + depth*darkPos);
		}
		points.Add(cdk, centerDark);
		
		Vector3 rotCen = new Vector3(windowGravityCenter.x+depth, windowGravityCenter.y, windowGravityCenter.z+depth);
		if (side == Direction.NORTH || side == Direction.SOUTH) {
			rotCen = new Vector3(windowGravityCenter.x, windowGravityCenter.y, windowGravityCenter.z+depth);
		}
		rotCen = getNewCoordinate(rotCen, plane, Axis.Y); // FUCK YEAH this is the solution.
		
		rotatePoints(rotCen, axis, angle);
		
		for (int i = 0; i < shape.points.Count; ++i) {
			addQuadrilateral(our+i, our+(i+1)%shape.points.Count, inr+(i+1)%shape.points.Count, inr+i, 1);
			addQuadrilateral(bor+i, bor+(i+1)%shape.points.Count, our+(i+1)%shape.points.Count, our+i, 1);
			addQuadrilateral(inr+i, inr+(i+1)%shape.points.Count, bir+(i+1)%shape.points.Count, bir+i, 1);
			addTriangle(bir+(i+1)%shape.points.Count, bir+i, cdk, 4);
		}
		
		// markPoint(windowPos, "o");
		// markPoint(rotCen, "r");
		
		foreach(Prop prop in shape.props) {
			buildProp(windowPos, prop, side);
		}
		
	}
	
	void buildProp(Vector3 windowPos, Prop prop, Direction side) {
		points.Clear();
		Vector3 center = prop.center;
		
		if (side == Direction.WEST || side == Direction.EAST) {
			center = new Vector3(-prop.center.z, prop.center.y, prop.center.x);
		}
		if (side == Direction.WEST || side == Direction.SOUTH) {
			center = new Vector3(center.x, center.y, center.z);
		}
		
		Vector3 propGravityCenter = new Vector3(0, 0, 0);
		for (int i = 0; i < prop.points.Count; ++i) {
			Vector3 point = prop.points[i];
			
			if (side == Direction.WEST || side == Direction.EAST) {
				point = new Vector3(-point.z, point.y, point.x);
			}
			if (side == Direction.WEST || side == Direction.SOUTH) {
				point = new Vector3(-point.x, point.y, -point.z);
			}
			propGravityCenter+= point;
		}
		propGravityCenter /= prop.points.Count;
		
		for (int i = 0; i < prop.points.Count; ++i) {
			Vector3 point = prop.points[i];
			
			if (side == Direction.WEST || side == Direction.EAST) {
				point = new Vector3(-point.z, point.y, point.x);
			}
			if (side == Direction.WEST || side == Direction.SOUTH) {
				point = new Vector3(-point.x, point.y, -point.z);
			}
			
			
			point -= propGravityCenter;
			point += windowPos;
			point += center;
			
			points.Add(""+i, point);
		}
		
		for (int i = 0; i < prop.faces.Count; ++i) {
			List<int> face = prop.faces[i];
			if (face.Count == 3) addTriangle(""+face[0], ""+face[1], ""+face[2], 1);
			if (face.Count == 4) addQuadrilateral(""+face[0], ""+face[1], ""+face[2], ""+face[3], 1);
		}
		
		
	}
	
	GameObject buildHouse(Story story) {
		GameObject house = new GameObject();
		Story currStory = story;
		int storyLevel = 0;
		house.AddComponent<MeshFilter> ();
		house.AddComponent<MeshRenderer> ();
		house.AddComponent<MeshCollider> ();

		Mesh mesh = house.GetComponent<MeshFilter> ().mesh;

		points.Clear();
		vertices.Clear();
		triangles.Clear();
		UVValues.Clear();

		while (currStory != null) {
			foreach (Rectangle room in currStory.getRooms()) {
				buildRoom(room, storyLevel);
			}
			currStory = currStory.getNext();
			++storyLevel;
		}

		while (vertices.Count > UVValues.Count) {
			UVValues.Add(new Vector2(0, 0));
		}

		mesh.vertices = vertices.ToArray ();
		mesh.triangles = triangles.ToArray ();
		mesh.uv = UVValues.ToArray ();
		mesh.Optimize ();
		mesh.RecalculateNormals ();
		house.gameObject.renderer.material = baseMaterial;


		return house;
	}
	
}
