using System;
using System.Collections.Generic;
using UnityEngine;

namespace LotGeneration {

public class Vertex {
	public float x;
	public float y;
	public float height;
	public int id;
	public int lbid;
	
	public List<HalfEdge> originOf = null;
	public List<int> adjacent = null;

	public Vertex(float xCoord, float yCoord, float h = 0) {
		x = xCoord;
		y = yCoord;
		originOf = new List<HalfEdge>();
		adjacent = new List<int>();
		height = h;
	}
	
	public Vector3 toVector3() {
		return new Vector3(x, height, y);
	}
}

public class HalfEdge : IComparable<HalfEdge>{ public static bool debug = false;
	public Vertex origin = null;
	public HalfEdge twin = null;
	
	public HalfEdge prev = null;
	public HalfEdge next = null;
	
	public Face face = null;
	
	public int id = 0;
	
	public HalfEdge(Vertex v) {
		origin = v;
		v.originOf.Add(this);
	}
	
	// NOTE: this should only be used on half-edges with the same origin point
	public int CompareTo(HalfEdge other) {
		if (other.id == this.id) return 0;
	
		Vertex endVertex = this.twin.origin;
		Vertex otherEndVertex = other.twin.origin;
		double c1 = Math.Atan2( (endVertex.y - origin.y) , (endVertex.x - origin.x) );
		double c2 = Math.Atan2( (otherEndVertex.y - origin.y) , (otherEndVertex.x - origin.x) );
		
		if(debug) Console.WriteLine(origin.id + ": ->" + endVertex.id + " " + c1 + ", ->" + otherEndVertex.id + " " + c2 + " " + ((c1 < c2)?1:2));
		
		if (c1 < c2) return 1;
		return -1;
	}
	
	public int compare(HalfEdge other) {
		if (other == null) return 1;
		
		Vertex endVertex = this.twin.origin;
		Vertex otherEndVertex = other.twin.origin;
		
		if (endVertex.x - origin.x >= 0 && otherEndVertex.x - origin.x < 0)
			return 1;
		if (endVertex.x - origin.x < 0 && otherEndVertex.x - origin.x >= 0)
			return 0;
		if (endVertex.x - origin.x == 0 && otherEndVertex.x - origin.x == 0) {
			if (endVertex.y - origin.y >= 0 || otherEndVertex.y - origin.y >= 0) {
				return (endVertex.y > otherEndVertex.y)?1:0;
			}
			return (otherEndVertex.y > endVertex.y)?1:0;
		}		
		
		float det = (endVertex.x - origin.x)*(otherEndVertex.y - origin.y) - (endVertex.y - origin.y)*(otherEndVertex.x - origin.x);
		if (det < 0) return 1;
		if (det < 0) return 0;
		
		float dist1 = (endVertex.x - origin.x)*(endVertex.x - origin.x) + (endVertex.y - origin.y)*(endVertex.y - origin.y);
		float dist2 = (otherEndVertex.x - origin.x)*(otherEndVertex.x - origin.x) + (otherEndVertex.y - origin.y)*(otherEndVertex.y - origin.y);
		
		return (dist1>dist2)?1:0;
	}

	public String toString() {
		return id + " (" + origin.id + ", " + twin.origin.id + ")";
	}
	
	}
	
public class Face {
	public HalfEdge edge = null;
	public List<Vertex> vertices = null;
	
	public Face() {
		vertices = new List<Vertex>();
	}
	
	public Vertex getCenter() {
		Vertex ans = new Vertex(0, 0);
	
		foreach (Vertex v in vertices) {
			ans.x += v.x;
			ans.y += v.y;
		}
	
		ans.x /= vertices.Count;
		ans.y /= vertices.Count;
	
		return ans;
	}
	
}

public class DCEL {

	Vertex leftmost = null;
	public List<Vertex> vertices = null;
	public List<HalfEdge> halfedges = null;
	public List<Face> faces = null;
	Face exterior = null;
	
	public DCEL() {
		vertices = new List<Vertex>();
		halfedges = new List<HalfEdge>();
		faces = new List<Face>();
	}
	
	public int addVertex(float x, float y) {
		Vertex v = new Vertex(x, y);
		vertices.Add(v);
		v.id = vertices.Count;
		
		return v.id;
	}
	
	public void addEdge(int origin, int end) {
		if (vertices.Count < origin || vertices.Count < end) {
			Console.WriteLine("Edge creation failed, reference to a non-existant point");
		}
	
		HalfEdge edge1 = new HalfEdge(vertices[origin-1]);
		HalfEdge edge2 = new HalfEdge(vertices[end-1]);
	
		edge1.twin = edge2;
		edge2.twin = edge1;	
	
		halfedges.Add(edge1);
		edge1.id = halfedges.Count;
		halfedges.Add(edge2);
		edge2.id = halfedges.Count;
	}
	
	public void assignNextAndPrev() {
		foreach (Vertex v in vertices) {
			if (v == null || v.originOf.Count == 0) continue;
			
			if (v.id == 5) HalfEdge.debug = true;
			v.originOf.Sort();
			if (v.id == 5) {HalfEdge.debug = false;
				foreach (HalfEdge h in v.originOf) {
					Vertex endVertex = h.twin.origin;
					Vertex origin = h.origin;
					double c1 = Math.Atan2( (endVertex.y - origin.y) , (endVertex.x - origin.x) );
					Console.WriteLine("  " + c1);
				}
			}
			
			Console.WriteLine("- Vertex " + v.id + " (" + v.x + ", " + v.y + ")");
			
			HalfEdge lastEdge = v.originOf[v.originOf.Count-1];
			for (int i = 0; i < v.originOf.Count; ++i) {
				Console.WriteLine(lastEdge.twin.toString() + " next " + v.originOf[i].toString());
				lastEdge.twin.next = v.originOf[i];
				v.originOf[i].prev = lastEdge.twin;
				lastEdge = v.originOf[i];
			}
			Console.WriteLine("");
		}
	}

	public void computeFaces() {
		// Start by identifying and computing the exterior face
		HalfEdge toSecondLeftmost = null;
		foreach (HalfEdge halfedge in leftmost.originOf) {
			if (toSecondLeftmost == null || halfedge.twin.origin.x < toSecondLeftmost.twin.origin.x) {
				toSecondLeftmost = halfedge;
			}
			
		}
			Debug.Log (leftmost.originOf.Count + " - " + toSecondLeftmost);
		if (toSecondLeftmost.twin.origin.y > leftmost.y) {
			toSecondLeftmost = toSecondLeftmost.twin;
		}
		exterior = constructFace(toSecondLeftmost.twin);
		
		Console.WriteLine("Generating faces");
		foreach (Vertex v in vertices) {
			if (v == null) continue;
			foreach (HalfEdge halfedge in v.originOf) {
				if (halfedge.face != null) continue;
				Face f = constructFace(halfedge);
				
				faces.Add(f);				
			}
		}
	}

	public Face constructFace(HalfEdge halfedge) {
		string s = "Generating face " + halfedge.origin.id;
		Face f = new Face();
		halfedge.face = f;
		f.edge = halfedge;
		f.vertices.Add(halfedge.origin);
		HalfEdge ite = halfedge.next;
		
		while (ite != halfedge) {
			s += " -> " + ite.origin.id;
			f.vertices.Add(ite.origin);
			ite.face = f;
			ite = ite.next;
		}
						
		
		Debug.Log(s);

		return f;
	}
	
	public void createDCEL() {
		Console.WriteLine("Halfedge created: " + halfedges.Count + "\n");
		removeDanglingPoints();
		assignNextAndPrev();
		computeFaces();
	}
	
	public void addLineBag(LineBag lineBag) {
		foreach (Vertex v in lineBag.vertices) {
			v.id = addVertex(v.x, v.y);
		}
		
		Console.WriteLine("POINTS " + vertices.Count);
		
		foreach (Line l in lineBag.lines) {
			addEdge(lineBag.vertices[l.p1.lbid-1].id, lineBag.vertices[l.p2.lbid-1].id);
		}
		
	}
	
	public void removeDanglingPoints() {
		List<int> pointsToRemove = new List<int>();
		foreach (Vertex v in vertices) {
			if (v.originOf.Count <= 1) { 
				foreach(HalfEdge e in v.originOf) {
					halfedges[e.id-1] = null;
					halfedges[e.twin.id-1] = null;
					e.twin.origin.originOf.Remove(e.twin);
				}
				pointsToRemove.Add(v.id);				
			}
			else {
				if (leftmost == null || v.x < leftmost.x) {
					leftmost = v;
				}
			}
		}
		
		foreach(int i in pointsToRemove) {
			vertices[i-1] = null; Console.WriteLine("Removing point " + (i-1));
		}	
	}
}

public class Line {
	public Vertex p1;
	public Vertex p2;
	
	public Line(Vertex v1, Vertex v2) {
		p1 = v1;
		p2 = v2;
	}
	
}

public class LineBag {
	public List<Vertex> vertices;
	public List<Line> lines;
	
	public LineBag() {
		vertices = new List<Vertex>();
		lines = new List<Line>();
	}
	
	public Vertex addVertex(Vertex v) {
		vertices.Add(v);
		v.lbid = vertices.Count;
		return v;
	}
	
	public void addLine(int id1, int id2, bool unique = false) {
		if (unique) {
			foreach (int i in vertices[id1-1].adjacent) {
				if (i == id2) return;
			}
			foreach (int i in vertices[id2-1].adjacent) {
				if (i == id1) return;
			}
		}
		
		Line l = new Line(vertices[id1-1], vertices[id2-1]);
		lines.Add(l);
		
		vertices[id1-1].adjacent.Add(id2);
		vertices[id2-1].adjacent.Add(id1);
	}

	public void intersectLines() {
		bool stop = false;
		float a, b, c, d, x, y;
		
		while (!stop) {
			stop = true;
			
			for (int i = 0; i < lines.Count; ++i) {
				Line l1 = lines[i];
				for (int j = i+1; j < lines.Count; ++j) {
					Line l2 = lines[j];
					
					// Special case : edges have a common end point
					if (l1.p1.lbid == l2.p1.lbid
						|| l1.p1.lbid == l2.p2.lbid
						|| l1.p2.lbid == l2.p1.lbid
						|| l1.p2.lbid == l2.p2.lbid) {
							continue;
						}
					
					if (l1.p2.x == l1.p1.x) {
						if (l2.p2.x == l2.p1.x) {
							Console.WriteLine("NOPE");
							continue;
						}
						c = (l2.p2.y - l2.p1.y) / (l2.p2.x - l2.p1.x);
						d = l2.p2.y - c*l2.p2.x;
						
						y = c*l1.p1.x + d;
						x = l1.p1.x;
						
						if (y >= Math.Min(l1.p2.y, l1.p1.y) && y <= Math.Max(l1.p2.y, l1.p1.y)
							&& x >= Math.Min(l2.p1.x, l2.p2.x) && x <= Math.Max(l2.p1.x, l2.p2.x)) {
							Vertex v = getVertex(x, y);						
							addLine(l1.p2.lbid, v.lbid);
							addLine(l2.p2.lbid, v.lbid);
							l1.p2 = v;
							l2.p2 = v;
							
							stop = false;
							continue;
						}
						
					}
					else if (l2.p2.x == l2.p1.x) {
						a = (l1.p2.y - l1.p1.y) / (l1.p2.x - l1.p1.x);
						b = l1.p2.y - a*l1.p2.x;
						
						y = a*l2.p1.x + b;
						x = l2.p1.x;
						
						if (y >= Math.Min(l2.p2.y, l2.p1.y) && y <= Math.Max(l2.p2.y, l2.p1.y)
							&& x >= Math.Min(l1.p1.x, l1.p2.x) && x <= Math.Max(l1.p1.x, l1.p2.x)) {
							Vertex v = getVertex(x, y);						
							addLine(l1.p2.lbid, v.lbid);
							addLine(l2.p2.lbid, v.lbid);
							l1.p2 = v;
							l2.p2 = v;
							
							stop = false;
							continue;
						}
						
					}
					
					a = (l1.p2.y - l1.p1.y) / (l1.p2.x - l1.p1.x);
					b = l1.p2.y - a*l1.p2.x;
					
					c = (l2.p2.y - l2.p1.y) / (l2.p2.x - l2.p1.x);
					d = l2.p2.y - c*l2.p2.x;
					
					x = (d-b)/(a-c);
					
					bool isInSegment = x >= Math.Min(l1.p1.x, l1.p2.x) && x <= Math.Max(l1.p1.x, l1.p2.x);
					
					if (isInSegment) {
						// There is an intersection
						y = a*x+b;
						
						Vertex v = getVertex(x, y);						
						addLine(l1.p2.lbid, v.lbid);
						addLine(l2.p2.lbid, v.lbid);
						l1.p2 = v;
						l2.p2 = v;
						
						stop = false;
					}
					
				
				}				
			}
			
		}
		
		
		
	}
	
	public void print() {
		string s = "Points (" + vertices.Count + ")\n";
		
		foreach (Vertex v in vertices) {
			s += v.lbid + " " + v.x + " " + v.y + "\n";
		}

		s += "\nLines (" + lines.Count + ")\n";
		
		foreach (Line l in lines) {
			s += l.p1.lbid + " " + l.p2.lbid + "\n";
		}
		
		s += "\n";
		
		Debug.Log(s);
		
	}
	
	public Vertex getVertex(float x, float y) {
		foreach (Vertex v in vertices) {
			if (v.x == x && v.y == y) {
				return v;
			}
		}
		return addVertex(new Vertex(x, y));
	}
	
}

	public class DebugUtil {

		public static void showPoint(float x, float y) {
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			cube.transform.position = new Vector3(x, 200, y);
		}

		public static void showLine(Vector3 p1, Vector3 p2) {
			GameObject lineObject = new GameObject();
			LineRenderer line = lineObject.AddComponent<LineRenderer>();
			line.SetVertexCount(2);
			line.SetPosition(0, p1);
			line.SetPosition(1, p2);
		}
	}
}



