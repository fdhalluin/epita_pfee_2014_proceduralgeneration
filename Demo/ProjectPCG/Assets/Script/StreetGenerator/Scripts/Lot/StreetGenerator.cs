﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LotGeneration;
using HouseGen;
using ScriptGen;

public class ScriptDescription {
	public string mScript;
	public int mWidth;
	public int mLength;
	
	public ScriptDescription(string iScript, int iWidth, int iLength) {
		mWidth = iWidth;
		mLength = iLength;
		mScript = iScript;
	}
}

public class StreetGenerator {
	public CityGenerator cityGenerator;
	public List<GameObject> mGameObjects = new List<GameObject>();
	public List<ScriptDescription> scripts = new List<ScriptDescription>();
	public float lotSize = 20;
	public houseBuilder builder = null;


	/*
	 * Delete all generated gameobject and reinit data
	 */ 
	public void ClearAll() {
		foreach (GameObject lObject in mGameObjects) {
			GameObject.DestroyImmediate(lObject);
		}
	}

	/*
	 * Check if the iPoly contains the iPoint
	 */ 
	private bool ContainsPoint(Vector3[] iPoly, Vector3 iPoint) { 
		int j = iPoly.Length - 1; 
		bool lInside = false; 
		for (int i = 0; i < iPoly.Length; j = i++) { 
			if (((iPoly[i].z <= iPoint.z && iPoint.z < iPoly[j].z) || (iPoly[j].z <= iPoint.z && iPoint.z < iPoly[i].z)) && 
			    (iPoint.x < (iPoly[j].x - iPoly[i].x) * (iPoint.z - iPoly[i].z) / (iPoly[j].z - iPoly[i].z) + iPoly[i].x)) {
				lInside = !lInside; 
			}
		} 
		return lInside; 
	}

	public void ConstructScript(Settings iSettings) {
		int width = 5;
		int length = 5;
		
		if (builder == null) {
			GameObject buildingsGenerator = GameObject.Find("BuildingsGenerator");
			builder = ((houseBuilder)buildingsGenerator.GetComponent(typeof(houseBuilder)));
		}
		
		for (int i = 0; i < iSettings.mNbDifferentHouses; ++i) {
			if (width % 20 == 0) {
				width = 5;
			}
			
			if (length % 20 == 0) {
				length = 5;
			}
			
			width += (int)Random.Range(0, 2);
			length += (int)Random.Range(0, 2);
			
			ScriptGenerator scriptGen = new ScriptGenerator();
			scriptGen.maxWidth = width;
			scriptGen.maxHeight = length;
			string script = scriptGen.getScript(BaseShape.RECT);
			ScriptDescription lScriptDescription = new ScriptDescription(script, width, length);
			scripts.Add(lScriptDescription);
		}
	}
	
	public GameObject constructHouse(string scriptName, Vector3 position, Vector3 rotation, bool treatAsScript = false) {
		bool lFind = false;
		
		if (position.x < 0
		    || position.z < 0
		    || position.x > Terrain.activeTerrain.terrainData.size.x
		    || position.z > Terrain.activeTerrain.terrainData.size.z) {
			return null;
		}
		
		for (int i = -4; i < 4; ++i) {
			for (int j = -4; j < 4; ++j) {
				float lX = position.x + i;
				float lZ = position.z + j;
				float lY = cityGenerator.GetY(lX, lZ);
				Collider[] hitColliders = Physics.OverlapSphere(new Vector3(lX, lY, lZ), 0.1f);
				if (TerrainGenerator.findWater(lX, lZ) || hitColliders.Length > 2) {
					lFind = true;
					break;
				}
			}
		}
		
		if (!lFind) {
			if (builder == null) {
				GameObject buildingsGenerator = GameObject.Find("BuildingsGenerator");
				builder = ((houseBuilder)buildingsGenerator.GetComponent(typeof(houseBuilder)));
			}
			GameObject house = builder.generateBuilding(scriptName, treatAsScript);
			
			house.transform.position = new Vector3(position.x, cityGenerator.GetY(position.x, position.z), position.z);
			house.transform.Rotate(rotation);
			house.transform.localScale = new Vector3(0.85f, 0.85f, 0.85f);
			house.transform.parent = cityGenerator.mRootHouses.transform;
			
			house.AddComponent<BoxCollider>();
			BoxCollider lRenderer = house.GetComponent<BoxCollider>();
			lRenderer.size = new Vector3(15, 15, 15);
			
			mGameObjects.Add(house);
			
			if (cityGenerator.mSettings.mHaveCarts && Random.Range(0, 9) > 6) {
				cityGenerator.PlaceCart(position + new Vector3(7, 0, 7));
				if (Random.Range(0, 9) > 6) {
					cityGenerator.PlaceCart(position + new Vector3(9, 0, -8));
				}
			}
			
			return house;
		}
		return null;
	}
	
	public List<GameObject> getLotCenters(Line l, PaletteKind iColor, bool whichSide = false, float perc = 1) {
		List<GameObject> ans = new List<GameObject>();
		float distance = Mathf.Sqrt( Mathf.Pow(l.p2.x - l.p1.x, 2) + Mathf.Pow(l.p2.y - l.p1.y, 2)  );
		int maximumLots = (int) Mathf.Floor(distance / lotSize) + 4;
		float distanceLeft = distance - maximumLots*lotSize;
		float spacePerc = (distanceLeft/(maximumLots+1))/distance;
		float lotPerc = lotSize/distance;
		
		Vector3 p1 = l.p1.toVector3();
		Vector3 p2 = l.p2.toVector3();
		
		Vector3 sideOffset = p2-p1;
		sideOffset.Normalize();
		sideOffset = Quaternion.Euler(0, 90, 0) * sideOffset;
		sideOffset *= 15f;
		
		for (float i = 1; i <= maximumLots; i += 1) {
			if ((float)Random.Range (0, 100)/100 < perc) continue;
			Vector3 lotCenter = Util.getPointByPercentage(p1, p2, (i-0.5f)*lotPerc + i*spacePerc);
			
			Vector3 rotation = CalculRotation(p1, p2);
			PaletteKind lColor = cityGenerator.ColorMutation(iColor);
			if (!whichSide) rotation += new Vector3(0, 180, 0);
			
			
			int lWidth = (int)Random.Range(6, 15);
			int lLength = (int)Random.Range(6, 15);
			
			string scriptName = "";
			foreach (ScriptDescription lDescription in scripts) {
				if (lDescription.mWidth == lWidth || lDescription.mLength == lLength) {
					scriptName = lDescription.mScript;
					if (Random.Range(0, 9) < 5) {
						break;
					}
				}
			}
			
			houseBuilder.textureToUse = lColor;
			
			if (whichSide) {
				ans.Add(constructHouse(scriptName, lotCenter + sideOffset, rotation, true));
				cityGenerator.mInternalEdges.Add(new Pair<Vector3, Vector3>(lotCenter, lotCenter + sideOffset));
			} else {
				ans.Add(constructHouse(scriptName, lotCenter - sideOffset, rotation, true));
				cityGenerator.mInternalEdges.Add(new Pair<Vector3, Vector3>(lotCenter, lotCenter - sideOffset));
			}
		}	
		
		return ans;
	}
	
	Vector3 getVertexCenter(Vertex v1, Vertex v2) {
		Vector3 p1 = new Vector3(v1.x, v1.height, v1.y);
		Vector3 p2 = new Vector3(v2.x, v2.height, v2.y);
		return (p1+p2)/2;
	}
	
	Vector3 CalculRotation(Vector3 iFirstPosition, Vector3 iSecondPosition) {
		float lTau = Mathf.PI - Mathf.Atan2(iSecondPosition.z - iFirstPosition.z, iSecondPosition.x - iFirstPosition.x);
		lTau *= 180 / Mathf.PI;
		
		return new Vector3(0, lTau, 0);
	}
}