﻿using System;
using CoherentNoise;
using CoherentNoise.Generation;
using CoherentNoise.Generation.Displacement;
using CoherentNoise.Generation.Fractal;
using CoherentNoise.Generation.Modification;
using CoherentNoise.Generation.Patterns;
using CoherentNoise.Texturing;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class TerrainDemo2 : MonoBehaviour
{
	// terrains that we would fly over. Terrain5 is currently showing, Terrain6 is farther away and being generated
	public Terrain Terrain5;
	public Terrain Terrain6;
	
	public float Speed = 50; // flight speed
	
	private Generator m_Generator;

 // noise generator for terrain height
	private Generator m_Weight; // noise generator for terrain type: hills vs desert
	private int m_NoiseCoord = 0; // current terrain coordinate in domain (noise) space
	private bool m_Move; // are we flying yet?
	private bool m_CameraChangingHeight; // is camera height changing?
	
	void Start()
	{
		
		// desert dune-like ridges are created using RidgeNoise. it is scaled down a bit, and Gain applied to make ridges more pronounced
		var desert = new Gain(
			new RidgeNoise(23478568)
			{
			OctaveCount = 8
		} * 0.6f, 0.4f);
		// hills use a simple pink noise. 
		var hills = new PinkNoise(34)
		{
			OctaveCount = 5, // smooth hills (no higher frequencies)
			Persistence = 0.46f // but steep (changes rapidly in the frequencies we do have)
		};

		m_Generator = desert;
		// as changes to terrains made in playmode get saved in the editor, we need to re-generate terrains
		// at start. The coroutine will set m_Move in the end, so that camera does not fly intil it's ready
		Terrain5 = CreateTerrain(Terrain5);
	}
	
	// Update is called once per frame
	void Update()
	{
	}
	
	private void SwitchTerrains()
	{
		// return camera to start (we want to always be near coordinate origin, so that float precision does not become an issue)
		var delta = Camera.main.transform.position.z;
		Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, Camera.main.transform.position.y, 0);
		// move terrains with camera
		Terrain5.transform.position = Terrain5.transform.position - new Vector3(0, 0, delta);
		Terrain6.transform.position = Terrain6.transform.position - new Vector3(0, 0, delta);
		// move terrain1 to farther position
		Terrain5.transform.position = Terrain5.transform.position + new Vector3(0, 0, 4000); // terrains are 2000x2000
		// swap terrains so that t1 becomes t2
		var t = Terrain5;
		Terrain5 = Terrain6;
		Terrain6 = t;
		// noise domain shifted
		m_NoiseCoord++;
		// aaaand start new terrain generation
		Terrain5 = CreateTerrain(t);
	}
	
	Terrain CreateTerrain(Terrain t)
	{
		var td = t.terrainData;


		// fixing resultions in code, as there's no place in editor to do that
		td.alphamapResolution = td.heightmapResolution;
		td.SetDetailResolution(td.heightmapWidth, 16);
		
		// arrays for various maps used in terrain
		var hm = new float[td.heightmapWidth, td.heightmapHeight]; // height
		var am = new float[td.heightmapWidth, td.heightmapHeight, 2]; // alpha (textures)
		var dm = new int[td.detailResolution, td.detailResolution]; // detail (grass)
		
		// create heightmap data
		for (int ii = 0; ii < td.heightmapWidth; ++ii)
			for (int jj = 0; jj < td.heightmapHeight; ++jj)
		{
			
			
			// Domain coordinates. Each terrain is 1x1 in domain space
			float x = (float)ii / (td.heightmapWidth - 1) + m_NoiseCoord;
			float y = (float)jj / (td.heightmapHeight - 1);


		//	System.IO.File.WriteAllText("C:/Users/Alexandre/Documents/log.txt", x + " " + y);
			
			// calculate height. This is where the performance hit is; everything else is peanuts compared to evaluating
			// a complex noise function
			var v = m_Generator.GetValue(x, y, 0);
			hm[ii, jj] = v;
			
			// texture alpha. Weight determines, whether we are generating hills or desert here, or something in between
	//		var w = Mathf.Clamp01(m_Weight.GetValue(x, y, 0) * 1.3f); // *1.3 to make more grassy areas. Looks nicer this way.
			//am[ii, jj, 0] = 1 - w;  // w==0 means deserts
			//am[ii, jj, 1] = w;      // w==1 means hills

	//		am[ii, jj, 1] = 0;
	//		am[ii, jj, 0] = 0;


			// details count
	//		var details = w > 0.5f ? (w - 0.5) * 40 : 0; // w<0.5 means desert, so no grass there. 
			// Unity does not allow to set detail map resultion at will, so we have to map heightmap coords to detailmap
			// (although in this case terrains are square, and resolutions match exactly)
	//		int dx = ii * td.detailResolution / td.heightmapWidth;
	//		int dy = jj * td.detailResolution / td.heightmapHeight;
			
			// when we have less than 1 blade of grass, add one randomly. This makes grass to no-grass transition smoother
	//		dm[dx, dy] = details > 1 ? (int)details : Random.value < details ? 1 : 0;
		}
		
		// apply changes to terrain
		//	yield return null; // end frame
		td.SetHeights(0, 0, hm);
		//	yield return null; // end frame
	//	td.SetAlphamaps(0, 0, am);
		//	yield return null; // end frame
	//	td.SetDetailLayer(0, 0, 0, dm);
		
		// according to documentation, we should register terrains as neighbours, so that LODs match
		// however, in practice they don't match anyway. There's probably some trick here.
		//Terrain6.SetNeighbors(null, null, Terrain5, null);
		//Terrain5.SetNeighbors(Terrain6, null, null, null);

		return t;
	}

	private int selectionGridInt = 0;
	private string[] selectionStrings = {"PinkNoise", "BillowNoise", "RidgeNoise"};
	private float hSliderOctave = 5f;
	private float hSliderFreq = 0.46f;
	private float hSliderMult = 1f;

	private string textFieldString = "42";

	private float hSliderMod = 1f;
	private int toolbarInt = 0;
	private string[] toolbarStrings = {"Nothing", "Bias", "Gain"};


	private bool BlendBool = false;

	private int selectionGridInt2 = 0;
	private float hSliderOctave2 = 5f;
	private float hSliderFreq2 = 0.46f;
	private float hSliderMult2 = 1f;

	private string textFieldString2 = "42";
	
	private float hSliderMod2 = 1f;
	private int toolbarInt2 = 0;

	private bool TerrainBool = true;
	private int selectionTerrainInt = 0;
	private string[] selectionTerrainStrings = {"Terrain1", "Terrain2"};
	private float hSliderMultTerrain = 1f;
	private Generator CustGenerator;

	void OnGUI () {

		TerrainBool = GUI.Toggle (new Rect (10, 10, 100, 30), TerrainBool, "Terrain");

		if (!TerrainBool)
		{
			if (!BlendBool)
					GUI.Box (new Rect (10, 10, 270, 350), "Test Terrain");
			else
					GUI.Box (new Rect (10, 10, 550, 350), "Test Terrain");

			BlendBool = GUI.Toggle (new Rect (120, 330, 100, 30), BlendBool, "Blend 2 noises");
			

			
			var noise1 = new PinkNoise(42)
			{
				OctaveCount = 5, // smooth hills (no higher frequencies)
				Persistence = 0.46f // but steep (changes rapidly in the frequencies we do have)
			};
			
			var noise2 = new BillowNoise (42)
			{
				OctaveCount = 5,
				Persistence = 0.46f
			};
			
			var noise3 = new RidgeNoise (42)
			{
				OctaveCount = 5
			};
			
			selectionGridInt = GUI.SelectionGrid (new Rect (20, 40, 250, 40), selectionGridInt, selectionStrings, 3);
			toolbarInt = GUI.Toolbar (new Rect (25, 240, 250, 30), toolbarInt, toolbarStrings);
			
			hSliderOctave = GUI.HorizontalSlider (new Rect (25, 120, 100, 30), hSliderOctave, 0.0f, 10.0f);
			if (selectionGridInt != 2)
				hSliderFreq = GUI.HorizontalSlider (new Rect (25, 160, 100, 30), hSliderFreq, 0.0f, 1.0f);
			hSliderMult = GUI.HorizontalSlider (new Rect (25, 200, 100, 30), hSliderMult, 0.0f, 1.0f);
			hSliderMod = GUI.HorizontalSlider (new Rect (25, 280, 100, 20), hSliderMod, 0f, 1.0f);
			
			textFieldString = GUI.TextField (new Rect (25, 300, 70, 20), textFieldString);
			GUI.Label (new Rect (105,300,40,50), "Seed");
			
			GUI.Label (new Rect (150,120,100,50), "Octave: " + ((int)hSliderOctave).ToString());
			if (selectionGridInt != 2)
				GUI.Label (new Rect (150,160,140,50), "Pers: " + hSliderFreq.ToString());
			GUI.Label (new Rect (150,200,100,50), "Mult: " + hSliderMult.ToString());
			GUI.Label (new Rect (150,270,100,50), "Mod: " + hSliderMod.ToString());
			
			var noise12 = new PinkNoise(42)
			{
				OctaveCount = 5, // smooth hills (no higher frequencies)
				Persistence = 0.46f // but steep (changes rapidly in the frequencies we do have)
			};
			
			var noise22 = new BillowNoise (42)
			{
				OctaveCount = 5,
				Persistence = 0.46f
			};
			
			var noise32 = new RidgeNoise (42)
			{
				OctaveCount = 5
			};
			
			if (BlendBool)
			{
				selectionGridInt2 = GUI.SelectionGrid (new Rect (300, 40, 250, 40), selectionGridInt2, selectionStrings, 3);
				toolbarInt2 = GUI.Toolbar (new Rect (300, 240, 250, 30), toolbarInt2, toolbarStrings);
				
				hSliderOctave2 = GUI.HorizontalSlider (new Rect (300, 120, 100, 30), hSliderOctave2, 0.0f, 10.0f);
				if (selectionGridInt2 != 2)
					hSliderFreq2 = GUI.HorizontalSlider (new Rect (300, 160, 100, 30), hSliderFreq2, 0.0f, 1.0f);
				hSliderMult2 = GUI.HorizontalSlider (new Rect (300, 200, 100, 30), hSliderMult2, 0.0f, 1.0f);
				hSliderMod2 = GUI.HorizontalSlider (new Rect (300, 280, 100, 20), hSliderMod2, 0f, 1.0f);
				
				textFieldString2 = GUI.TextField (new Rect (300, 300, 70, 20), textFieldString2);
				GUI.Label (new Rect (385,300,40,50), "Seed");
				
				
				GUI.Label (new Rect (420,120,100,50), "Octave: " + ((int)hSliderOctave2).ToString());
				if (selectionGridInt2 != 2)
					GUI.Label (new Rect (420,160,140,50), "Pers: " + hSliderFreq2.ToString());
				GUI.Label (new Rect (420,200,100,50), "Mult: " + hSliderMult2.ToString());
				GUI.Label (new Rect (420,270,100,50), "Mod: " + hSliderMod2.ToString());
			}
			
			// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
			if(GUI.Button(new Rect(20,330,80,20), "Generate")) {
				
				int seed1 = 42;
				int seed2 = 42;
				
				int.TryParse(textFieldString, out seed1);
				
				if (seed1 <= 0)
					seed1 = 42;
				
				int.TryParse(textFieldString, out seed2);
				
				if (seed2 <= 0)
					seed2 = 42;
				
				
				if (selectionGridInt == 0)
				{
					noise1 = new PinkNoise(seed1)
					{
						OctaveCount = (int) hSliderOctave,
						Persistence = hSliderFreq
					};
					m_Generator = noise1;
				}
				else if (selectionGridInt == 1)
				{
					noise2 = new BillowNoise(seed1)
					{
						OctaveCount = (int) hSliderOctave,
						Persistence = hSliderFreq
					};
					m_Generator = noise2;
				}
				else
				{
					noise3 = new RidgeNoise(seed1)
					{
						OctaveCount = (int) hSliderOctave
					};
					//noise3.Frequency = (int) hSliderFreq;
					m_Generator = noise3;
				}
				m_Generator *= hSliderMult;
				
				if (toolbarInt == 1)
					m_Generator = new Bias(m_Generator, hSliderMod);
				else if (toolbarInt == 2)
					m_Generator = new Gain(m_Generator, hSliderMod);
				
				if (selectionGridInt == 0)
				{
					noise1.OctaveCount = (int) hSliderOctave;
					noise1.Persistence = hSliderFreq;
					m_Generator = noise1;
				}
				else if (selectionGridInt == 1)
				{
					noise2.OctaveCount = (int) hSliderOctave;
					noise2.Persistence = hSliderFreq;
					m_Generator = noise2;
				}
				else
				{
					noise3.OctaveCount = (int) hSliderOctave;
					//noise3.Frequency = (int) hSliderFreq;
					m_Generator = noise3;
				}
				m_Generator *= hSliderMult;
				
				if (toolbarInt == 1)
					m_Generator = new Bias(m_Generator, hSliderMod);
				else if (toolbarInt == 2)
					m_Generator = new Gain(m_Generator, hSliderMod);
				
				if (BlendBool)
				{
					Generator generator2;
					Generator weight;
					
					if (selectionGridInt2 == 0)
					{
						noise12 = new PinkNoise(seed2)
						{
							OctaveCount = (int) hSliderOctave2,
							Persistence = hSliderFreq2
						};
						generator2 = noise12;
					}
					else if (selectionGridInt2 == 1)
					{
						noise22 = new BillowNoise(seed2)
						{
							OctaveCount = (int) hSliderOctave2,
							Persistence = hSliderFreq2
						};
						generator2 = noise22;
					}
					else
					{
						noise32 = new RidgeNoise(seed2)
						{
							OctaveCount = (int) hSliderOctave2
						};
						//noise3.Frequency = (int) hSliderFreq;
						generator2 = noise32;
					}
					generator2 *= hSliderMult2;
					
					if (toolbarInt2 == 1)
						generator2 = new Bias(generator2, hSliderMod2);
					else if (toolbarInt2 == 2)
						generator2 = new Gain(generator2, hSliderMod2);
					
					weight = new Cache(new PinkNoise(346546)
					                   {
						Frequency = 0.5f,
						OctaveCount = 3
					}.ScaleShift(0.5f, 0.5f));
					
					m_Generator = m_Generator.Blend(generator2, weight).ScaleShift(0.5f, 0.5f);
				}
				
				
				Terrain5 = CreateTerrain(Terrain5);
				
			}

		}
		else
		{
			GUI.Box (new Rect (10, 10, 270, 350), "Test Terrain");

			m_Generator = CustGenerator;
			selectionTerrainInt = GUI.SelectionGrid (new Rect (20, 40, 250, 40), selectionTerrainInt, selectionTerrainStrings, 3);
			hSliderMultTerrain = GUI.HorizontalSlider (new Rect (25, 120, 100, 30), hSliderMultTerrain, 0.0f, 1.0f);
			GUI.Label (new Rect (150,115,100,50), "Mult: " + hSliderMultTerrain.ToString());

			if(GUI.Button(new Rect(20,330,80,20), "Generate"))
			{
				if (selectionTerrainInt == 0)
				{
					CreateCustomNoise1();
				}
				else if (selectionTerrainInt == 1)
				{
					CreateCustomNoise2();
				}

				CustGenerator = m_Generator;
				m_Generator *= hSliderMultTerrain;
				Terrain5 = CreateTerrain(Terrain5);
			}

			if(GUI.Button(new Rect(120,330,80,20), "Modif Size"))
			{
				m_Generator *= hSliderMultTerrain;
				Terrain5 = CreateTerrain(Terrain5);
			}

		}
	}

	private void CreateCustomNoise1()
	{
		var desert = new Gain(
			new RidgeNoise((int)Random.Range(0, 99999999))
			{
			OctaveCount = 8
		} * 0.6f, 0.4f);

		var hills = new PinkNoise((int)Random.Range(0, 99999999))
		{
			OctaveCount = 5,
			Persistence = 0.56f 
		};


		m_Generator = desert.Blend(hills, desert).ScaleShift(0.5f, 0.5f);
	}

	private void CreateCustomNoise2()
	{
		var noise = new Bias(
			new PinkNoise((int)Random.Range(0, 99999999))
			{
			OctaveCount = 5,
			Persistence = 0.4f
		}, 0.4f);

		m_Generator = noise;
	}
}