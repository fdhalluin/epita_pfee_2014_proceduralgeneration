﻿using System;
using CoherentNoise;
using CoherentNoise.Generation;
using CoherentNoise.Generation.Displacement;
using CoherentNoise.Generation.Fractal;
using CoherentNoise.Generation.Modification;
using CoherentNoise.Generation.Patterns;
using CoherentNoise.Texturing;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using UnityEditor;
using System.Collections.Generic;



/*
 * Generate a terrain
 * Generate height and trees
 */


public class TerrainGenerator : MonoBehaviour
{
	public Terrain Terrain;
	
	void Start()
	{	
		Terrain.terrainData.size = new Vector3 (1000, 600, 1000);
	}
	
	// Update is called once per frame
	void Update()
	{
	}


	/*
	 * Generate the terrain
	 * TerrainSettings give the information of the new terrain
	 */

	public void CreateTerrain(TerrainSettings terSet)
	{
		Terrain.terrainData.size = new Vector3 ((float) terSet.getWidth(), 600, (float) terSet.getLength());
		var td = Terrain.terrainData;

		// fixing resultions in code, as there's no place in editor to do that
		td.alphamapResolution = td.heightmapResolution;
		td.SetDetailResolution(td.heightmapWidth, 16);
		
		// arrays for various maps used in terrain
		var hm = new float[td.heightmapWidth, td.heightmapHeight]; // height
		var am = new float[td.heightmapWidth, td.heightmapHeight, 4]; // alpha (textures)
		var dm = new int[td.detailResolution, td.detailResolution]; // detail (grass)
		
		// create heightmap data
		for (int ii = 0; ii < td.heightmapWidth; ++ii)
			for (int jj = 0; jj < td.heightmapHeight; ++jj)
		{
			
			
			// Domain coordinates. Each terrain is 1x1 in domain space
			float x = (float)ii / (td.heightmapWidth - 1);
			float y = (float)jj / (td.heightmapHeight - 1);
			
			var v = terSet.getGenerator().GetValue(x, y, 0);

			hm[ii, jj] = v;

			if (v > 0 && v < 0.2f)
			{
				if (Random.value > 0.2f)
				{
					am[ii, jj, 0] = 0;
					am[ii, jj, 1] = 0;
					am[ii, jj, 2] = 1;
					am[ii, jj, 3] = 0;
				}
				else
				{
					am[ii, jj, 0] = 0;
					am[ii, jj, 1] = 1;
					am[ii, jj, 2] = 0;
					am[ii, jj, 3] = 0;
				}
			}
			else if (v >= 0.2f)
			{
				am[ii, jj, 0] = 0;
				am[ii, jj, 1] = 0;
				am[ii, jj, 2] = 1;
				am[ii, jj, 3] = 0;
			}
			else
			{
				am[ii, jj, 0] = 0;
				am[ii, jj, 1] = 0;
				am[ii, jj, 2] = 0;
				am[ii, jj, 3] = 1;
			}
			
		}
		// apply changes to terrain	
		td.SetHeights(0, 0, hm);
		td.SetAlphamaps(0, 0, am);

	}

	/*
	 *  Delete all the trees and the prototypes
	 */

	public void ClearTrees()
	{
		TerrainData terrain = Terrain.terrainData;
		List<TreeInstance> treeInstances = new List<TreeInstance>(terrain.treeInstances);;
		
		
		treeInstances.Clear();
		terrain.treeInstances = treeInstances.ToArray();
		
		// Now refresh the terrain, getting rid of the darn collider
		float[,] heights = terrain.GetHeights(0, 0, 0, 0);
		Terrain.activeTerrain.terrainData.SetHeights(0, 0, heights);
		terrain.treePrototypes = null;

	}

	/*
	 * Add Trees in the terrain
	 * Add a prefab tree and generate some trees
	 */

	public void AddTrees()
	{
		ClearTrees ();

		TerrainData currentTerrainData = Terrain.terrainData;
		List<TreePrototype> treePrototypesList = new List<TreePrototype>(currentTerrainData.treePrototypes);
		
		TreePrototype treePrototype = new TreePrototype();
		GameObject prefab = Resources.LoadAssetAtPath<GameObject>("Assets/Prefabs/01 sycamore trees finalized/syc_01 manual.prefab");
		if(prefab != null)
		{
			treePrototype.prefab = prefab;
			treePrototypesList.Add(treePrototype);
		}
		
		currentTerrainData.treePrototypes = treePrototypesList.ToArray();
		Terrain.Flush();
		currentTerrainData.RefreshPrototypes();


		List<TreeInstance> treeInstances = new List<TreeInstance> ();


		var noise = new BillowNoise ((int)Random.Range(0, 99999999))
		    {
				Persistence = 0.6f,
				OctaveCount = 4
			};


		for (int ii = 0; ii < currentTerrainData.heightmapWidth; ++ii)
			for (int jj = 0; jj < currentTerrainData.heightmapHeight; ++jj) 
		{
			
			
			// Domain coordinates. Each terrain is 1x1 in domain space
			float x = (float)ii / (currentTerrainData.heightmapWidth - 1);
			float y = (float)jj / (currentTerrainData.heightmapHeight - 1);

			var v = noise.GetValue(x, y, 0);

			if (v > 0 && Random.value > 0.9f)
			{
				TreeInstance newInstance = new TreeInstance();
				newInstance.position = getCordinateTree(y * Terrain.terrainData.size[2], x * Terrain.terrainData.size[0]);
				newInstance.heightScale = 1;
				newInstance.widthScale = 1;
				newInstance.prototypeIndex = 0;
				newInstance.lightmapColor = Color.white;
				newInstance.color = Color.white;
				
				treeInstances.Add (newInstance);
			}
		}

		currentTerrainData.treeInstances = treeInstances.ToArray();
	
		//Terrain.AddTreeInstance(newInstance);
		Terrain.Flush();
	}

	/*
	 * Get the coordinates for the dimension of the terrain
	 */

	public Vector3 getCordinateTree(float x, float z)
	{
		float x2 = x / Terrain.terrainData.size [0];
		float z2 = z / Terrain.terrainData.size [2];
		float y = Terrain.activeTerrain.SampleHeight(new Vector3(x, 0, z)) / Terrain.terrainData.size[1];

		return new Vector3(x2, y, z2);
	}
}