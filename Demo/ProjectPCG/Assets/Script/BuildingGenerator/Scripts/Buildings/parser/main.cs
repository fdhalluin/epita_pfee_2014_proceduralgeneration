using System;
using HouseGen;
using System.Collections.Generic;


namespace ProgramSpace {

class Program2 {

    static void Main(string[] args)
    {
		Parser p = new Parser();
		
		Dictionary<string, RoomDescription> ans = 
			p.parse("mainRoom:\n" +
					"size 15 8 # size Width Height\n" +
					"height 2 # height Height\n" +
					"windows NORTH 2 9 1 CIRCLE # windows Direction Storey WindowSize Spacing WindowShape\n" +
					"windows NORTH 0 3 0 DOOR\n" +
					"addRoom SOUTH nef CENTER # addRoom Direction RoomModel [PositionType]\n" +
					"roof 3 LINEAR # roof RoofOffset RoofType\n" +
					"onTop nef 2 2 # onTop RoomModel XOffset YOffset\n" +
					"addTower NORTH EAST 2 3 # addTower DirectionVertical DirectionHorizontal Radius [Height]\n" +
					"\n" +
					"nef:\n" +
					"size 4 5\n" +
					"height 1\n" +
					"roof 1 POINT\n");
		
		Console.WriteLine("");
		foreach (string roomName in ans.Keys) {
			Console.WriteLine(roomName + ":");
			RoomDescription room = ans[roomName];
			foreach (string command in room.properties.Keys) {
				Console.WriteLine(room.properties[command]);
			}
			Console.WriteLine("");
		}
		
    }
	
	static Dictionary<string, RoomDescription> getDict() {
		Parser p = new Parser();
		
		Dictionary<string, RoomDescription> ans = 
			p.parse("mainRoom:\n" +
					"size 15 8 # size Width Height\n" +
					"height 2 # height Height\n" +
					"windows NORTH 2 9 1 CIRCLE # windows Direction Storey WindowSize Spacing WindowShape\n" +
					"windows NORTH 0 3 0 DOOR\n" +
					"addRoom SOUTH nef CENTER # addRoom Direction RoomModel [PositionType]\n" +
					"roof 3 LINEAR # roof RoofOffset RoofType\n" +
					"onTop nef 2 2 # onTop RoomModel XOffset YOffset\n" +
					"addTower NORTH EAST 2 3 # addTower DirectionVertical DirectionHorizontal Radius [Height]\n" +
					"\n" +
					"nef:\n" +
					"size 4 5\n" +
					"height 1\n" +
					"roof 1 POINT\n");
					
		return ans;
	}

};


}