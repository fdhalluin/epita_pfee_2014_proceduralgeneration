using UnityEngine;
using System.Collections.Generic;

public class CartoNode : ANode {

	public CartoNode(Vector3 iPosition) {
		mAdjacentNodes = new Dictionary<ANode, int>();
		mPosition = iPosition;
	}

	public CartoNode(float iX, float iY, float iZ) {
		mAdjacentNodes = new Dictionary<ANode, int>();
		mPosition = new Vector3(iX, iY, iZ);
	}
}