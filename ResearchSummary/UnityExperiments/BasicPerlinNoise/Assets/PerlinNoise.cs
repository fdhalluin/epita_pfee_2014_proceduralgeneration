﻿using UnityEngine;
using System.Collections;

public class PerlinNoise : MonoBehaviour {
	public float mScale = 1.0F;
	public float mPower = 3.0f;
	private Vector2 mVector = new Vector2(0f, 0f);
	
	void Start() {
		ProcessNoise();
	}
	
	void Update() {
		if (Input.GetKeyDown (KeyCode.Space)) {
			mVector = new Vector2(Random.Range(0.0f, 100.0f), Random.Range(0.0f, 100.0f));
			ProcessNoise();
		}
	}
	
	void ProcessNoise() {
		MeshFilter lMeshFilter = GetComponent<MeshFilter>();
		Vector3[] lVertices = lMeshFilter.mesh.vertices;
		for (int i = 0; i < lVertices.Length; i++) {   
			float lXCoord = mVector.x + lVertices[i].x * mScale;
			float lYCoord = mVector.y + lVertices[i].z * mScale; /* Because we are in 3D, Z corresponds to the lenght/width */
			lVertices[i].y = (Mathf.PerlinNoise(lXCoord, lYCoord) - 0.5f) * mPower; /* Because we are in 3D, Y corresponds to the depth */
		}
		lMeshFilter.mesh.vertices = lVertices;
		lMeshFilter.mesh.RecalculateBounds();
		lMeshFilter.mesh.RecalculateNormals();
	}
	
	void OnGUI() {
		GUI.Window(0, new Rect(10, 10, 200, 150), DrawGUI, "PerlinNoise");
	}
	
	void DrawGUI(int id) {
		GUILayout.BeginVertical();
		GUILayout.Label("Scale:");
		mScale = GUILayout.HorizontalSlider(mScale, 0, 200);
		GUILayout.Label("Power:");
		mPower = GUILayout.HorizontalSlider(mPower, 0, 20);
		GUILayout.EndVertical();
		GUILayout.Label("Press Space Bar to update");
	}
}

