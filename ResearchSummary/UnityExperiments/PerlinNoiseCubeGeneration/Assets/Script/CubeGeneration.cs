using UnityEngine;
using System.Collections;

public class CubeGeneration : MonoBehaviour {

	private World mWorld;
	public GameObject mCubeGrass;
	public GameObject mCubeRock;
	public GameObject mCubeSand;
	public bool mOcclusionCullingBool = true;

	void Start () {
		mWorld = new World();
		mWorld.Init();
		CreateWorld();
		DisplayWorld();
	}
	
	void Update() {}

	void CreateWorld() {
		int lMaxHeight = mWorld.mSizeY;
		int lOctaveCount = 5;
		int lCurrentHeight;
		float[][] lPerlinNoiseArray = new float[mWorld.mSizeX][];

		lPerlinNoiseArray = PerlinNoise.GetPerlinNoise(mWorld.mSizeX, mWorld.mSizeZ, lOctaveCount);
		
		for (int i = 0; i < mWorld.mSizeX; i++) {
			for(int k = 0; k < mWorld.mSizeZ; k++) {
				lCurrentHeight = (int)(lMaxHeight * Mathf.Clamp01(lPerlinNoiseArray[i][k]));

				/* Specify the kind of the cube */
				for (int j = 0; j < lCurrentHeight; j++) {
					if (j == lCurrentHeight - 1) {
						mWorld.mBlocksArray[i, j, k] = 1; // Grass
					}
					else if(j == lCurrentHeight - 2) {
						mWorld.mBlocksArray[i, j, k] = 3; // Sand
					}
					else {
						mWorld.mBlocksArray[i, j, k] = 2; // Rock
					}
					if (j > 8) {
						mWorld.mBlocksArray[i, j, k] = 2; // Grass
					}
				}
			}	
		}
	}
	
	void DisplayWorld() {
		bool lCubeIsClosed = false;
		
		for (int i = 0; i < mWorld.mSizeX; i++) {
			for (int j = 0; j < mWorld.mSizeY; j++) {
				for (int k = 0; k < mWorld.mSizeZ; k++) {
					lCubeIsClosed = false;
					if (mWorld.mBlocksArray[i, j, k] != 0) {
						if (mOcclusionCullingBool == true) {
							/* Test if the Cube is on the limit of the World */
							if ((i > 0 && i < mWorld.mSizeX - 1) && (j > 0 && j < mWorld.mSizeY - 1) && (k > 0 && k < mWorld.mSizeZ - 1)) {
								/* Test if the cube is enclosed by other cubes */
								if ((mWorld.mBlocksArray[i + 1, j, k] != 0
								    && mWorld.mBlocksArray[i - 1, j, k] != 0
								    && mWorld.mBlocksArray[i, j + 1, k] != 0
								    && mWorld.mBlocksArray[i, j - 1, k] != 0
								    && mWorld.mBlocksArray[i, j, k + 1] != 0
								    && mWorld.mBlocksArray[i, j, k - 1] != 0)) {
									lCubeIsClosed = true;;
								}
							}
						}
						if (!lCubeIsClosed) {
							CreateCube(i, j, k, mWorld.mBlocksArray[i, j, k]);
						}
					}
				}
			}
		}
	}

	void CreateCube(int iPosX, int iPosY, int iPosZ, int iBlockType) {
		GameObject iCurCube = mCubeRock;
		if (iBlockType == 1) {
			iCurCube = mCubeGrass;	
		}
		else if (iBlockType == 2) {
			iCurCube = mCubeRock;
		}
		else if (iBlockType == 3) {
			iCurCube = mCubeSand;
		}

		GameObject.Instantiate(iCurCube, new Vector3(iPosX, iPosY, iPosZ), Quaternion.identity);
	}
}
