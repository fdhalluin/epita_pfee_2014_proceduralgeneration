﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MazeGenerator : MonoBehaviour {

	public int width;
	public int height;
	public GameObject mazeCell;
	public GameObject player;
	public WinDetection winDetection;

	private int[,] maze;
	private List<Vector2> stack;
	private List<GameObject> mazeCells;

	// Use this for initialization
	void Start () {
		stack = new List<Vector2> ();
		mazeCells = new List<GameObject> ();
		maze = new int[width, height];
		playAMaze ();
	}

	public void playAMaze() {
		stack.Clear ();

		foreach (GameObject cell in mazeCells) {
			Destroy(cell);
		}
		mazeCells.Clear ();

		for (int i = 0; i < width; ++i) {
			for(int j = 0; j < height; ++j) {
				maze[i, j] = 0;
			}
		}

		generateMaze ();
		spawnMaze ();
		player.transform.position = getSpawnPoint();
		player.rigidbody.velocity = new Vector3 (0, -20, 0);
	}

	Vector3 getSpawnPoint() {
		for (int i = 0; i < width; ++i) {
			for(int j = 0; j < height; ++j) {
				if (getMaze(i, j) == 1) {
					return new Vector3( (i - width/2 + 1)*mazeCell.transform.localScale.x ,
					                             (getMaze(i, j)==1?0:1) ,
					                             (j - height/2)*mazeCell.transform.localScale.z);
				}
			}
		}
		return new Vector3(0, 0);
	}

	void spawnMaze() {
		GameObject mazeEnd=null;
		int endScore = 0;
		for (int i = -1; i < width+1; ++i) {
			for(int j = -1; j < height+1; ++j) {
				Vector3 offset = new Vector3( (i - width/2)*mazeCell.transform.localScale.x ,
				                             (getMaze(i, j)==1?0:1) ,
				                             (j - height/2)*mazeCell.transform.localScale.z);
				GameObject mazePiece = Instantiate(mazeCell, transform.localPosition + offset, transform.rotation) as GameObject;
				mazeCells.Add(mazePiece);
				if (getMaze(i, j) != 1) {
					mazePiece.transform.localScale = mazePiece.transform.localScale + new Vector3(0, 5, 0);
				}

				if (i+j > endScore && getMaze(i, j) == 1) {
					mazeEnd = mazePiece;
					endScore = i+j;
				}

			}
		}

		mazeEnd.renderer.material.color = Color.blue;
		winDetection.transform.position = mazeEnd.transform.position + new Vector3(0, -1, 0);
		winDetection.transform.localScale = mazeEnd.transform.localScale;
	}

	void showMaze() {
		string s = "";
		for (int i = 0; i < width; ++i) {
			for(int j = 0; j < height; ++j) {
				s += maze[i, j]+1;
			}
			s += "\n";
		}
		Debug.Log (s);
	}

	// A positive number indicates a maze cell
	// A negative number indicates a wall cell
	void generateMaze() {
		int x = (int)Mathf.Floor(Random.Range (0, width));
		int y = (int)Mathf.Floor(Random.Range (0, height));
		Vector2 startCell = new Vector2 (x, y);

		markCell (startCell, 1);

		foreach(Vector2 neighbour in getNeighbours(startCell)) {
			toExplore(neighbour);
		}

		explore ();
	}

	void explore() {
		Vector2 currentCell;

		while (stack.Count > 0) {
			currentCell = getRandomCell();
			stack.Remove(currentCell);

			if (getMaze(currentCell) != 0) {
				continue;
			}

			markCell(currentCell, -1);

			foreach(Vector2 neighbour in getNeighbours(currentCell)) {
				Vector2 diff = (neighbour-currentCell);
				Vector2 orthoDiff = new Vector2(diff.y, diff.x);
				Vector2 furtherNeighbour = currentCell + 2*diff;
				Vector2 right = currentCell + orthoDiff;
				Vector2 left = currentCell - orthoDiff;

				// Neighbour must be unexplored, and furtherNeighbour must not be a maze cell
				if (getMaze(neighbour) == 0
				    && getMaze(furtherNeighbour) <= 0
				    && getMaze(right) <= 0
				    && getMaze(left) <= 0) {
					// This cell is part of the maze
					markCell(currentCell, 1);
					addNeighbours(currentCell);
					break;
				}

			}
		}
	}

	void addNeighbours(Vector2 cell) {
		foreach(Vector2 neighbour in getNeighbours(cell)) {
			if (getMaze(neighbour) == 0)
				toExplore(neighbour);
		}
	}

	void markCell(Vector2 cell, int type) {
		maze[(int)cell.x, (int)cell.y] = type;
	}

	Vector2 getRandomCell() {
		int index = Random.Range (0, stack.Count - 1);

		return stack[index];
	}

	Vector2[] getNeighbours(Vector2 cell) {
		List<Vector2> neigh = new List<Vector2> ();

		if (cell.x > 0)
			neigh.Add (cell + new Vector2(-1, 0));
		if (cell.x < width-1)
			neigh.Add (cell + new Vector2(1, 0));

		if (cell.y > 0)
			neigh.Add (cell + new Vector2(0, -1));
		if (cell.y < height-1)
			neigh.Add (cell + new Vector2(0, 1));

		return neigh.ToArray();
	}

	bool filled(Vector2 cell) {
		return getMaze (cell.x, cell.y) > 0;
	}

	int getMaze(Vector2 v) {
		return getMaze (v.x, v.y);
	}

	int getMaze(float x, float y){
		if (x < 0 || x >= width)
			return -1;
		if (y < 0 || y >= height)
			return -1;

		return maze[(int)x, (int)y];
	}

	void toExplore(int x, int y) {
		stack.Add (new Vector2 (x, y));
	}

	void toExplore(Vector2 v) {
		stack.Add (v);
	}

}
