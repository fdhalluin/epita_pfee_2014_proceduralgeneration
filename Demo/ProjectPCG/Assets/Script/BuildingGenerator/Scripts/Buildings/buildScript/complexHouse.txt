# Design document, used as a model
# ?(a, b) will be replaced by a random integer between a and b (WHEN IMPLEMENTED)

build mainRoom 0 20

mainRoom:
size 20 10
height 4
windows NORTH 1 3 1 SQUARE
windows SOUTH 1 5 1 SQUARE
windows EAST 1 7 1 SQUARE
windows EAST 1 8 1 SQUARE

windows NORTH 3 3 1 SQUARE
windows SOUTH 3 5 1 SQUARE
windows EAST 3 7 1 SQUARE
windows EAST 3 8 1 SQUARE
addRoom NORTH wing FIRST_MATCH
roof 3 POINT 0.6
#addRoom EAST secondRoom CENTER

#roof 0 FLAT
#onTop secondRoom 2 2


onTop chimney 2 7

chimney:
size 1 1
height 2

wing:
size 8 10
height 3
roof 2 POINT 0.6
windows EAST 0 6 1 DOOR
windows NORTH 1 2 1 SQUARE
windows EAST 1 3 1 SQUARE

secondRoom:
size 15 8
height 3
roof 5 POINT 0.6
windows NORTH 1 2 3 CIRCLE
windows NORTH 2 2 3 SEMICIRCULAR
onTop chimney 2 7