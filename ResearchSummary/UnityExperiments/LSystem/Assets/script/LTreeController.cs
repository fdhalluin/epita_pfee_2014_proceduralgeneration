﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class Ltree
{
	public Vector3 position;
	public Vector3 rotation;
	public GameObject progenitor = null;
	
	public Ltree()
	{
	}
	
	public Ltree(Vector3 position, Vector3 rotation)
	{
		this.position = position;
		this.rotation = rotation;
	}
}

public class LTreeController : MonoBehaviour 
{
	private float initial_length = 1;
	private float initial_radius = 0.5f;
	private int iteration = 4;
	private List<char> variable = new List<char>();
	private List<string> rules = new List<string>();
	private string ltree = "";
	
	
	List<Ltree> list = new List<Ltree>();
	float angle = 22;
	float length = 0.03f;
	public Material lineMaterial;
	
	// Use this for initialization
	void Start () 
	{
		init ();
		constructLSystem ();
	}
	
	
	
	public void constructLSystem()
	{
		Ltree rootNode = new Ltree (new Vector3(0,0,0), new Vector3(1,45,1));
		list.Add (rootNode);
		
		for (int i = 0; i < ltree.Length; ++i) 
		{
			Ltree tree2 = new Ltree();
			tree2.position = list[list.Count - 1].position;
			tree2.rotation = list[list.Count - 1].rotation;
			tree2.progenitor = list[list.Count - 1].progenitor;
			
			
			if (ltree[i] == '[')
			{
				list.Add(tree2);
			}
			else if (ltree[i] == ']')
			{
				list.RemoveAt(list.Count - 1);
				
				
			}
			else if (ltree[i] == '+')
			{
				list[list.Count - 1].rotation = Quaternion.AngleAxis(angle, Vector3.forward)*list[list.Count - 1].rotation;
				//list[list.Count - 1].rotation.Normalize();
				
			}
			else if (ltree[i] == '-')
			{
				list[list.Count - 1].rotation = Quaternion.AngleAxis(-angle, Vector3.forward)*list[list.Count - 1].rotation;
				//list[list.Count - 1].rotation.Normalize();
				
			}
			else if (ltree[i] == '&')
			{
				list[list.Count - 1].rotation = Quaternion.AngleAxis(angle, Vector3.right)*list[list.Count - 1].rotation;
				//list[list.Count - 1].rotation.Normalize();
			}
			else if (ltree[i] == '^')
			{
				list[list.Count - 1].rotation = Quaternion.AngleAxis(-angle, Vector3.right)*list[list.Count - 1].rotation;
				//list[list.Count - 1].rotation.Normalize();
			}
			else if (ltree[i] == '<')
			{
				list[list.Count - 1].rotation = Quaternion.AngleAxis(angle, Vector3.up)*list[list.Count - 1].rotation;
				//list[list.Count - 1].rotation.Normalize();
			}
			else if (ltree[i] == '>')
			{
				list[list.Count - 1].rotation = Quaternion.AngleAxis(-angle, Vector3.up)*list[list.Count - 1].rotation;
				//list[list.Count - 1].rotation.Normalize();
			}
			else if (ltree[i] == '|')
			{
				
			}
			else if (ltree[i] == 'F')
			{
				GameObject appearance = new GameObject();
				
				appearance.name = "Appearance";
				
				appearance.AddComponent<LineRenderer>();
				LineRenderer renderer = appearance.GetComponent<LineRenderer>();
				renderer.SetWidth(0.2f, 0.1f);
				renderer.SetVertexCount(2);
				renderer.SetPosition(0, list[list.Count - 1].position);
				renderer.SetPosition(1, list[list.Count - 1].position + list[list.Count - 1].rotation * length);
				renderer.material =  new Material (Shader.Find("Particles/Additive"));;
				
				list[list.Count - 1].position = list[list.Count - 1].position + list[list.Count - 1].rotation * length;
				//list[list.Count - 1].progenitor = progenitor;
			}
		}
	}
	
	
	public void init()
	{
		variable.Add('F');
		variable.Add('[');
		variable.Add(']');
		variable.Add('+');
		variable.Add('-');
		variable.Add ('&');
		variable.Add ('^');
		variable.Add ('<');
		variable.Add('>');
		variable.Add('|');
		
		rules.Add ("F[&+F]F[->F][->F][&F]");
		rules.Add ("[");
		rules.Add ("]");
		rules.Add ("+");
		rules.Add ("-");
		rules.Add ("&");
		rules.Add("^");
		rules.Add ("<");
		rules.Add(">");
		rules.Add("|");
		
		ltree = "F";
		
		for (int i = 0; i < iteration; ++i) 
		{
			int len = ltree.Length;
			string newString = "";
			
			for (int j = 0; j < len; ++j)
			{
				for (int k = 0; k < variable.Count; ++k)
				{
					if (ltree[j] == variable[k])
					{
						newString += rules[k];
						break;
					}
				}
			}
			
			ltree = newString;
		}
		
		Debug.Log(ltree);
	}
	
	
	// Update is called once per frame
	void Update () {
		
	}
}
