﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	private float mSpeed = 10.0f;

	void Start () {}
	
	void Update () {
		Vector3 lMovement = Vector3.zero;
			
		if (Input.GetKey("w")) {
			lMovement.z++;
		}
		if (Input.GetKey("s")) {
			lMovement.z--;
		}
		if (Input.GetKey("a")) {
			lMovement.x--;
		}
		if (Input.GetKey("d")) {
			lMovement.x++;
		}
		transform.Translate(lMovement * mSpeed * Time.deltaTime, Space.Self);
	}
}
