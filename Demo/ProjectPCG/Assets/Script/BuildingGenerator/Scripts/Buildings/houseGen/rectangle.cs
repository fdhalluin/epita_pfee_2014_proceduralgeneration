using UnityEngine;
using System;
using System.Collections.Generic;

namespace HouseGen {

// This class stores all the information needed to place one window
// in the building
public class Window {
	public Vector3 position;
	public WindowShape shape;
}

// This class stores all the information needed to place one pillar
// in the building
public class Pillar {
	// Grid-coordinates of the pillar
	public int x, y;
	// How many storeys this pillar spawns
	public int height;
	// The pillar style
	public Rectangle.PillarType pillarType;
	// The height where this pillar will start
	public int startHeight;
	
	public Pillar(int x, int y, int height, int startHeight = 0, Rectangle.PillarType pType = Rectangle.PillarType.CIRCLE) {
		this.x = x;
		this.y = y;
		this.height = height;
		this.pillarType = pType;
		this.startHeight = startHeight;
	}
}

// The Rectangle class contains all the information needed to create a room with
// windows and pillar outside of it. It also has links to the rooms it is placed
// next to thanks to the addRectangleToSide() method.
public class Rectangle {
	// The coordinates of the NORTH-EAST corner of the room, along with its width(w)
	// and length (named h, at it was its height in the two dimensional plane);
	public int x, y, w, h;
	// An identifier for the room
	public int roomId;
	// This enum is a way to differentiate between a room added to represent a Tower (mostly unused), or just another room
	public enum Type {SIMPLE=0, TOWER=1};
	// This enum defined the kind of roof the room
	public enum RoofType {FLAT=0, POINT=1, LINEAR=2, SLOPE=3};
	// This enum is used to differentiate different ways of placing a room alongside an other room
	public enum PositionType {RANDOM=0, COMPLETE_MATCH=1, CENTER=2, FIRST_MATCH=3};
	// This enum defines every type of pillar that can be used
	public enum PillarType {CIRCLE=0};
	
	// A reference to the room it has been placed on the side of
	public Rectangle parent;
	// The side of the parent room on which this room is.
	public Direction parentSide;
	// A reference to the storey this rooms is in.
	public Story floor;
	// A list of rooms placed along this one, sorted by side
	Dictionary<Direction, List<Rectangle>> sides;
	// A list of windows placed on this room, sorted by side
	Dictionary<Direction, List<Window>> windows;
	// Whether this room belong to a tower or is a simple room
	public Type roomType;
	// A tower is stored as a integer : (North/South) * 10 + (West/East);
	List<Rectangle> towerList;
	// An offset to determine how high should the roof go
	public float roofOffset;
	// How many storey are still needed for this room to be fully grown
	public int roomHeight;
	// The kind of roof used by this room
	public RoofType roofType;
	// A list of all rooms placed on top of this one
	public List<Rectangle> onTop;
	
	// The standard height of a wall
	public static int wallHeight = 2;
	
	// References to this room's layer ono the previous and next storeys
	public Rectangle previous = null;
	public Rectangle next = null;
	
	// Displacement for the floor and ceiling of this room. Thos are the value
	// that define how sloped the walls of a room will be.
	public float baseDispl = 0f;
	public float topDispl = 0f;
	
	// A lsit of displacement values for the next layers of the room
	public List<float> displacements;
	
	// An offset to determine how far should the roof extend horizontally
	public float extendedRoof = 0;
	
	// LINEAR and SLOPE roofs are orientation-dependant
	public Direction roofDirection;
	
	// This determines the thickness of a volumetric roof (used only when
	// lowModels mode is not enabled)
	public float roofDepth;
	
	// A list of all pillars belonging to this room
	public List<Pillar> pillars;
	
	public Rectangle(int xs, int ys, int ws, int hs) {
		x = xs;
		y = ys;
		w = ws;
		h = hs;
		
		sides = new Dictionary<Direction, List<Rectangle>>();
		sides[Direction.NORTH] = new List<Rectangle>();
		sides[Direction.SOUTH] = new List<Rectangle>();
		sides[Direction.WEST] = new List<Rectangle>();
		sides[Direction.EAST] = new List<Rectangle>();
		
		parentSide = Direction.NONE;
		roomType = Type.SIMPLE;
		
		roofOffset = ((float)Util.rand.Next(80, 220) / 100)*(wallHeight);
		roomHeight = 0;
		roofType = RoofType.FLAT;
		
		windows = new Dictionary<Direction, List<Window>>();
		windows[Direction.NORTH] = new List<Window>();
		windows[Direction.SOUTH] = new List<Window>();
		windows[Direction.WEST] = new List<Window>();
		windows[Direction.EAST] = new List<Window>();
		
		towerList = new List<Rectangle>();
		
		onTop = new List<Rectangle>();
		
		displacements = new List<float> {0, 0};
		
		roofDirection = Direction.NORTH;
		roofDepth = 0.2f;
		
		pillars = new List<Pillar>();
		
	}
	
	public Rectangle(int xs, int ys, int ws, int hs, Type t) : this(xs, ys, ws, hs) {
		roomType = t;
	}
	
	public Rectangle(int xs, int ys, int ws, int hs, int rH) : this(xs, ys, ws, hs) {
		roomHeight = rH;
		
		while (displacements.Count <= rH+1) {
			displacements.Add(displacements[displacements.Count-1]);
		}
	}
	
	// Creating a room from its previous layer representative allows to copy each important parameter easily
	public Rectangle(Rectangle previousRoom) : this(previousRoom.x, previousRoom.y, previousRoom.w, previousRoom.h, previousRoom.roomHeight-1){
		roofOffset = previousRoom.roofOffset;
		roofType = previousRoom.roofType;
		
		previousRoom.next = this;
		this.previous = previousRoom;
		this.setDisplacements(previousRoom.displacements.GetRange(1, previousRoom.displacements.Count-1));
		
		this.extendedRoof = previousRoom.extendedRoof;
		
		this.roofDirection = previousRoom.roofDirection;
		this.roofDepth = previousRoom.roofDepth;
		
		this.pillars = previousRoom.pillars;
	}
	
	// Returns all rooms added to a specific side of the room
	public List<Rectangle> getSide(Direction d) {
		return sides[d];
	}
	
	// Returns all windows added to a specific side of the room
	public List<Window> getWindows(Direction d) {
		return windows[d];
	}
	
	public Rectangle addRectangleToSide(Direction side, int rectangleSize, int minLen = 3, int maxLen = 10, PositionType positionType = PositionType.RANDOM, bool doNotAdd = false) {
		// The Rectangle ans will eventually be returned from this method
		Rectangle ans = new Rectangle(0, 0, 0, 0);
		Rectangle room = this;
		// Those are the direction in which the room will extend
		// They are used to generalized the search as much as possible, to avoid writing fours different loops.
		int dirX = 0, dirY = 0;
		
		// Set the search directions
		switch(side) {
			case Direction.NORTH: dirY = -1; break;
			case Direction.SOUTH: dirY = 1; break;
			case Direction.WEST: dirX = -1; break;
			case Direction.EAST: dirX = 1; break;
		}
		
		// Determine the search range
		int start=0, end=0;
		bool completeMatch = positionType==PositionType.COMPLETE_MATCH || positionType==PositionType.RANDOM ;
		switch (side) {
		case Direction.NORTH:
		case Direction.SOUTH:
			start = room.x - rectangleSize*(completeMatch?0:1) + 1;
			end = room.x + room.w - 1 - rectangleSize*(completeMatch?1:0);
			break;
			
		case Direction.WEST:
		case Direction.EAST:
			start = room.y - rectangleSize*(completeMatch?0:1) + 1;
			end = room.y + room.h - 1 - rectangleSize*(completeMatch?1:0);
			break;
		}
		
		// We cannot place the corner of the room in negative coordiantes, as they are not considered by the storey
		start = Math.Max(start, 0);
		
		// Search an area to place the new room
		int bestInd = -1, bestLen = -1;
		bool stopSearch = false;
		for (int ind = start; ind <= end; ++ind) {
			if (stopSearch) {
				break;
			}
			int x = 0, y = 0;
			// Determine where will the new room be placed on the designated side
			switch (side) {
			case Direction.NORTH:
			case Direction.SOUTH:
				x = ind; y = room.y + (1 - ((int)side)%2)*room.h;
				break;
			case Direction.WEST:
			case Direction.EAST:
				x = room.x + (1 - ((int)side)%2)*room.w; y = ind;
				break;
			}
			
			// Search the area in front of the side to determine whether the room can be placed here
			for (int len = 1; len <= maxLen; ++len) {
				int sum = 0; // if non zero, the search encountered an already-used area;
				if (stopSearch) {
					break;
				}
				
				if ((int)side >= (int)Direction.WEST) {
					// Horizontal Search
					int sX = 0;
					if (dirX == 1) sX = x+(len-1)*dirX;
					if (dirX == -1) sX = x+(len)*dirX;
					
					if (y+rectangleSize > floor.getHeight()) {
						break;
					}
					
					if (sX < 0 || sX >= floor.getWidth()) {
						break;
					}
					
					for (int sY = y; sY < y+rectangleSize; ++sY) {
						sum += floor.getPlan()[sX, sY];
					}				
					
					if (sum != 0) {
						break;
					}
					else {
						if (len > bestLen
						    || (len == bestLen && Util.rand.Next(100) > 85 && !(positionType == PositionType.FIRST_MATCH))
						    || (len >= bestLen && (positionType == PositionType.CENTER))) {
							bestInd = ind;
							bestLen = len;
							if (y+rectangleSize/2 >= this.y + this.h/2){
								stopSearch = true;
							}
						}
					}
				}
				else {
					// Vertical Search
					int sY = 0;
					if (dirY == 1) sY = y+(len-1)*dirY;
					if (dirY == -1) sY = y+(len)*dirY;
					
					if (x+rectangleSize > floor.getWidth()) {
						break;
					}
					
					if (sY < 0 || sY >= floor.getHeight()) {
						break;
					}
					
					for (int sX = x; sX < Math.Min(x+rectangleSize, floor.getWidth()-1); ++sX) {
						sum += floor.getPlan()[sX, sY];
					}				
					
					if (sum != 0) {
						break;
					}
					else {
						if (len > bestLen
						    || ((len == bestLen && Util.rand.Next(100) > 85 && !(positionType == PositionType.FIRST_MATCH)))
						    || (len >= bestLen && (positionType == PositionType.CENTER))) {
							bestInd = ind;
							bestLen = len;
							if (x+rectangleSize/2 >= this.x + this.w/2){
								stopSearch = true;
							}
						}
					}
				}
				
				
			}
			
		}
		
		// Then, we will construct the room
		if ((int)side >= (int)Direction.WEST) {
			// Horizontal Construction of the room
			int xF = room.x + (dirX==1?room.w:0) + (dirX==-1?-bestLen:0);
			int yF = bestInd;
			ans = new Rectangle(xF, yF, bestLen, rectangleSize, Math.Max(roomHeight-1, 0));
			
			if (!(ans.y < room.y) && !(ans.y+ans.h > room.y + room.h)) {
				float ansMiddle = ans.y + (float)ans.h/2;
				ansMiddle -= this.y;
				float roofOffsetPerc = 1- (((float)Math.Abs(room.h/2 - ansMiddle))/(room.h/2));
				ans.roofOffset = roofOffset*(roofOffsetPerc-0.01f); // not exactly the same to avoid z-fighting
			}
		}
		else {
			// Vertical Construction of the room
			int yF = room.y + (dirY==1?room.h:0) + (dirY==-1?-bestLen:0);
			int xF = bestInd;
			ans = new Rectangle(xF, yF, rectangleSize, bestLen, Math.Max(roomHeight-1, 0));
			
			if (!(ans.x < room.x) && !(ans.x+ans.w > room.x + room.w)) {
				float ansMiddle = ans.x + (float)ans.w/2;
				ansMiddle -= this.x;
				float roofOffsetPerc = 1- (((float)Math.Abs(room.w/2 - ansMiddle))/(room.w/2));
				ans.roofOffset = roofOffset*(roofOffsetPerc-0.01f); // not exactly the same to avoid z-fighting
			}
			
		}
		
		// If we are only testing for the place needed to add a room, we do not
		// want to add the room to our side
		if (!doNotAdd) {
			addGeneratedRoomToSide(side, ans);
		}
		
		return ans;
	}
	
	// This methods adds a tower to a specified corner of a room
	public Rectangle addTower(Direction vertical, Direction horizontal, int radius, int height = 4) {
		Rectangle ans = new Rectangle(-1, -1, -1, -1, height);
		int xT = x - radius ;
		int yT = y - radius ;
		if (vertical == Direction.SOUTH) yT += h-1;
		if (horizontal == Direction.EAST) xT += w-1;
		
		// Check if there is enough space around the corner to add the tower
		bool valid = true;
		int[,] roomPlan = floor.getRoomPlan();
		for (int i = 0; i < 1+2*radius; ++i) {
			for (int j = 0; j < 1+2*radius; ++j) {
				if (roomPlan[xT+i, yT+j] != 0 && roomPlan[xT+i, yT+j] != this.roomId) {
					// The tower cannot overlap a room other than the one it is added to
					valid = false;
					break;
				}
			}
			if (!valid) break;
		}
		
		if (valid) {
			ans.x = xT;
			ans.y = yT;
			ans.w = 1 + 2*radius;
			ans.h = 1 + 2*radius;
			
			// Save this room in the tower list
			int towerIndex = ((int)vertical)*10 + (int)horizontal;
			while (towerList.Count <= towerIndex) {
				towerList.Add(null);
			}
			towerList.Insert(towerIndex, ans);
			
			ans.parentSide = Direction.NONE;
			ans.roofType = RoofType.POINT;
			ans.roofOffset = 4;
			floor.addRoom(ans);
		}
		
		return ans;
	}
	
	// This method adds pillars to the room, spacing them according to a specified distance
	// between two pillars and checking that the space is not occupied before adding a pillar.
	public void addPillars(Direction side, int dist, int spacing, int startHeight, int height) {
		int[,] plan = floor.getPlan();
		int[,] roomPlan = floor.getRoomPlan();
		if ((int)side >= (int)Direction.WEST) {
			// Vertical line of pillars
			int xS = x;
			if (side == Direction.WEST) xS -= dist;
			else xS += w + dist - 1;
			
			int spaceCount = spacing/2;
			for (int yS = y; yS < y+h; ++yS) {
				if (plan[xS, yS] != 0 || roomPlan[xS, yS] != 0) {
					spaceCount = spacing/2;
				}
				else {
					if (spaceCount == 0) {
						plan[xS, yS] = (int)side + 4;
						Pillar pillar = new Pillar(xS, yS, height, startHeight);
						pillars.Add(pillar);
						spaceCount = spacing;
					}
					else {
						--spaceCount;
					}
				}
			}
			
		}
		else {
			// Horizontal line of pillars
			int yS = y;
			if (side == Direction.NORTH) yS -= dist;
			else yS += h + dist - 1;
			
			int spaceCount = spacing/2;
			for (int xS = x; xS < x+w; ++xS) {
				if (plan[xS, yS] != 0 || roomPlan[xS, yS] != 0) {
					spaceCount = spacing/2;
				}
				else {
					if (spaceCount == 0) {
						plan[xS, yS] = (int)side + 4;
						Pillar pillar = new Pillar(xS, yS, startHeight, height);
						pillars.Add(pillar);
						spaceCount = spacing;
					}
					else {
						--spaceCount;
					}
				}
			}
		}
	}
	
	// This method was mostly copied from the above addPillars method
	// The size argument is the size of the window itself (how many units it spans)
	public void addWindows(Direction side, int size, int spacing, WindowShape shape = WindowShape.SQUARE) {
		int[,] plan = floor.getPlan();
		int[,] roomPlan = floor.getRoomPlan();
		//		int inward, 
		int outward;
		if ((int)side >= (int)Direction.WEST) {
			// Vertical line of windows
			outward = (side==Direction.EAST?1:-1);
			//			inward = -outward;
			int xS = x;
			if (side == Direction.EAST) xS += w - 1;
			
			int spaceCount = 0;
			// reduced interval so as to not put windows right on the corner of the wall
			for (int yS = y+1; yS < y+h-1; ++yS) {
				bool placeWindow = true;
				for (int sizeInd = 0; sizeInd < size; ++sizeInd) {
					if (yS+sizeInd >= y+h-1 || roomPlan[xS, yS+sizeInd] != roomId || (xS+outward >= 0 && xS+outward < floor.getWidth() && roomPlan[xS+outward, yS+sizeInd] != 0) ) {
						spaceCount = spacing/2;
						placeWindow = false;
						break;
					}
				}
				
				if (placeWindow) {
					if (spaceCount == 0) {
						for (int sizeInd = 0; sizeInd < size; ++sizeInd) {
							plan[xS, yS+sizeInd] = (int)side + 4;
						}
						Window window = new Window();
						window.position = new Vector3(xS + (side==Direction.EAST?1:0), size, yS);
						window.shape = shape;
						windows[side].Add(window);
						spaceCount = spacing;
						yS += size-1;
					}
					else {
						--spaceCount;
					}
				}
				
			}
			
		}
		else {
			outward = (side==Direction.SOUTH?1:-1);
			//			inward = -outward;
			int yS = y;
			if (side == Direction.SOUTH) yS += h - 1;
			
			int spaceCount = 0;
			// reduced interval so as to not put windows right on the corner of the wall
			for (int xS = x+1; xS < x+w-1; ++xS) {
				bool placeWindow = true;
				for (int sizeInd = 0; sizeInd < size; ++sizeInd) {
					if (xS+sizeInd >= x+w-1 || roomPlan[xS+sizeInd, yS] != roomId || (yS+outward >= 0 && yS+outward < floor.getHeight() && roomPlan[xS+sizeInd, yS+outward] != 0) ) {
						spaceCount = spacing/2;
						placeWindow = false;
						break;
					}
				}
				
				if (placeWindow) {
					if (spaceCount == 0) {
						for (int sizeInd = 0; sizeInd < size; ++sizeInd) {
							plan[xS+sizeInd, yS] = (int)side + 4;
						}
						Window window = new Window();
						window.position = new Vector3(xS, size, yS + (side==Direction.SOUTH?1:0));
						window.shape = shape;
						windows[side].Add(window);
						spaceCount = spacing;
						xS += size-1;
					}
					else {
						--spaceCount;
					}
				}
				
			}
		}
	}
	
	// This method adds a single window to a specific side of the room. As opposed to bulk-adding windows
	// with the above addWindows method, it does not check for walls or other windows before adding it.
	public void addSingleWindow(Direction side, float offset, WindowShape shape = WindowShape.SQUARE) {
		float xS = x;
		float yS = y;
		
		if (side == Direction.EAST) xS += w - 1;
		if (side == Direction.SOUTH) yS += h - 1;
		
		if ((int)side >= (int)Direction.WEST) {
			yS += offset;
		}
		else {
			xS += offset;
		}
		
		Window window = new Window();
		window.position = new Vector3(xS+ (side==Direction.EAST?1:0), 1, yS + (side==Direction.SOUTH?1:0));
		window.shape = shape;
		windows[side].Add(window);
		
	}
	
	// Get the Rectangle representing this room at a specified storey height
	public Rectangle getRoomAtStory(int story) {
		if (story <= 0) {
			return this;
		}
		else{
			if (next == null) {
				return null;
			}
		}
		
		return next.getRoomAtStory(story-1);		
	}
	
	// Get a specific tower added to this room. May return null if no tower was
	// added to the specified corner
	public Rectangle getTower(Direction vertical, Direction horizontal) {
		int towerIndex = ((int)vertical)*10 + (int)horizontal;
		if (towerIndex >= towerList.Count){
			return null;
		}
		
		return towerList[towerIndex];
	}
	
	// Add a newly-generated room to specific side of this room
	public void addGeneratedRoomToSide(Direction side, Rectangle room) {
		room.parent = this;
		room.parentSide = Util.reverse(side);
		sides[side].Add(room);		
		floor.addRoom(room);
	}
	
	// Get all the rooms added to every side of the house
	public Dictionary<Direction, List<Rectangle>> getSides() {
		return sides;
	}
	
	// Get the list of towers added to this room
	public List<Rectangle> getTowerList() {
		return towerList;
	}
	
	// Get all the rooms on top of this one
	public List<Rectangle> getOnTop() {
		return onTop;
	}
	
	// Returns the total count of rooms added to the side of this one.
	public int getRoomsOnSidesCount() {
		int northCount = sides[Direction.NORTH].Count;
		int southCount = sides[Direction.SOUTH].Count;
		int westCount = sides[Direction.WEST].Count;
		int eastCount = sides[Direction.EAST].Count;
		
		return northCount + southCount + westCount + eastCount;
	}
	
	// A setter method for the displacement values of the room
	public void setDisplacements(List<float> f) {
		displacements = f;
		
		// In case of the displacement list not being long enough,
		// which may happen in manually written scripts.
		while (displacements.Count <= roomHeight+2) {
			displacements.Add(0);
		}
		
		baseDispl = displacements[0];
		topDispl = displacements[1];
	}
	
}

}