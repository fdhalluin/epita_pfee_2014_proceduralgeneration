﻿using UnityEngine;
using System.Collections;
using CoherentNoise;
using CoherentNoise.Generation;
using CoherentNoise.Generation.Displacement;
using CoherentNoise.Generation.Fractal;
using CoherentNoise.Generation.Modification;
using CoherentNoise.Generation.Patterns;


/*
 *  Settings for the terrain
 * 	Generator is the noise for heights
 *	width of the terrain
 *	length of the terrain
 */

public sealed class TerrainSettings 
{
	private Generator m_Generator;
	private Generator perso_Generator;
	private int width;
	private int length;
	private bool discreteTerrain = false;
	private float waterLevel = 0f;
	private int seedTrees = 42;
	private float densTrees = 0.5f;
	private float deltaHeight = 0f;
	private float dilatTerrain = 1f;
	private int DiscreteDiv = 40;
	private int DiscreteMult = 40;
	private bool centerTerrain = false;
	private bool snowTerrain = false;
	private float[,,] alphaData;
	private float[,] heightData;


	private static TerrainSettings instance = null;
	private static readonly object padlock = new object();


	public TerrainSettings()
	{
		width = 1000;
		length = 1000;

		var desert = new Gain(
			new RidgeNoise(23478568)
			{
			OctaveCount = 8
		} * 0.6f, 0.4f);
		
		m_Generator = desert;
		perso_Generator = m_Generator;

	}

	public static TerrainSettings Instance
	{
		get
		{
			lock (padlock)
			{
				if (instance == null)
				{
					instance = new TerrainSettings();
				}
				return instance;
			}
		}
	}

	// m_generator
	public Generator getGenerator()
	{
		return m_Generator;
	}

	public void setGenerator(Generator generator)
	{
		m_Generator = generator;
	}

	// perso_generator
	public Generator getPersoGenerator()
	{
		return perso_Generator;
	}
	
	public void setPersoGenerator(Generator generator)
	{
		perso_Generator = generator;
	}

	// width
	public int getWidth()
	{
		return width;
	}

	public void setWidth(int value)
	{
		width = value;
	}

	// length
	public int getLength()
	{
		return length;
	}
	
	public void setLength(int value)
	{
		length = value;
	}

	// discrete terrain
	public bool getDiscreteTerrain()
	{
		return discreteTerrain;
	}
	
	public void setDiscreteTerrain(bool value)
	{
		discreteTerrain = value;
	}

	// center terrain
	public bool getCenterTerrain()
	{
		return centerTerrain;
	}
	
	public void setCenterTerrain(bool value)
	{
		centerTerrain = value;
	}

	// water lvl
	public float getWaterLevel()
	{
		return waterLevel;
	}
	
	public void setWaterLevel(float value)
	{
		waterLevel = value;
	}

	// seed trees
	public int getSeedTrees()
	{
		return seedTrees;
	}
	
	public void setSeedTrees(int value)
	{
		seedTrees = value;
	}

	// dens trees
	public float getDensTrees()
	{
		return densTrees;
	}
	
	public void setDensTrees(float value)
	{
		densTrees = value;
	}

	// dens trees
	public float getDeltaHeight()
	{
		return deltaHeight;
	}
	
	public void setDeltaHeight(float value)
	{
		deltaHeight = value;
	}

	// dilatation terrain
	public float getDilatTerrain()
	{
		return dilatTerrain;
	}
	
	public void setDilatTerrain(float value)
	{
		dilatTerrain = value;
	}

	// Discrete Div
	public int getDisDiv()
	{
		return DiscreteDiv;
	}
	
	public void setDisDiv(int value)
	{
		DiscreteDiv = value;
	}

	// Discrete Mult
	public int getDisMult()
	{
		return DiscreteMult;
	}
	
	public void setDisMult(int value)
	{
		DiscreteMult = value;
	}

	public void setAlphaMap(float[,,] iAlphaData) {
		alphaData = iAlphaData;
	}

	public float[,,] getAlphaMap() {
		return alphaData;
	}

	public void setAlphaHeight(float[,] iAlphaHeight) {
		heightData = iAlphaHeight;
	}
	
	public float[,] getAlphaHeight() {
		return heightData;
	}

	// Snow Terrain
	public bool getSnowTerrain()
	{
		return snowTerrain;
	}
	
	public void setSnowTerrain(bool value)
	{
		snowTerrain = value;
	}


}