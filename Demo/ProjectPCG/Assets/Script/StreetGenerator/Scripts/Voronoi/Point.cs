using System;
using System.Collections.Generic;
using UnityEngine;

namespace graph
{
	[System.Serializable]
	public class Point
	{
		/// <summary>The following element are used when building a world map</summary>
		internal int elevation;
		public bool primordial = false;

		public static int count = 0;

		public float x = 0;
		public int id = -1;
		public float y = 0;

		public CityNode cityNode = null;

		public Point()
		{
			id = count++; 
		}

		public virtual int getElevation()
		{
			return elevation;
		}

		public virtual void setElevation(int elevation)
		{
			this.elevation = Math.Min(Math.Max(0, elevation), 255);
		}

		public Point(float x, float y) : this()
		{
			this.x = x;
			this.y = y;
		}

		public Point(Point p) : this()
		{
			this.x = p.x;
			this.y = p.y;
		}

		public double distance(Point p2) {
			return Math.Sqrt( Math.Pow(this.x - p2.x, 2) + Math.Pow(this.y - p2.y, 2)  );
		}

		public static graph.Point average(params graph.Point[] points)
		{
			float cx = 0;
			float cy = 0;
			int size = points.Length;
			foreach (graph.Point p in points)
			{
				cx += p.x;
				cy += p.y;
			}
			return new graph.Point(cx / size, cy / size);
		}

		public static graph.Point average(System.Collections.Generic.ICollection<graph.Point> points)
		{
			float cx = 0;
			float cy = 0;
			int size = points.Count;
			foreach (graph.Point p in points)
			{
				cx += p.x;
				cy += p.y;
			}
			return new graph.Point(cx / size, cy / size);
		}

		public override string ToString()
		{
			return "(" + x + " " + y + ")";
		}

		public Vector3 toVector3() {
			return new Vector3(x, 0, y);
		}
	}
}
