﻿using System;
using CoherentNoise;
using CoherentNoise.Generation;
using CoherentNoise.Generation.Displacement;
using CoherentNoise.Generation.Fractal;
using CoherentNoise.Generation.Modification;
using CoherentNoise.Generation.Patterns;
using CoherentNoise.Texturing;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using UnityEditor;


/*
 * Editor window for the terrain
 */

public class TerrainEditor : EditorWindow 
{
	private TerrainGenerator terrain;

	// Settings for the terrain
	private TerrainSettings terSet = new TerrainSettings();
	private Generator m_Generator;
	private int intFieldLength = 1000;
	private int intFieldWidth = 1000;


	// Noise 1
	private int selectionGridInt = 0;
	private string[] selectionStrings = {"PinkNoise", "BillowNoise", "RidgeNoise"};

	private int hSliderOctave = 5;
	private float hSliderFreq = 0.46f;
	private float hSliderMult = 1f;
	private int intFieldSeed = 42;
	private float hSliderMod = 1f;

	private int toolbarInt = 0;
	private string[] toolbarStrings = {"Nothing", "Bias", "Gain"};
	
	// Blend 2 noises
	private bool BlendBool = false;

	// Noise 2
	private int selectionGridInt2 = 0;

	private int hSliderOctave2 = 5;
	private float hSliderFreq2 = 0.46f;
	private float hSliderMult2 = 1f;
	private int intFieldSeed2 = 42;
	private float hSliderMod2 = 1f;

	private int toolbarInt2 = 0;

	// Custom Noises
	private bool TerrainBool = true;
	private int selectionTerrainInt = 0;
	private string[] selectionTerrainStrings = {"Terrain1", "Terrain2", "Terrain3", "Terrain4"};
	private float hSliderMultTerrain = 1f;

	private Generator CustGenerator;




	[MenuItem ("Window/Terrain Editor")]
	
	static void Init () {
		TerrainEditor window = (TerrainEditor) EditorWindow.GetWindow(typeof (TerrainEditor));
	}



	void OnGUI () {
		
		TerrainBool = EditorGUI.Toggle (new Rect (10, 10, 165, 20), "Custom Terrain", TerrainBool);
		CreateGuiSize ();
		CreateGuiTrees ();
		
		if (!TerrainBool)
		{
		//	GUI.Box (new Rect (10, 10, 550, 350), "Test Terrain");
		
			BlendBool = EditorGUI.Toggle (new Rect (25, 320, 165, 30), "Blend 2 noises", BlendBool);
			
			CreateGuiNoise1();
			
			if (BlendBool)
				CreateGUINoise2();

			if(GUI.Button(new Rect(25,450,80,20), "Generate"))
			{
				if (intFieldSeed < 0)
					intFieldSeed = 42;
				
				if (intFieldSeed2 < 0)
					intFieldSeed2 = 42;
				
				GenerateNoise1(intFieldSeed);
				
				if (BlendBool)
					GenerateNoise2(intFieldSeed2);

				GenerateTerrain();
			}
		}
		else
			GenerateCustomNoise();
		
	}

	// GUI for trees
	private void CreateGuiTrees()
	{
		if (GUI.Button (new Rect (25,420, 80, 20), "Add Trees")) 
		{
			terrain = FindObjectOfType<TerrainGenerator>();
			terrain.AddTrees();
		}

		if (GUI.Button (new Rect (115,420, 85, 20), "Delete Trees")) 
		{
			terrain = FindObjectOfType<TerrainGenerator>();
			terrain.ClearTrees();
		}


	}

	/*
	 * Generate a terrain with seletected parameter
	 */
	private void GenerateTerrain()
	{
		if (intFieldWidth < 500)
			intFieldWidth = 500;
		
		if (intFieldWidth > 3000)
			intFieldWidth = 3000;

		if (intFieldLength < 500)
			intFieldLength = 500;

		if (intFieldLength > 3000)
			intFieldLength = 3000;
		
		terSet.setWidth (intFieldWidth);
		terSet.setLength (intFieldLength);
		terSet.setGenerator (m_Generator);

		terrain = FindObjectOfType<TerrainGenerator>();
		terrain.CreateTerrain(terSet);		
	}

	/*
	 * GUI for the size of the terrain
	 */
	private void CreateGuiSize()
	{
		intFieldWidth = EditorGUI.IntField(new Rect(25, 360, 200, 20),
		                               "Terrain Width:", 
		                               intFieldWidth);
		intFieldLength = EditorGUI.IntField(new Rect(25,390, 200, 20),
		                                     "Terrain Length:", 
		                                     intFieldLength);

		if (intFieldWidth < 500)
			intFieldWidth = 500;
		
		if (intFieldWidth > 3000)
			intFieldWidth = 3000;

		if (intFieldLength < 500)
			intFieldLength = 500;
		
		if (intFieldLength > 3000)
			intFieldLength = 3000;
	}

	/*
	 * GUI for noise1
	 */
	private void CreateGuiNoise1()
	{
		selectionGridInt = GUI.SelectionGrid (new Rect (25, 40, 255, 30), selectionGridInt, selectionStrings, 3);
		toolbarInt = GUI.Toolbar (new Rect (25, 200, 255, 30), toolbarInt, toolbarStrings);

		hSliderOctave = EditorGUI.IntSlider (new Rect (120, 85, 160, 20), hSliderOctave, 1, 10);
	//	hSliderOctave = GUI.HorizontalSlider (new Rect (25, 120, 100, 30), hSliderOctave, 0.0f, 10.0f);
		if (selectionGridInt != 2)
			hSliderFreq = EditorGUI.Slider (new Rect (120, 115, 160, 20), hSliderFreq, 0.0f, 1.0f);
		hSliderMult = EditorGUI.Slider (new Rect (120, 145, 160, 20), hSliderMult, 0.0f, 1.0f);
		hSliderMod = EditorGUI.Slider (new Rect (120, 245, 160, 20), hSliderMod, 0f, 1.0f);
		
		intFieldSeed = EditorGUI.IntField (new Rect (25, 285, 200, 20), "Seed:", intFieldSeed);

		if (intFieldSeed <= 0)
			intFieldSeed = 0;

		// Label for sliders
		EditorGUI.LabelField (new Rect (25,85,90,50), "Octave:");
		if (selectionGridInt != 2)
			EditorGUI.LabelField (new Rect (25,115,90,50), "Persistence:");
		EditorGUI.LabelField (new Rect (25,145,90,50), "Multiplication:");
		EditorGUI.LabelField (new Rect (25,245,90,50), "Modification:");
	}


	/*
	 *  GUI for noise2 if blend 2 noises is selected
	 */
	private void CreateGUINoise2()
	{
		selectionGridInt2 = GUI.SelectionGrid (new Rect (300, 40, 255, 30), selectionGridInt2, selectionStrings, 3);
		toolbarInt2 = GUI.Toolbar (new Rect (300, 200, 255, 30), toolbarInt2, toolbarStrings);

		hSliderOctave2 = EditorGUI.IntSlider (new Rect (395, 85, 160, 20), hSliderOctave2, 1, 10);
		if (selectionGridInt2 != 2)
			hSliderFreq2 = EditorGUI.Slider (new Rect (395, 115, 160, 20), hSliderFreq2, 0.0f, 1.0f);
		hSliderMult2 = EditorGUI.Slider (new Rect (395, 145, 160, 20), hSliderMult2, 0.0f, 1.0f);
		hSliderMod2 = EditorGUI.Slider (new Rect (395, 245, 160, 20), hSliderMod2, 0f, 1.0f);
		
		intFieldSeed2 = EditorGUI.IntField (new Rect (300, 285, 200, 20), "Seed:", intFieldSeed2);

		if (intFieldSeed2 <= 0)
			intFieldSeed2 = 0;
		
		// Label for sliders
		EditorGUI.LabelField (new Rect (300,85,90,50), "Octave: ");
		if (selectionGridInt2 != 2)
			EditorGUI.LabelField (new Rect (300,115,90,50), "Persistence:");
		EditorGUI.LabelField (new Rect (300,145,90,50), "Multiplication:");
		EditorGUI.LabelField (new Rect (300, 245,90,50), "Modification:");
	}

	/*
	 * Create Generator for noise1 with selected parameters
	 */
	private void GenerateNoise1(int seed1)
	{
		Generator generator1;
		
		if (selectionGridInt == 0)
		{
			var noise1 = new PinkNoise(seed1)
			{
				OctaveCount = hSliderOctave,
				Persistence = hSliderFreq
			};
			generator1 = noise1;
		}
		else if (selectionGridInt == 1)
		{
			var noise2 = new BillowNoise(seed1)
			{
				OctaveCount = hSliderOctave,
				Persistence = hSliderFreq
			};
			generator1 = noise2;
		}
		else
		{
			var noise3 = new RidgeNoise(seed1)
			{
				OctaveCount = hSliderOctave
			};
			//noise3.Frequency = (int) hSliderFreq;
			generator1 = noise3;
		}
		
		m_Generator = generator1 * hSliderMult;
		
		if (toolbarInt == 1)
			m_Generator = new Bias(m_Generator, hSliderMod);
		else if (toolbarInt == 2)
			m_Generator = new Gain(m_Generator, hSliderMod);


		terSet.setGenerator (m_Generator);
	}

	/*
	 * Create Generator with selected parameters
	 */

	private void GenerateNoise2(int seed2)
	{
		Generator generator2;
		Generator weight;
		
		if (selectionGridInt2 == 0)
		{
			var noise1 = new PinkNoise(seed2)
			{
				OctaveCount = hSliderOctave2,
				Persistence = hSliderFreq2
			};
			generator2 = noise1;
		}
		else if (selectionGridInt2 == 1)
		{
			var noise2 = new BillowNoise(seed2)
			{
				OctaveCount = hSliderOctave2,
				Persistence = hSliderFreq2
			};
			generator2 = noise2;
		}
		else
		{
			var noise3 = new RidgeNoise(seed2)
			{
				OctaveCount = hSliderOctave2
			};
			//noise3.Frequency = (int) hSliderFreq;
			generator2 = noise3;
		}
		generator2 *= hSliderMult2;
		
		if (toolbarInt2 == 1)
			generator2 = new Bias(generator2, hSliderMod2);
		else if (toolbarInt2 == 2)
			generator2 = new Gain(generator2, hSliderMod2);
		
		weight = new Cache(new PinkNoise(346546)
		                   {
			Frequency = 0.5f,
			OctaveCount = 3
		}.ScaleShift(0.5f, 0.5f));
		
		m_Generator = m_Generator.Blend(generator2, weight).ScaleShift(0.5f, 0.5f);

		terSet.setGenerator (m_Generator);
	}

	/*
	 * GUI for Custom noise
	 */

	private void GenerateCustomNoise()
	{
//		GUI.Box (new Rect (10, 10, 270, 350), "Test Terrain");
		
		m_Generator = CustGenerator;
		selectionTerrainInt = GUI.SelectionGrid (new Rect (25, 40, 255, 60), selectionTerrainInt, selectionTerrainStrings, 2);
		hSliderMultTerrain = EditorGUI.Slider (new Rect (120, 120, 160, 20), hSliderMultTerrain, 0.0f, 1.0f);
		EditorGUI.LabelField (new Rect (25,120,90,50), "Multiplication:");
		
		if(GUI.Button(new Rect(25,450,80,20), "Generate"))
		{
			if (selectionTerrainInt == 0)
			{
				CreateCustomNoise1();
			}
			else if (selectionTerrainInt == 1)
			{
				CreateCustomNoise2();
			}
			else if (selectionTerrainInt == 2)
			{
				CreateCustomNoise3();
			}
			else
			{
				CreateCustomNoise4();
			}
			
			CustGenerator = m_Generator;
			m_Generator *= hSliderMultTerrain;
			terSet.setGenerator (m_Generator);

			GenerateTerrain();
		}
		
		if(GUI.Button(new Rect(25,140,80,20), "Modif Size"))
		{
			m_Generator *= hSliderMultTerrain;
			terSet.setGenerator (m_Generator);

			GenerateTerrain();
		}
	}


	/*
	 * Parameters for custom noise1
	 */
	private void CreateCustomNoise1()
	{
		var desert = new Gain(
			new RidgeNoise((int)Random.Range(0, 99999999))
			{
			OctaveCount = 8
		} * 0.6f, 0.4f);
		
		var hills = new PinkNoise((int)Random.Range(0, 99999999))
		{
			OctaveCount = 5,
			Persistence = 0.56f 
		};

		Generator weight = new Cache(new PinkNoise(346546)
		{
			Frequency = 0.5f,
			OctaveCount = 3
		}.ScaleShift(0.5f, 0.5f));
		
		m_Generator = desert.Blend(hills, weight).ScaleShift(0.5f, 0.5f);
		terSet.setGenerator (m_Generator);
	}

	/*
	 * Parameters for custom noise2
	 */
	private void CreateCustomNoise2()
	{
		var noise = new Bias(
			new PinkNoise((int)Random.Range(0, 99999999))
			{
			OctaveCount = 5,
			Persistence = 0.4f
		}, 0.4f);
		
		m_Generator = noise;
		terSet.setGenerator (m_Generator);
	}
	
	/*
	 * Parameters for custom noise3
	 */
	private void CreateCustomNoise3()
	{
		var noise = new BillowNoise(125)
		{
			OctaveCount = 5,
			Persistence = 0.44f
		};

		var noise2 = new PinkNoise(112)
		{
			OctaveCount = 5,
			Persistence = 0.43f
		};

		Generator weight = new Cache(new PinkNoise(346546)
		{
			Frequency = 0.5f,
			OctaveCount = 3
		}.ScaleShift(0.5f, 0.5f));
		
		m_Generator = noise.Blend(noise2, weight).ScaleShift(0.5f, 0.5f);
		terSet.setGenerator (m_Generator);
	}
	
	/*
	 * Parameters for custom noise4
	 */
	private void CreateCustomNoise4()
	{
		var noise = new BillowNoise(45645)
		{
			OctaveCount = 5,
			Persistence = 0.44f
		};
		
		var noise2 = new PinkNoise(8)
		{
			OctaveCount = 5,
			Persistence = 0.43f
		};
		
		Generator weight = new Cache(new PinkNoise(346546)
		                             {
			Frequency = 0.5f,
			OctaveCount = 3
		}.ScaleShift(0.5f, 0.5f));
		
		m_Generator = noise.Blend(noise2, weight).ScaleShift(0.5f, 0.5f);
		terSet.setGenerator (m_Generator);
	}

}