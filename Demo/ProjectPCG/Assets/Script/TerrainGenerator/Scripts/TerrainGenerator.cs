using System;
using CoherentNoise;
using CoherentNoise.Generation;
using CoherentNoise.Generation.Displacement;
using CoherentNoise.Generation.Fractal;
using CoherentNoise.Generation.Modification;
using CoherentNoise.Generation.Patterns;
using CoherentNoise.Texturing;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using UnityEditor;
using System.Collections.Generic;
using HouseGen;

/*
 * Generate a terrain
 * Generate height and trees
 */


public class TerrainGenerator : MonoBehaviour
{
	public GameObject pTree;
	public Terrain Terrain;
	private HeightNode[,] mHMap;
	private HeightGraph mHeightGraph = new HeightGraph();
	private TerrainSettings terSet = TerrainSettings.Instance;
	
	void Start()
	{	
		Terrain.terrainData.size = new Vector3 (1000, 600, 1000);
	}
	
	// Update is called once per frame
	void Update()
	{
	}
	
	/*
	 * Generate the terrain
	 * TerrainSettings give the information of the new terrain
	 */

	public void CreateTerrain()
	{
		Terrain.terrainData.size = new Vector3 ((float) terSet.getWidth(), 600, (float) terSet.getLength());
		terSet = TerrainSettings.Instance;
		float waterLevel = terSet.getWaterLevel();
		terSet.setDeltaHeight(0f);

		var td = Terrain.terrainData;

		// fixing resultions in code, as there's no place in editor to do that
		td.alphamapResolution = td.heightmapResolution;
		td.SetDetailResolution(td.heightmapWidth, 16);
		
		// arrays for various maps used in terrain
		var hm = new float[td.heightmapWidth, td.heightmapHeight]; // height
		float[,,] am = new float[td.heightmapWidth, td.heightmapHeight, 6]; // alpha (textures)
		var dm = new int[td.detailResolution, td.detailResolution]; // detail (grass)

		int minX = 0;
		int minZ = 0;
		float minHeight = 2.0f;


		int discreteDiv = terSet.getDisDiv ();
		int discreteMult = terSet.getDisMult ();

		// create heightmap data
		for (int ii = 0; ii < td.heightmapWidth; ++ii)
			for (int jj = 0; jj < td.heightmapHeight; ++jj)
		{
			// Domain coordinates. Each terrain is 1x1 in domain space
			float x = (float)ii / (td.heightmapWidth - 1);
			float y = (float)jj / (td.heightmapHeight - 1);

			var v = terSet.getGenerator().GetValue(x / terSet.getDilatTerrain(), y / terSet.getDilatTerrain(), 0);
			if (v <= 0)
				v = 0;

			if (terSet.getDiscreteTerrain())
				hm[ii, jj] = (float) Math.Round(v * discreteMult) / discreteDiv;
			else
				hm[ii, jj] = v;
	

			if (minHeight > hm[ii, jj])
			{
				minX = ii;
				minZ = jj;
				minHeight = hm[ii, jj];
			}
			
		}

		float[,] heights = hm;
		float delta = heights[minX, minZ];

		// If we need to center the terrqin

		if (terSet.getCenterTerrain()) 
		{

			KeyValuePair<KeyValuePair<int, int>, float> xy1 = getGoodTerrain((int) (td.heightmapWidth - 1) / 2, (int) (td.heightmapHeight - 1) / 2, 0);
			KeyValuePair<KeyValuePair<int, int>, float> xy2 = getGoodTerrain((int)(td.heightmapWidth - 1) / 2, (int) (td.heightmapHeight - 1) / 2, 1);
			KeyValuePair<KeyValuePair<int, int>, float> xy3 = getGoodTerrain((int)(td.heightmapWidth - 1) / 2, (int) (td.heightmapHeight - 1) / 2, 2);
			KeyValuePair<KeyValuePair<int, int>, float> xy4 = getGoodTerrain((int)(td.heightmapWidth - 1) / 2, (int) (td.heightmapHeight - 1) / 2, 3);



			var list = new List<KeyValuePair<KeyValuePair<int, int>, float>>();
			list.Add(xy1);
			list.Add(xy2);
			list.Add(xy3);
			list.Add(xy4);

			list.Sort(Compare2);

			float deltaX = ((float)list[0].Key.Key - ((td.heightmapWidth - 1) / 2)) / td.heightmapWidth;
			float deltaY = ((float)list[0].Key.Value - ((td.heightmapHeight - 1) / 2)) / td.heightmapHeight;


			if (deltaX != 0 || deltaY != 0)
			{
				for (int ii = 0; ii < td.heightmapWidth; ++ii)
					for (int jj = 0; jj < td.heightmapHeight; ++jj)
				{
					// Domain coordinates. Each terrain is 1x1 in domain space
					float x = (float)ii / (td.heightmapWidth - 1);
					float y = (float)jj / (td.heightmapHeight - 1);


					
					var v = terSet.getGenerator().GetValue((x / terSet.getDilatTerrain()) + deltaX, (y / terSet.getDilatTerrain()) + deltaY, 0);
					if (v <= 0)
						v = 0;
					
					if (terSet.getDiscreteTerrain())
						heights[ii, jj] = (float) Math.Round(v * discreteMult) / discreteDiv;
					else
						heights[ii, jj] = v;

					if (heights[ii, jj] < 0)
						heights[ii, jj] = 0;
					
					if (minHeight > heights[ii, jj])
					{
						minX = ii;
						minZ = jj;
						minHeight = heights[ii, jj];
					}
					
				}
			}
		}

		td.SetHeights(0, 0, heights);
		delta = heights[minX, minZ];
		RaiseHeightMap (td, -delta);

		AddWater(waterLevel);
		AddTrees(terSet.getDensTrees(), terSet.getSeedTrees());
		AddGrass(terSet.getDensTrees(), terSet.getSeedTrees() + 2);
		ClearRiver();
		ClearLake();
		terSet.setAlphaMap(td.GetAlphamaps(0, 0, td.heightmapWidth, td.heightmapHeight));
	}

	/*
	*	function compare to sort KeyValuePair<KeyValuePair<int, int>, float>
	*/

	static int Compare2(KeyValuePair<KeyValuePair<int, int>, float> a, KeyValuePair<KeyValuePair<int, int>, float> b)
	{
		return a.Value.CompareTo(b.Value);
	}

	/*
	 * Return the coordinate of the minimum height of a noise
	 * 
	 */

	public KeyValuePair<KeyValuePair<int, int>, float> getGoodTerrain(int x, int y, int i)
	{
		var td = Terrain.terrainData;

		float xx = (float)x / (td.heightmapWidth - 1);
		float yy = (float)y / (td.heightmapHeight - 1);

		float v = terSet.getGenerator().GetValue(xx / terSet.getDilatTerrain(), yy / terSet.getDilatTerrain(), 0);

		int delta = 20;

		if (i == 0) 
		{
			float xx2 = (float) (x-delta) / (td.heightmapWidth - 1);
			float yy2 = (float) y / (td.heightmapHeight - 1);
			
			float v2 = terSet.getGenerator().GetValue(xx2 / terSet.getDilatTerrain(), yy2 / terSet.getDilatTerrain(), 0);

			if (v2 <= v)
				return getGoodTerrain(x-delta, y, 0);
		}
		else if (i == 1) 
		{
			float xx2 = (float) (x+delta) / (td.heightmapWidth - 1);
			float yy2 = (float) y / (td.heightmapHeight - 1);
			
			float v2 = terSet.getGenerator().GetValue(xx2 / terSet.getDilatTerrain(), yy2 / terSet.getDilatTerrain(), 0);

			if (v2 <= v)
				return getGoodTerrain(x+delta, y, 1);
		}
		else if (i == 2)
		{
			float xx2 = (float) x / (td.heightmapWidth - 1);
			float yy2 = (float) (y-delta) / (td.heightmapHeight - 1);
			
			float v2 = terSet.getGenerator().GetValue(xx2 / terSet.getDilatTerrain(), yy2 / terSet.getDilatTerrain(), 0);

			if (v2 <= v)
				return getGoodTerrain(x, y-delta, 2);
		}
		else
		{
			float xx2 = (float) x / (td.heightmapWidth - 1);
			float yy2 = (float) (y+delta) / (td.heightmapHeight - 1);
			
			float v2 = terSet.getGenerator().GetValue(xx2 / terSet.getDilatTerrain(), yy2 / terSet.getDilatTerrain(), 0);

			if (v2 <= v)
				return getGoodTerrain(x, y+delta, 3);
		}

		return new KeyValuePair<KeyValuePair<int, int>, float> (new KeyValuePair<int, int>(x, y), v);
	}

	/*
	*	Generate Lake in a terrain
	*
	*/

	public void AddLake()
	{
		var td = Terrain.terrainData;
		float[,] heights = td.GetHeights(0, 0, td.heightmapWidth, td.heightmapHeight);
		terSet = TerrainSettings.Instance;
		float minVar = 1;
		float minmoyenne = 1;
	
		for (int i = 0; i < 20; ++i)
		{

			int minX = -1;
			int minY = -1;
			float min = 700;

			float[] value = new float[20*20];
			float moyenne = 0.0f;
				
			int lSeed = (int) Random.Range(1, 500);
			Random.seed = lSeed;

			int k = (int) Random.Range(10, td.heightmapHeight - 10);
			int j = (int) Random.Range(10, td.heightmapWidth - 10);

					
			int k1 = 0;
			int j1 = 0;

			for (int x = k - 10; x < k + 10; ++x)
			{
				j1 = 0;
				for (int y = j - 10; y < j + 10; ++y)
			{
					moyenne += heights[x,y];
					value[k1 * 20 + j1 % 20] = heights[x,y];

					if (heights[x,y] < min)
					{
						minX = x;
						minY = y;
						min = heights[x,y];
					}
					++j1;
				}
				++k1;
			}
			moyenne = moyenne / 20*20;
			
			float variance = 0;
			for (int z = 0; z < 20*20; ++z)
			{
				variance += Mathf.Pow((value[z] - moyenne), 2);
			}
			variance /= 20*20;
		
			if (variance < 0.02f)
			{
				if (heights[minX, minY] < 0.05f)
				{
					terSet.setDeltaHeight(terSet.getDeltaHeight() + 0.03f);
					RaiseHeightMap(td, 0.03f);
					min += 0.03f;

				}
				heights = td.GetHeights(0, 0, td.heightmapWidth, td.heightmapHeight);
				


				for (int x = k - 10; x < k + 10; ++x)
				{
					for (int y = j - 10; y < j + 10; ++y)
					{
						if (x == k - 10 || x == k + 9 || y == j - 10 || y == j + 9)
						{
							if (Random.value > 0.3f)
								heights[x,y] = min - 0.01f;
						}
						else
							heights[x,y] = min - 0.01f;
					}
				}

				heights[k-10,j-10] = min;
				heights[k-10,j+9] = min;
				heights[k+9,j-10] = min;
				heights[k+9,j+9] = min;

				GameObject water = new GameObject();
				water.name = "GeneratedLake";
				
				GameObject prefab = Resources.LoadAssetAtPath<GameObject>("Assets/Prefabs/Standard Assets/Water (Basic)/Daylight Simple Water.prefab");
	//			prefab.name = "Daylight Simple Water";
				td.SetHeights(0, 0, heights);
				GameObject prefabInstance = PrefabUtility.InstantiatePrefab (prefab) as GameObject;
				prefabInstance.transform.position = new Vector3 (j * Terrain.terrainData.size.x / td.heightmapWidth, (min - 0.004f) * Terrain.terrainData.size.y, k * Terrain.terrainData.size.z / td.heightmapHeight);
				prefabInstance.transform.localScale = new Vector3 (30, 1f, 30);
				prefabInstance.transform.parent = water.transform;

				break;

			}
		}	

	}

	/*
	*	Delete Lake
	*/

	public void ClearLake()
	{
		while (GameObject.Find ("GeneratedLake") != null)
			GameObject.DestroyImmediate(GameObject.Find("GeneratedLake"));
		
	}

	/*
	*	Raise the heightmap of the terrain by float delta
	*/

	public void RaiseHeightMap(TerrainData td, float delta)
	{
		float[,] heights = td.GetHeights(0, 0, td.heightmapWidth, td.heightmapHeight);
		float[,,] am = new float[td.heightmapWidth, td.heightmapHeight, 9];

		for (int ii = 0; ii < td.heightmapWidth; ++ii)
			for (int jj = 0; jj < td.heightmapHeight; ++jj) 
		{		
			heights[ii, jj] += delta;
			
			am = ChangeTexture (ii, jj, heights[ii, jj], am);
			
		}
		
		// apply changes to terrain	
		td.SetHeights(0, 0, heights);
		td.SetAlphamaps(0, 0, am);
	}

	/*
	*	Change the texture at the coordinate x z
	*/

	public float[,,] ChangeTexture(int x, int z, float height, float[,,] am)
	{
		terSet = TerrainSettings.Instance;

		am[x, z, 0] = 0;
		am[x, z, 1] = 0;
		am[x, z, 2] = 0;
		am[x, z, 3] = 0;
		am[x, z, 4] = 0;
		am[x, z, 5] = 0;
		am[x, z, 6] = 0;
		am[x, z, 7] = 0;
		am[x, z, 8] = 0;


		terSet = TerrainSettings.Instance;

		height = height - terSet.getWaterLevel () - terSet.getDeltaHeight();

		if (terSet.getSnowTerrain ())
		{
			if (height > 0.01f && height < 0.1f)
			{
				if (Random.value > 0.2f)
					am[x, z, 2] = 1;
				else
					am[x, z, 1] = 1;
			}
			else if (height >= 0.1f && height <= 0.15f)
			{
				if (Random.value > 0.02f)
					am[x, z, 2] = 1;
				else
				{
					
					am[x, z, 8] = 0.85f;
					am[x, z, 2] = 0.15f;
				}
			}
			else if (height >= 0.15f && height < 0.2f)
			{
				if (Random.value > 0.3f)
					am[x, z, 2] = 1;
				else
				{
					
					am[x, z, 8] = 0.85f;
					am[x, z, 2] = 0.15f;
				}
			}
			else if (height >= 0.2f && height < 0.25f)
			{
				if (Random.value > 0.1f)
				{
					am[x, z, 8] = 0.85f;
					am[x, z, 2] = 0.15f;
				}
				else
					am[x, z, 2] = 1;
			}
			else if (height >= 0.25f)
				am[x, z, 8] = 1;
			else
				am[x, z, 1] = 1;
		}
		else
		{
			if (height > 0.01f && height < 0.2f)
			{
				if (Random.value > 0.2f)
				{
					am[x, z, 2] = 1;
				}
				else
				{
					am[x, z, 1] = 1;
				}
			}
			else if (height >= 0.2f)
			{
				am[x, z, 2] = 1;
			}
			else
			{
				am[x, z, 1] = 1;
			}
		}

		return am;	
	}

	/*
	*	Change the texture at the coordinate x z in the texture numText
	*/

	public float[,,] ChangeOneTexture(int x, int z, int numText, float[,,] am)
	{
		am[x, z, 0] = 0;
		am[x, z, 1] = 0;
		am[x, z, 2] = 0;
		am[x, z, 3] = 0;
		am[x, z, 4] = 0;
		am[x, z, 5] = 0;
		am[x, z, 6] = 0;
		am[x, z, 7] = 0;
		am[x, z, 8] = 0;

		am[x, z, numText] = 1;

		return am;
	}

	/*
	*	Generate a river
	*/

	public void AddRiver(float waterLevel)
	{
		TerrainData td = Terrain.terrainData;
		terSet = TerrainSettings.Instance;
		terSet.setWaterLevel(waterLevel);

		float[,] heights = td.GetHeights(0, 0, td.heightmapWidth, td.heightmapHeight);

		if (!(terSet.getDeltaHeight () >= 0.1f && GameObject.Find ("GeneratedRiver") != null)) 
		{
			terSet.setDeltaHeight (0.1f);
			RaiseHeightMap (td, 0.1f);
		}

		mHeightGraph.mNodes.Clear();
		heights = td.GetHeights(0, 0, td.heightmapWidth, td.heightmapHeight);
		CreateHeightGraph (heights);
	
		int x1 = 0;
		int z1 = 0;
		float maxH = -1f;

		for (int ii = 0; ii < td.heightmapWidth; ++ii)
			for (int jj = 0; jj < td.heightmapHeight; ++jj) 
		{		
			if (heights[ii, jj] > maxH)
			{
				x1 = ii;
				z1 = jj;
				maxH = heights[ii, jj];
			}

		}

		x1 = 0;
		z1 = 0;

		int x2 = 512;
		int z2 = 512;

		List<HeightNode> lPath = mHeightGraph.getDijkstra(mHMap[x1, z1], 
		                                           mHMap[x2, z2]);
		Queue<HeightNode> lQueue = new Queue<HeightNode>(lPath);
		HeightNode lCurrentNode = mHMap[x1, z1];
		int lCurrentPositionX = lCurrentNode.mX;
		int lCurrentPositionZ = lCurrentNode.mZ;
		int lNextPositionX = 0;
		int lNextPositionZ = 0;

		GameObject water = new GameObject();
		water.name = "GeneratedRiver";
		
		GameObject prefab = Resources.LoadAssetAtPath<GameObject>("Assets/Prefabs/Standard Assets/Water (Basic)/Daylight Simple Water.prefab");
	//	prefab.name = "water";

		float[,] newHeights = new float[td.heightmapWidth, td.heightmapHeight];
		newHeights = (float[,]) heights.Clone();

		float[,,] am = new float[td.heightmapWidth, td.heightmapHeight, 6];
		am = td.GetAlphamaps (0, 0, td.heightmapWidth, td.heightmapHeight);

		KeyValuePair<float[,], float[,,]> newTer = new KeyValuePair<float[,], float[,,]> (newHeights, am);

		do {
			HeightNode lNextNode = lQueue.Dequeue();
			lNextPositionX = lNextNode.mX;
			lNextPositionZ = lNextNode.mZ;

			newTer = SizeRiver(heights, newHeights, lCurrentPositionX, lCurrentPositionZ, am);
			newHeights = newTer.Key;
			am = newTer.Value;

			if (lCurrentPositionX % 5 == 0 || lCurrentPositionZ % 5 == 0 || true)
			{
				float xx = (float)lCurrentPositionX / (Terrain.terrainData.heightmapWidth - 1);
				float zz = (float)lCurrentPositionZ / (Terrain.terrainData.heightmapHeight - 1);


				GameObject prefabInstance = PrefabUtility.InstantiatePrefab(prefab) as GameObject;
				prefabInstance.transform.position = new Vector3(zz * Terrain.terrainData.size.x, (heights[lCurrentPositionX, lCurrentPositionZ] - 0.005f) * Terrain.terrainData.size.y , xx * Terrain.terrainData.size.z);
				prefabInstance.transform.localScale = new Vector3(4f, 1f, 4f);
				prefabInstance.transform.rotation = Quaternion.FromToRotation(Vector3.up, td.GetInterpolatedNormal (zz, xx));
				prefabInstance.transform.parent = water.transform;
				prefabInstance.AddComponent<MeshCollider>();
			}
			
			lCurrentNode = lNextNode;
			lCurrentPositionX = lNextPositionX;
			lCurrentPositionZ = lNextPositionZ;

		} while (lQueue.Count != 0);
		
		CombineChildren (water);
		while (GameObject.Find ("Daylight Simple Water") != null)
			GameObject.DestroyImmediate(GameObject.Find("Daylight Simple Water"));

		td.SetHeights(0, 0, newHeights);
		td.SetAlphamaps(0, 0, am);

	}

	private KeyValuePair<float[,], float[,,]> SizeRiver(float[,] heights, float[,] newHeights, int lCurrentPositionX, int lCurrentPositionZ, float[,,] am)
	{
		TerrainData td = Terrain.terrainData;

		newHeights[lCurrentPositionX, lCurrentPositionZ] = heights[lCurrentPositionX, lCurrentPositionZ] - 0.01f;
		am = ChangeOneTexture(lCurrentPositionX, lCurrentPositionZ, 3, am);
		
		
		if (lCurrentPositionX > 0)
		{
			newHeights[lCurrentPositionX - 1, lCurrentPositionZ] = heights[lCurrentPositionX - 1, lCurrentPositionZ] - 0.008f;
			am = ChangeOneTexture(lCurrentPositionX - 1, lCurrentPositionZ, 3, am);
		}

		if (lCurrentPositionX < td.heightmapWidth - 1)
		{
			newHeights[lCurrentPositionX + 1, lCurrentPositionZ] = heights[lCurrentPositionX + 1, lCurrentPositionZ] - 0.008f;
			am = ChangeOneTexture(lCurrentPositionX + 1, lCurrentPositionZ, 3, am);
		}
		
		if (lCurrentPositionZ > 0)
		{
			newHeights[lCurrentPositionX, lCurrentPositionZ - 1] = heights[lCurrentPositionX, lCurrentPositionZ - 1] - 0.008f;
			am = ChangeOneTexture(lCurrentPositionX, lCurrentPositionZ - 1, 3, am);
		}
		
		if (lCurrentPositionZ < td.heightmapHeight - 1)
		{
			newHeights[lCurrentPositionX, lCurrentPositionZ + 1] = heights[lCurrentPositionX, lCurrentPositionZ + 1] - 0.008f;
			am = ChangeOneTexture(lCurrentPositionX, lCurrentPositionZ + 1, 3, am);
		}

		return new KeyValuePair<float[,], float[,,]> (newHeights, am);
	}

	/*
	*	Create Graph of the heightmap
	*/

	private void CreateHeightGraph(float[,] heights) {
		mHMap = new HeightNode[513, 513];
		int i = 0;
		int j = 0;
		for (int x = 0; x < 513 && i < 513; x += 1) {
			for (int z = 0; z < 513 && j < 513; z += 1) {

				mHMap[i, j] = new HeightNode(x, z, heights[x,z] * 100);
				j++;
			}
			i++;
			j = 0;
		}

		for (int x = 0; x < 513; x++) {
			for (int z = 0; z < 513; z++) {
				HeightNode lCurrentNode = mHMap[x, z];
				if (x > 0) {
					//					int lValue = TerrainGenerator.findTree(x - 1, z) ? 99999 : (int)(Mathf.Abs(mMap[x, z].mPosition.y - mMap[x - 1, z].mPosition.y)) + 1;
					float lValue = heights[x - 1, z] * 100 + Random.Range(1, 3);
					lCurrentNode.addAdjacentNode(mHMap[x - 1, z], lValue);
				}
				
				if (x < 512) {
					//					int lValue = TerrainGenerator.findTree(x + 1, z) ? 99999 : (int)(Mathf.Abs(mMap[x, z].mPosition.y - mMap[x + 1, z].mPosition.y)) + 1;
					float lValue = heights[x + 1, z] * 100 + Random.Range(1, 3);
					lCurrentNode.addAdjacentNode(mHMap[x + 1, z], lValue);
				}
				
				if (z > 0) {
					//					int lValue = TerrainGenerator.findTree(x, z - 1) ? 99999 : (int)(Mathf.Abs(mMap[x, z].mPosition.y - mMap[x, z - 1].mPosition.y)) + 1;
					float lValue = heights[x, z - 1] * 100 + Random.Range(1, 3);
					lCurrentNode.addAdjacentNode(mHMap[x, z - 1], lValue);
				}
				
				if (z < 512) {
					//					int lValue = TerrainGenerator.findcTree(x, z + 1) ? 99999 : (int)(Mathf.Abs(mMap[x, z].mPosition.y - mMap[x, z + 1].mPosition.y)) + 1;
					float lValue = heights[x, z + 1] * 100 + Random.Range(1, 3);
					lCurrentNode.addAdjacentNode(mHMap[x, z + 1], lValue);
				}
				mHeightGraph.addNode(lCurrentNode);
			}
		}
	}

	/*
	*	Delete the river
	*/

	public void ClearRiver()
	{
		while (GameObject.Find ("GeneratedRiver") != null)
			GameObject.DestroyImmediate(GameObject.Find("GeneratedRiver"));
	}

	/*
	*	Generate water at the height of float waterLevel
	*/

	public void AddWater(float waterlevel)
	{


		ClearWater ();
		terSet = TerrainSettings.Instance;
		terSet.setWaterLevel (waterlevel);

		GameObject water = new GameObject();
		water.name = "GeneratedWater";

		GameObject prefab = Resources.LoadAssetAtPath<GameObject>("Assets/Prefabs/Standard Assets/Water (Basic)/Daylight Simple Water.prefab");
	//	prefab.name = "water";
		float[,] heights = Terrain.terrainData.GetHeights(0, 0, Terrain.terrainData.heightmapWidth, Terrain.terrainData.heightmapHeight);
	
		if (waterlevel > 0) 
		{
			GameObject prefabInstance = PrefabUtility.InstantiatePrefab (prefab) as GameObject;
			prefabInstance.transform.position = new Vector3 (Terrain.terrainData.size.x / 2, waterlevel * Terrain.terrainData.size.y, Terrain.terrainData.size.z / 2);
			prefabInstance.transform.localScale = new Vector3 (3 * Terrain.terrainData.size.x / 4, 1f, 3 * Terrain.terrainData.size.z / 4);
			prefabInstance.transform.parent = water.transform;
		}


		RemoveTreesUnderWater();
	}

	/*
	*	Delete Water
	*/

	public void ClearWater()
	{
		while (GameObject.Find ("GeneratedWater") != null)
			GameObject.DestroyImmediate(GameObject.Find("GeneratedWater"));

	}

	/*
	*	Remove trees under the water
	*/

	public void RemoveTreesUnderWater()
	{
		if (GameObject.Find ("GeneratedWater") != null)
		{
			terSet = TerrainSettings.Instance;

			List<TreeInstance> treeInstances = new List<TreeInstance>(Terrain.terrainData.treeInstances);
			List<TreeInstance> treeInstances2 = new List<TreeInstance>();

			for (int i = 0; i < treeInstances.Count; ++i)
			{
				if (treeInstances[i].position.y >= terSet.getWaterLevel())
					treeInstances2.Add(treeInstances[i]);
			}

			Terrain.terrainData.treeInstances = treeInstances2.ToArray();
		}
	}

	/*
	 *  Delete all the trees and the prototypes
	 */

	public void ClearTrees()
	{
		TerrainData terrain = Terrain.terrainData;
		List<TreeInstance> treeInstances = new List<TreeInstance>(terrain.treeInstances);
		
		
		treeInstances.Clear();
		terrain.treeInstances = treeInstances.ToArray();
		
		// Now refresh the terrain, getting rid of the darn collider
		float[,] heights = terrain.GetHeights(0, 0, 0, 0);
		Terrain.activeTerrain.terrainData.SetHeights(0, 0, heights);
		terrain.treePrototypes = null;

	}

	/*
	 * Add Trees in the terrain
	 * Add a prefab tree and generate some trees
	 */

	public void AddTrees(float densTree, int seed)
	{
		ClearTrees ();
		terSet = TerrainSettings.Instance;
		
		terSet.setSeedTrees(seed);
		terSet.setDensTrees(densTree);

		TerrainData currentTerrainData = Terrain.terrainData;
		List<TreePrototype> treePrototypesList = new List<TreePrototype>(currentTerrainData.treePrototypes);
		
		TreePrototype treePrototype = new TreePrototype();
		GameObject prefab = Resources.LoadAssetAtPath<GameObject>("Assets/Prefabs/Mesh/Tree_1.prefab");
		//GameObject prefab = Resources.LoadAssetAtPath<GameObject>("Assets/TerrainGenerator/Prefabs/Stylized foliages pack/Bush 02 double prefab.prefab");
		if(prefab != null)
		{
			treePrototype.prefab = prefab;
			treePrototypesList.Add(treePrototype);
		}
		prefab = Resources.LoadAssetAtPath<GameObject>("Assets/Prefabs/Mesh/Tree_1.prefab");
		//prefab = Resources.LoadAssetAtPath<GameObject>("Assets/TerrainGenerator/Prefabs/Stylized foliages pack/Bush 02 double prefab.prefab");
		if(prefab != null)
		{
			treePrototype = new TreePrototype();
			treePrototype.prefab = prefab;
			treePrototypesList.Add(treePrototype);
		}

		currentTerrainData.treePrototypes = treePrototypesList.ToArray();
		Terrain.Flush();
		currentTerrainData.RefreshPrototypes();

		List<TreeInstance> treeInstances = new List<TreeInstance> ();

		var noise = new BillowNoise (seed)
		    {
				Persistence = 0.6f,
				OctaveCount = 4
			};

	
		for (int ii = 0; ii < currentTerrainData.heightmapWidth; ++ii)
			for (int jj = 0; jj < currentTerrainData.heightmapHeight; ++jj) 
		{
			
			
			// Domain coordinates. Each terrain is 1x1 in domain space
			float x = (float)ii / (currentTerrainData.heightmapWidth - 1);
			float y = (float)jj / (currentTerrainData.heightmapHeight - 1);

			var v = noise.GetValue(x, y, 0);
			float randomValue = densTree * 0.20f;
			if (v > 0 && Random.value > (1.0f - randomValue)) {

				TreeInstance newInstance = new TreeInstance();
				newInstance.position = getCordinateTree(y * Terrain.terrainData.size[2], x * Terrain.terrainData.size[0]);
				newInstance.heightScale = 1;
				newInstance.widthScale = 1;
				newInstance.prototypeIndex = 0;
				newInstance.lightmapColor = Color.white;
				newInstance.color = Color.white;

				var angleDiff = Vector3.Angle(Vector3.up, currentTerrainData.GetInterpolatedNormal (newInstance.position.x, newInstance.position.z));

				if (angleDiff < 25 && (GameObject.Find("GeneratedWater") == null || newInstance.position.y >= terSet.getWaterLevel()))
					treeInstances.Add (newInstance);
			}
		}
		
		currentTerrainData.treeInstances = treeInstances.ToArray();
	
		Terrain.Flush();
	}


	/*
	 * Add Grass in the terrain
	 * 
	 */

	public void AddGrass(float densTree, int seed)
	{
		terSet = TerrainSettings.Instance;
		
		terSet.setSeedTrees(seed);
		terSet.setDensTrees(densTree);
		
		TerrainData currentTerrainData = Terrain.terrainData;
		List<TreePrototype> treePrototypesList = new List<TreePrototype>(currentTerrainData.treePrototypes);

		TreePrototype treePrototype = new TreePrototype();
		GameObject prefab = Resources.LoadAssetAtPath<GameObject>("Assets/Prefabs/Stylized foliages pack/Bush 02 double prefab.prefab");
		if(prefab != null)
		{
			treePrototype.prefab = prefab;
			treePrototypesList.Add(treePrototype);
		}
		prefab = Resources.LoadAssetAtPath<GameObject>("Assets/Prefabs/Stylized foliages pack/Bush 02 double prefab.prefab");
		if(prefab != null)
		{
			treePrototype = new TreePrototype();
			treePrototype.prefab = prefab;
			treePrototypesList.Add(treePrototype);
		}
		
		currentTerrainData.treePrototypes = treePrototypesList.ToArray();
		Terrain.Flush();
		currentTerrainData.RefreshPrototypes();
		
		List<TreeInstance> treeInstances = new List<TreeInstance>(currentTerrainData.treeInstances);

		var noise = new BillowNoise (seed)
		{
			Persistence = 0.6f,
			OctaveCount = 4
		};

		for (int ii = 0; ii < currentTerrainData.heightmapWidth; ++ii)
			for (int jj = 0; jj < currentTerrainData.heightmapHeight; ++jj) 
		{
			// Domain coordinates. Each terrain is 1x1 in domain space

			Random.seed = 42;
			float x = (float)ii / (currentTerrainData.heightmapWidth - 1);
			float y = (float)jj / (currentTerrainData.heightmapHeight - 1);
			if (Util.rand.Next(0, 100) < 9 && noise.GetValue(x, y, 0) < 0.20f*0.99999f) {
				
				//float randomValue = densTree * 0.20f;

				TreeInstance newInstance = new TreeInstance();
				newInstance.position = getCordinateTree(y * Terrain.terrainData.size[2], x * Terrain.terrainData.size[0]);
				newInstance.heightScale = 1;
				newInstance.widthScale = 1;
				newInstance.prototypeIndex = 2;
				newInstance.lightmapColor = Color.white;
				newInstance.color = Color.white;
					
				var angleDiff = Vector3.Angle(Vector3.up, currentTerrainData.GetInterpolatedNormal (newInstance.position.x, newInstance.position.z));
					
				if (angleDiff < 25 && (GameObject.Find("GeneratedWater") == null || newInstance.position.y >= terSet.getWaterLevel()))
					treeInstances.Add(newInstance);
			}
		}
		
		currentTerrainData.treeInstances = treeInstances.ToArray();
		Terrain.Flush();
	}
	
	/*
	 * Get the coordinates for the dimension of the terrain
	 */

	public Vector3 getCordinateTree(float x, float z)
	{
		float x2 = x / Terrain.terrainData.size [0];
		float z2 = z / Terrain.terrainData.size [2];
		float y = Terrain.activeTerrain.SampleHeight(new Vector3(x, 0, z)) / Terrain.terrainData.size[1];

		return new Vector3(x2, y, z2);
	}
	
	public static bool findTree(float x, float z)
	{
		TerrainData terrain = Terrain.activeTerrain.terrainData;
		TreeInstance[] treeInstances = terrain.treeInstances;

		float maxDistance = 5.0f;
		Vector3 closestTreePosition = new Vector3();


		int closestTreeIndex = 0;
		for (int i = 0; i < treeInstances.Length; i++)
		{
			TreeInstance currentTree = treeInstances[i];

			Vector3 currentTreeWorldPosition = Vector3.Scale(currentTree.position, terrain.size) + Terrain.activeTerrain.transform.position;
			Vector3 currentPosition = new Vector3(x, Terrain.activeTerrain.SampleHeight(new Vector3(x, 0, z)), z);

			float distance = Vector3.Distance(currentTreeWorldPosition, currentPosition);

			if (distance < maxDistance)
				return true;
		}
		return false;
	}

	/*
	*	Search if there is water at the coordinate x z
	*/

	public static bool findWater(float x, float z)
	{
		RaycastHit hit;
	

		if (Physics.Raycast(new Vector3(x, 600, z), -Vector3.up, out hit))
		{
			if (hit.collider.gameObject.name == "CombinedRiver" || hit.collider.gameObject.name == "Daylight Simple Water")
				return true;
		

		}
		return false;
	}
	
	/*
	*	Combine the mesh of the children of gobj
	*/

	public void CombineChildren(GameObject gobj)
	{

		Matrix4x4 myTransform = gobj.transform.worldToLocalMatrix;
		Dictionary<Material, List<CombineInstance>> combines = new Dictionary<Material, List<CombineInstance>>();
		MeshRenderer[] meshRenderers = gobj.GetComponentsInChildren<MeshRenderer>();
		foreach (var meshRenderer in meshRenderers)
		{
			foreach (var material in meshRenderer.sharedMaterials)
				if (material != null && !combines.ContainsKey(material))
					combines.Add(material, new List<CombineInstance>());
		}
		
		MeshFilter[] meshFilters = gobj.GetComponentsInChildren<MeshFilter>();
		foreach(var filter in meshFilters)
		{
			if (filter.sharedMesh == null)
				continue;
			CombineInstance ci = new CombineInstance();
			ci.mesh = filter.sharedMesh;
			ci.transform = myTransform * filter.transform.localToWorldMatrix;
			combines[filter.renderer.sharedMaterial].Add(ci);
			filter.renderer.enabled = false;
		}
		
		foreach(Material m in combines.Keys)
		{
			var go = new GameObject("CombinedRiver");
			go.transform.parent = gobj.transform;
			go.transform.localPosition = Vector3.zero;
			go.transform.localRotation = Quaternion.identity;
			go.transform.localScale = Vector3.one;
	
			var filter = go.AddComponent<MeshFilter>();
			filter.sharedMesh = new Mesh();
			filter.sharedMesh.CombineMeshes(combines[m].ToArray(), true, true);

			var renderer = go.AddComponent<MeshRenderer>();
			go.AddComponent<MeshCollider>();

			renderer.material = m;

		}

	}

}