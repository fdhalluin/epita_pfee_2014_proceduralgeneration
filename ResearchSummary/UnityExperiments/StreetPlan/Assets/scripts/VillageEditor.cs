using UnityEngine;
using System.Collections;
using UnityEditor;

public class Settings {
	public bool mOrganic { get; internal set; }
	public float mDensity { get; internal set; }
	public float mExpansion { get; internal set; }
	public int mMaxAdjacentNodes { get; internal set; }
	public int mStep { get; internal set; }
	public int mNbNodes { get; internal set; }
	public int mMinDistanceNodes { get; internal set; }
	public int mMaxDistanceNodes { get; internal set; }
	public int mCoreSize { get; internal set; }
	public int mCitySize { get; internal set; }
	public int mQuality { get; internal set; }
	public int mSprawl { get; internal set; }
	
	public Settings() {
		mDensity = 1.0f;
		mExpansion = 1000.0f;
		mMaxAdjacentNodes = 2;
		mStep = 5;
		mNbNodes = 150;
		mMinDistanceNodes = 500;
		mMaxDistanceNodes = 1000;
		mOrganic = true;
		mSprawl = 0;
		mQuality = 0;
		mCitySize = 0;
		mCoreSize = 0;
	}
}

public class VillageEditor : EditorWindow {
	private CityGenerator mCityGenerator;
	private Settings mSettings = new Settings();

	[MenuItem ("Window/Street Editor")]

	static void Init () {
		VillageEditor window = (VillageEditor) EditorWindow.GetWindow(typeof (VillageEditor));
	}
	
	void OnGUI () {
//		GUILayout.Label ("Base Settings", EditorStyles.boldLabel);
		mSettings.mDensity = EditorGUILayout.Slider("Density", mSettings.mDensity, 1, 5f);
		mSettings.mCoreSize = EditorGUILayout.IntSlider("Size of the core city", mSettings.mCoreSize, 1, 10);
		mSettings.mCitySize = EditorGUILayout.IntSlider("Size of the city", mSettings.mCitySize, 1, 10);
		mSettings.mQuality = EditorGUILayout.IntSlider("Infrastructure quality", mSettings.mQuality, 0, 10);
		mSettings.mSprawl = EditorGUILayout.IntSlider("Urban sprawl", mSettings.mSprawl, 0, 10);
		mSettings.mOrganic = EditorGUILayout.Toggle("Organic", mSettings.mOrganic);

		EditorGUILayout.Separator();
		
		if (GUI.Button(new Rect(100,180,120,25), "Generate")) {
			mCityGenerator = FindObjectOfType<CityGenerator>();

			mCityGenerator.ClearAll();
			mCityGenerator.GenerateCity(mSettings);
		}
		
		if (GUI.Button(new Rect(100,210,120,25), "Growth")) {
			mCityGenerator.Growth();
		}
	}
}
