using System;
using HouseGen;

namespace ProgramSpace {

class Program {

    static void Main(string[] args)
    {
		Story floor = new HouseGen.Story(40, 20, "Floor");
		
		Rectangle r = new Rectangle(8, 4, 8, 12);
		floor.addRoom(r);		

		Rectangle r1 = r.addRectangleToSide(Direction.WEST, 4, 1, 3);
		Rectangle r1P5 = r.addRectangleToSide(Direction.NORTH, 12, 1, 3);
		Rectangle r2 = r.addRectangleToSide(Direction.EAST, 8, 1, 9, Rectangle.PositionType.FIRST_MATCH);
		
		r.addPillars(Direction.WEST, 2, 2);
		r.addPillars(Direction.EAST, 3, 1);
		r.addPillars(Direction.SOUTH, 3, 1);


		floor.print(true);
		floor.print();

		
		Story first = new Story(floor, "Floor 1");
		
		//first.print();
		
    }

};


}