﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayMusic : MonoBehaviour
{

    private MelodyGenerator generator;
	private int max_channel_count = 10;

    public void Start()
    {
		//Random.seed = 1053;

        for (int i = 0; i < max_channel_count; ++i)
            this.gameObject.AddComponent<AudioSource>();
        generator = new Jazzy();
    }
	
    public void Generate() {
	if (MusicGlobal.seed == 0)
		MusicGlobal.seed = Random.seed;
	else
		Random.seed = MusicGlobal.seed;
	StopAllCoroutines();
	StartCoroutine(playMusic(generator.GenerateMelody()));
	}

    private IEnumerator playMusic(GenericMelody melody)
    {
        int pitchindex=0;
        int durationindex=0;
        int velocityindex=0;
        int sampleindex=0;
        int channelindex=0;

        AudioSource[] audiosources = GetComponents<AudioSource>();

        while(true)
        {
            float pitch = melody.frequencies[pitchindex];
            float duration = melody.durations[durationindex];
            float velocity = melody.volumes[velocityindex] / 2;
            AudioClip sample = melody.sounds[sampleindex];

            AudioSource a_s;
            a_s = audiosources[channelindex];

            a_s.pitch = pitch;
            a_s.volume = velocity;
            //produces clicks :(
            //if (melody.clearchannelonretrigger)
            //	a_s.Stop();
            a_s.PlayOneShot(sample);
			yield return new WaitForSeconds(60 / MusicGlobal.tempo * duration);

            pitchindex = (pitchindex+1) % melody.frequencies.Count;
            durationindex = (durationindex+1) % melody.durations.Count;
            velocityindex = (velocityindex+1) % melody.volumes.Count;
            sampleindex = (sampleindex+1) % melody.sounds.Count;
			channelindex = (channelindex + 1) % MusicGlobal.channel_count;
        }
    }
}
