﻿using UnityEngine;
using System.Collections.Generic;
using LotGeneration;

public enum BuildingKind {
	house,
	cuteHouse,
	building
}

public enum CityKind {
	village,
	greekVillage,
	city,
	manhattan
}

public enum PaletteKind {
	redBeige,
	purpleLightBrown,
	blueWhite,
	redBrown,
	grayGray
}

public enum SkyKind {
	morning,
	noon,
	overcast,
	evening,
	night
}

public class Settings {
	public int mSeed { get;  set; }
	public int mExpansion { get;  set; }
	public int mSmooth { get;  set; }
	public int mSmoothMainRoad { get;  set; }
	public int mStep { get;  set; }
	public int mNbNodes { get;  set; }
	public int mMinDistanceNodes { get;  set; }
	public int mMaxDistanceNodes { get;  set; }
	public int mNbFields { get;  set; }
	public float mCitySize { get;  set; }
	public int mNbLights { get;  set; }
	public int mNbDifferentHouses { get;  set; }
	public int mNbOutsideNodes { get;  set; }
	public int mMinDistanceOusideNodes { get;  set; }
	public int mMinLongStreet { get;  set; }
	public int mMaxHeightDiff { get;  set; }
	public int mLloydRelaxation { get;  set; }
	public int mRoadPrecision { get;  set; }
	public int mStreetSpacing { get;  set; }
	public bool mPathFindingTree { get;  set; }
	public bool mRandom { get;  set; }
	public bool mPopulated { get;  set; }
	public bool mIsTowered { get;  set; }
	public bool mHaveCityPlace { get;  set; }
	public bool mDebug { get;  set; }
	public Vector3 mCityCenter { get;  set; }
	public List<LotType> mRequiredLotTypes { get;  set; }
	public PriorityQueue<Lot> mInterestingLots  { get;  set; }
	public float mLotDepth  { get;  set; }
	public CityKind mCityKind { get;  set; }
	public SkyKind mSkyKind { get;  set; }
	public BuildingKind mBuildingKind { get;  set; }
	public PaletteKind mFirstPaletteKind { get;  set; }
	public PaletteKind mSecondPaletteKind { get;  set; }
	public PaletteKind mThirdPaletteKind { get;  set; }
	public bool mLowEndModels { get;  set; }
	public bool mHaveCarts { get;  set; }
	public bool mHaveStreetLights { get;  set; }
	public bool mHavePhones { get;  set; }
	public bool mHavePylones { get;  set; }
	public int mDistanceExtremityPylone { get;  set; }
	public int mDistanceBetweenPylone { get;  set; }
	
	public Settings() {
		mDistanceExtremityPylone = 1100;
		mDistanceBetweenPylone = 14;
		mFirstPaletteKind = PaletteKind.redBeige;
		mSecondPaletteKind = PaletteKind.purpleLightBrown;
		mThirdPaletteKind = PaletteKind.redBeige;
		mSkyKind = SkyKind.noon;
		mBuildingKind = BuildingKind.house;
		mIsTowered = false;
		mHaveCityPlace = true;
		mHaveCarts = true;
		mHavePhones = true;
		mHavePylones = true;
		mHaveStreetLights = true;
		mSeed = 219;
		mNbLights = 50;
		mExpansion = 75;
		mNbDifferentHouses = 75;
		mStep = 10;
		mSmooth = 7;
		mSmoothMainRoad = 5;
		mNbNodes = 45;
		mMinDistanceNodes = 100;
		mNbOutsideNodes = 3;
		mCitySize = 210f;
		mMinDistanceOusideNodes = 820;
		mMinLongStreet = 2;
		mMaxDistanceNodes = 1000;
		mMaxHeightDiff = 5;
		mLloydRelaxation = 3;
		mPathFindingTree = false;
		mRandom = false;
		mPopulated = false;
		mRoadPrecision = 1;
		mLotDepth = 15;
		mStreetSpacing = 5;
		mNbFields = 2;
		mDebug = false;
		mInterestingLots = new PriorityQueue<Lot>(Lot.Comparer);
		mRequiredLotTypes = new List<LotType>();
		mLowEndModels = false;
	}
	
	public void GetHouseLots() {
		mRequiredLotTypes.Clear();
		mRequiredLotTypes.Add(LotType.CHURCH);
		mRequiredLotTypes.Add(LotType.RESIDENTIAL);
		mRequiredLotTypes.Add(LotType.RESIDENTIAL);
		mRequiredLotTypes.Add(LotType.RESIDENTIAL);
		//mRequiredLotTypes.Add(LotType.TOWER);
		//		mRequiredLotTypes.Add(LotType.TOWER);
		//		mRequiredLotTypes.Add(LotType.TOWER);
		//		mRequiredLotTypes.Add(LotType.TOWER);
	}
	
	public void GetBuildingLots() {
		mRequiredLotTypes.Clear();
		for (int i = 0; i < 400; ++i) {
			mRequiredLotTypes.Add(LotType.SKYSCRAPER);
		}
		
		mRequiredLotTypes.Add(LotType.RESIDENTIAL);
		mRequiredLotTypes.Add(LotType.RESIDENTIAL);
		mRequiredLotTypes.Add(LotType.RESIDENTIAL);
	}
	
	public void GetCuteHouseLots() {
		mRequiredLotTypes.Clear();
		mRequiredLotTypes.Add(LotType.CHURCH);
		mRequiredLotTypes.Add(LotType.RESIDENTIAL);
		mRequiredLotTypes.Add(LotType.MINIHOUSE);
		mRequiredLotTypes.Add(LotType.MINIHOUSE);
		mRequiredLotTypes.Add(LotType.MINIHOUSE);
		mRequiredLotTypes.Add(LotType.MINIHOUSE);
		mRequiredLotTypes.Add(LotType.MINIHOUSE);
		mRequiredLotTypes.Add(LotType.STAIRHOUSE);
		mRequiredLotTypes.Add(LotType.STAIRHOUSE);
		mRequiredLotTypes.Add(LotType.STAIRHOUSE);
		mRequiredLotTypes.Add(LotType.RESIDENTIAL);
		mRequiredLotTypes.Add(LotType.TOWER);
		mRequiredLotTypes.Add(LotType.TOWER);
		mRequiredLotTypes.Add(LotType.RESIDENTIAL);
	}
}

public class GreekSettings : Settings {
	static GreekSettings sInstance;
	
	public static GreekSettings getInstance() {
		return sInstance == null ? sInstance = new GreekSettings() : sInstance;
	}
	
	private GreekSettings() {
		mDistanceExtremityPylone = 1100;
		mDistanceBetweenPylone = 14;
		mFirstPaletteKind = PaletteKind.blueWhite;
		mSecondPaletteKind = PaletteKind.blueWhite;
		mThirdPaletteKind = PaletteKind.purpleLightBrown;
		mSkyKind = SkyKind.morning;
		mBuildingKind = BuildingKind.cuteHouse;
		mSeed = 292;
		mIsTowered = false;
		mHaveCityPlace = true;
		mHaveCarts = true;
		mHavePhones = true;
		mHavePylones = true;
		mHaveStreetLights = true;
		mExpansion = 75;
		mNbDifferentHouses = 10;
		mStep = 10;
		mSmooth = 7;
		mDebug = false;
		mSmoothMainRoad = 5;
		mNbNodes = 48;
		mMinDistanceNodes = 100;
		mNbLights = 50;
		mNbOutsideNodes = 3;
		mCitySize = 120f;
		mMinDistanceOusideNodes = 820;
		mMinLongStreet = 2;
		mMaxDistanceNodes = 1000;
		mMaxHeightDiff = 7;
		mLloydRelaxation = 5;
		mPathFindingTree = false;
		mRandom = false;
		mPopulated = true;
		mRoadPrecision = 1;
		mLotDepth = 15;
		mStreetSpacing = 5;
		mNbFields = 1;
		mInterestingLots = new PriorityQueue<Lot>(Lot.Comparer);
		mRequiredLotTypes = new List<LotType>();
	}
}

public class VillageSettings : Settings {
	static VillageSettings sInstance;
	
	public static VillageSettings getInstance() {
		return sInstance == null ? sInstance = new VillageSettings() : sInstance;
	}
	
	private VillageSettings() {
		mDistanceExtremityPylone = 1100;
		mDistanceBetweenPylone = 14;
		mFirstPaletteKind = PaletteKind.redBeige;
		mSecondPaletteKind = PaletteKind.purpleLightBrown;
		mThirdPaletteKind = PaletteKind.redBeige;
		mSkyKind = SkyKind.noon;
		mBuildingKind = BuildingKind.house;
		mSeed = 219;
		mIsTowered = false;
		mHaveCityPlace = true;
		mHaveCarts = true;
		mHavePhones = true;
		mHavePylones = true;
		mHaveStreetLights = true;
		mExpansion = 75;
		mNbDifferentHouses = 75;
		mStep = 10;
		mSmooth = 7;
		mDebug = false;
		mSmoothMainRoad = 5;
		mNbNodes = 45;
		mMinDistanceNodes = 100;
		mNbLights = 50;
		mNbOutsideNodes = 3;
		mCitySize = 210f;
		mMinDistanceOusideNodes = 820;
		mMinLongStreet = 2;
		mMaxDistanceNodes = 1000;
		mMaxHeightDiff = 5;
		mLloydRelaxation = 3;
		mPathFindingTree = false;
		mRandom = false;
		mPopulated = false;
		mRoadPrecision = 1;
		mLotDepth = 15;
		mStreetSpacing = 5;
		mNbFields = 3;
		mInterestingLots = new PriorityQueue<Lot>(Lot.Comparer);
		mRequiredLotTypes = new List<LotType>();
	}
}

public class CitySettings : Settings {
	static CitySettings sInstance;
	
	public static CitySettings getInstance() {
		return sInstance == null ? sInstance = new CitySettings() : sInstance;
	}
	
	private CitySettings() {
		mDistanceExtremityPylone = 1100;
		mDistanceBetweenPylone = 14;
		mFirstPaletteKind = PaletteKind.redBeige;
		mSecondPaletteKind = PaletteKind.purpleLightBrown;
		mThirdPaletteKind = PaletteKind.redBeige;
		mSkyKind = SkyKind.noon;
		mBuildingKind = BuildingKind.house;
		mSeed = 159;
		mExpansion = 75;
		mStep = 10;
		mDebug = false;
		mHaveCityPlace = true;
		mHavePylones = true;
		mHaveCarts = true;
		mHavePhones = true;
		mHaveStreetLights = true;
		mIsTowered = false;
		mSmooth = 7;
		mSmoothMainRoad = 5;
		mNbLights = 50;
		mNbDifferentHouses = 75;
		mNbNodes = 115;
		mMinDistanceNodes = 100;
		mNbOutsideNodes = 3;
		mCitySize = 800f;
		mMinDistanceOusideNodes = 820;
		mMinLongStreet = 2;
		mMaxDistanceNodes = 1000;
		mMaxHeightDiff = 7;
		mLloydRelaxation = 3;
		mPathFindingTree = false;
		mRandom = false;
		mPopulated = true;
		mRoadPrecision = 1;
		mLotDepth = 15;
		mStreetSpacing = 5;
		mNbFields = 2;
		mInterestingLots = new PriorityQueue<Lot>(Lot.Comparer);
		mRequiredLotTypes = new List<LotType>();
	}
}

public class ManhatthanSettings : Settings {
	static ManhatthanSettings sInstance;
	
	public static ManhatthanSettings getInstance() {
		return sInstance == null ? sInstance = new ManhatthanSettings() : sInstance;
	}
	
	private ManhatthanSettings() {
		mDistanceExtremityPylone = 1100;
		mDistanceBetweenPylone = 14;
		mFirstPaletteKind = PaletteKind.grayGray;
		mSecondPaletteKind = PaletteKind.redBeige;
		mThirdPaletteKind = PaletteKind.redBrown;
		mSkyKind = SkyKind.noon;
		mBuildingKind = BuildingKind.building;
		mSeed = 256;
		mDebug = false;
		mExpansion = 75;
		mStep = 10;
		mSmooth = 5;
		mSmoothMainRoad = 5;
		mNbNodes = 148;
		mMinDistanceNodes = 100;
		mNbOutsideNodes = 3;
		mCitySize = 529f;
		mHaveCityPlace = false;
		mHaveCarts = false;
		mHavePhones = true;
		mHaveStreetLights = true;
		mHavePylones = true;
		mNbDifferentHouses = 75;
		mIsTowered = true;
		mMinDistanceOusideNodes = 820;
		mNbLights = 50;
		mMinLongStreet = 2;
		mMaxDistanceNodes = 1000;
		mMaxHeightDiff = 7;
		mLloydRelaxation = 5;
		mPathFindingTree = false;
		mRandom = false;
		mPopulated = true;
		mRoadPrecision = 1;
		mLotDepth = 15;
		mStreetSpacing = 5;
		mNbFields = 0;
		mInterestingLots = new PriorityQueue<Lot>(Lot.Comparer);
		mRequiredLotTypes = new List<LotType>();
	}
}
