﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;



public class MusicEditor : EditorWindow {

	[MenuItem ("Town/Music Editor")]

	public static void Init () {
		MusicEditor window = (MusicEditor) EditorWindow.GetWindow(typeof (MusicEditor));
	}

	public void OnGUI () {
		MusicGlobal.music_kind = (MusicKind)EditorGUILayout.EnumPopup("Theme", MusicGlobal.music_kind);

		switch (MusicGlobal.music_kind) {
		case MusicKind.empty:
			MusicGlobal.setDefaultSettings();
			break;
		case MusicKind.walk:
			MusicGlobal.setFirstSettings();
			break;
		case MusicKind.countryside:
			MusicGlobal.setSecondSettings();
			break;
		case MusicKind.colibri:
			MusicGlobal.setThirdSettings();
			break;
		case MusicKind.classic:
			MusicGlobal.setFourthSettings();
			break;
		case MusicKind.futur:
			MusicGlobal.setFifthSettings();
			break;
		case MusicKind.melancholia:
			MusicGlobal.setSixthSettings();
			break;
		default:
			break;
		}

		MusicGlobal.channel_count = EditorGUILayout.IntSlider("Channel count", MusicGlobal.channel_count, 0, MusicGlobal.max_channel_count);
		MusicGlobal.tempo = EditorGUILayout.Slider("Tempo", MusicGlobal.tempo, 60, 120);
		MusicGlobal.lead = (AudioClip)EditorGUILayout.ObjectField("Lead instrument", MusicGlobal.lead, typeof (AudioClip), true);
		MusicGlobal.backing = (AudioClip) EditorGUILayout.ObjectField("Backing instrument", MusicGlobal.backing, typeof (AudioClip), true);
		MusicGlobal.key = (KEY) EditorGUILayout.EnumPopup("Key", MusicGlobal.key);
		MusicGlobal.seed = EditorGUILayout.IntField("Seed", MusicGlobal.seed);

		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Generate"))
		{
			PlayMusic play = FindObjectOfType<PlayMusic>();
			play.Generate();
		}
		GUILayout.EndHorizontal();
	}
}
