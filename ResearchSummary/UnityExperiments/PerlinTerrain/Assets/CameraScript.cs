﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	public float speed = 50;
	public Terrain t;
	public float tilesize = 20;

	// Use this for initialization
	void Start () {
		var td = t.terrainData;
		var hm = new float[td.heightmapWidth, td.heightmapHeight];
		for (int i = 0; i < td.heightmapWidth; ++i)
			for (int j = 0; j < td.heightmapHeight; ++j)
			{
			hm[i,j] = Mathf.PerlinNoise((float)i / td.heightmapWidth * tilesize, (float)j / td.heightmapHeight * tilesize);
			}
		td.SetHeights(0, 0, hm);
	}
	
	// Update is called once per frame
	void Update () {
	
		Camera.main.transform.position += new Vector3 (0, 0, speed * Time.deltaTime);
	}
}
