using System;
using System.Collections.Generic;
using LotGeneration;
using UnityEngine;
using System.Linq;
using System.Text;
//using UnityEditor;
using HouseGen;
using graph;
using ScriptGen;

namespace graph
{
	[System.Serializable]
	public class VoronoiPolygon
	{
		internal HashSet<graph.VoronoiPolygon> neighbours;
		internal List<graph.Point> vertices;

		internal graph.Point center;

		public int npoints = 0;

		public float[] xpoints = new float[20];
		public float[] ypoints = new float[20];

		// The triangles forming this polygon
		internal HashSet<graph.Triangle> triangles = null;

		public int length = 0;
		public int depth = 0;

		public List<CityPrimordialEdge> primordialEdges;

		/// used to identify the polygon
		public virtual graph.Point getCenter()
		{
			return center;
		}

		public virtual graph.VoronoiPolygon setCenter(graph.Point center)
		{
			this.center = center;
			return this;
		}

		public virtual void addPoint(graph.Point p)
		{
			xpoints[npoints] = p.x;
			ypoints[npoints] = p.y;
			++npoints;
			vertices.Add(p);
		}

		public virtual void addPoint(Vector3 p)
		{
			xpoints[npoints] = p.x;
			ypoints[npoints] = p.z;
			++npoints;
			vertices.Add(new Point(p.x, p.z));
		}

		public VoronoiPolygon() : base()
		{
			neighbours = new HashSet<graph.VoronoiPolygon>();
			vertices = new List<graph.Point>();
			primordialEdges = new List<CityPrimordialEdge>();
		}

		public virtual void addNeighbour(graph.VoronoiPolygon p)
		{
			neighbours.Add(p);
		}

		public virtual HashSet<graph.VoronoiPolygon> getNeighbours()
		{
			return neighbours;
		}

		public override int GetHashCode()
		{
			return center.GetHashCode();
		}

		public virtual bool isAdjacent(graph.VoronoiPolygon p)
		{
			if (p == this)
			{
				return false;
			}
			foreach (graph.Edge e1 in graph.Edge.toEdges(this))
			{
				foreach (graph.Edge e2 in graph.Edge.toEdges(p))
				{
					if (e1.equals(e2))
					{
						return true;
					}
				}
			}
			return false;
		}

		public virtual int getElevation()
		{
			int avrg = 0;
			foreach (graph.Point p in vertices)
			{
				avrg += p.getElevation() == null ? 0 : p.getElevation();
				//BORDER IS OCEAN
				if (p.x < 0 || p.y < 0)
				{
					return 0;
				}
				if (p.x > 800 || p.y > 800)
				{
					return 0;
				}
			}
			return avrg / vertices.Count;
		}

		public virtual graph.Point getCentroid()
		{
			return graph.Point.average(vertices);
		}

		private void generateTriangles() {
			if (triangles != null) return;
			triangles = new HashSet<graph.Triangle>();

			Point lastP = null;
			Point firstP = null;
			foreach (Point p in vertices) {
				if (lastP != null) {
					triangles.Add(new Triangle(lastP, p, center));
				}
				else {
					firstP = p;
				}

				lastP = p;
			}
			triangles.Add(new Triangle(lastP, firstP, center));

		}

		public bool contains(Vector3 point) {
			generateTriangles();
			Point p = new Point(point.x, point.z);
			foreach (Triangle t in triangles) {
				if (t.contains(p)) {
					return true;
				}
			}

			return false;
		}

		private bool isSquareInside(float x, float y, float size) {
			bool ne = false;
			bool nw = false;
			bool se = false;
			bool sw = false;

			Point pne = new Point(x, y+size);
			Point pnw = new Point(x+size, y+size);
			Point pse = new Point(x, y);
			Point psw = new Point(x+size, y);

			generateTriangles();
			foreach (Triangle t in triangles) {
				ne = ne || t.contains(pne);
				nw = nw || t.contains(pnw);
				se = se || t.contains(pse);
				sw = sw || t.contains(psw);
			}

			return ne && nw && se && sw;
		}

		public LotMesh generateLots(Settings settings, PaletteKind lColor, float lotSize = 10f) {
			LotMesh lotMesh = null;
			// Determine the mesh dimensions and initialize it
			float minX = float.PositiveInfinity;
			float minY = float.PositiveInfinity;
			float maxX = float.NegativeInfinity;
			float maxY = float.NegativeInfinity;
			foreach (graph.Point p in vertices) {
				if (new Vector2(p.x, p.y).magnitude > 2000) return null;

				if (p.x < minX) {minX = p.x;}
				if (p.x > maxX) {maxX = p.x;}

				if (p.y < minY) {minY = p.y;}
				if (p.y > maxY) {maxY = p.y;}
			}

			int meshWidth = (int) Math.Ceiling((maxX-minX)/lotSize);
			int meshLength = (int) Math.Ceiling((maxY-minY)/lotSize);

			lotMesh = new LotMesh(meshWidth, meshLength);
			lotMesh.x = minX;
			lotMesh.y = minY;
			lotMesh.lotSize = lotSize;
			lotMesh.color = lColor;

			// For each square unit in the mesh, determine whether the square
			// is inside the voronoi polygon
			for (int i = 0; i < meshWidth; ++i) {
				for (int j = 0; j < meshLength; ++j) {
					if (isSquareInside(minX + i*lotSize, minY + j*lotSize, lotSize)) {
						lotMesh.mesh[i, j] = new Lot(minX + i*lotSize, minY + j*lotSize, lotSize, lotSize);
					}
				}
			}

			return lotMesh.doAssimilate(settings);
		}
	}
}
