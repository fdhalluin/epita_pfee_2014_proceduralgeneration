using UnityEngine;
using System.Collections.Generic;
using graph;

public class CityPrimordialEdge {
	public Vector3 mFirstPoint;
	public Vector3 mSecondPoint;
	public bool mOneSide;
	public bool mOtherSide;

	public CityPrimordialEdge(Vector3 iFirstPoint, Vector3 iSecondPoint) {
		mOneSide = false;
		mOtherSide = false;
		mFirstPoint = iFirstPoint;
		mSecondPoint = iSecondPoint;
	}

	public void ProcessSides(CityGenerator iGen, HashSet<VoronoiPolygon> voroPolys) {
		Vector3 lMiddlePosition = (mFirstPoint + mSecondPoint) / 2;
		
		Vector3 offset = mFirstPoint - mSecondPoint;
		offset.Normalize();
		offset = Quaternion.Euler(0, 90, 0) * offset;
		offset *= 0.1f;
		
		Vector3 oneSide = lMiddlePosition + offset;
		Vector3 otherSide = lMiddlePosition - offset;

		mOneSide = !iGen.isInQuarters(oneSide, voroPolys) && Mathf.Abs(oneSide.y - iGen.mCityCenter.y) <= iGen.mSettings.mMaxHeightDiff;
		mOtherSide = !iGen.isInQuarters(otherSide, voroPolys) && Mathf.Abs(otherSide.y - iGen.mCityCenter.y) <= iGen.mSettings.mMaxHeightDiff;
	}
}