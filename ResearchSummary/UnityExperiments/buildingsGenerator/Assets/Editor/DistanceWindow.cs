using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

namespace Editor
{
class DistanceWindow : EditorWindow
{
	bool recMode = false;
	string recModeButtonText;
	Vector3 point1;
	Vector3 point2;
	int recordedPoints = 0;
	bool waitingForMouseUp = false;
	List<Vector3> renderList;

	public DistanceWindow() {
		renderList = new List<Vector3>();
	}
	
	[MenuItem ("Window/Distance window")]
	static void Init () {
		DistanceWindow window = (DistanceWindow)EditorWindow.GetWindow (typeof (DistanceWindow));
	}

	void SceneGUIDraw(SceneView sceneView) {
		
		for (int i = 0; i < renderList.Count;++i) {
			if (i > 0 && i%2 == 1) {
				drawDistance(renderList[i], renderList[i-1]);
			}
		}
		
		if (renderList.Count % 2 == 1) {
			RaycastHit hit;
			Event curr = Event.current;
			Ray ray = HandleUtility.GUIPointToWorldRay(curr.mousePosition);
			Vector3 point2;
			if (Physics.Raycast(ray, out hit)) {
				point2 = hit.point;
				drawDistance(renderList[renderList.Count-1], point2);
			}
		}
		
		HandleUtility.Repaint();
	}
	
	void drawDistance(Vector3 p1, Vector3 p2) {
		Handles.DrawLine(p1, p2);
		Handles.Label((p1+p2)/2 + new Vector3(-0.3f, 0.1f, 0), ""+Vector3.Distance(p1,p2));
	}
	
	void OnDestroy() {
		if (recMode) {
			SceneView.onSceneGUIDelegate -= OnSceneGUI;
		}
		else {
			SceneView.onSceneGUIDelegate -= SceneGUIDraw;
		}
	}
	
	void OnSceneGUI (SceneView sceneView) {
		int ID = GUIUtility.GetControlID(GetHashCode(), FocusType.Passive);
		Event curr = Event.current;
		EventType type = curr.GetTypeForControl(ID);
		
		SceneGUIDraw(sceneView);
		
		if (type == EventType.Layout) {
			HandleUtility.AddDefaultControl(ID);
		}
		
		if (type == EventType.MouseUp) {
			waitingForMouseUp = false;
			if (GUIUtility.hotControl == ID) {
				GUIUtility.hotControl = 0;
			}
			
		}
		
		if (type == EventType.MouseDown && recMode && !waitingForMouseUp && HandleUtility.nearestControl == ID) {
		RaycastHit hit;
		
		if (recordedPoints == 0) {
			Ray ray = HandleUtility.GUIPointToWorldRay(curr.mousePosition);
			if (Physics.Raycast(ray, out hit)) {
				point1 = hit.point;
				++recordedPoints;
				waitingForMouseUp = true;
				Debug.Log("GOT POINT 1");
				GUIUtility.hotControl = ID;
				renderList.Add(point1);
				curr.Use();
				Repaint();
				return;
			}
		}

		if (recordedPoints == 1) {
			Ray ray = HandleUtility.GUIPointToWorldRay(curr.mousePosition);
			if (Physics.Raycast(ray, out hit)) {
				point2 = hit.point;
				++recordedPoints;
				Debug.Log("GOT POINT 2");
				curr.Use();
				recMode = false;
				Repaint();
				SceneView.onSceneGUIDelegate -= OnSceneGUI;
				SceneView.onSceneGUIDelegate += SceneGUIDraw;
				renderList.Add(point2);
				return;
			}
		}
		
		}
		
	}
	
	void OnGUI () {
		GUILayout.Label ("THIS IS A TEXT");
		
		if (!recMode) {
			recModeButtonText = "Start recording points";
		}
		
		if (GUILayout.Button(recModeButtonText)) {
			if (!recMode) {
				recMode = true;
				recModeButtonText = "Click on the first point ...";
				recordedPoints = 0;
				SceneView.onSceneGUIDelegate -= SceneGUIDraw;
				SceneView.onSceneGUIDelegate += OnSceneGUI;
			}			
		}
		
		if (recordedPoints == 1) {
			GUILayout.Label("From: " + renderList[renderList.Count-1]);
			recModeButtonText = "Click on the second point ...";
		}
		if (recordedPoints > 1) {
			GUILayout.Label("From: " + renderList[renderList.Count-2]);
			GUILayout.Label("To: " + renderList[renderList.Count-1]);
			GUILayout.Label("XYZ distance: " + Vector3.Distance(point1, point2));
		}
		
		if (renderList.Count > 1) {
			if (GUILayout.Button("Clear the point list")) {
				renderList.Clear();
				recordedPoints = 0;
				recMode = false;
			}
		}
		
	}
	
}

}
