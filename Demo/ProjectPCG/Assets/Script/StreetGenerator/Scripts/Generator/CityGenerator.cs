using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
//using UnityEditor;
using LotGeneration;
using HouseGen;
using graph;
using ScriptGen;

public class CityGenerator : MonoBehaviour {
	private StreetGenerator mStreetGenerator;

	private List<GameObject> mTrash = new List<GameObject>();
	private List<GameObject> mHouses = new List<GameObject>();
	private List<GameObject> mProps = new List<GameObject>();
	private List<GameObject> mBridges = new List<GameObject>();
	private List<GameObject> mTrees = new List<GameObject>();
	private List<GameObject> mOutsideStreets = new List<GameObject>();
	private List<CityNode> mOutsideNodes = new List<CityNode>();
	private List<CityNode> mNodes = new List<CityNode>();
	private List<CityEdge> mEdges = new List<CityEdge>();
	private List<CityQuarter> mQuarters = new List<CityQuarter>();
	private List<CityPrimordialEdge> mPrimordialEdges = new List<CityPrimordialEdge>();
	public List<Pair<Vector3, Vector3>> mInternalEdges = new List<Pair<Vector3, Vector3>>();
	
	private Voronoi mVoronoi;
	private Graph<CartoNode> mGraphCarto = new Graph<CartoNode>();
	private Graph<CityNode> mGraphCity = new Graph<CityNode>();
	private CartoNode[,] mMap;
	private CityQuarter mCenterQuarter;
	public Vector3 mCityCenter = new Vector3(0, 0, 0);

	public Settings mSettings = new Settings();

	private double[] mXTab;
	private double[] mZTab;

	private GameObject mRootMainRoads;
	private GameObject mRootTrees;
	private GameObject mRootBarriers;
	private GameObject mRootCarts;
	private GameObject mRootBridges;
	private GameObject mRootSheeps;
	private GameObject mRootBarrels;
	private GameObject mRootMarkets;
	private GameObject mRootPylones;
	public GameObject mRootHouses;

	public LightSettings LightMorning;
	public LightSettings LightNoon;
	public LightSettings LightOvercast;
	public LightSettings LightEvening;
	public LightSettings LightNight;

	public GameObject pFirstTree;
	public GameObject pSecondTree;
	public GameObject pBarrier;
	public GameObject pCart1;
	public GameObject pCart2;
	public GameObject pCart3;
	public GameObject pBridge;
	public GameObject pSheep;
	public GameObject pGrass;
	public GameObject pFontain;
	public GameObject pBarrel;
	public GameObject pMarket;
	public GameObject pPylone;
	public GameObject pTraffic;
	public GameObject pLight;
	public GameObject pPhone;

	private float mAverage = 0.0f;
	private Material baseMaterial;

	private float[,,] mAlphaData;


	/* Create all needed gameobjects */
	void Initialize()
	{
		if (mStreetGenerator == null)
		{
			mStreetGenerator = new StreetGenerator();
			mStreetGenerator.cityGenerator = this;
		}

		if (mRootBarriers == null)
		{
			mRootBarriers = GameObject.Find("Barriers");
			if (mRootBarriers == null)
			{
				mRootBarriers = new GameObject();
				mRootBarriers.name = "Barriers";
			}
		}

		if (mRootBridges == null)
		{
			mRootBridges = GameObject.Find("Bridges");
			if (mRootBridges == null)
			{
				mRootBridges = new GameObject();
				mRootBridges.name = "Bridges";
			}
		}

		if (mRootCarts == null)
		{
			mRootCarts = GameObject.Find("Props");
			if (mRootCarts == null)
			{
				mRootCarts = new GameObject();
				mRootCarts.name = "Props";
			}
		}

		if (mRootMainRoads == null)
		{
			mRootMainRoads = GameObject.Find("Roads");
			if (mRootMainRoads == null)
			{
				mRootMainRoads = new GameObject();
				mRootMainRoads.name = "Roads";
			}
		}

		if (mRootSheeps == null)
		{
			mRootSheeps = GameObject.Find("Sheeps");
			if (mRootSheeps == null)
			{
				mRootSheeps = new GameObject();
				mRootSheeps.name = "Sheeps";
			}
		}

		if (mRootHouses == null)
		{
			mRootHouses = GameObject.Find("Houses");
			if (mRootHouses == null)
			{
				mRootHouses = new GameObject();
				mRootHouses.name = "Houses";
			}
		}

		if (mRootTrees == null)
		{
			mRootTrees = GameObject.Find("Trees");
			if (mRootTrees == null)
			{
				mRootTrees = new GameObject();
				mRootTrees.name = "Trees";
			}
		}

		if (mRootBarrels == null)
		{
			mRootBarrels = GameObject.Find("Barrels");
			if (mRootBarrels == null)
			{
				mRootBarrels = new GameObject();
				mRootBarrels.name = "Barrels";
			}
		}

		if (mRootMarkets == null)
		{
			mRootMarkets = GameObject.Find("Markets");
			if (mRootMarkets == null)
			{
				mRootMarkets = new GameObject();
				mRootMarkets.name = "Markets";
			}
		}

		if (mRootPylones == null)
		{
			mRootPylones = GameObject.Find("Pylones");
			if (mRootPylones == null)
			{
				mRootPylones = new GameObject();
				mRootPylones.name = "Pylones";
			}
		}
	}

	/* Unused : show generated lots */
	public void ShowLots() {
		baseMaterial = new Material(Shader.Find("Particles/Additive"));
		foreach (CityQuarter lQuarter in mQuarters) {
			foreach (VoronoiPolygon lPoly in lQuarter.mStreetLotPoly) {
				mTrash.Add(Util.drawPolygon(lPoly, 1, baseMaterial));
			}
			Util.drawLots(lQuarter.mLots, mTrash, baseMaterial);
		}
	}

	/* Apply all textures trought the city */
	public void ApplyTexture() {
		CreateGraphCarto();
		foreach (CityNode lNode in mNodes) {
			lNode.mIsConnect = false;
		}
		TerrainData lData = Terrain.activeTerrain.terrainData;
		mAlphaData = lData.GetAlphamaps(0, 0, lData.alphamapWidth, lData.alphamapHeight);

		foreach (CityEdge lCityEdge in mEdges) {
			if (lCityEdge.mThereIsHouse && (lCityEdge.mKind == EdgeKind.external || lCityEdge.mKind == EdgeKind.major)) {
				ChangeTextureRoad(lCityEdge.mFirstNode.mPosition, lCityEdge.mSecondNode.mPosition, 0, 20);
			}
		}

		foreach (CityQuarter lQuarter in mQuarters) {
			ChangeTextureQuarter(lQuarter);
		}

		foreach (CityEdge lCityEdge in mEdges) {
			if (lCityEdge.mKind == EdgeKind.external) {
				ChangeTextureRoad(lCityEdge.mFirstNode.mPosition, lCityEdge.mSecondNode.mPosition, 4, 6);
			}
		}

		foreach (CityEdge lCityEdge in mEdges) {
			if (lCityEdge.mKind == EdgeKind.minor) {
				ChangeTextureRoad(lCityEdge.mFirstNode.mPosition, lCityEdge.mSecondNode.mPosition, 4, 4);
			}
		}
		
		foreach (CityEdge lCityEdge in mEdges) {
			if (lCityEdge.mKind == EdgeKind.medium) {
				ChangeTextureRoad(lCityEdge.mFirstNode.mPosition, lCityEdge.mSecondNode.mPosition, 4, 6);
			}
		}

		foreach (Pair<Vector3, Vector3> lPair in mInternalEdges) {
			ChangeTextureRoad(lPair.First, lPair.Second, 4, 4);
		}

		PostRemoveBadTrees();

		foreach (GameObject lObject in mTrees) {
			if (lObject != null) {
				ChangeTexture(lObject.transform.position, 1, 5);
			}
		}

		foreach (CityEdge lCityEdge in mEdges) {
			if (lCityEdge.mKind == EdgeKind.major) {
				ChangeTextureRoad(lCityEdge.mFirstNode.mPosition, lCityEdge.mSecondNode.mPosition, 5, 8);
			}
		}

		foreach (GameObject lObject in mOutsideStreets) {
			DestroyImmediate(lObject);
		}
		
		foreach (CityNode lNode in mOutsideNodes) {
			LinkOutsideNode(lNode.mPosition, true, EdgeKind.major);
		}

		if (!mSettings.mDebug) {
			foreach (GameObject lObject in mOutsideStreets)
			{
				DestroyImmediate(lObject);
			}
			
			if (mRootMainRoads != null)
			{
				GameObject.DestroyImmediate(mRootMainRoads);
				mRootMainRoads = new GameObject();
				mRootMainRoads.name = "Roads";
			}
		}

		//CreateRoadMesh();
		lData.SetAlphamaps(0, 0, mAlphaData);
	}

	/* Unused : create a road mesh */
	private void CreateRoadMesh() {
		float roadSize = 1f;
		StreetMeshGenerator smg = new StreetMeshGenerator();
		Dictionary<int, List<int>> neighbours = new Dictionary<int, List<int>>();
		List<int> alreadyProcessed = new List<int>();
		
		foreach (CityNode lNode in mNodes) {
			lNode.mID = alreadyProcessed.Count();
			smg.addPoint("cN_" + lNode.mID, lNode.mPosition);
			alreadyProcessed.Add (lNode.mID);
		}
		
		alreadyProcessed.Clear();
		
		Debug.Log ("PASSED");
		
		foreach (CityEdge lCityEdge in mEdges) {
			if (!neighbours.ContainsKey(lCityEdge.mFirstNode.mID)) {
				neighbours.Add(lCityEdge.mFirstNode.mID, new List<int>());
			}
			if (!neighbours.ContainsKey(lCityEdge.mSecondNode.mID)) {
				neighbours.Add(lCityEdge.mSecondNode.mID, new List<int>());
			}
			
			neighbours[lCityEdge.mFirstNode.mID].Add(lCityEdge.mSecondNode.mID);
			neighbours[lCityEdge.mSecondNode.mID].Add(lCityEdge.mFirstNode.mID);		
		}
		
		// Sorting neighbours to iterate through them in a determined order
		foreach(int node in neighbours.Keys) {
			neighbours[node].Sort((n1, n2)=>{
				Vector3 dir1 = smg.getPoint("cN_" + n1) - smg.getPoint("cN_" + node);
				Vector3 dir2 = smg.getPoint("cN_" + n2) - smg.getPoint("cN_" + node);
				float angle1 = Mathf.Atan2(dir1.z, dir1.x);
				float angle2 = Mathf.Atan2(dir2.z, dir2.x);
				
				return angle2.CompareTo(angle1);
			});
		}
		
		// Create intermediate points
		string inter = "int_";
		foreach(int node in neighbours.Keys) {
			// Dangling points should not exist anyway
			if (neighbours[node].Count() < 2) continue;
			Vector3 currNode = smg.getPoint("cN_" + node);
			// This is a waypoint, the most common type of node
			if (neighbours[node].Count() == 2) {
				Vector3 dir1 = smg.getPoint("cN_" + neighbours[node][0]) - currNode;
				Vector3 dir2 = smg.getPoint("cN_" + neighbours[node][1]) - currNode;
				dir1.Normalize();
				dir2.Normalize();
				if (Vector3.Dot(dir1, dir2) < 0) dir2 *= -1;
				Vector3 dir = (dir1+dir2)/2;

				dir = smg.getPoint("cN_" + neighbours[node][1]) - smg.getPoint("cN_" + neighbours[node][0]);
				dir.Normalize();
				dir = new Vector3(-dir.z, 0, dir.x);


				dir.Normalize();

				dir *= roadSize;
				
				smg.addPoint(inter + node + "_" + 1, currNode + dir);
				smg.addPoint(inter + node + "_" + 0, currNode - dir);

			}
			// This is an intersection between 3 or more roads
			if (neighbours[node].Count() > 2) {
				
				for(int i = 0; i < neighbours[node].Count(); ++i) {
					Vector3 p1 = smg.getPoint("cN_" + neighbours[node][i]);
					Vector3 p2 = smg.getPoint("cN_" + neighbours[node][(i+1)%neighbours[node].Count()]);
					Vector3 dir1 = (p1 - currNode).normalized;
					Vector3 dir2 = (p2 - currNode).normalized;
					Vector3 dir = ((p1+p2)/2 - currNode).normalized;
					Vector3 right = Vector3.Cross (dir2, Vector3.up);
					
					if (Vector3.Angle(right, dir1) > 90) {
						dir *= -1;
					}
					dir *= roadSize;

					smg.addPoint(inter + node + "_" + (i+1)%neighbours[node].Count(), currNode+dir);
				}
			}
		}
		
		// Draw roads
		
		foreach(int node in neighbours.Keys) {
			// Dangling points should not exist anyway
			if (neighbours[node].Count() < 2) continue;
			for(int i = 0; i < neighbours[node].Count(); ++i) {
				int nextNode = neighbours[node][i];
				if (alreadyProcessed.Contains(nextNode)) {
					continue;
				}
				
				// Index of our node in the nextNode neighbourhood
				int j = 0;
				for (; j < neighbours[nextNode].Count(); ++j) {
					if (neighbours[nextNode][j] == node) {
						break;
					}
				}
				string p1 = inter + node + "_" + i;
				string p2 = inter + node + "_" + (i+1)%neighbours[node].Count();
				string p3 = inter + nextNode + "_" + j;
				string p4 = inter + nextNode + "_" + (j+1)%neighbours[nextNode].Count();
				
				smg.addQuadrilateral(p1, p2, p3, p4, Util.rand.Next(0, 4), false);
				
			}
			
			alreadyProcessed.Add(node);
		}
		
		GameObject roads = smg.createGameObject();
		mTrash.Add (roads);
	}

	/*
	 * Entrypoint of the generation
	 */
	public void GenerateCity(Settings iSettings)
	{
		mSettings = iSettings;

		if (mSettings.mRandom) {
			int lSeed = (int) Random.Range(1, 300);
			Random.seed = lSeed;
			Debug.Log("Random seed : " + lSeed);
		} else {
			Random.seed = mSettings.mSeed;
		}

		Initialize();

		CreateGraphCarto();
		FindVillageCenter();

		CreateNodes(mCityCenter);
		CreateCityStructure();
		CreateOutsideNodes();
		UpdateCityCenter();
		AssignLot();

		CreateGraphCity();
		DetermineRoadPriority();

		PrintEdges();
	}

	/*
	 * Delete all generated gameobject and reinit data
	 */ 
	public void ClearAll()
	{
		Initialize();

		foreach (GameObject lObject in mOutsideStreets)
		{
			DestroyImmediate(lObject);
		}

		foreach (GameObject lObject in mHouses)
		{
			DestroyImmediate(lObject);
		}

		foreach (GameObject lObject in mTrash)
		{
			DestroyImmediate(lObject);
		}

		foreach (GameObject lObject in mTrees)
		{
			DestroyImmediate(lObject);
		}

		foreach (GameObject lObject in mBridges)
		{
			DestroyImmediate(lObject);
		}

		if (mStreetGenerator != null) {
			mStreetGenerator.ClearAll();
		}
		mSettings.mInterestingLots.Clear();
		mInternalEdges.Clear();
		mPrimordialEdges.Clear();
		mEdges.Clear();
		mNodes.Clear();
		mHouses.Clear();
		mTrash.Clear();
		mBridges.Clear();
		mQuarters.Clear();
		mOutsideStreets.Clear();
		mOutsideNodes.Clear();
		mGraphCarto.mNodes.Clear();
		mGraphCity.mNodes.Clear();
		mCityCenter = new Vector3(0, 0, 0);
		mAverage = 0.0f;
		Point.count = 0;
		houseBuilder.totalLights = mSettings.mNbLights;

		if (mRootBarriers != null)
		{
			GameObject.DestroyImmediate(mRootBarriers);
		}
		
		if (mRootBridges != null)
		{
			GameObject.DestroyImmediate(mRootBridges);
		}
		
		if (mRootCarts != null)
		{
			GameObject.DestroyImmediate(mRootCarts);
		}
		
		if (mRootMainRoads != null)
		{
			GameObject.DestroyImmediate(mRootMainRoads);
		}

		if (mRootSheeps != null)
		{
			GameObject.DestroyImmediate(mRootSheeps);
		}

		if (mRootBarrels != null)
		{
			GameObject.DestroyImmediate(mRootBarrels);
		}

		if (mRootMarkets != null)
		{
			GameObject.DestroyImmediate(mRootMarkets);
		}
		
		if (mRootHouses != null)
		{
			GameObject.DestroyImmediate(mRootHouses);
		}
		
		if (mRootTrees != null)
		{
			GameObject.DestroyImmediate(mRootTrees);
		}

		if (mRootPylones != null)
		{
			GameObject.DestroyImmediate(mRootPylones);
		}
	}

	/* Update skybox */
	public void UpdateSky()	{
		switch(mSettings.mSkyKind) {
		case SkyKind.morning:
			if (LightMorning != null)
				LightMorning.Apply();
			TurnOffLight();
			break;
		case SkyKind.noon:
			if (LightNoon != null)
				LightNoon.Apply();
			TurnOffLight();
			break;
		case SkyKind.overcast:
			if (LightOvercast != null)
				LightOvercast.Apply();
			TurnOffLight();
			break;
		case SkyKind.evening:
			if (LightEvening != null)
				LightEvening.Apply();
			TurnOnLight();
			break;
		case SkyKind.night:
			if (LightNight != null)
				LightNight.Apply();
			TurnOnLight();
			break;
		}
	}

	/*
	 * Generate all props in the city
	 */
	public void BuildHouses() {
		mStreetGenerator.ConstructScript(mSettings);
		PlaceInternalHouses();
		PlaceExternalHouses();
		if (mSettings.mHaveCityPlace) {
			PlaceCenterVillage();
		}
		EncloseFields();
		if (mSettings.mHavePylones) {
			ConstructPyloneWay();
		}
		if (mSettings.mHaveStreetLights) {
			FindPlaceTrafficLight();
			FindPlaceLight();
		}
		if (mSettings.mHavePhones) {
			FindPlacePhone();
		}

		FindPlaceTree();

		UpdateSky();
	}

	/* Create the center place of the village, with market, cart and fontain */
	private void PlaceCenterVillage() {
		if (!mCenterQuarter.mIsField) {
			PlaceFontain(mCenterQuarter.mPosition);
			Vector3 lCenter = mCenterQuarter.mPosition;

			if (GetQuarterSize(mCenterQuarter) > 10000) {
				float lX, lZ;
				float lAngle = 0;
				float lStepSize = Mathf.PI / 3;
				float lRadius = 12f;
			
				while (lAngle < 2 * Mathf.PI) {
					lX = lRadius * Mathf.Cos(lAngle);
					lZ = lRadius * Mathf.Sin(lAngle);
					Vector3 lPosition = new Vector3(lX, 0, lZ) + lCenter;
					lPosition.y = GetY(lPosition);
					Vector3 lRotation = CalculRotation(lCenter, lPosition);
					lRotation.y += 180;
					PlaceMarket(lPosition, lRotation);
					lAngle += lStepSize;
				}

				lStepSize = Mathf.PI / 5;
				lRadius = 17f;
				lAngle = 0;

				while (lAngle < 2 * Mathf.PI) {
					lX = lRadius * Mathf.Cos(lAngle);
					lZ = lRadius * Mathf.Sin(lAngle);
					Vector3 lPosition = new Vector3(lX, 0, lZ) + lCenter;
					lPosition.y = GetY(lPosition);
					if (Random.Range(0, 9) > 4.5f) {
						PlaceBarrel(lPosition);
					} else {
						PlaceCart(lPosition);
					}
					lAngle += lStepSize;
				}
			}
		}
	}

	/* Mutation of a color, based on main, secondary and minor color */
	public PaletteKind ColorMutation(PaletteKind iColor) {
		if (iColor == mSettings.mFirstPaletteKind && Random.Range(0, 9) >= 7f) {
			iColor = mSettings.mSecondPaletteKind;
		} else if (iColor == mSettings.mSecondPaletteKind && Random.Range(0, 9) >= 7f) {
			iColor = mSettings.mFirstPaletteKind;
		}

		if (Random.Range(0, 9) >= 8f) {
			iColor = mSettings.mThirdPaletteKind;
		}

		return iColor;
	}

	/* Create lot trough the city */
	private void AssignLot() {
		float dist = mSettings.mLotDepth;

		foreach (CityQuarter lQuarter in mQuarters) {
			PaletteKind lColor;
			if (Random.Range(0, 9) > 4) {
				lColor = mSettings.mFirstPaletteKind;
			} else {
				lColor = mSettings.mSecondPaletteKind;
			}
			if (!lQuarter.mIsField) {
				Vector3 center = lQuarter.mPosition;
				List<CityNode> lPrimordialNode = new List<CityNode>();
				List<CityNode> interiorNodes = new List<CityNode>();
				List<CityNode> lCenterNodes = new List<CityNode>();
				VoronoiPolygon interiorVoroPoly = new VoronoiPolygon();
				Vector3 interiorCenter = new Vector3(0, 0, 0);

				foreach (CityNode lNode in lQuarter.mNodes) {
					if (lNode.mPoint.primordial) {
						lPrimordialNode.Add(lNode);
					}
				}

				foreach (CityNode cNode in lPrimordialNode) {
					Vector3 position = Util.getPointByDistance(cNode.mPosition, center, 12);
					CityNode newNode = new CityNode(position);
					newNode.mPoint = new Point(newNode.mPosition.x, newNode.mPosition.z);
					newNode.mPoint.cityNode = newNode;
					interiorNodes.Add(newNode);
				}

				foreach (CityNode cNode in interiorNodes) {
					Vector3 position;
					if (Vector3.Distance(center, cNode.mPosition) < dist) {
						position = Util.getPointByDistance(cNode.mPosition, center, 3 * (Vector3.Distance(center, cNode.mPosition) - dist) / 4);
					} else {
						position = Util.getPointByDistance(cNode.mPosition, center, dist);
					}
					
					CityNode newNode = new CityNode(position);
					newNode.mPoint = new Point(newNode.mPosition.x, newNode.mPosition.z);
					newNode.mPoint.cityNode = newNode;
					
					lCenterNodes.Add(newNode);
					lQuarter.mCenterPoly.addPoint(newNode.mPoint);
					interiorVoroPoly.addPoint(newNode.mPoint);
					interiorCenter += newNode.mPoint.toVector3();			
				}

				interiorCenter /= lCenterNodes.Count();
				interiorVoroPoly.setCenter(new Point(interiorCenter.x, interiorCenter.z));
				if (lQuarter != mCenterQuarter || !mSettings.mHaveCityPlace) {
					lQuarter.mLots = interiorVoroPoly.generateLots(mSettings, ColorMutation(lColor), 15);
				}

				// Construct each trapezoid
				for (int i = 0; i < interiorNodes.Count; ++i) {
					int currInd = i;
					int lastInd = ((i + interiorNodes.Count) - 1) % interiorNodes.Count;
					CityNode currNode = interiorNodes[currInd];
					CityNode lastNode = interiorNodes[lastInd];

					VoronoiPolygon voroPoly = new VoronoiPolygon();

					voroPoly.addPoint(currNode.mPoint);
					voroPoly.addPoint(lastNode.mPoint);

					voroPoly.addPoint(lCenterNodes[lastInd].mPoint);
					voroPoly.addPoint(lCenterNodes[currInd].mPoint);

					List<VoronoiPolygon> rectangles = findRectangle(voroPoly);
					foreach (VoronoiPolygon rect in rectangles) {
						Vector3 p1 = lPrimordialNode[currInd].mPosition;
						Vector3 p2 = lPrimordialNode[lastInd].mPosition;
						Vector3 projectedLotCenter = projection(p1, p2, rect.center.toVector3());
						mInternalEdges.Add(new Pair<Vector3, Vector3>(projectedLotCenter, rect.center.toVector3()));
						lQuarter.mStreetLotPoly.Add(rect);	
					}
				}
			}

			foreach (VoronoiPolygon lVoroPoly in lQuarter.mStreetLotPoly) {
				if (!mSettings.mPopulated) {
					Lot lLot = new Lot(0, 0, lVoroPoly.depth, lVoroPoly.length);
					Point lFirstPoint = lVoroPoly.vertices[0];
					Point lSecondPoint = lVoroPoly.vertices[1];
					Point lThirdPoint = lVoroPoly.vertices[2];
					lLot.firstPoint = new Vector3(lFirstPoint.x, GetY(lFirstPoint.x, lFirstPoint.y), lFirstPoint.y);
					lLot.secondPoint = new Vector3(lSecondPoint.x, GetY(lSecondPoint.x, lSecondPoint.y), lSecondPoint.y);
					lLot.lotCenter = (new Vector3(lFirstPoint.x, GetY(lFirstPoint.x, lFirstPoint.y), lFirstPoint.y)
					                  + new Vector3(lThirdPoint.x, GetY(lThirdPoint.x, lThirdPoint.y), lThirdPoint.y)) / 2;
					lLot.color = ColorMutation(lColor);
					mSettings.mInterestingLots.Push(lLot);
				} else {
					float lMarge = 5;
					
					Point lFirstPoint = lVoroPoly.vertices[0];
					Point lSecondPoint = lVoroPoly.vertices[1];
					Point lThirdPoint = lVoroPoly.vertices[2];
					Point lFourthPoint = lVoroPoly.vertices[3];
					
					Vector3 lFirstPosition = lFirstPoint.toVector3();
					Vector3 lSecondPosition = lSecondPoint.toVector3();
					Vector3 lThirdPosition = lThirdPoint.toVector3();
					Vector3 lFourthPosition = lFourthPoint.toVector3();
					
					Vector3 lLeftMiddle = (lSecondPosition + lThirdPosition) / 2;
					Vector3 lRightMiddle = (lFirstPosition + lFourthPosition) / 2;
					Vector3 lCenter = (lFirstPosition
					                   + lThirdPosition) / 2;
					
					// First
					
					Lot lLot = new Lot(0, 0, lVoroPoly.depth, lVoroPoly.length);
					lLot.firstPoint = Vector3.MoveTowards(lFirstPosition, lSecondPosition, lMarge);
					lLot.secondPoint = Vector3.MoveTowards(lSecondPosition, lFirstPosition, -lMarge);
					lLot.lotCenter = Vector3.MoveTowards(lCenter, lLeftMiddle, lMarge);
					lLot.color = ColorMutation(lColor);
		
					mSettings.mInterestingLots.Push(lLot);
					
					// Second
					
					Lot lSecondLot = new Lot(0, 0, lVoroPoly.depth, lVoroPoly.length);
					lSecondLot.firstPoint = Vector3.MoveTowards(lSecondPosition, lFirstPosition, lMarge);
					lSecondLot.secondPoint = Vector3.MoveTowards(lFirstPosition, lSecondPosition, -lMarge);
					lSecondLot.lotCenter = Vector3.MoveTowards(lCenter, lRightMiddle, lMarge);
					lSecondLot.color = ColorMutation(lColor);

					mSettings.mInterestingLots.Push(lSecondLot);


					// Third 

					if (lQuarter != mCenterQuarter || !mSettings.mHaveCityPlace) {
						Lot lThirdLot = new Lot(0, 0, lVoroPoly.depth, lVoroPoly.length);
						lThirdLot.firstPoint = Vector3.MoveTowards(lFirstPosition, lQuarter.mPosition, 14);
						lThirdLot.secondPoint = Vector3.MoveTowards(lSecondPosition, lQuarter.mPosition, 14);
						lThirdLot.lotCenter = Vector3.MoveTowards((lFirstPosition + lThirdPosition) / 2, lQuarter.mPosition, 14);
						lThirdLot.color = ColorMutation(lColor);

						mSettings.mInterestingLots.Push(lThirdLot);
					}
				}
			}
		}
	}

	/* Process a projection on a segment create from p1 to p2 */
	private Vector3 projection(Vector3 p1, Vector3 p2, Vector3 toProject) {
		float a = (p2.z - p1.z) / (p2.x - p1.x + 0.00001f);
		float b = p2.z - a * p2.x;

		float a2 = -1/(a + 0.00001f);
		float b2 = toProject.z - a2 * toProject.x;

		float x = (b2-b)/(a-a2);
		float z = a*x + b;

		return new Vector3(x, GetY(x, z), z);
	}

	// Find rectangle areas in a trapezoid
	private List<VoronoiPolygon> findRectangle(VoronoiPolygon trapezoid) {
		VoronoiPolygon ans = new VoronoiPolygon();
		List<VoronoiPolygon> lotList = new List<VoronoiPolygon>();
		Vector3[] point = new Vector3[4];
		Vector3[] rect = new Vector3[4];
		Vector3 center = new Vector3(0, 0, 0);
		int i = 0;
		int lotLength = 10;

		// Load the point in temporary variable
		for (i = 0; i < 4; ++i) {
			point[i] = trapezoid.vertices[i].toVector3();
			center += point[i];
		}
		center /= 4;

		// Align the trapezoid to the X-axis
		float angle = Mathf.Atan2(point[0].z-point[1].z, point[0].x-point[1].x);

		for (i = 0; i < 4; ++i) {
			point[i] = Util.rotatePoint(point[i], center, -angle, Axis.Y);
		}

		// Project the point of the non-aligned sign to the plane orthogonal to the X-aligned edge
		Vector4 plane = Util.computePlane(point[0], point[1], (point[0] + point[1] + new Vector3(0, 1, 0))/2);
		Vector4 oppositePlane = Util.computePlane(point[2], point[3], (point[2] + point[3] + new Vector3(0, 1, 0))/2);

		point[2] = Util.projectOnPlane(point[2], plane, Axis.Z);
		point[3] = Util.projectOnPlane(point[3], plane, Axis.Z);

		// Sort all those point to take the middle ones
		// This ensures that the rectangle will be contained in the trapezoid
		List<Vector3> pointList = new List<Vector3>();
		for (i = 0; i < 4; ++i) {
			pointList.Add (point[i]);
		}
		pointList.Sort((a, b) => {return (a.x<b.x?-1:1);});

		rect[0] = pointList[1];
		rect[1] = pointList[2];

		pointList.Clear();

		// The base for the rectangle has been determined
		rect[2] = Util.projectOnPlane(rect[1], oppositePlane, Axis.Z);
		rect[3] = Util.projectOnPlane(rect[0], oppositePlane, Axis.Z);

		float length0 = Vector3.Distance(rect[0], rect[3]); 
		float length1 = Vector3.Distance(rect[1], rect[2]);

		if (length0 < length1) {
			rect[2].z = rect[3].z;
		}
		else {
			rect[3].z = rect[2].z;
		}

		// Make the rectangle dimensions integers
		length1 = Mathf.Floor(Vector3.Distance(rect[0], rect[3]));
		rect[2].z = rect[0].z + length1*Mathf.Sign (rect[2].z - rect[1].z);
		rect[3].z = rect[0].z + length1*Mathf.Sign (rect[2].z - rect[1].z);

		length0 = Mathf.Floor(Vector3.Distance(rect[0], rect[1]));
		rect[2].x = rect[0].x + length0*Mathf.Sign (rect[1].x - rect[0].x);
		rect[1].x = rect[0].x + length0*Mathf.Sign (rect[1].x - rect[0].x);

		float horizontalDir = Mathf.Sign (rect[1].x - rect[0].x);
		float verticalDir = Mathf.Sign (rect[2].z - rect[1].z);
		for (i = 0; (i+1)*lotLength <= length0; ++i) {
			VoronoiPolygon lot = new VoronoiPolygon();
			Vector3[] p = new Vector3[4];
			Vector3 lotCenter = new Vector3(0, 0, 0);

			p[0] = rect[0] + horizontalDir * (new Vector3(i * lotLength, 0, 0));
			p[1] = rect[0] + horizontalDir * (new Vector3((i + 1) * lotLength, 0, 0));

			p[2] = rect[0] + horizontalDir * (new Vector3((i + 1) * lotLength, 0, 0)) + verticalDir * (new Vector3(0, 0, length1));
			p[3] = rect[0] + horizontalDir * (new Vector3(i * lotLength, 0, 0))  + verticalDir * (new Vector3(0, 0, length1));

			lot.length = (int)lotLength;
			lot.depth = (int)length1;

			lotCenter = (p[0] + p[1] + p[2] + p[3])/4;

			for(int j = 0; j < 4; ++j) {
				p[j] -= lotCenter;
			}

			lotCenter = Util.rotatePoint(lotCenter, center, angle, Axis.Y);

			for(int j = 0; j < 4; ++j) {
				p[j] += lotCenter;
			}

			for (int j = 0; j < 4; ++j) {
				p[j] = Util.rotatePoint(p[j], lotCenter, angle, Axis.Y);
				lot.addPoint(p[j]);
			}
			lot.center = new Point(lotCenter.x, lotCenter.z);
			lotList.Add(lot);
		}


		if (lotList.Count() <= 0) {
			for (i = 0; i < 4; ++i) {
				rect[i] = Util.rotatePoint(rect[i], center, angle, Axis.Y);
			}

			for (i = 0; i < 4; ++i) {
				rect[i].y = 0;
				ans.addPoint (rect[i]);
			}

			ans.center = new Point(center.x, center.z);

			lotList.Add(ans);
		}

		return lotList;
	}

	/* Update the center of the city */
	private void UpdateCityCenter() {
		Vector3 lCenter = new Vector3();
		foreach (CityEdge lEdge in mEdges) {
			lCenter += lEdge.mPosition;
		}
		lCenter /= mEdges.Count;
		mCityCenter = lCenter;
		mSettings.mCityCenter = lCenter;

		float lMinDistance = 99999f;
		foreach (CityQuarter lQuarter in mQuarters) {
			float lCurrentDistance = Vector3.Distance(lQuarter.mPosition, mCityCenter);
			if (lCurrentDistance < lMinDistance) {
				mCenterQuarter = lQuarter;
				mCenterQuarter.mIsField = false;
				lMinDistance = lCurrentDistance;
			}
		}
	}

	/*
	 * Place node randomly on the map
	 * The placement relies on many rules
	 */
	private void CreateNodes(Vector3 iCenterPosition) {
		mXTab = new double[mSettings.mNbNodes];
		mZTab = new double[mSettings.mNbNodes];

		float lPreviousX = iCenterPosition.x;
		float lPreviousZ = iCenterPosition.z;
		float lPreviousY = GetY(lPreviousX, lPreviousZ) + 1;
		Vector3 lPreviousPosition = new Vector3(lPreviousX, lPreviousY, lPreviousZ);
		
		for (int i = 0; i < mSettings.mNbNodes; ++i) {
			float lX = 0f;
			float lZ = 0f;
			float lY = 0f;
			bool lCanPlaced = false;
			do {
				int j = 0;
				if (i > 0) {
					foreach (CityNode lPlacedNode in mNodes) {
						do {
							lX = Random.Range(Mathf.Max(0, iCenterPosition.x - mSettings.mCitySize),
							                  Mathf.Min(Terrain.activeTerrain.terrainData.size.x, iCenterPosition.x + mSettings.mCitySize));
							lZ = Random.Range(Mathf.Max(0, iCenterPosition.z - mSettings.mCitySize),
							                  Mathf.Min(Terrain.activeTerrain.terrainData.size.z, iCenterPosition.z + mSettings.mCitySize));
							lY = GetY(lX, lZ);
						} while (Vector3.Distance(lPlacedNode.mPosition, new Vector3(lX, lY, lZ)) < mSettings.mMinDistanceNodes
						         && Mathf.Abs(mAverage - lY) > 10
						         && (!mSettings.mPathFindingTree
						    	 || (mSettings.mPathFindingTree
						    	 && !TerrainGenerator.findTree(lX, lZ))));
							j++;
					}
					if (j == mNodes.Count) {
						lCanPlaced = true;
					}
				} else {
					lX = Random.Range(iCenterPosition.x - mSettings.mExpansion * 3.5f, iCenterPosition.x + mSettings.mExpansion * 3.5f);
					lZ = Random.Range(iCenterPosition.z - mSettings.mExpansion * 3.5f, iCenterPosition.z + mSettings.mExpansion * 3.5f);
					lY = GetY(lX, lZ) + 1;
					lCanPlaced = true;
				}
			} while (!lCanPlaced);

			lPreviousPosition = new Vector3(lX, lY, lZ);
			CityNode lCityNode = new CityNode(lPreviousPosition);

			mXTab[i] = lX;
			mZTab[i] = lZ;

			mNodes.Add(lCityNode);
		}
	}

	/*
	 * Generation of the Voronoi diagram
	 * Creation of edges between nodes of the diagram
	 */ 
	private void CreateCityStructure() {
		Delaunay lDelaunay = new Delaunay();

		for (int i = 0; i < mSettings.mNbNodes; ++i) {
			lDelaunay.addPointToGraph(new Point((int)mXTab[i], (int)mZTab[i]));
		}

		lDelaunay.llyodRelaxation(mSettings.mLloydRelaxation);
		mVoronoi = lDelaunay.getVoronoi();
		mNodes.Clear();

		List<VoronoiPolygon> lGoodPolygons = SmoothVoronoi();
		Vector3 lRealCenter = new Vector3();

		foreach (VoronoiPolygon lPoly in lGoodPolygons) {
			Vector3 lCenter = GetPolyCenter(lPoly);
			bool lFind = false;
			int lSize = lPoly.vertices.Count;
			for (int j = 0; j < lSize; ++j) {
				Point lPoint = lPoly.vertices[j];
			
				if (lPoint.x < 0 || lPoint.x > Terrain.activeTerrain.terrainData.size.x
			    || lPoint.y < 0 || lPoint.y > Terrain.activeTerrain.terrainData.size.z
				|| Mathf.Abs(mAverage - GetY(lPoint.x, lPoint.y)) > mSettings.mMaxHeightDiff) {
					lFind = true;
					break;
				}
			}
			if (!lFind) {
				lRealCenter += lCenter;
			}
		}

		lRealCenter /= lGoodPolygons.Count;
		mCityCenter = lRealCenter;
		mSettings.mCityCenter = mCityCenter;

		foreach (VoronoiPolygon lPoly in lGoodPolygons) {
			Vector3 lCenter = GetPolyCenter(lPoly);
			if (Vector3.Distance(lCenter, mCityCenter) < mSettings.mCitySize / mSettings.mNbFields) {
				CityQuarter lNewQuarter = createQuarterFromVoronoiPolygon(lPoly, lCenter);
				mQuarters.Add(lNewQuarter);
			} else {
				CityQuarter lNewQuarter = createQuarterFromVoronoiPolygon(lPoly, lCenter, true);
				mQuarters.Add(lNewQuarter);
			}
		}
	}

	private CityQuarter createQuarterFromVoronoiPolygon(VoronoiPolygon lPoly, Vector3 lCenter, bool isField = false) {
		CityQuarter lNewQuarter = new CityQuarter(lCenter);
		lNewQuarter.mIsField = isField;
		Point lastPoint = null;
		foreach (Point lPoint in lPoly.vertices) {
			CityNode lCurrentNode = null;
			if (lPoint.cityNode != null) {
				lCurrentNode = lPoint.cityNode;
				lCurrentNode.mPoint = lPoint;
			} else {
				lCurrentNode = new CityNode(lPoint.x, GetY(lPoint.x, lPoint.y), lPoint.y);
				lPoint.cityNode = lCurrentNode;	
				lCurrentNode.mPoint = lPoint;
				mNodes.Add(lCurrentNode);
			}
			lNewQuarter.mNodes.Add(lCurrentNode);
			
			if (lastPoint != null) {
				if (!lastPoint.cityNode.mAdjacentNodes.ContainsKey(lPoint.cityNode)) {
					CityEdge lCityEdge = new CityEdge(lastPoint.cityNode, lPoint.cityNode);
					lCityEdge.mThereIsHouse = !isField;
					mEdges.Add(lCityEdge);
					lPoint.cityNode.mAdjacentNodes.Add(lastPoint.cityNode, 0);
					lastPoint.cityNode.mAdjacentNodes.Add(lPoint.cityNode, 0);
				}
			}
			lastPoint = lPoint;
		}
		IEnumerator<Point> iterator = lPoly.vertices.GetEnumerator();
		iterator.MoveNext();
		Point lOtherPoint = iterator.Current;
		if (!lastPoint.cityNode.mAdjacentNodes.ContainsKey(lOtherPoint.cityNode)) {
			CityEdge lCityEdge = new CityEdge(lastPoint.cityNode, lOtherPoint.cityNode);
			lCityEdge.mThereIsHouse = !isField;
			mEdges.Add(lCityEdge);
			lOtherPoint.cityNode.mAdjacentNodes.Add(lastPoint.cityNode, 0);
			lastPoint.cityNode.mAdjacentNodes.Add(lOtherPoint.cityNode, 0);
		}
		
		lNewQuarter.mVoroPoly = lPoly;
		return lNewQuarter;
	}

	/* Create debug edges for the urban planning generation */
	private void PrintEdges() {
		foreach (CityEdge lCityEdge in mEdges) {
			mTrash.Add(CreateRoad(lCityEdge.mFirstNode.mPosition, lCityEdge.mSecondNode.mPosition, false, lCityEdge.mKind));
		}
	}

	/* Create intermediate points in the Voronoi graph*/
	private List<VoronoiPolygon> SmoothVoronoi() {
		List<VoronoiPolygon> lGoodPolygons = new List<VoronoiPolygon>();
		int lSize = Point.count;
		List<Point>[,] lTab = new List<Point>[lSize, lSize];

		HashSet<VoronoiPolygon> relevantVoroPolys = new HashSet<VoronoiPolygon>();

		foreach (VoronoiPolygon lPoly in mVoronoi.getPolygons()) {
			bool lFind = false;
			foreach (Point lPoint in lPoly.vertices) {
				if (lPoint.x < 0 || lPoint.x > Terrain.activeTerrain.terrainData.size.x
				    || lPoint.y < 0 || lPoint.y > Terrain.activeTerrain.terrainData.size.z
				    || Mathf.Abs(mAverage - GetY(lPoint.x, lPoint.y)) > mSettings.mMaxHeightDiff) {
					lFind = true;
					break;
				}
			}
			if (!lFind) {
				relevantVoroPolys.Add(lPoly);
				lGoodPolygons.Add(lPoly);
			}
		}

		foreach (VoronoiPolygon lPoly in relevantVoroPolys) {
			int lSizeVertices = lPoly.vertices.Count();
			for (int i = 0; i < lSizeVertices; i += 1) {
				Point lFirstPoint = lPoly.vertices[i];
				Point lSecondPoint = lPoly.vertices[(i + 1) % lSizeVertices];
				lFirstPoint.primordial = true;
				lSecondPoint.primordial = true;
				CityPrimordialEdge lPrimoEdge = 
					new CityPrimordialEdge(new Vector3(lFirstPoint.x, GetY(lFirstPoint.x, lFirstPoint.y), lFirstPoint.y),
					                       new Vector3(lSecondPoint.x, GetY(lSecondPoint.x, lSecondPoint.y), lSecondPoint.y));
				mPrimordialEdges.Add(lPrimoEdge);
				lPoly.primordialEdges.Add(lPrimoEdge);
				lPrimoEdge.ProcessSides(this, relevantVoroPolys);
			}
		}

		foreach (VoronoiPolygon lPoly in relevantVoroPolys) {
				int lSizeVertices = lPoly.vertices.Count();
				int lNbSmoothPoints = 0;
				for (int i = 0; i < lSizeVertices; i += lNbSmoothPoints + 1) {
					Point lFirstPoint = lPoly.vertices[i];
					Point lSecondPoint = lPoly.vertices[(i + 1) % lSizeVertices];
					lNbSmoothPoints = (int)((Vector3.Distance(lFirstPoint.toVector3(), lSecondPoint.toVector3())
				                   		* mSettings.mSmooth) / 50 + 1);
					float lX = lFirstPoint.x;
					float lZ = lFirstPoint.y;
					float lY = GetY(lX, lZ);

					if (lTab[lFirstPoint.id, lSecondPoint.id] == null && lTab[lSecondPoint.id, lFirstPoint.id] == null) {
						lTab[lFirstPoint.id, lSecondPoint.id] = new List<Point>();
						lTab[lSecondPoint.id, lFirstPoint.id] = null;

						Vector3 lOldPosition = new Vector3(lX, lY, lZ);
						Vector3 lDestination = new Vector3(lSecondPoint.x, GetY(lSecondPoint.x, lSecondPoint.y), lSecondPoint.y);
						Vector3 lNorm = lDestination - lOldPosition;
						lNorm = Quaternion.Euler(0, 90, 0) * lNorm;
						for (int j = 0; j < lNbSmoothPoints; j++) {
							float dist = Vector3.Distance(lOldPosition, lDestination) / lNbSmoothPoints * (j + 1);
							Vector3 lNewPosition = Vector3.MoveTowards(lOldPosition,
							                                           lDestination,
						                                           	   dist);

							float lRand = Random.Range(-2, 2);
							lNewPosition = Vector3.MoveTowards(lNewPosition, lNorm, lRand);							
							Point lNewPoint = new Point(lNewPosition.x, lNewPosition.z);
							lPoly.vertices.Insert(i + j + 1, lNewPoint);
							lTab[lFirstPoint.id, lSecondPoint.id].Add(lNewPoint);
							++lSizeVertices;
						}
					} else {
						if (lTab[lSecondPoint.id, lFirstPoint.id] != null) {
							lTab[lSecondPoint.id, lFirstPoint.id].Reverse();
							lTab[lFirstPoint.id, lSecondPoint.id] = lTab[lSecondPoint.id, lFirstPoint.id];
							lTab[lSecondPoint.id, lFirstPoint.id] = null;
						}

						List<Point> lPoints = lTab[lFirstPoint.id, lSecondPoint.id];

						for (int j = 0; j < lPoints.Count; ++j) {
							lPoly.vertices.Insert(i + j + 1, lPoints[j]);
							++lSizeVertices;
						}
					}
				}
		}
		lTab = null;
		return lGoodPolygons;
	}

	/* Debug : Place a cube of a speicific size at a position */
	private void POINT_CUBE(Vector3 position, float size, string name="DEBUG ") {
		GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
		cube.transform.position = position;
		cube.name = name;
		cube.transform.localScale = new Vector3(size, 200, size);
		mTrash.Add(cube);
	}

	/*
	 * Placement of outside nodes
	 */
	private void CreateOutsideNodes() {
		for (int i = 1; i <= mSettings.mNbOutsideNodes; ++i) {
			float lCityY  = mCityCenter.y;
			bool lPlaced = false;

			for (int x = 0; x < Terrain.activeTerrain.terrainData.size.x; ++x) {
				for (int z = 0;
				     z < Terrain.activeTerrain.terrainData.size.z;
				     z += (x == 0 || x == Terrain.activeTerrain.terrainData.size.x - 1) ? 1 : (int)Terrain.activeTerrain.terrainData.size.z - 1) {

					float y = GetY(x, z);
					if (/*Mathf.Abs(lCityY - y) < 10 || */y <= lCityY + 5) {
						Vector3 lPosition = new Vector3(x, y, z);
						bool lClose = false;
						foreach (CityNode lPlacedNode in mOutsideNodes) {
							if (Vector3.Distance(lPosition, lPlacedNode.mPosition) < mSettings.mMinDistanceOusideNodes) {
								lClose = true;
								break;
							}
						}
						if (!lClose) {
							lPosition.x += x == 0 ? 10 : 0;
							lPosition.z += z == 0 ? 10 : 0;

							LinkOutsideNode(lPosition);
							lPlaced = true;
						}
					}
					if (lPlaced) {break;}
				}
				if (lPlaced) {break;}
			}
		}
	}

	/*
	 * Link the iPosition to the closest node of the city
	 * Based on a Dijkstra algorithm
	 */ 
	private CityNode LinkOutsideNode(Vector3 iPosition, bool iApplyTexture = false, EdgeKind iKind = EdgeKind.major) {
		CityNode lNode = null;
		CityNode lClosestNode = null;
		float lClosestDistance = 99999f;

		foreach (CityNode lCityNode in mOutsideNodes) {
			if (Mathf.Abs(lCityNode.mPosition.sqrMagnitude - iPosition.sqrMagnitude) < 0.5f) {
				lNode = lCityNode;
				break;
			}
		}

		if (lNode == null) {
			lNode = new CityNode(iPosition);
			mOutsideNodes.Add(lNode);
			mNodes.Add(lNode);
		}

		foreach (CityNode lCityNode in mNodes) {
			if (!mOutsideNodes.Contains(lCityNode)) {
				float lDistance = Vector3.Distance(lCityNode.mPosition, iPosition);
				if (!lCityNode.mIsConnect && lDistance < lClosestDistance) {
					lClosestDistance = lDistance;
					lClosestNode = lCityNode;
				}
			}
		}

		if (!lNode.mAdjacentNodes.ContainsKey(lClosestNode)) {
			lNode.addAdjacentNode(lClosestNode, 0);
		}

		if (!lClosestNode.mAdjacentNodes.ContainsKey(lNode)) {
			lClosestNode.addAdjacentNode(lNode, 0);
		}
		lClosestNode.mIsConnect = true;

		int lX = ((int)lNode.mPosition.x / mSettings.mStep) < 0 ? 0 : (int)lNode.mPosition.x / mSettings.mStep;
		lX = ((int)lNode.mPosition.x / mSettings.mStep) >= (int)Terrain.activeTerrain.terrainData.size.x / mSettings.mStep ? (int)Terrain.activeTerrain.terrainData.size.x / mSettings.mStep - 1 : (int)lNode.mPosition.x / mSettings.mStep;
		int lZ = ((int)lNode.mPosition.z / mSettings.mStep) < 0 ? 0 : (int)lNode.mPosition.z / mSettings.mStep;
		lZ = ((int)lNode.mPosition.z / mSettings.mStep) >= (int)Terrain.activeTerrain.terrainData.size.z / mSettings.mStep ? (int)Terrain.activeTerrain.terrainData.size.z / mSettings.mStep - 1 : (int)lNode.mPosition.z / mSettings.mStep;

		List<CartoNode> lPath = mGraphCarto.getDijkstra(mMap[lX, lZ], 
		                                      mMap[(int)lClosestNode.mPosition.x / mSettings.mStep, (int)lClosestNode.mPosition.z / mSettings.mStep]);
		SmoothStreets(lPath);
		Queue<CartoNode> lQueue = new Queue<CartoNode>(lPath);
		CartoNode lCurrentNode = mMap[(int)lNode.mPosition.x / mSettings.mStep, (int)lNode.mPosition.z / mSettings.mStep];
		Vector3 lCurrentPosition = lCurrentNode.mPosition;
		Vector3 lNextPosition = new Vector3();

		do {
			CartoNode lNextNode = null; 
			for (int i = 0; i < 2; ++i) {
				if (lQueue.Count != 0) {
					lNextNode = lQueue.Dequeue();
				}
			}
			lNextPosition = lNextNode.mPosition;
			mOutsideStreets.Add(CreateRoad(lCurrentPosition, lNextPosition, iApplyTexture, iKind));
			lCurrentNode = lNextNode;
			lCurrentPosition = lNextPosition;
		} while (lQueue.Count != 0);

		mOutsideStreets.Add(CreateRoad(lCurrentPosition, lClosestNode.mPosition, iApplyTexture, iKind));
		return lClosestNode;
	}

	/*
	 * Generate a road from a iRef position to the iTarget position
	 */ 
	private GameObject CreateRoad(Vector3 iRef, Vector3 iTarget, bool iApplyTexture = false, EdgeKind iKind = EdgeKind.medium) {
		iRef.y += 0.5f;
		iTarget.y += 0.5f;
		GameObject lMainStreet = new GameObject();
		lMainStreet.transform.parent = mRootMainRoads.transform;
		lMainStreet.name = "Road " + iKind.ToString();
		lMainStreet.transform.position = (iRef + iTarget) / 2;
		
		lMainStreet.AddComponent<LineRenderer>();
		LineRenderer lRenderer = lMainStreet.GetComponent<LineRenderer>();
		lRenderer.material = new Material(Shader.Find("Particles/Additive"));

		Color lColor = new Color(0, 0, 0);
		int lSize = 0;
		int lTexture = 0;
		switch (iKind) {
		case EdgeKind.major:
			lColor = Color.red;
			lSize = 8;
			lTexture = 5;
			break;
		case EdgeKind.medium:
			lColor = Color.white;
			lSize = 6;
			lTexture = 4;
			break;
		case EdgeKind.external:
			lColor = Color.blue;
			lSize = 6;
			lTexture = 4;
			break;
		case EdgeKind.minor:
			lColor = Color.cyan;
			lSize = 4;
			lTexture = 4;
			break;
		};

		lRenderer.SetColors(lColor, lColor);
		float lLineWidth = 0.9f;
		lRenderer.SetWidth(lLineWidth, lLineWidth);
		lRenderer.SetVertexCount(2);
		lRenderer.SetPosition(0, iRef);
		lRenderer.SetPosition(1, iTarget);

		if (iApplyTexture) {
			int lMaxIter = 10;
			for (int i = 1; i <= lMaxIter; ++i) {
				ChangeTexture(Vector3.MoveTowards(iRef, iTarget, Vector3.Distance(iRef, iTarget) / lMaxIter * i), lTexture, lSize);
			}
		} else {
			PlaceBridge(iRef, iTarget);
		}
		return lMainStreet;
	}

	/* Place a bridge between two positions */
	private void PlaceBridge(Vector3 iStart, Vector3 iTarget) {
		float lDistance = Vector3.Distance(iStart, iTarget);
		Vector3 lCurrentPosition = iStart;
		Vector3 lEarthPosition = iStart;
		for (float i = 0; i < lDistance; ++i) {
			if (TerrainGenerator.findWater(lCurrentPosition.x, lCurrentPosition.z)) {
				bool lFind = false;
				foreach (GameObject lPosedBridge in mBridges) {
					if (Vector3.Distance(lCurrentPosition, lPosedBridge.transform.position) < 30) {
						lFind = true;
						break;
					}
				}

				if (!lFind) {
					Vector3 lBrigePosition = Vector3.MoveTowards(lCurrentPosition, iTarget, 5);
					lBrigePosition.y = Mathf.Max(GetY(lEarthPosition), mCityCenter.y);
					Vector3 lBridgeRotation = CalculRotation(lCurrentPosition, iTarget);
					GameObject lBridge = (GameObject) Instantiate(pBridge, 
					                                 lBrigePosition, 
					                                 pFirstTree.transform.rotation);
					lBridge.transform.Rotate(new Vector3(-90, 0, lBridgeRotation.y + 90));
					lBridge.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
					lBridge.transform.parent = mRootBridges.transform;
					mBridges.Add(lBridge);
					break;
				}
			} else {
				lEarthPosition = lCurrentPosition;
			}
			lCurrentPosition = Vector3.MoveTowards(lCurrentPosition, iTarget, 1);
		}
	}

	/* Change the texture between two point of a road */
	private void ChangeTextureRoad(Vector3 iStartPosition, Vector3 iTargetPosition, int iTexture = 4, int iSize = 10) {
		float lMarge = 4f;
		float lDistance = Vector3.Distance(iStartPosition, iTargetPosition);
		Vector3 lCurrentPosition = Vector3.MoveTowards(iStartPosition, iTargetPosition, lMarge);
		ChangeTexture(iStartPosition, iTexture, iSize);
		for (int i = 0; i <= lDistance / lMarge; i++) {
			lCurrentPosition.y = GetY(lCurrentPosition);
			ChangeTexture(lCurrentPosition, iTexture, iSize);
			lCurrentPosition = Vector3.MoveTowards(lCurrentPosition, iTargetPosition, lMarge);
		}
		ChangeTexture(iTargetPosition, iTexture, iSize);
	}

	/* Change the texture in a quarter */
	private void ChangeTextureQuarter(CityQuarter iQuarter) {
		float lMarge = 4;
		float lMinX = 99999f;
		float lMaxX = 0f;
		float lMinZ = 99999f;
		float lMaxZ = 0f;

		foreach (CityNode lCityNode in iQuarter.mNodes) {
			if (lCityNode.mPosition.x < lMinX) {
				lMinX = lCityNode.mPosition.x;
			}

			if (lCityNode.mPosition.x > lMaxX) {
				lMaxX = lCityNode.mPosition.x;
			}

			if (lCityNode.mPosition.z < lMinZ) {
				lMinZ = lCityNode.mPosition.z;
			}

			if (lCityNode.mPosition.z > lMaxZ) {
				lMaxZ = lCityNode.mPosition.z;
			}
		}

		iQuarter.mSize = (lMaxX - lMinX) * (lMaxZ - lMinZ);

		bool lIsInvert = false;

		if (Random.Range(0, 9) > 5f) {
			lIsInvert = true;
		}

		for (float x = lMinX; x <= lMaxX; x += lMarge) {
			for (float z = lMinZ; z <= lMaxZ; z += lMarge) {
				Vector3 lCurrentPosition = new Vector3(x, GetY(x, z), z);
				if (ContainsPoint(iQuarter.mNodes, lCurrentPosition)) {
					if (Mathf.Abs(lCurrentPosition.y - mCityCenter.y) <= mSettings.mMaxHeightDiff) {
						if (!iQuarter.mIsField) {
							ChangeTexture(lCurrentPosition, 3, 5);
						} else {
							if (iQuarter.mSize > 12000/*24000 / mSettings.mNbFields*/) {
								if (!lIsInvert) {
									ChangeTexture(lCurrentPosition, 6, 5);
								} else {
									ChangeTexture(lCurrentPosition, 7, 5);
								}
							} else {
								ChangeTexture(lCurrentPosition, 2, 5);
							}
						}
					}
				}
			}
		}

		if (!iQuarter.mIsField) {
			for (int i = 0; i < iQuarter.mNodes.Count; ++i) {
				CityNode lCurrNode = iQuarter.mNodes[i];
				CityNode lNextNode = iQuarter.mNodes[(i + 1) % iQuarter.mNodes.Count];
				ChangeTextureRoad(Vector3.MoveTowards(lCurrNode.mPosition, iQuarter.mPosition, 25), Vector3.MoveTowards(lNextNode.mPosition, iQuarter.mPosition, 25), 4, 4);
			}
		}
	}

	/* Apply a specific texture at a specific position */
	private void ChangeTexture(Vector3 iPosition, int iTexture, int iSize = 10) {
		if (iPosition.y <= mCityCenter.y + mSettings.mMaxHeightDiff) {
			int lNbTexture = 8;
			TerrainData lData = Terrain.activeTerrain.terrainData;
			for (int z = (int)iPosition.z >= iSize / 2 ? (int)iPosition.z - iSize / 2 : (int)iPosition.z; z < (int)iPosition.z + iSize / 2; z++){
				for (int x = (int)iPosition.x >= iSize / 2 ? (int)iPosition.x - iSize / 2 : (int)iPosition.x; x < (int)iPosition.x + iSize / 2; x++){
					int lIndX = Mathf.RoundToInt(z * lData.heightmapHeight / lData.size.x);
					int lIndZ = Mathf.RoundToInt(x * lData.heightmapWidth / lData.size.z); 
					for (int i = 0; i < lNbTexture; i++) {
						if (lIndX < 511 && lIndZ < 511 && lIndX >= 0 && lIndZ >= 0) {
							if (i == iTexture) {
								mAlphaData[lIndX, lIndZ, i] = 1f;
							} else {
								mAlphaData[lIndX, lIndZ, i] = 0;
							}
						}
					}
				}
			}
		}
	}

	/*
	 * Create a graph for pathfinding algorithm
	 * This graph is based on the terrain, not on the city
	 */ 
	private void CreateGraphCarto() {
		mMap = new CartoNode[(int)Terrain.activeTerrain.terrainData.size.x / mSettings.mStep, (int)Terrain.activeTerrain.terrainData.size.z / mSettings.mStep];
		int i = 0;
		int j = 0;
		for (int x = 0; x < Terrain.activeTerrain.terrainData.size.x && i < Terrain.activeTerrain.terrainData.size.x / mSettings.mStep; x += mSettings.mStep) {
			for (int z = 0; z < Terrain.activeTerrain.terrainData.size.z && j < Terrain.activeTerrain.terrainData.size.z / mSettings.mStep; z += mSettings.mStep) {
				float y = GetY(x, z);
				mMap[i, j] = new CartoNode(new Vector3(x, y, z));
				j++;
			}
			i++;
			j = 0;
		}
		
		for (int x = 0; x < Terrain.activeTerrain.terrainData.size.x / mSettings.mStep; x++) {
			for (int z = 0; z < Terrain.activeTerrain.terrainData.size.z / mSettings.mStep; z++) {
				CartoNode lCurrentNode = mMap[x, z];

				int lValue = TerrainGenerator.findWater(lCurrentNode.mPosition.x, lCurrentNode.mPosition.z) ? 99999 : 0;
				if (x > 0) {
					lValue += (int)(Mathf.Abs(Mathf.Pow(mMap[x, z].mPosition.y - mMap[x - 1, z].mPosition.y, 2)));
					lValue += Random.Range(1, 5);
					lValue += (int)mMap[x - 1, z].mPosition.y;
					lCurrentNode.addAdjacentNode(mMap[x - 1, z], lValue);
				}
				
				if (x < (int) Terrain.activeTerrain.terrainData.size.x / mSettings.mStep - 1) {
					lValue += (int)(Mathf.Abs(Mathf.Pow(mMap[x, z].mPosition.y - mMap[x + 1, z].mPosition.y, 2)));
					lValue += Random.Range(1, 5);
					lValue += (int)mMap[x + 1, z].mPosition.y;
					lCurrentNode.addAdjacentNode(mMap[x + 1, z], lValue);
				}
				
				if (z > 0) {
					lValue += (int)(Mathf.Abs(Mathf.Pow(mMap[x, z].mPosition.y - mMap[x, z - 1].mPosition.y, 2)));
					lValue += Random.Range(1, 5);
					lValue += (int)mMap[x, z - 1].mPosition.y;
					lCurrentNode.addAdjacentNode(mMap[x, z - 1], lValue);
				}
				
				if (z < (int) Terrain.activeTerrain.terrainData.size.z / mSettings.mStep - 1) {
					lValue += (int)(Mathf.Abs(Mathf.Pow(mMap[x, z].mPosition.y - mMap[x, z + 1].mPosition.y, 2)));
					lValue += Random.Range(1, 5);
					lValue += (int)mMap[x, z + 1].mPosition.y;
					lCurrentNode.addAdjacentNode(mMap[x, z + 1], lValue);
				}
				mGraphCarto.addNode(lCurrentNode);
			}
		}
	}

	/* Create the graph of the city to find the shortest path though it */
	private void CreateGraphCity() {
		int i = 0;
		foreach (CityNode lCityNode in mNodes) {
			foreach (CityNode lOtherNode in mNodes) {
				if (lCityNode != lOtherNode) {
					if (lCityNode.mAdjacentNodes.ContainsKey(lOtherNode)) {
						int lValue = 20;
						if (mCenterQuarter.mNodes.Contains(lOtherNode)) {
							lValue = 1;
						}
						lCityNode.mAdjacentNodes[lOtherNode] = lValue;
						lOtherNode.mAdjacentNodes[lCityNode] = lValue;
						i++;
					}
				}
			}
			mGraphCity.addNode(lCityNode);
		}
	}

	/* Dispatch between medium, external, and major roads */
	private void DetermineRoadPriority() {
		// External Roads
		foreach (CityEdge lCityEdge in mEdges) {
			Vector3 offset = lCityEdge.mFirstNode.mPosition - lCityEdge.mSecondNode.mPosition;
			offset.Normalize();
			offset = Quaternion.Euler(0, 90, 0) * offset;
			offset *= 0.1f;

			Vector3 oneSide = lCityEdge.mPosition + offset;
			Vector3 otherSide = lCityEdge.mPosition - offset;
			
			if (!isInQuarters(otherSide) || !isInQuarters(oneSide)) {
				lCityEdge.mFirstNode.mAdjacentNodes[lCityEdge.mSecondNode] = 300;
				lCityEdge.mSecondNode.mAdjacentNodes[lCityEdge.mFirstNode] = 300;
				lCityEdge.mKind = EdgeKind.external;
			}
		}

		// Major Roads
		CityNode lFirstExtremity = null;
		CityNode lSecondExtremity = null;
		float lMaxDistance = 0;

		foreach (CityNode lFirstNode in mNodes) {
			if (lFirstNode.mIsConnect) {
				foreach (CityNode lSecondNode in mNodes) {
					if (lSecondNode.mIsConnect) {
						if (lFirstNode != lSecondNode) {
							float lDistance = Vector3.Distance(lFirstNode.mPosition, lSecondNode.mPosition);
							if (lDistance > lMaxDistance) {
								lMaxDistance = lDistance;
								lFirstExtremity = lFirstNode;
								lSecondExtremity = lSecondNode;
							}
						}
					}
				}
			}
		}

		if (lFirstExtremity != null && lSecondExtremity != null) {
			List<CityNode> lPath = mGraphCity.getDijkstra(lFirstExtremity, lSecondExtremity);
			SmoothStreets(lPath);
			lPath.Add(lSecondExtremity);

			CityNode lPreviousNode = null;
			CityNode lCurrentNode = null;

			for (int i = 1; i < lPath.Count; ++i) {
				lPreviousNode = lPath[i - 1];
				lCurrentNode = lPath[i];

				foreach (CityEdge lEdge in mEdges) {
					if ((lEdge.mFirstNode == lCurrentNode && lEdge.mSecondNode == lPreviousNode)
					    || (lEdge.mFirstNode == lPreviousNode && lEdge.mSecondNode == lCurrentNode)) {
							lEdge.mKind = EdgeKind.major;
							break;
					}
				}
			}
		}
	}

	/* Apply a linear interpolation between all position in input */
	private void SmoothStreets(List<CityNode> iNodes) {
		CityNode lPreviousNode = null;
		CityNode lCurrentNode = null;
		CityNode lNextNode = null;
		for (int k = 0; k < mSettings.mSmoothMainRoad; ++k) {
			int lFirstInc = (int)Random.Range(1, 3);
			for (int i = lFirstInc; i < iNodes.Count - 1; ++i) {
				lPreviousNode = iNodes[i - 1];
				lCurrentNode = iNodes[i];
				lNextNode = iNodes[i + (i == iNodes.Count - 1 ? 0 : 1)];
				for (int j = 0; j < 500; ++j) {
					lCurrentNode.mPosition = Vector3.Lerp(lCurrentNode.mPosition, (lPreviousNode.mPosition + lNextNode.mPosition) * 0.5f, 15.5f);
				}
			}
		}
	}

	/* Apply a linear interpolation between all position in input */
	private void SmoothStreets(List<CartoNode> iNodes) {
		CartoNode lPreviousNode = null;
		CartoNode lCurrentNode = null;
		CartoNode lNextNode = null;
		for (int k = 0; k < mSettings.mSmoothMainRoad; ++k) {
			int lFirstInc = (int)Random.Range(1, 3);
			for (int i = lFirstInc; i < iNodes.Count - 1; ++i) {
				lPreviousNode = iNodes[i - 1];
				lCurrentNode = iNodes[i];
				lNextNode = iNodes[i + (i == iNodes.Count - 1 ? 0 : 1)];
				for (int j = 0; j < 500; ++j) {
					lCurrentNode.mPosition = Vector3.Lerp(lCurrentNode.mPosition, (lPreviousNode.mPosition + lNextNode.mPosition) * 0.5f, 15.5f);
				}
			}
		}
	}

	/* Apply a linear interpolation between all position in input */
	private void SmoothStreets(VoronoiPolygon iPoly) {
		Vector3 lPreviousNode = new Vector3();
		Vector3 lCurrentNode = new Vector3();
		Vector3 lNextNode = new Vector3();
		for (int k = 0; k < mSettings.mSmoothMainRoad; ++k) {
			int lFirstInc = (int)Random.Range(1, 3);
			for (int i = lFirstInc; i < iPoly.vertices.Count - 1; ++i) {
				lPreviousNode = iPoly.vertices[i - 1].toVector3();
				lCurrentNode = iPoly.vertices[i].toVector3();
				lNextNode = iPoly.vertices[i + (i == iPoly.vertices.Count - 1 ? 0 : 1)].toVector3();
				for (int j = 0; j < 500; ++j) {
					lCurrentNode = Vector3.Lerp(lCurrentNode, (lPreviousNode + lNextNode) * 0.5f, 15.5f);
				}
				iPoly.vertices[i].x = lCurrentNode.x;
				iPoly.vertices[i].y = lCurrentNode.z;
			}
		}
	}

	/*
	 * Get the center position of the village
	 */
	private void FindVillageCenter() {
		int lLimit = 200;

		do {
			CartoNode lRandomNode = null;
			do {
				lRandomNode = mGraphCarto.mNodes[(int)Random.Range(0, mGraphCarto.mNodes.Count - 1)];
			} while (lRandomNode.mPosition.x < mSettings.mExpansion
			         || lRandomNode.mPosition.z < mSettings.mExpansion
			         || lRandomNode.mPosition.x > Terrain.activeTerrain.terrainData.size.x - mSettings.mExpansion
			         || lRandomNode.mPosition.z > Terrain.activeTerrain.terrainData.size.z - mSettings.mExpansion);

			float lY = lRandomNode.mPosition.y;
			mAverage = 0.0f;
			List<CartoNode> lVisitedNodes = new List<CartoNode>();
			Vector3[] lPoly = new Vector3[] {
				lRandomNode.mPosition + new Vector3(mSettings.mExpansion, 0, mSettings.mExpansion),
				lRandomNode.mPosition + new Vector3(-mSettings.mExpansion, 0, mSettings.mExpansion),
				lRandomNode.mPosition + new Vector3(-mSettings.mExpansion, 0, -mSettings.mExpansion),
				lRandomNode.mPosition + new Vector3(mSettings.mExpansion, 0, -mSettings.mExpansion),
			};

			RecFindVillageCenter(lRandomNode, lVisitedNodes, lPoly, lY);

			mAverage /= lVisitedNodes.Count;

			if (Mathf.Abs(mAverage - lY) < 10) {
				mCityCenter = lRandomNode.mPosition;
				mSettings.mCityCenter = mCityCenter;
				break;
			}
			--lLimit;
		} while (lLimit > 0);

		if (lLimit <= 0) {
			Debug.Log("Place not found to build village ! :( ");
		} 
	}
	/* Recursive call to find the village center */
	private void RecFindVillageCenter(CartoNode iNode, List<CartoNode> iVisitedNodes, Vector3[] iLimit, float iRefY) {
		if (!iVisitedNodes.Contains(iNode) && ContainsPoint(iLimit, iNode.mPosition)) {
			if (Mathf.Abs(iRefY - iNode.mPosition.y) > mSettings.mMaxHeightDiff + 5) {
				mAverage += 999999;
			} 
			mAverage += iNode.mPosition.y;
			iVisitedNodes.Add(iNode);
			foreach (KeyValuePair<ANode, int> lEntry in iNode.getAdjacentNodes()) {
				CartoNode lAdjacentNode = (CartoNode)lEntry.Key;
				RecFindVillageCenter(lAdjacentNode, iVisitedNodes, iLimit, iRefY);
			}
		}
	}

	/*
	 * Get the Y value of the iX, iZ position
	 */ 
	public float GetY(float iX, float iZ) {
		return Terrain.activeTerrain.SampleHeight(new Vector3(iX, 0, iZ));
	}

	/*
	 * Get the Y value of the iVector position
	 * The iVector.y value doesn't matter
	 */ 
	public float GetY(Vector3 iVector) {
		return Terrain.activeTerrain.SampleHeight(iVector);
	}

	/* Check if a position is in the city */
	public bool isInQuarters(Vector3 point) {
		foreach (CityQuarter quarter in mQuarters) {
			if (ContainsPoint(quarter.mNodes, point)) {
				return true;
			}
		}
		return false;
	}

	/* Check if a position is in the city */
	public bool isInQuarters(Vector3 point, HashSet<VoronoiPolygon> voroPolys) {
		foreach (VoronoiPolygon voroPoly in voroPolys) {
			if (ContainsPoint(voroPoly, point)) {
				return true;
			}
		}
		return false;
	}

	/* Place all house that are based on a lot */
	private void PlaceInternalHouses() {
		foreach (LotType lotType in mSettings.mRequiredLotTypes) {
			if (mSettings.mInterestingLots.Count != 0) {
				Lot lot = mSettings.mInterestingLots.Pop();
				houseBuilder.textureToUse = lot.color;
				Vector3 position = lot.lotCenter;
				Vector3 lRotation = CalculRotation(lot.secondPoint, lot.firstPoint);

				switch(lotType) {
				case LotType.CHURCH:
					GameObject church = mStreetGenerator.constructHouse("church2", position, lRotation);
					mHouses.Add(church);
					break;

				case LotType.TOWER:
					GameObject tower = mStreetGenerator.constructHouse("godusGreatTower", position, lRotation);
					mHouses.Add(tower);
					break;

				case LotType.RESIDENTIAL:
					GameObject res = mStreetGenerator.constructHouse("godusHouse", position, lRotation);
					mHouses.Add(res);
					break;

				case LotType.SKYSCRAPER:
					GameObject skyscraper0 = mStreetGenerator.constructHouse("skyscraper0", position, lRotation);
					mHouses.Add(skyscraper0);
					break;

				case LotType.MINIHOUSE:
					GameObject basichouse = mStreetGenerator.constructHouse("godusBasicHouse", position, lRotation);
					mHouses.Add(basichouse);
					break;

				case LotType.STAIRHOUSE:
					GameObject stairhouse = mStreetGenerator.constructHouse("godusRichHouse", position, lRotation);
					mHouses.Add(stairhouse);
					break;

				case LotType.RESTAURANT:
					GameObject resto = mStreetGenerator.constructHouse("restaurant_1", position, lRotation);
					mHouses.Add(resto);
					break;

				case LotType.SHOP:
					GameObject shop = mStreetGenerator.constructHouse("shop_1", position, lRotation);
					mHouses.Add(shop);
					break;
				}
			}
		}

		while (mSettings.mInterestingLots.Count != 0) {
			Lot lLot = mSettings.mInterestingLots.Pop ();
			BuildOnLot (lLot);
		}
	}

	/* Create a house on the a specific lot */
	private void BuildOnLot(Lot lLot) {
		Vector3 lPosition = lLot.lotCenter;
		houseBuilder.textureToUse = lLot.color;
		GameObject house = null;
		Vector3 lRotation = CalculRotation (lLot.secondPoint, lLot.firstPoint);
		string script = "";
		foreach (ScriptDescription lDescription in mStreetGenerator.scripts) {
			if (lDescription.mWidth == (int)lLot.width || lDescription.mLength == (int)lLot.length || lDescription.mLength + 1 == (int)lLot.length || lDescription.mWidth + 1 == (int)lLot.width) {
				script = lDescription.mScript;
				if (Random.Range(0, 9) < 4) {
					break;
				}
			}
		}
		if (Random.Range(0, 9) > 2.5f && script.Length != 0) {
			house = mStreetGenerator.constructHouse (script, lPosition, lRotation, true);
		} else {
			if (Random.Range(0, 9) > 6.3f) {
				house = mStreetGenerator.constructHouse ("restaurant_1", lPosition, lRotation);
			} else {
				house = mStreetGenerator.constructHouse ("shop_1", lPosition, lRotation);
			}
		}
		if (Random.Range (0, 9) > 7f) {
			GameObject tower = mStreetGenerator.constructHouse ("godusTower", lPosition + new Vector3 (5, 0, 5), lRotation);
			mHouses.Add(tower);
		}
		mHouses.Add(house);
	}

	/* Place all house that are not in a CityQyarter */
	private void PlaceExternalHouses() {
		foreach (CityQuarter lQuarter in mQuarters) {
			bool lIsInField = false;
			PaletteKind lColor;
			if (Random.Range(0, 9) > 4) {
				lColor = mSettings.mFirstPaletteKind;
			} else {
				lColor = mSettings.mSecondPaletteKind;
			}
			foreach (CityPrimordialEdge lEdge in lQuarter.mVoroPoly.primordialEdges) {
				if (lQuarter.mIsField) {
					lIsInField = true;
				}
				if (!lIsInField) {
					Line l = new Line(new Vertex(lEdge.mFirstPoint.x, lEdge.mFirstPoint.z),
					                  new Vertex(lEdge.mSecondPoint.x, lEdge.mSecondPoint.z));
					if (lEdge.mOneSide) {
						mHouses.AddRange(mStreetGenerator.getLotCenters(l, lColor, false, 0.3f));
					} else if (lEdge.mOtherSide) {
						mHouses.AddRange(mStreetGenerator.getLotCenters(l, lColor, true, 0.3f));
					}
				}
			}
		}
	}

	/* Place a tree at a specific position, it can be enclose with barriers */
	public bool PlaceTree(Vector3 iPosition, bool iWithBarrier = false) {
		Collider[] hitColliders = Physics.OverlapSphere(new Vector3(iPosition.x, iPosition.y + 5, iPosition.z), 4.2f);

		if (hitColliders.Length > 1) {
			return false;
		}

		if (!TerrainGenerator.findWater(iPosition.x, iPosition.z)) {
			iPosition.y = GetY(iPosition);
			GameObject lTree = null;
			if (Random.Range(0, 9) > 5) {
				lTree = (GameObject) Instantiate(pFirstTree, 
			            	    	             iPosition, 
			                	    	         pFirstTree.transform.rotation);
			} else {
				lTree = (GameObject) Instantiate(pSecondTree, 
				                                 iPosition, 
				                                 pSecondTree.transform.rotation);
			}

			if (iWithBarrier) {
				float lDist = 2.5f;
				PlaceBarrier(iPosition + new Vector3(lDist, -0.78f, lDist), CalculRotation(iPosition, iPosition + new Vector3(lDist, -0.78f, lDist)) + new Vector3(0, -135, 0), lTree);
				PlaceBarrier(iPosition + new Vector3(lDist, -0.78f, -lDist), CalculRotation(iPosition, iPosition + new Vector3(lDist, -0.78f, -lDist)) + new Vector3(0, -135, 0), lTree);
				PlaceBarrier(iPosition + new Vector3(-lDist, -0.78f, lDist), CalculRotation(iPosition, iPosition + new Vector3(-lDist, -0.78f, lDist)) + new Vector3(0, -135, 0), lTree);
				PlaceBarrier(iPosition + new Vector3(-lDist, -0.78f, -lDist), CalculRotation(iPosition, iPosition + new Vector3(-lDist, -0.78f, -lDist)) + new Vector3(0, -135, 0), lTree);
			}

			lTree.transform.parent = mRootTrees.transform;
			mTrees.Add(lTree);
			lTree.name = "Tree for building #" + houseBuilder.buildNum;
			return true;
		}
		return false;
	}

	/* Place a bush at a specific position */
	public void PlaceGrass(Vector3 iPosition) {
		if (!TerrainGenerator.findWater(iPosition.x, iPosition.z)) {
			GameObject lTree = (GameObject) Instantiate(pGrass, 
				                                 iPosition, 
			                                     pGrass.transform.rotation);
			lTree.transform.parent = mRootTrees.transform;
			mTrees.Add(lTree);
		}
	}

	/* Place a pylone at a specific position */
	public void PlacePylone(Vector3 iPosition, Vector3 iRotation, int iID) {
		GameObject lPylone = (GameObject) Instantiate(pPylone, 
		                                            iPosition, 
		                                            pPylone.transform.rotation);
		lPylone.transform.Rotate(iRotation);
		lPylone.transform.parent = mRootPylones.transform;
		lPylone.name = "Pylone " + iID;
		mTrash.Add(lPylone);
	}

	/* Place a phone booth at a specific position */
	public void PlacePhone(Vector3 iPosition, Vector3 iRotation) {
		GameObject lPhone = (GameObject) Instantiate(pPhone, 
		                                              iPosition, 
		                                              pPhone.transform.rotation);
		lPhone.transform.Rotate(iRotation);
		lPhone.transform.parent = mRootCarts.transform;
		mProps.Add(lPhone);
	}

	/* Place a sheep at a specific position */
	public void PlaceSheep(Vector3 iPosition) {
		if (!TerrainGenerator.findWater(iPosition.x, iPosition.z)) {
			iPosition.y = GetY(iPosition) + 1.7f;
			GameObject lSheep = (GameObject) Instantiate(pSheep, 
			                                 iPosition, 
			                                 pSheep.transform.rotation);
			lSheep.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)));
			lSheep.transform.parent = mRootSheeps.transform;
			lSheep.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
			mTrash.Add(lSheep);
		}
	}

	/* Place a traffic light at a specific position */
	public void PlaceTraffic(Vector3 iPosition, Vector3 iRotation) {
		if (!TerrainGenerator.findWater(iPosition.x, iPosition.z)) {
			GameObject lTraffic = (GameObject) Instantiate(pTraffic, 
			                                               iPosition, 
			                                               pTraffic.transform.rotation);
			lTraffic.transform.Rotate(iRotation);
			lTraffic.transform.parent = mRootCarts.transform;
			mProps.Add(lTraffic);
		}
	}

	/* Place a street light at a specific position */
	public void PlaceLight(Vector3 iPosition, Vector3 iRotation) {
		if (!TerrainGenerator.findWater(iPosition.x, iPosition.z)) {
			GameObject lLight = (GameObject) Instantiate(pLight, 
			                                             iPosition, 
			                                             pLight.transform.rotation);
			lLight.transform.Rotate(iRotation);
			lLight.transform.parent = mRootCarts.transform;
			mProps.Add(lLight);
		}
	}

	/* Place a barrel set at a specific position */
	public void PlaceBarrel(Vector3 iPosition) {
		if (!TerrainGenerator.findWater(iPosition.x, iPosition.z)) {
			GameObject lBarrel = (GameObject) Instantiate(pBarrel, 
			                                  iPosition, 
			                                  pBarrel.transform.rotation);
			lBarrel.transform.Rotate(new Vector3(0, Random.Range(0, 360), 0));
			lBarrel.transform.parent = mRootBarrels.transform;
			lBarrel.transform.localScale = new Vector3(0.73f, 0.73f, 0.73f);
			mProps.Add(lBarrel);
		}
	}

	/* Place a fontain at a specific position */
	public void PlaceFontain(Vector3 iPosition) {
		if (!TerrainGenerator.findWater(iPosition.x, iPosition.z)) {
			GameObject lFontain = (GameObject) Instantiate(pFontain, 
			                                  iPosition, 
			                                  pFontain.transform.rotation);
			lFontain.transform.parent = mRootMarkets.transform;
			mProps.Add(lFontain);
		}
	}

	/* Place a market at a specific position */
	public void PlaceMarket(Vector3 iPosition, Vector3 iRotation) {
		if (!TerrainGenerator.findWater(iPosition.x, iPosition.z)) {
			GameObject lMarket = (GameObject) Instantiate(pMarket, 
			                                              iPosition, 
			                                              new Quaternion(0, 0, 0, 0));
			lMarket.transform.Rotate(new Vector3(-90, iRotation.y, 0));
			lMarket.transform.parent = mRootMarkets.transform;
			lMarket.transform.localScale = new Vector3(0.73f, 0.73f, 0.73f);
			mProps.Add(lMarket);
		}
	}

	/* Place a randomized cart at a specific position */
	public void PlaceCart(Vector3 iPosition) {
		Collider[] hitColliders = Physics.OverlapSphere(iPosition, 0.05f);
		
		if (hitColliders.Length > 1) {
			return;
		}

		if (!TerrainGenerator.findWater(iPosition.x, iPosition.z)) {
			iPosition.y = GetY(iPosition);
			GameObject lCart = null;
			float lRand = Random.Range(0, 9);
			if (lRand < 3) {
				lCart = (GameObject) Instantiate(pCart1, 
				                                 iPosition,
				                                 pCart1.transform.rotation);
			} else if (lRand >= 3 && lRand < 6){
				lCart = (GameObject) Instantiate(pCart2, 
				                                 iPosition, 
				                                 pCart2.transform.rotation);
			} else {
				lCart = (GameObject) Instantiate(pCart3, 
				                                 iPosition, 
				                                 pCart3.transform.rotation);
			}
			lCart.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)));
			lCart.transform.parent = mRootCarts.transform;
			lCart.transform.localScale = new Vector3(0.55f, 0.55f, 0.55f);
			mProps.Add(lCart);
		}
	}

	/* Place a barrier at a specific position */
	public void PlaceBarrier(Vector3 iPosition, Vector3 iRotation, GameObject iFather = null) {
		if (!TerrainGenerator.findWater(iPosition.x, iPosition.z)) {
			GameObject lBarrier = (GameObject) Instantiate(pBarrier, 
			                 				                iPosition, 
			                                               pBarrier.transform.rotation);
			lBarrier.transform.Rotate(iRotation);
			lBarrier.transform.localScale = new Vector3(1, 1, 1);
			if (iFather != null) {
				lBarrier.transform.parent = iFather.transform;
			} else {
				lBarrier.transform.parent = mRootBarriers.transform;
			}
			mTrash.Add(lBarrier);
		}
	}

	/* Get the size of a CityQuarter */
	private float GetQuarterSize(CityQuarter iQuarter) {
		float lMinX = 99999f;
		float lMaxX = 0f;
		float lMinZ = 99999f;
		float lMaxZ = 0f;
		
		foreach (CityNode lCityNode in iQuarter.mNodes) {
			if (lCityNode.mPosition.x < lMinX) {
				lMinX = lCityNode.mPosition.x;
			}
			
			if (lCityNode.mPosition.x > lMaxX) {
				lMaxX = lCityNode.mPosition.x;
			}
			
			if (lCityNode.mPosition.z < lMinZ) {
				lMinZ = lCityNode.mPosition.z;
			}
			
			if (lCityNode.mPosition.z > lMaxZ) {
				lMaxZ = lCityNode.mPosition.z;
			}
		}
		
		iQuarter.mSize = (lMaxX - lMinX) * (lMaxZ - lMinZ);
		return iQuarter.mSize;
	}

	/* Create a closed field with sheeps */
	private void EncloseFields() {
		float lBarrierSize = 4f;
		foreach (CityQuarter lQuarter in mQuarters) {
			if (lQuarter.mIsField && GetQuarterSize(lQuarter) < 12000) {

				for (int i = 0; i < 9; ++i) {
					float lX = lQuarter.mPosition.x + Random.Range(-10, 10);
					float lZ = lQuarter.mPosition.z + Random.Range(-10, 10);
					float lY = GetY(lX, lZ);
					if (Random.Range(0, 9) > 6.4f) {
						PlaceTree(new Vector3(lX, lY, lZ));
					} else {
						PlaceSheep(new Vector3(lX, lY, lZ));
					}
				}

				VoronoiPolygon lCopyVoroPoly = lQuarter.mVoroPoly;
				SmoothStreets(lCopyVoroPoly);
				for (int i = 0; i < lCopyVoroPoly.vertices.Count; ++i) {
					Point lCurrentPoint = lCopyVoroPoly.vertices[i];
					Point lEndPoint = lCopyVoroPoly.vertices[(i + 1) % lCopyVoroPoly.vertices.Count];
					Vector3 lCurrentPosition = lCurrentPoint.toVector3();
					Vector3 lEndPosition = lEndPoint.toVector3();

					lCurrentPosition = Vector3.MoveTowards(lCurrentPosition, lQuarter.mPosition, 10);
					lEndPosition = Vector3.MoveTowards(lEndPosition, lQuarter.mPosition, 10);

					Vector3 lRotation = CalculRotation(lCurrentPosition, lEndPosition);
					float lDistance = Vector3.Distance(lCurrentPosition, lEndPosition);

					for (float j = 0; j < lDistance - 1.5f; j += lBarrierSize) {
						lCurrentPosition.y = GetY(lCurrentPosition);
						PlaceBarrier(lCurrentPosition, lRotation);
						lCurrentPosition = Vector3.MoveTowards(lCurrentPosition, lEndPosition, lBarrierSize);
					}
				}
			}
		}
	}
	
	/* Check if a point own to a shape made of many positions */ 
	private bool ContainsPoint(Vector3[] iPoly, Vector3 iPoint) { 
		int j = iPoly.Length - 1; 
		bool lInside = false; 
		for (int i = 0; i < iPoly.Length; j = i++) { 
			if (((iPoly[i].z <= iPoint.z && iPoint.z < iPoly[j].z) || (iPoly[j].z <= iPoint.z && iPoint.z < iPoly[i].z)) && 
			    (iPoint.x <= (iPoly[j].x - iPoly[i].x) * (iPoint.z - iPoly[i].z) / (iPoly[j].z - iPoly[i].z) + iPoly[i].x)) {
				lInside = !lInside;
			}
		} 
		return lInside; 
	}

	/* Check if a point own to a shape made of CityNode */
	private bool ContainsPoint(List<CityNode> iPoly, Vector3 iPoint) { 
		int j = iPoly.Count - 1; 
		bool lInside = false; 
		for (int i = 0; i < iPoly.Count; j = i++) { 
			if (((iPoly[i].mPosition.z <= iPoint.z && iPoint.z < iPoly[j].mPosition.z) || (iPoly[j].mPosition.z <= iPoint.z && iPoint.z < iPoly[i].mPosition.z)) && 
			    (iPoint.x < (iPoly[j].mPosition.x - iPoly[i].mPosition.x) * (iPoint.z - iPoly[i].mPosition.z) / (iPoly[j].mPosition.z - iPoly[i].mPosition.z) + iPoly[i].mPosition.x)) {
				lInside = !lInside; 
			}
		} 
		return lInside; 
	}

	/* Check if a point own to a voronoi polygon */
	private bool ContainsPoint(VoronoiPolygon iPoly, Vector3 iPoint) { 
		Point[] lPoly = new Point[iPoly.vertices.Count];
		iPoly.vertices.CopyTo(lPoly);

		int j = lPoly.Length - 1; 
		bool lInside = false; 
		for (int i = 0; i < lPoly.Length; j = i++) { 
			if (((lPoly[i].y <= iPoint.z && iPoint.z < lPoly[j].y) || (lPoly[j].y <= iPoint.z && iPoint.z < lPoly[i].y)) && 
			    (iPoint.x < (lPoly[j].x - lPoly[i].x) * (iPoint.z - lPoly[i].y) / (lPoly[j].y - lPoly[i].y) + lPoly[i].x)) {
				lInside = !lInside; 
			}
		} 
		return lInside; 
	}

	/* Get the position of the center of a Voronoi polygon */
	private Vector3 GetPolyCenter(VoronoiPolygon iPoly) {
		Vector3 lPolyCenter = new Vector3();
		foreach(Point lPoint in iPoly.vertices) {
			lPolyCenter += new Vector3(lPoint.x, 0, lPoint.y);
		}
		lPolyCenter /= iPoly.vertices.Count;
		lPolyCenter.y = GetY(lPolyCenter);
		return lPolyCenter;
	}

	/* Get the relative angle between three positions */
	private float GetAngle(Vector3 p0, Vector3 p1, Vector3 p2) {
		Vector3 u = p0 - p1;
		Vector3 v = p2 - p1;
		u.Normalize();
		v.Normalize();
		float d = Vector3.Dot(u.normalized, v.normalized);
		return Mathf.Acos(d) * 180 / Mathf.PI;
	}

	/* Get the global angle between two positions */
	private Vector3 CalculRotation(Vector3 iFirstPosition, Vector3 iSecondPosition) {
		float lTau = Mathf.PI - Mathf.Atan2(iSecondPosition.z - iFirstPosition.z, iSecondPosition.x - iFirstPosition.x);
		lTau *= 180 / Mathf.PI;
		return new Vector3(0, lTau, 0);
	}

	/* Get all light to turn them on */
	private void TurnOnLight() {
		foreach(GameObject lLight in GameObject.FindGameObjectsWithTag("LightHouse")) {
			lLight.light.intensity = 0.76f;
		}
	}

	/* Get all light to turn them off */
	private void TurnOffLight() {
		foreach(GameObject lLight in GameObject.FindGameObjectsWithTag("LightHouse")) {
			lLight.light.intensity = 0;
		}
	}

	/* Remove houses if there are on the main road */
	public void RemoveBadHouses() {
		for (int i = 0; i < mHouses.Count; ++i) {
			GameObject lObject = mHouses[i];
			if (lObject != null) {
				if (TextureIsActive(lObject.transform.position, 5)) {
					mHouses.Remove(lObject);
					DestroyImmediate(lObject);
					--i;
				}
			}
		}
	}

	/* Remove terrain trees if they are on a road */
	public void PreRemoveBadTrees() {
		TerrainData terrain = Terrain.activeTerrain.terrainData;
		TreeInstance[] treeInstances = terrain.treeInstances;
		List<TreeInstance> lGoodTrees = new List<TreeInstance>();
		
		for (int i = 0; i < treeInstances.Length; i++) {
			TreeInstance currentTree = treeInstances[i];
			int lX = (int)(currentTree.position.z * (terrain.alphamapWidth - 1));
			int lZ = (int)(currentTree.position.x * (terrain.alphamapHeight - 1));
			bool lGreenTexture = TextureIsActiveFromTree(lX, lZ, 1);
			
			if (lGreenTexture) {
				lGoodTrees.Add(currentTree);
			}
		}
		terrain.treeInstances = lGoodTrees.ToArray();
	}

	/* Remove prefab trees if they are on a road */
	public void PostRemoveBadTrees() {
		for (int i = 0; i < mTrees.Count; ++i) {
			GameObject lTree = mTrees[i];
			if (lTree != null) {
				Collider[] hitColliders = Physics.OverlapSphere(lTree.transform.position, 0.1f);
				bool lRoadTexture = TextureIsActive(lTree.transform.position, 4)
									|| TextureIsActive(lTree.transform.position, 5);
				if (lRoadTexture || hitColliders.Count() > 1) {
					DestroyImmediate(lTree);
					--i;
				}
			}
		}
	}

	/* Check if a texture is active at a specific position (Tree space) */
	private bool TextureIsActiveFromTree(int iX, int iZ, int iTexture) {
		if (mAlphaData[iX, iZ, iTexture] == 1f) {
			return true;
		}
		return false;
	}

	/* Check if a texture is active at a specific position */
	private bool TextureIsActive(Vector3 iPosition, int iTexture) {
		TerrainData terrain = Terrain.activeTerrain.terrainData;
		if (mAlphaData[Mathf.RoundToInt(iPosition.z * terrain.heightmapHeight / terrain.size.x),
		               Mathf.RoundToInt(iPosition.x * terrain.heightmapWidth / terrain.size.z), iTexture] == 1f) {
			return true;
		}
		return false;
	}

	/* Get the index of the texture at the position */
	private int TextureAtPosition(Vector3 iPosition) {
		int lNbTexture = 8;
		int lActiveTexture = 0;
		TerrainData terrain = Terrain.activeTerrain.terrainData;
		for (int j = 0; j < lNbTexture; ++j) {
			if (mAlphaData[Mathf.RoundToInt(iPosition.z * terrain.heightmapHeight / terrain.size.x),
			               Mathf.RoundToInt(iPosition.x * terrain.heightmapWidth / terrain.size.z), j] == 1f) {
				lActiveTexture = j;
				break;
			}
		}
		return lActiveTexture;
	}

	/* Create the pylone circuit, place pylones and cables between them */
	private void ConstructPyloneWay() {
		CreateGraphPylone();
		int lDistanceBetweenExtremity = mSettings.mDistanceExtremityPylone;
		Vector3 lStartPosition = new Vector3();
		Vector3 lEndPosition = new Vector3();
		bool lFind = false;

		for (int x = 0; x < Terrain.activeTerrain.terrainData.size.x; ++x) {
			for (int z = 0;
			     z < Terrain.activeTerrain.terrainData.size.z;
			     z += (x == 0 || x == Terrain.activeTerrain.terrainData.size.x - 1) ? 1 : (int)Terrain.activeTerrain.terrainData.size.z - 1) {
				float y = GetY(x, z);
				if (Mathf.Abs(y - mCityCenter.y) < 50) {
					lStartPosition = new Vector3(x, y, z);
					lFind = true;
				}
				if (lFind) {break;}
			}
			if (lFind) {break;}
		}

		lFind = false;

		for (int x = 0; x < Terrain.activeTerrain.terrainData.size.x; ++x) {
			for (int z = 0;
			     z < Terrain.activeTerrain.terrainData.size.z;
			     z += (x == 0 || x == Terrain.activeTerrain.terrainData.size.x - 1) ? 1 : (int)Terrain.activeTerrain.terrainData.size.z - 1) {
				float y = GetY(x, z);
				if (Mathf.Abs(y - mCityCenter.y) < 50 && Vector3.Distance(lStartPosition, new Vector3(x, y, z)) >= lDistanceBetweenExtremity) {
					lEndPosition = new Vector3(x, y, z);

					lFind = true;
				}
				if (lFind) {break;}
			}
			if (lFind) {break;}
		}

		List<CartoNode> lPath = mGraphCarto.getDijkstra(mMap[(int)lStartPosition.x / mSettings.mStep, (int)lStartPosition.z / mSettings.mStep], 
		                                                mMap[(int)lEndPosition.x / mSettings.mStep, (int)lEndPosition.z / mSettings.mStep]);
		Queue<CartoNode> lQueue = new Queue<CartoNode>(lPath);
		Vector3 lCurrentPosition = new Vector3();
		Vector3 lNextPosition = new Vector3();
		List<Vector3> lPylonePositions = new List<Vector3>();

		do {
			CartoNode lNextNode = null; 
			for (int i = 0; i < mSettings.mDistanceBetweenPylone; ++i) {
				if (lQueue.Count != 0) {
					lNextNode = lQueue.Dequeue();
					if (i > 5
					    && Mathf.Abs(lCurrentPosition.y - lNextNode.mPosition.y) > 7
					    && !TerrainGenerator.findWater(lNextNode.mPosition.x, lNextNode.mPosition.z)) {
						break;
					}
					if (TerrainGenerator.findWater(lNextNode.mPosition.x, lNextNode.mPosition.z)) {
						--i;
					}
				}
			}
			lNextPosition = lNextNode.mPosition;
			lCurrentPosition = lNextPosition;
			lPylonePositions.Add(lCurrentPosition);
		} while (lQueue.Count != 0);

		for (int i = 0; i < lPylonePositions.Count; ++i) {
			Vector3 lCurrPosition = lPylonePositions[i];
			Vector3 lPrevPosition = i > 0 ? lPylonePositions[i - 1] : lCurrPosition;
			Vector3 lNxPosition = i < lPylonePositions.Count - 1 ? lPylonePositions[i + 1] : lCurrPosition;

			Vector3 lPrevAngle = CalculRotation(lPrevPosition, lCurrPosition);
			Vector3 lNxAngle = CalculRotation(lCurrPosition, lNxPosition);
			Vector3 lRotation = (lPrevAngle + lNxAngle) / 2;
			lRotation.y += 90;
			PlacePylone(lCurrPosition, lRotation, i);

			if (i > 0) {
				List<GameObject> lPrevSubPylones = new List<GameObject>();
				List<GameObject> lCurrSubPylones = new List<GameObject>();
				foreach(GameObject lObject in GameObject.FindObjectsOfType(typeof(GameObject))) {
					if (lObject != null && lObject.transform.parent != null) {
						if (lObject.transform.parent.name == "Pylone " + (i - 1)) {
							if (lObject.name == "electricity_insulator_lod0") {
								lPrevSubPylones.Add(lObject);
							}
						}

						if (lObject.transform.parent.name == "Pylone " + i) {
							if (lObject.name == "electricity_insulator_lod0") {
								lCurrSubPylones.Add(lObject);
							}
						}
					}
				}

				for (int j = 0; j < 6; ++j) {
					CreateCable(lPrevSubPylones[j].transform.position, lCurrSubPylones[j].transform.position);
				}
			}
		}
	}

	/* Create a cable from one position to an other position */
	private void CreateCable(Vector3 iStartPosition, Vector3 iEndPosition) {
		GameObject lObject = new GameObject();
		lObject.name = "Cable";
		lObject.AddComponent<LineRenderer>();
		LineRenderer lCable = lObject.GetComponent<LineRenderer>();

		lCable.material = new Material(Shader.Find("Particles/Additive"));
		lCable.SetColors(Color.grey, Color.grey);
		lCable.transform.position = (iStartPosition + iEndPosition) / 2;
		float lLineWidth = 0.07f;
		lCable.SetWidth(lLineWidth, lLineWidth);
		lCable.SetVertexCount(2);
		lCable.SetPosition(0, iStartPosition + new Vector3(0, -0.5f, 0));
		lCable.SetPosition(1, iEndPosition + new Vector3(0, -0.5f, 0));

		lObject.transform.parent = mRootPylones.transform;
		mTrash.Add(lObject);
	}

	/* Create the graph for placing external pylones */
	/* Pylone will avoid cities */
	private void CreateGraphPylone() {
		mMap = new CartoNode[(int)Terrain.activeTerrain.terrainData.size.x / mSettings.mStep, (int)Terrain.activeTerrain.terrainData.size.z / mSettings.mStep];
		int i = 0;
		int j = 0;
		for (int x = 0; x < Terrain.activeTerrain.terrainData.size.x && i < Terrain.activeTerrain.terrainData.size.x / mSettings.mStep; x += mSettings.mStep) {
			for (int z = 0; z < Terrain.activeTerrain.terrainData.size.z && j < Terrain.activeTerrain.terrainData.size.z / mSettings.mStep; z += mSettings.mStep) {
				float y = GetY(x, z);
				mMap[i, j] = new CartoNode(new Vector3(x, y, z));
				j++;
			}
			i++;
			j = 0;
		}
		
		for (int x = 0; x < Terrain.activeTerrain.terrainData.size.x / mSettings.mStep; x++) {
			for (int z = 0; z < Terrain.activeTerrain.terrainData.size.z / mSettings.mStep; z++) {
				CartoNode lCurrentNode = mMap[x, z];
				int lValue = 100;

				if (x > 0) {
					if (isInQuarters(mMap[x - 1, z].mPosition)) {
						lValue += 99999;
					}
					lValue += (int)mMap[x - 1, z].mPosition.y;
					lCurrentNode.addAdjacentNode(mMap[x - 1, z], lValue);
				}
				
				if (x < (int) Terrain.activeTerrain.terrainData.size.x / mSettings.mStep - 1) {
					if (isInQuarters(mMap[x + 1, z].mPosition)) {
						lValue += 99999;
					}
					lValue += (int)mMap[x + 1, z].mPosition.y;
					lCurrentNode.addAdjacentNode(mMap[x + 1, z], lValue);
				}
				
				if (z > 0) {
					if (isInQuarters(mMap[x, z - 1].mPosition)) {
						lValue += 99999;
					}
					lValue += (int)mMap[x, z - 1].mPosition.y;
					lCurrentNode.addAdjacentNode(mMap[x, z - 1], lValue);
				}
				
				if (z < (int) Terrain.activeTerrain.terrainData.size.z / mSettings.mStep - 1) {
					if (isInQuarters(mMap[x, z + 1].mPosition)) {
						lValue += 99999;
					}
					lValue += (int)mMap[x, z + 1].mPosition.y;
					lCurrentNode.addAdjacentNode(mMap[x, z + 1], lValue);
				}
				mGraphCarto.addNode(lCurrentNode);
			}
		}
	}

	/* Place traffic lights in some corner */
	private void FindPlaceTrafficLight() {
		foreach (CityQuarter lQuarter in mQuarters) {
			if (!lQuarter.mIsField) {
				foreach (CityNode lNode in lQuarter.mNodes) {
					if (lNode.mAdjacentNodes.Count > 2 && Random.Range(0, 9) > 6.4f) {
						Vector3 lRotation = CalculRotation(lNode.mPosition, lQuarter.mPosition);
						lRotation.y -= 90;
						PlaceTraffic(Vector3.MoveTowards(lNode.mPosition, lQuarter.mPosition, 7), lRotation);
					}
				}
			}
		}
	}

	/* Place street lights along quarters */
	private void FindPlaceLight() {
		foreach (CityQuarter lQuarter in mQuarters) {
			if (!lQuarter.mIsField) {
				for (int i = 0; i < lQuarter.mNodes.Count; ++i) {
					CityNode lNode = lQuarter.mNodes[i];
					if (i % 4 == 0 && lNode.mAdjacentNodes.Count <= 2) {
						Vector3 lRotation = CalculRotation(lNode.mPosition, lQuarter.mPosition);
						lRotation.y += 180;
						PlaceLight(Vector3.MoveTowards(lNode.mPosition, lQuarter.mPosition, 4), lRotation);
					}
				}
			}
		}
	}

	/* Place trees along quarters */
	private void FindPlaceTree() {
		foreach (CityQuarter lQuarter in mQuarters) {
			if (!lQuarter.mIsField) {
				for (int i = 0; i < lQuarter.mNodes.Count; ++i) {
					CityNode lNode = lQuarter.mNodes[i];
					if (i % 5 == 0 && lNode.mAdjacentNodes.Count <= 2) {
						PlaceTree(Vector3.MoveTowards(lNode.mPosition, lQuarter.mPosition, 7.5f), true);
					}
				}
			}
		}
	}

	/* Place Phone booth along quarters */
	private void FindPlacePhone() {
		foreach (CityQuarter lQuarter in mQuarters) {
			if (!lQuarter.mIsField) {
				for (int i = 0; i < lQuarter.mNodes.Count; ++i) {
					CityNode lNode = lQuarter.mNodes[i];
					if (i % 45 == 0 && Random.Range(0, 10) > 5) {
						Vector3 lRotation = CalculRotation(lNode.mPosition, lQuarter.mPosition);
						PlacePhone(Vector3.MoveTowards(lNode.mPosition, lQuarter.mPosition, 8), lRotation);
					}
				}
			}
		}
	}
}
