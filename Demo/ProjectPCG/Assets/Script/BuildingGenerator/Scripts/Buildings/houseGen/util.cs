using System;
using System.Collections.Generic;
using UnityEngine;
using graph;
using LotGeneration;

namespace HouseGen {

// This enum is used to do rotation along one of those axis
public enum Axis {X=0, Y=1, Z=2};

class Util {
	// Having a (standard) RNG stored here allows the house generation
	// to use random numbers wihtout influencing the other modules.
	public static System.Random rand = new System.Random();
	
	// Returns the opposite direction of the one passed as an argument
	public static Direction reverse(Direction d) {
		switch (d) {
		case Direction.NORTH: return Direction.SOUTH;
		case Direction.SOUTH: return Direction.NORTH;
		case Direction.WEST: return Direction.EAST;
		case Direction.EAST: return Direction.WEST;
		}

		return Direction.NONE;
		}

	// Get a Vector3 thats ios a certain percentage of length from the first one to the second
	// along the direction of the vector defined from p1 to p2
	public static Vector3 getPointByPercentage(Vector3 p1, Vector3 p2, float percentage) {
			Vector3 p = new Vector3(p1.x + (p2.x - p1.x)*percentage,
			                        p1.y + (p2.y - p1.y)*percentage,
			                        p1.z + (p2.z - p1.z)*percentage);

			return p;
	}

	// Get a Vector3 that is a certain distance away from p1 along the direction of the
	// vector defined from p1 to p2
	public static Vector3 getPointByDistance(Vector3 p1, Vector3 p2, float distance) {
		Vector3 reference = (p2-p1).normalized;
		Vector3 p = new Vector3(p1.x + reference.x*distance,
		                        p1.y + reference.y*distance,
		                        p1.z + reference.z*distance);
		return p;
	}
	
	// This method returns a point that has been rotated around the center
	// point from a certain angle along a specific axis.
	public static Vector3 rotatePoint(Vector3 target, Vector3 center, double angle, Axis axis) {
		double x=target.x;
		double y=target.y;
		double z=target.z;
		
		switch (axis) {
			case Axis.X:
				x=target.x;
				y=(target.z-center.z)*Math.Sin(angle) + (target.y-center.y)*Math.Cos(angle) + center.y;
				z=(target.z-center.z)*Math.Cos(angle) - (target.y-center.y)*Math.Sin(angle) + center.z;
				break;
			case Axis.Y:
				x=(target.x-center.x)*Math.Cos(angle) - (target.z-center.z)*Math.Sin(angle) + center.x;
				y=target.y;
				z=(target.x-center.x)*Math.Sin(angle) + (target.z-center.z)*Math.Cos(angle) + center.z;
				break;
			case Axis.Z:
				x=(target.x-center.x)*Math.Cos(angle) - (target.y-center.y)*Math.Sin(angle) + center.x;
				y=(target.x-center.x)*Math.Sin(angle) + (target.y-center.y)*Math.Cos(angle) + center.y;
				z=target.z;
				break;
		}
		
		return new Vector3((float)x, (float)y, (float)z);		
	}	
	
	// This method computes the angle the vector defined from p1 to p2
	// is forming with the horizontal axis
	public static float computeAngle(Vector2 p1, Vector2 p2) {
		if (p1.x == p2.x) return 0;
		Vector2 c = new Vector2(p2.x-p1.x, p2.y-p1.y);
		
		return (float) Math.Atan((double)c.y/c.x) + (p2.x<p1.x?(float)Math.PI:0);
	}
	
	// This method computes the coefficient (a,b,c,d in the equation ax+by+cz+d=0)
	// used to defined the plane containing the three points passed as argument.
	public static Vector4 computePlane(Vector3 p1, Vector3 p2, Vector3 p3) {
		Vector3 P = p2-p1;
		Vector3 Q = p3-p1;
		Vector3 coeffs = Vector3.Cross(P, Q);
		float d = -coeffs.x*p1.x - coeffs.y*p1.y - coeffs.z*p1.z;
	
		return new Vector4(coeffs.x, coeffs.y, coeffs.z, d);
	}

	// This method returns the projection of a point on a plane along a specific axis.
	// The plane argument should contains the fours coefficient of the cartesian equation
	// of a plan, which can be computed by the aboove computePlane method.
	public static Vector3 projectOnPlane(Vector3 point, Vector4 plane, Axis axis) {
		if (plane[(int)axis] == 0) return point;
		Vector3 ans = new Vector3(point.x, point.y, point.z);
		
		switch (axis) {
		case Axis.X:
			ans.x = -(point.y*plane.y + point.z*plane.z + plane.w )/plane.x;
			break;
		case Axis.Y:
			ans.y = -(point.x*plane.x + point.z*plane.z + plane.w )/plane.y;
			break;
		case Axis.Z:
			ans.z = -(point.y*plane.y + point.x*plane.x + plane.w )/plane.z;
			break;
		}
		
		// Debug.Log(point + " -> " + ans);
		
		return ans;		
	}

	// This is a debug method used to print a lot inside of a quarter
	public static GameObject drawPolygon(VoronoiPolygon vp, float height = 300, Material baseMaterial = null) {
		Vector3 center = new Vector3(0, 0, 0);
		List<Vector3> points = new List<Vector3>();
		List<int> triangles = new List<int>();
		List<Vector2> UVValues = new List<Vector2>();


		int i = 0;
		foreach (Point p in vp.vertices) {
			Vector3 v3 = new Vector3(p.x, height, p.y);
			center += v3;
		}
		center /= vp.vertices.Count;


		foreach (Point p in vp.vertices) {
			Vector3 v3 = new Vector3(p.x, height, p.y) - center;

			// Built-in protection against border polygons
			if (v3.magnitude > 1000) return null;

			points.Add (v3);

			triangles.Add((i + 1)%(vp.vertices.Count));
			triangles.Add(i);
			triangles.Add(vp.vertices.Count);

			triangles.Add(i);
			triangles.Add((++i)%(vp.vertices.Count));
			triangles.Add(vp.vertices.Count);

		}


		points.Add(center - center); // Lulz0rZ

		// DRAW THE MESH

		GameObject house = new GameObject();
		house.AddComponent<MeshFilter> ();
		house.AddComponent<MeshRenderer> ();
		house.AddComponent<MeshCollider> ();
		
		Mesh mesh = house.GetComponent<MeshFilter> ().mesh;

		while (points.Count > UVValues.Count) {
			UVValues.Add(new Vector2(0.8f, 0.5f));
		}

		mesh.vertices = points.ToArray();
		mesh.triangles = triangles.ToArray();
		mesh.uv = UVValues.ToArray ();
		mesh.Optimize ();
		mesh.RecalculateNormals ();
		house.gameObject.renderer.material = baseMaterial;
		house.transform.localScale = new Vector3(0.9f, 1, 0.9f);
		house.transform.position = center;

		return house;
	}

	// This is a debug method used to print the lots inside of a quarter
	public static void drawLots(LotMesh lots, List<GameObject> mMainStreets, Material baseMaterial) {
			if (lots == null) return;

			for (int i = 0; i < lots.width; ++i) {
				for (int j = 0; j < lots.length; ++j) {
					if (lots.mesh[i, j] != null /*&& lots.mesh[i, j].interestingScore != -1*/) {
						GameObject lot = lots.mesh[i, j].draw(baseMaterial);
						lot.name = "LOT " + lots.mesh[i, j].interestingScore;
						mMainStreets.Add(lot);
					}
				}
			}
		}
	}
}