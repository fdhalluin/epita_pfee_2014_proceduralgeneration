﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {


	public GameObject camera;
	// Use this for initialization
	void Start () {
	
	}


	public float speed = 5.0f;
	double xSpeed = 125;
	bool rightclicked = false;
	private float x = 0.0f;
	private float y = 0.0f;


	void Update()
	{
		if(Input.GetKey(KeyCode.RightArrow))
		{
			transform.position += Input.GetAxis("Horizontal") * Camera.main.transform.TransformDirection(speed * Time.deltaTime,0,0);
		}
		if(Input.GetKey(KeyCode.LeftArrow))
		{
			transform.position -= Input.GetAxis("Horizontal") * Camera.main.transform.TransformDirection(-speed * Time.deltaTime,0,0);
		}
		if(Input.GetKey(KeyCode.DownArrow))
		{
			transform.position -= Input.GetAxis("Vertical") * Camera.main.transform.TransformDirection(0,0,-speed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.UpArrow))
		{
			transform.position += Input.GetAxis("Vertical") * Camera.main.transform.TransformDirection(0,0,speed * Time.deltaTime);
		}
		if(Input.GetKey(KeyCode.Space))
		{
			transform.position += new Vector3(0,speed * Time.deltaTime,0);
		}

		transform.position += new Vector3(0, 50 * speed * Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime,0);



		if (Input.GetMouseButtonDown(1))
		{ 
			rightclicked = true; 
		} 

		if (Input.GetMouseButtonUp(1))
		{
			rightclicked = false;
		}

	}


	void LateUpdate ()
	{
		if (transform && rightclicked == true) {
			x += (float) (Input.GetAxis("Mouse X") * xSpeed * speed * 0.02f);
			y += (float) (Input.GetAxis("Mouse Y") * xSpeed * speed * 0.02f);
			Quaternion rotation = Quaternion.Euler(-y, x, 0.0f);
			Vector3 position = rotation * new Vector3(-speed, -speed, 0.0f) + transform.position;
			transform.rotation = rotation;
			//transform.position = position;
		}   
	}
}
