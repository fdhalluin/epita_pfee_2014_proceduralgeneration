﻿using UnityEngine;
using System.Collections;

public class WinDetection : MonoBehaviour {

	public GUIText YOUWIN_Text;
	public GUIText RESTART_Text;
	public MazeGenerator mazeGenerator;
	public GameObject player;
	private bool win;

	// Use this for initialization
	void Start () {
		YOUWIN_Text.guiText.enabled = false;
		RESTART_Text.guiText.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (win && Input.GetKeyDown (KeyCode.Return)) {
			YOUWIN_Text.guiText.enabled = false;
			RESTART_Text.guiText.enabled = false;
			mazeGenerator.playAMaze();
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.name == player.name) {
			YOUWIN_Text.guiText.enabled = true;
			RESTART_Text.guiText.enabled = true;
			win = true;
			Debug.Log ("WIN");
		}
	}
}
