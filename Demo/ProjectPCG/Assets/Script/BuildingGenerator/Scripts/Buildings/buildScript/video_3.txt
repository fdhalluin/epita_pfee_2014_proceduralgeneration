
Room_35:
size ?(17, 20) ?(19, 22)
height 3
build Room_35 50 50
addRoom NORTH Door_36 RANDOM
onTop Room_37 1 2
displacement 0 -0.2 0.2 -0.3 
windows NORTH 0 2 1 SQUARE
windows SOUTH 0 2 2 SQUARE
windows WEST 0 1 1 SQUARE
windows EAST 0 1 1 SQUARE
windows NORTH 1 2 1 SQUARE
windows SOUTH 1 3 2 SQUARE
windows WEST 1 2 2 SQUARE
windows EAST 1 2 2 SQUARE
windows NORTH 2 3 2 SQUARE
windows SOUTH 2 2 1 BALCONEY
windows WEST 2 2 2 SQUARE
windows EAST 2 2 1 BALCONEY
roof 0 SLOPE 0.9 0.3

Room_37:
size ?(7, 10) ?(14, 17)
height 2
onTop Room_38 1 5
displacement 0 -0.2 -0.1 
windows NORTH 0 2 3 SQUARE
windows SOUTH 0 2 3 SQUARE
windows WEST 0 3 2 SQUARE
windows EAST 0 1 1 SQUARE
windows NORTH 1 1 3 BALCONEY
windows SOUTH 1 1 1 BALCONEY
windows WEST 1 3 1 BALCONEY
windows EAST 1 2 3 BALCONEY
roof 0 SLOPE 0.9 0.3

Room_38:
size ?(3, 6) ?(4, 7)
height 1
displacement 0 0 
windows NORTH 0 3 2 SQUARE
windows SOUTH 0 1 2 SQUARE
windows WEST 0 3 3 SQUARE
windows EAST 0 1 3 SQUARE
roof 1 POINT 0.4







Door_36:
size ?(3, 6) ?(1, 4)
height 1
addWindow NORTH 0 1 DOOR
displacement 
roof 0 SLOPE 0.7 0.4 NORTH


