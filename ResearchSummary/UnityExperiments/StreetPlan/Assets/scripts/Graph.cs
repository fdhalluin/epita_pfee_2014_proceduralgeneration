﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum NodeKind {
	city,
	empty,
	water
}

public interface IIndexedObject {
	int Index { get; set; }
}

public class CartoNode : IComparer<CartoNode>, IIndexedObject {

	public static readonly CartoNode Comparer = new CartoNode(new Vector3(), NodeKind.empty);

	private Dictionary<CartoNode, int> mAdjacentNodes;
	public int mMinDistance = 9999999;
	public CartoNode mPrievousNode = null;
	public float mX { get; internal set; }
	public float mZ { get; internal set; }
	public Vector3 mPosition { get; internal set; }
	public int Index { get; set; }
	public NodeKind mNodeKind { get; set; }
	
	public CartoNode(Vector3 iPosition, NodeKind iKind) {
		mAdjacentNodes = new Dictionary<CartoNode, int>();
		mPosition = iPosition;
		mNodeKind = iKind;
	}

	public void addAdjacentNode(CartoNode iNode, int iWeight) {
		mAdjacentNodes.Add(iNode, iWeight);
	}
	
	public Dictionary<CartoNode, int> getAdjacentNodes() {
		return mAdjacentNodes;
	}

	public int Compare(CartoNode x, CartoNode y) {
		if (x.mMinDistance < y.mMinDistance)
			return -1;
		else if (x.mMinDistance > y.mMinDistance)
			return 1;
		return 0;
	}
}

public class Graph  {
	public List<CartoNode> mNodes { get; set; }

	public Graph() {
		mNodes = new List<CartoNode>();
	}

	public void addNode(CartoNode iNode) {
		mNodes.Add(iNode);
	}

	public List<CartoNode> getDijkstra(CartoNode iSourceNode, CartoNode iDestinationNode) {
		reset();
		computePaths(iSourceNode);
		return shortestPathTo(iSourceNode, iDestinationNode);
	}
	
	private void computePaths(CartoNode iSourceNode) {
		iSourceNode.mMinDistance = 0;
		PriorityQueue<CartoNode> lNodeQueue = new PriorityQueue<CartoNode>(CartoNode.Comparer);
		lNodeQueue.Push(iSourceNode);
		
		while (lNodeQueue.Count != 0) {
			CartoNode lCurrentNode = lNodeQueue.Pop();
			foreach (KeyValuePair<CartoNode, int> lEntry in lCurrentNode.getAdjacentNodes()) {
				CartoNode lAdjacentNode = lEntry.Key;
				int lWeight = lEntry.Value;
				int lDistanceNode = lCurrentNode.mMinDistance + lWeight;
				if (lDistanceNode < lAdjacentNode.mMinDistance) {
					lNodeQueue.Remove(lAdjacentNode);
					lAdjacentNode.mMinDistance = lDistanceNode;
					lAdjacentNode.mPrievousNode = lCurrentNode;
					lNodeQueue.Push(lAdjacentNode);
				}
			}
		}
	}
	
	private List<CartoNode> shortestPathTo(CartoNode iSourceNode, CartoNode iDestinationNode) {
		List<CartoNode> lPath = new List<CartoNode>();
		for (CartoNode lNode = iDestinationNode; lNode != null; lNode = lNode.mPrievousNode) {
			lPath.Add(lNode);
		}
		lPath.Reverse();
		
		return lPath;
	}

	private void reset() {
		foreach (CartoNode lNode in mNodes) {
			lNode.mMinDistance = 9999999;
			lNode.mPrievousNode = null;
		}
	}
}
