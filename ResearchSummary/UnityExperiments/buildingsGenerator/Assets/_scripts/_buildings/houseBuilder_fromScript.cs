using UnityEngine;
using System.Collections.Generic;
using HouseGen;
using System;

public partial class houseBuilder {

	GameObject generatedBuilding;
	
	List<float> getDisplacementList(string displ) { Debug.Log(displ);
		List<float> ans = new List<float>();
		string[] displacements = displ.Split(" ".ToCharArray());
		
		for (int i = 1; i < displacements.Length; ++i) {
			if (displacements[i].Length <= 0) continue;
			Debug.Log(""+displacements[i]);
			ans.Add((float)Double.Parse(""+displacements[i]));
			Debug.Log("OK");
		}
		
		
		return ans;
	}
	
	Rectangle addRoomFromRoomDescription(string buildStatement, RoomDescription roomDesc, Dictionary<string, Rectangle> rooms, Story floor) {
		string[] buildTokens = buildStatement.Split(' ');
		int xOffset = Int32.Parse(buildTokens[2]);
		int yOffset = Int32.Parse(buildTokens[3]);
		int roomWidth = Int32.Parse(roomDesc.properties["size"].Split(' ')[1]);
		int roomLength = Int32.Parse(roomDesc.properties["size"].Split(' ')[2]);
		int roomHeight = Int32.Parse(roomDesc.properties["height"].Split(' ')[1])-1;
		float roofOffset = Single.Parse(roomDesc.properties["roof"].Split(' ')[1]);
		Rectangle.RoofType roofType = (Rectangle.RoofType)Enum.Parse(typeof(Rectangle.RoofType), roomDesc.properties["roof"].Split(' ')[2]);		
		Rectangle room = new Rectangle(xOffset, yOffset, roomWidth, roomLength, roomHeight);
		room.setDisplacements(getDisplacementList(roomDesc.properties["displacement"]));
		room.roofOffset = roofOffset;
		room.roofType = roofType;
		rooms[roomDesc.name] = room;
		roomDesc.roomsBuffer.Add(room);
		
		// TODO: retrieve and specify information regarding the roof type
		
		return floor.addRoom(room);	
	}
	
	void growRooms(Story floor) {
		bool stopGrowing = false;
		Story currFloor = floor;
		while (!stopGrowing) {
			stopGrowing = true;
			if (currFloor.getRooms().Count > 0) {
				if (currFloor.getNext() == null) {
						currFloor = currFloor.generateNextStory("");
					}
					else {
						currFloor = currFloor.getNext();
						currFloor.growRoomsFromPrevious();
					}
					stopGrowing = false;
			}
		}
	}
	
	void flushRoomsBuffer(Dictionary<string, RoomDescription> building) {
		foreach (RoomDescription roomDesc in building.Values) {
			roomDesc.unloadRoomBuffer();
		}
	}
	
	public void initFromScript(string scriptText) {
		Parser p = new Parser();
		Dictionary<string, RoomDescription> building = p.parse(scriptText);
		RoomDescription toBuild = building[Parser.roomsToBuild];
		Dictionary<string, Rectangle> rooms = new Dictionary<string, Rectangle>();
		Dictionary<string, RoomDescription> addToBuilding = new Dictionary<string, RoomDescription>();
		
		Story floor = new Story(200, 200, "Floor #0");
		
		// First, lay out each room on the first floor
		
		foreach (string roomNum in toBuild.properties.Keys) {
			string[] buildTokens = toBuild.properties[roomNum].Split(' ');
			RoomDescription roomDesc = building[buildTokens[1]];
			addRoomFromRoomDescription(toBuild.properties[roomNum], roomDesc, rooms, floor);		
		}
		flushRoomsBuffer(building);
	
		bool allRoomsAdded = false;
		
		while (!allRoomsAdded) {
			allRoomsAdded = true;

			// Then, add the rooms on the sides
				
			
			foreach (string roomName in building.Keys) {
				RoomDescription roomDesc = building[roomName];		
				
				foreach(Rectangle room in roomDesc.rooms) {
					if (room.getRoomsOnSidesCount() + room.getTowerList().Count > 0) {
						continue;
					}
					for (int i = 0; i < roomDesc.commandsCount[(int)Parser.CommandType.ROOM]; ++i) {
						string[] addTokens = roomDesc.properties[Parser.CommandType.ROOM + "_" + i].Split(' ');
						
					
						if (addTokens[0].Equals("addRoom")) {
							Direction side = (Direction) Enum.Parse(typeof(Direction), addTokens[1]);
							RoomDescription newRoomDesc = building[addTokens[2]];
							Rectangle.PositionType positionType = (Rectangle.PositionType) Enum.Parse(typeof(Rectangle.PositionType),addTokens[3]);
							int para = Int32.Parse(newRoomDesc.properties["size"].Split(' ')[1]);
							int perp = Int32.Parse(newRoomDesc.properties["size"].Split(' ')[2]);
							int roomHeight = Int32.Parse(newRoomDesc.properties["height"].Split(' ')[1])-1;
							float roofOffset = Single.Parse(newRoomDesc.properties["roof"].Split(' ')[1]);
							Rectangle.RoofType roofType = (Rectangle.RoofType)Enum.Parse(typeof(Rectangle.RoofType), newRoomDesc.properties["roof"].Split(' ')[2]);
							// TODO: retrieve and use data about the height and RoofType of the room
							
							if ((int)side >= (int)Direction.WEST) {
								int temp = para;
								para = perp;
								perp = temp;
							}	
							
							Rectangle potentialNewRoom = room.addRectangleToSide(side, para, perp, perp, positionType, true);
							
							if ( (potentialNewRoom.w == para && potentialNewRoom.h == perp)
								|| (potentialNewRoom.w == perp && potentialNewRoom.h == para) ) {
								room.addGeneratedRoomToSide(side, potentialNewRoom);
								addToBuilding[newRoomDesc.name] = newRoomDesc;Debug.Log(newRoomDesc.name);
								potentialNewRoom.roomHeight = roomHeight;
								potentialNewRoom.roofOffset = roofOffset;
								potentialNewRoom.roofType = roofType;
								newRoomDesc.rooms.Add(potentialNewRoom);
								potentialNewRoom.setDisplacements(getDisplacementList(newRoomDesc.properties["displacement"]));
								allRoomsAdded = false;
							}
							else {
								Debug.Log("Could not add room " + newRoomDesc.name);
								Debug.Log("para " + para + " perp " + perp);
								Debug.Log("w " + potentialNewRoom.w + " h " + potentialNewRoom.h);
							}
							
						}
						if (addTokens[0].Equals("addTower")) {
							Direction vertical = (Direction) Enum.Parse(typeof(Direction), addTokens[1]);
							Direction horizontal = (Direction) Enum.Parse(typeof(Direction), addTokens[2]);
							int radius = Int32.Parse(addTokens[3]);
							int height = Int32.Parse(addTokens[4]) - 1;
							
							room.addTower(vertical, horizontal, radius, height);
							allRoomsAdded = false;
						}
					
					}
					
				}
				
		
			}
			foreach (string key in addToBuilding.Keys) {
				building[key] = addToBuilding[key];
			}
			addToBuilding.Clear();
			
			// Following that, grow all rooms to their height and add the onTop rooms over them
			
			growRooms(floor);
			
			int indCount = 0;
			foreach (string roomName in building.Keys) {
				RoomDescription roomDesc = building[roomName];
				
				for (int i = 0; i < roomDesc.commandsCount[(int)Parser.CommandType.TOP]; ++i) {
					string[] topTokens = roomDesc.properties[Parser.CommandType.TOP + "_" + i].Split(' ');
					RoomDescription newRoomDesc = building[topTokens[1]];
					int xOffset = Int32.Parse(topTokens[2]);
					int yOffset = Int32.Parse(topTokens[3]);
					
					foreach (Rectangle room in roomDesc.rooms) {
					
						if (room.getOnTop().Count > i) continue;
					
						Story targetStory = room.floor.getStoryAtHeight(room.roomHeight+1);
						
						Rectangle newRoom = addRoomFromRoomDescription("build " + newRoomDesc.name + " " + (room.x + xOffset) + " " + (room.y + yOffset), newRoomDesc, rooms, targetStory);				
					
						if (newRoom == null) {
							Debug.Log("NAY");
						}
						else {
							addToBuilding[newRoomDesc.name + "_" + (++indCount)] = newRoomDesc;
							room.getOnTop().Add(newRoom);
							allRoomsAdded = false;
						}
					}
				
				}
				
			}
			foreach (string key in addToBuilding.Keys) {
				building[key] = addToBuilding[key];
			}
			addToBuilding.Clear();
			flushRoomsBuffer(building);
			
			growRooms(floor); 
			
		
		}		
		
		// Finally, add windows when specified
		
		foreach (string roomName in building.Keys) {
			RoomDescription roomDesc = building[roomName];
			
			for (int i = 0; i < roomDesc.commandsCount[(int)Parser.CommandType.WINDOWS]; ++i) {
				string[] winTokens = roomDesc.properties[Parser.CommandType.WINDOWS + "_" + i].Split(' ');
				Direction side = (Direction)Enum.Parse(typeof(Direction), winTokens[1]);
				int storey = Int32.Parse(winTokens[2]);
				int windowSize = Int32.Parse(winTokens[3]);
				int spacing = Int32.Parse(winTokens[4]);
				WindowShape shape = (WindowShape)Enum.Parse(typeof(WindowShape), winTokens[5]);
				
				foreach (Rectangle room in roomDesc.rooms) {
					room.getRoomAtStory(storey).addWindows(side, windowSize, spacing, shape);					
				}
				
			}
			
		}
		
		// Generate the building's mesh

		if (generatedBuilding != null) {
			Destroy(generatedBuilding);
		}

		generatedBuilding = buildHouse(floor);
			
	}
	
}
