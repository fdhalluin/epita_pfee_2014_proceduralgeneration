using System;
using System.Collections.Generic;

namespace graph
{
	public interface IGraph
	{
		HashSet<graph.Point> getVertices();

		HashSet<graph.Edge> getEdges();
	}
}
