﻿//#pragme strict
using UnityEngine;
using System.Collections.Generic;
using HouseGen;
using System;
using Utils;

// WARNING: this class is partially defined in another file, houseBuilder_fromScript.cs
// The method in the current files are the core method that generates a mesh from a building
// represented by a sequence of Story objects.
public partial class houseBuilder : MonoBehaviour {

	// The five different palette used by buildings.
	public Material baseMaterial;
	public Material baseMaterial2;
	public Material baseMaterial3;
	public Material baseMaterial4;
	public Material baseMaterial5;

	// A global parameter used to set a palette colour for the generated buildings.
	public static PaletteKind textureToUse = PaletteKind.redBeige;

	// The vertices of the mesh
	List<Vector3> vertices;
	// The faces of the mesh
	List<int> triangles;
	// The UV mapping of each points are stored in this list
	List<Vector2> UVValues;
	// This a temporary dictionnary to store our points and work on them before storing them in the mesh.
	// Points already stored in the mesh can be destroyed from this dictionnary without fear.
	Dictionary<string, Vector3> points;
	// A list of all the center of the windows, used to place lights after the model has been generated.
	List<Vector3> mWindows;
	// A prefab for the light used to illuminate windows.
	public GameObject lightWindow;
	// A list of colours assignations, only used to export an OBJ mode of the house.
	List<int> colours;

	// The maximum number of lights allowed ; can be changed by the suer in the Village Editor.
	static public int totalLights = 500;

	// The maximum number of colours in the palette. One is currently unused.
	// See the OBJ file export method writeAsObj() to get an explanantion of which index is used for which
	// part of the house.
	int totalColors = 5;
	// The standard height of a storey.
	int wallHeight = Rectangle.wallHeight;
	// A counter of how many buildings have been generated.
	static public int buildNum = 0;
	// An option that deactivate some features like volumetric roofs and windows to allow for models with
	// a lower footprint on the rendering times. 
	static public bool lowEndModels = false;

	// An optional offset to the anchor point of the model, set by the caller of a generation.
	float dX;
	float dY;
	float dZ;

	// Those strings are here to ease the process of adding point to the mesh
	// - 'nw', 'ne', 'sw' and 'se' points are named after the cardinal directions and represent
	//	 the corners of a room.
	// - 'd' and 'u' indicates if the point is at floor ('d') or ceiling ('u') level for the room.
	// - 'top' point are part of the roof.
	// - 'e' points are extended versions of other points, which allow to make the roof go beyond
	//	 the walls.
	string nwd = "nwd";
	string ned = "ned";
	string sed = "sed";
	string swd = "swd";
	string nwu = "nwu";
	string neu = "neu";
	string seu = "seu";
	string swu = "swu";
	
	string top = "top";
	string wtop = "wtop";
	string etop = "etop";
	string ntop = "ntop";
	string stop = "stop";
	
	string nwp = "nwp";
	string nep = "nep";
	string swp = "swp";
	string sep = "sep";

	string nwtop = "nwtop";
	string netop = "netop";
	string setop = "setop";
	string swtop = "swtop";
	string nwtope = "nwtope";
	string netope = "netope";
	string setope = "setope";
	string swtope = "swtope";
	string nwtopeu = "nwtopeu";
	string netopeu = "netopeu";
	string setopeu = "setopeu";
	string swtopeu = "swtopeu";

	string topu = "topu";
	string nwpu = "nwpu";
	string nepu = "nepu";
	string swpu = "swpu";
	string sepu = "sepu";

	// The path to the directory containg the buildings' scripts
	string PATH = "Assets/Script/BuildingGenerator/Scripts/Buildings/buildScript/";

	// This is the point used by Unity to position and rotate the mesh
	// As the house is constructed by offsetting points from (0,0,0),
	// we need to recenter it manually later on
	Vector3 anchorPoint;
	int anchorCount;

	public GameObject generateBuilding(string scriptName, bool treatAsScript = false) {
		// Those lists should be cleared between each generation
		vertices.Clear();
		triangles.Clear();
		UVValues.Clear();
		points.Clear();
		colours.Clear ();

		if (treatAsScript) {
			// The given script will be treated as the content of the script file
			return initFromScript(scriptName);
		}
		// Else the script's content will be read from the disk from a file with
		// the given name in the script folder
		return initFromScript(System.IO.File.ReadAllText(PATH + scriptName + ".txt"));
	}	
	
	// Use this for initialization
	houseBuilder () {
		vertices = new List<Vector3> ();
		triangles = new List<int> ();
		UVValues = new List<Vector2> ();
		points = new Dictionary<string, Vector3> ();
		colours = new List<int>();
		mWindows = new List<Vector3>();
		initPropPrefabs();
	}

	// This method adds a point to the mesh that is currently being built, and
	// assigns it a particular colour on the texture of the building
	int addPoint(Vector3 point, Vector2 color, bool flatShading = false) {
		int ind = 0;

		// If the point already exists (same coordinates and colour), we return the existing index
		while ((ind = vertices.IndexOf(point, ind, vertices.Count-ind)) != -1 && !flatShading) {
			if (UVValues[ind].Equals(color)) {
				break;
			}
			++ind;
		}

		// Else, we create a new point. Also disables standard Unity shadows
		// because the triangles are not connected anymore.
		if (ind == -1 || flatShading) {
			ind = vertices.Count;
			vertices.Add(point);
			UVValues.Add(color);
		}
		
		return ind;
	}

	// This method takes the name of three point in the "database" of important points in the building,
	// a colour, and builds a triangle from them.
	// Because Unity uses backward-culling, points should be specified counter-clockwisely for the face
	// to be visible. Enabling the boolean twoFaces will create two reverse-sided triangles from the points,
	// but often causes lighting issues (triangles being darker than they should be).
	void addTriangle(string pointName1, string pointName2, string pointName3, int color, bool twoFaces = false) {
		// We add + 0.01f to avoid a strange behaviour with the UVMapping
		Vector2 colorVect = new Vector2((float)color / totalColors + 0.01f, 0.5f);
		int point1 = vertices.Count;
		vertices.Add(points[pointName1]);
		UVValues.Add(colorVect);
		int point2 = vertices.Count;
		vertices.Add(points[pointName2]);
		UVValues.Add(colorVect);
		int point3 = vertices.Count;
		vertices.Add(points[pointName3]);
		UVValues.Add(colorVect);

		triangles.Add (point1);
		triangles.Add (point2);
		triangles.Add (point3);
		colours.Add(color);

		if (twoFaces) {
			triangles.Add (point1);
			triangles.Add (point3);
			triangles.Add (point2);
			colours.Add(color);
		}
	}

	// This method builds a quadrilateral by splitting it into two triangles. The same rule applies regarding
	// specifying the points in a counter-clockwise order.
	void addQuadrilateral(string pointName1, string pointName2, string pointName3, string pointName4, int color) {
		addTriangle (pointName2, pointName1, pointName4, color);
		addTriangle (pointName2, pointName4, pointName3, color);
	}

	// This method is used to apply displacement to existing points (which makes the walls into slopes)
	// The level char tells us if we are modifying the down or up points of the room
	void expandPoints(char level, float d, Dictionary<string, Vector3> points) {
		string nw = "nw";
		string ne = "ne";
		string se = "se";
		string sw = "sw";
		points[nw+level] = new Vector3(points[nw+level].x-d, points[nw+level].y, points[nw+level].z-d);
		points[ne+level] = new Vector3(points[ne+level].x+d, points[ne+level].y, points[ne+level].z-d);
		points[se+level] = new Vector3(points[se+level].x+d, points[se+level].y, points[se+level].z+d);
		points[sw+level] = new Vector3(points[sw+level].x-d, points[sw+level].y, points[sw+level].z+d);
	}

	// This method builds a mesh for the Rectangle room at the height story given as a parameter
	void buildRoom(Rectangle room, int story = 0) {
		// Those points are temporary and should be reset for each room
		points.Clear();

		float storyOffset = story * wallHeight;

		// We add the floor points of the room
		points.Add(nwd, new Vector3(room.x, storyOffset, room.y));
		points.Add(ned, new Vector3(room.x+room.w, storyOffset, room.y));
		points.Add(sed, new Vector3(room.x+room.w, storyOffset, room.y+room.h));
		points.Add(swd, new Vector3(room.x, storyOffset, room.y+room.h));

		// Those points will be used later to determine the new anchor point of the mesh
		// We are not using all points because other features of the house have a higher
		// level of detail, thus increasing their weight in the mean point
		foreach (Vector3 point in points.Values) {
			anchorPoint += point;
			++anchorCount;
		}

		// We add the ceiling points of the room
		points.Add(nwu, new Vector3(room.x, storyOffset + wallHeight, room.y));
		points.Add(neu, new Vector3(room.x+room.w, storyOffset + wallHeight, room.y));
		points.Add(seu, new Vector3(room.x+room.w, storyOffset + wallHeight, room.y+room.h));
		points.Add(swu, new Vector3(room.x, storyOffset + wallHeight, room.y+room.h));

		// Those two sets of points are displaced according to the displacement value of the room.
		// This will make the walls into slopes.
		expandPoints('d', room.baseDispl, points);
		expandPoints('u', room.topDispl, points);
		
		// Then we describe each wall, with two triangles each.
		addQuadrilateral(neu, nwu, nwd, ned, 0);
		addQuadrilateral(seu, neu, ned, sed, 0);
		addQuadrilateral(swu, seu, sed, swd, 0);
		addQuadrilateral(nwu, swu, swd, nwd, 0);

		// We do not want to draw a floor when lowModels are activated, to save rendering time.
		if (!lowEndModels) {
			addQuadrilateral(ned, nwd, swd, sed, 0);
		}

		// A roomHeight value of 0 indicates that this room does not have any other room on its roof,
		// allowing to place a custom roof ratehr than a flat one
		if (room.roomHeight == 0) {
			buildRoof(room, storyOffset);
		}
		else {
			// This can be uncommented to still place a roof while forcing it to be flat
			//buildRoof(room, storyOffset, true);
		}

		// windows are the most expensive fetaure of houses rendering-wise, so we do not want them to be
		// created when in lowModels mode
		if (!lowEndModels) {
			buildWindows(room, story);
		}

		// House can also have pillars, although this is not used quite often ...
		buildPillars(room, story);		
	}

	// This method is used to place a roof on top of the current room
	void buildRoof(Rectangle room, float story = 0, bool forceFlat = false) {
		// We start by retrieving parameters from the room itself
		float roofDepth = room.roofDepth;
		bool flatTop = room.roofType == Rectangle.RoofType.FLAT || room.roofType == null;
		bool pointTop = room.roofType == Rectangle.RoofType.POINT;
		bool linearTop = room.roofType == Rectangle.RoofType.LINEAR;
		bool slopeTop = room.roofType == Rectangle.RoofType.SLOPE;

		// The flatTop is nothing more than a quadrilateral placed on top of the house.
		// We double-side the quadrilateral so as to be sure that it gets rendered from evry side.
		if (flatTop || forceFlat) {
			points.Add(nwp, Util.getPointByDistance(points[nwu], points[seu], -room.extendedRoof));
			points.Add(nep, Util.getPointByDistance(points[neu], points[swu], -room.extendedRoof));
			points.Add(sep, Util.getPointByDistance(points[seu], points[nwu], -room.extendedRoof));
			points.Add(swp, Util.getPointByDistance(points[swu], points[neu], -room.extendedRoof));
			addQuadrilateral(nwp, nep, sep, swp, 2);
			addQuadrilateral(nep, nwp, swp, sep, 2);
		}

		// This ones gives the room a pointy top
		if (pointTop && !forceFlat) {
			points.Add(top, new Vector3(room.x+(float)room.w/2, story + wallHeight + room.roofOffset, room.y+(float)room.h/2));
			points.Add (topu, points[top] + new Vector3(0, roofDepth, 0));

			points.Add(nwp, Util.getPointByDistance(points[nwu], points[top], -room.extendedRoof) );
			points.Add(nep, Util.getPointByDistance(points[neu], points[top], -room.extendedRoof) );
			points.Add(swp, Util.getPointByDistance(points[swu], points[top], -room.extendedRoof) );
			points.Add(sep, Util.getPointByDistance(points[seu], points[top], -room.extendedRoof) );

			points.Add(nwpu, points[nwp] + new Vector3(0, roofDepth, 0));
			points.Add(nepu, points[nep] + new Vector3(0, roofDepth, 0));
			points.Add(swpu, points[swp] + new Vector3(0, roofDepth, 0));
			points.Add(sepu, points[sep] + new Vector3(0, roofDepth, 0));

			addTriangle(nwp, nep, top, 2);
			addTriangle(top, nep, sep, 2);
			addTriangle(top, sep, swp, 2);
			addTriangle(nwp, top, swp, 2);

			if (lowEndModels) {
				// When in lowModels mode, we only double-side the triangle to be sure that they
				// will be rendered both from the top and from the bottom.
				addTriangle(nwp, top, nep, 2);
				addTriangle(top, sep, nep, 2);
				addTriangle(top, swp, sep, 2);
				addTriangle(nwp, swp, top, 2);
			}
			else {
				// If we allow for better models, the roof will be given a volume of its own.
				addQuadrilateral(nepu, nwpu, nwp, nep, 2);
				addQuadrilateral(nwpu, swpu, swp, nwp, 2);
				addQuadrilateral(swpu, sepu, sep, swp, 2);
				addQuadrilateral(sepu, nepu, nep, sep, 2);

				addTriangle(nepu, nwpu, topu, 2);
				addTriangle(nepu, topu, sepu, 2);
				addTriangle(sepu, topu, swpu, 2);
				addTriangle(topu, nwpu, swpu, 2);
			}

		}

		// Linear top are not used in generation, but can still be called manually when building a house script.
		// This is a triangular type of roof which try to connect to adjacent orthogonal linear roofs, helping
		// to create the impression of a whole building from two separated rooms.
		// Since this roof has been lost in development, it still meets the lowModels requirement and does not have
		// a volumetric version of itself.
		if (linearTop && !forceFlat) {
			// Two cases are needed, for whether the house is wider than it is large or the opposite.
			if (room.w > room.h) {
				float parentHalfOffset = 0;
				if (room.parent != null && room.parent.roomHeight == 0) {
					parentHalfOffset = room.parent.x + (float)room.parent.w/2 - room.x;
					if (room.parentSide == Direction.EAST) parentHalfOffset -= room.w;
					if (room.y < room.parent.y || room.y+room.h > room.parent.y + room.parent.h) parentHalfOffset = 0;
				}

				points.Add(wtop, new Vector3(room.x + (room.parentSide == Direction.WEST?parentHalfOffset:0), story + wallHeight + room.roofOffset, room.y+(float)room.h/2));
				points.Add(etop, new Vector3(room.x + room.w + (room.parentSide == Direction.EAST?parentHalfOffset:0), story + wallHeight + room.roofOffset, room.y+(float)room.h/2));

				addTriangle(wtop, nwu, swu, 1);
				addTriangle(etop, seu, neu, 1);

				addQuadrilateral(etop, wtop, nwu, neu, 2);
				addQuadrilateral(wtop, etop, seu, swu, 2);
				
			}
			else {
				float parentHalfOffset = 0;
				if (room.parent != null && room.parent.roomHeight == 0) {
					parentHalfOffset = room.parent.y + (float)room.parent.h/2 - room.y;
					if (room.parentSide == Direction.SOUTH) parentHalfOffset -= room.h;
					if (room.x < room.parent.x || room.x+room.w > room.parent.x + room.parent.w) parentHalfOffset = 0;
				}

				points.Add(ntop, new Vector3(room.x+(float)room.w/2, story + wallHeight + room.roofOffset, room.y + (room.parentSide == Direction.NORTH?parentHalfOffset:0)));
				points.Add(stop, new Vector3(room.x+(float)room.w/2, story + wallHeight + room.roofOffset, room.y+room.h + (room.parentSide == Direction.SOUTH?parentHalfOffset:0)));


				addTriangle(ntop, neu, nwu, 1);
				addTriangle(stop, swu, seu, 1);

				addQuadrilateral(ntop, stop, swu, nwu, 2);
				addQuadrilateral(stop, ntop, neu, seu, 2);
			}
		}

		// This roof is sloped, but only from one side (which you can chose).
		if (slopeTop) {
			points.Add(nwtop, points[nwu]
			           + ((room.roofDirection == Direction.NORTH
			    || room.roofDirection == Direction.WEST)?new Vector3(0, room.roofOffset, 0):new Vector3(0, 0, 0)) );
			points.Add(netop, points[neu]
			           + ((room.roofDirection == Direction.NORTH
			    || room.roofDirection == Direction.EAST)?new Vector3(0, room.roofOffset, 0):new Vector3(0, 0, 0)) );
			points.Add(setop, points[seu]
			           + ((room.roofDirection == Direction.SOUTH
			    || room.roofDirection == Direction.EAST)?new Vector3(0, room.roofOffset, 0):new Vector3(0, 0, 0)) );
			points.Add(swtop, points[swu]
			           + ((room.roofDirection == Direction.SOUTH
			    || room.roofDirection == Direction.WEST)?new Vector3(0, room.roofOffset, 0):new Vector3(0, 0, 0)) );


			points.Add(nwtope, Util.getPointByDistance(points[nwtop], points[setop], -room.extendedRoof));
			points.Add(netope, Util.getPointByDistance(points[netop], points[swtop], -room.extendedRoof));
			points.Add(setope, Util.getPointByDistance(points[setop], points[nwtop], -room.extendedRoof));
			points.Add(swtope, Util.getPointByDistance(points[swtop], points[netop], -room.extendedRoof));

			points.Add(nwtopeu, points[nwtope] + new Vector3(0, roofDepth, 0));
			points.Add(netopeu, points[netope] + new Vector3(0, roofDepth, 0));
			points.Add(setopeu, points[setope] + new Vector3(0, roofDepth, 0));
			points.Add(swtopeu, points[swtope] + new Vector3(0, roofDepth, 0));

			addQuadrilateral(netop, nwtop, nwu, neu, 0);
			addQuadrilateral(nwtop, swtop, swu, nwu, 0);
			addQuadrilateral(swtop, setop, seu, swu, 0);
			addQuadrilateral(setop, netop, neu, seu, 0);

			addQuadrilateral(netope, nwtope, swtope, setope, 2);

			if (lowEndModels) {
				// Once again, lowModels mode will only double-side the quadrilateral used for the roof ...
				addQuadrilateral(nwtope, netope, setope, swtope, 2);
			}
			else {
				// ... while using better models will give you a volumetric roof 
				addQuadrilateral(netopeu, nwtopeu, nwtope, netope, 2);
				addQuadrilateral(nwtopeu, swtopeu, swtope, nwtope, 2);
				addQuadrilateral(swtopeu, setopeu, setope, swtope, 2);
				addQuadrilateral(setopeu, netopeu, netope, setope, 2);

				addQuadrilateral(nwtopeu, netopeu, setopeu, swtopeu, 2);
			}

		}
	
	}

	// This (rather simple to understand) method builds pillars in each of the decided position
	void buildPillars(Rectangle room, int storey = 0) {
		foreach(Pillar pillar in room.pillars) {
			if (pillar.startHeight == storey) {
				buildPillar(pillar);
			}
		}
	}

	void buildPillar(Pillar pillar) {
		points.Clear();
		Vector3 center = new Vector3(pillar.x + 0.5f, 0, pillar.y + 0.5f);
		float boxSize = 0.3f;
		float circleRadius = 0.33f;
		int circlePointsNum = 16;
		float storyOffset = pillar.startHeight * wallHeight;

		// The lower support
		points.Add(nwd, new Vector3(pillar.x, storyOffset, pillar.y));
		points.Add(ned, new Vector3(pillar.x+1, storyOffset, pillar.y));
		points.Add(sed, new Vector3(pillar.x+1, storyOffset, pillar.y+1));
		points.Add(swd, new Vector3(pillar.x, storyOffset, pillar.y+1));

		points.Add(nwu, new Vector3(pillar.x, storyOffset+boxSize, pillar.y));
		points.Add(neu, new Vector3(pillar.x+1, storyOffset+boxSize, pillar.y));
		points.Add(seu, new Vector3(pillar.x+1, storyOffset+boxSize, pillar.y+1));
		points.Add(swu, new Vector3(pillar.x, storyOffset+boxSize, pillar.y+1));

		// Faces for the lower support of the pillars are built here.
		addQuadrilateral(neu, nwu, nwd, ned, 2);
		addQuadrilateral(seu, neu, ned, sed, 2);
		addQuadrilateral(swu, seu, sed, swd, 2);
		addQuadrilateral(nwu, swu, swd, nwd, 2);
		addQuadrilateral(nwu, neu, seu, swu, 2);
		addQuadrilateral(ned, nwd, swd, sed, 2);

		if (pillar.pillarType == Rectangle.PillarType.CIRCLE || true) {
			for (int h = pillar.startHeight; h <= pillar.startHeight+pillar.height; ++h) {
				for (int i = circlePointsNum-1; i >= 0; --i) {
					float theta = (float)i*2*Mathf.PI / circlePointsNum;
					Vector3 point = new Vector3(circleRadius*Mathf.Cos(theta),
					                            h*wallHeight,
					                            circleRadius*Mathf.Sin(theta));
					point += center;
					points.Add(h+"_"+i, point);
				}
			}

			for (int h = pillar.startHeight; h < pillar.startHeight+pillar.height; ++h) {
				int nextH = h+1;
				for (int i = circlePointsNum-1; i >= 0; --i) {
					int nextI = (i==0?circlePointsNum-1:i-1);

					addQuadrilateral(nextH+"_"+i, nextH+"_"+nextI, h+"_"+nextI, h+"_"+i, 3);
				}
			}
		}

		// We have to clear the points because we will need the names used by the lower support
		// to generate the upper one. We can erase the others from our temporary dictionnary
		// because they have already been written to the mesh's vertices.
		points.Clear();

		// The upper support
		storyOffset = (pillar.startHeight+pillar.height)*wallHeight;
		points.Add(nwd, new Vector3(pillar.x, storyOffset-boxSize, pillar.y));
		points.Add(ned, new Vector3(pillar.x+1, storyOffset-boxSize, pillar.y));
		points.Add(sed, new Vector3(pillar.x+1, storyOffset-boxSize, pillar.y+1));
		points.Add(swd, new Vector3(pillar.x, storyOffset-boxSize, pillar.y+1));
		
		points.Add(nwu, new Vector3(pillar.x, storyOffset, pillar.y));
		points.Add(neu, new Vector3(pillar.x+1, storyOffset, pillar.y));
		points.Add(seu, new Vector3(pillar.x+1, storyOffset, pillar.y+1));
		points.Add(swu, new Vector3(pillar.x, storyOffset, pillar.y+1));

		// Faces for the upper support of the pillars are built here.
		addQuadrilateral(neu, nwu, nwd, ned, 2);
		addQuadrilateral(seu, neu, ned, sed, 2);
		addQuadrilateral(swu, seu, sed, swd, 2);
		addQuadrilateral(nwu, swu, swd, nwd, 2);
		addQuadrilateral(nwu, neu, seu, swu, 2);
		addQuadrilateral(ned, nwd, swd, sed, 2);

	}

	// This method computes the 4 coefficients of the equation of the plane representing a certain wall
	// of the room given as an argument (ax + by + cz + d = 0). It is used to project windows on the wall
	// correctly.
	Vector4 computePlaneFromSide(Rectangle room, Direction side, int story = 0) {
		Vector3[] p = new Vector3[3];
		float storyOffset = story*wallHeight;
		Dictionary<string, Vector3> roomPoints = new Dictionary<string, Vector3>();
		roomPoints.Add(nwd, new Vector3(room.x, storyOffset, room.y));
		roomPoints.Add(ned, new Vector3(room.x+room.w, storyOffset, room.y));
		roomPoints.Add(sed, new Vector3(room.x+room.w, storyOffset, room.y+room.h));
		roomPoints.Add(swd, new Vector3(room.x, storyOffset, room.y+room.h));
		roomPoints.Add(nwu, new Vector3(room.x, storyOffset + wallHeight, room.y));
		roomPoints.Add(neu, new Vector3(room.x+room.w, storyOffset + wallHeight, room.y));
		roomPoints.Add(seu, new Vector3(room.x+room.w, storyOffset + wallHeight, room.y+room.h));
		roomPoints.Add(swu, new Vector3(room.x, storyOffset + wallHeight, room.y+room.h));
		expandPoints('d', room.baseDispl, roomPoints);
		expandPoints('u', room.topDispl, roomPoints);
		
		switch (side) {
			case Direction.NORTH:
				p[0] = roomPoints[neu];
				p[1] = roomPoints[nwu];
				p[2] = roomPoints[nwd];
				break;
			case Direction.SOUTH:
				p[0] = roomPoints[swu];
				p[1] = roomPoints[seu];
				p[2] = roomPoints[sed];
				break;
			case Direction.WEST:
				p[0] = roomPoints[nwu];
				p[1] = roomPoints[swu];
				p[2] = roomPoints[swd];
				break;
			case Direction.EAST:
				p[0] = roomPoints[seu];
				p[1] = roomPoints[neu];
				p[2] = roomPoints[ned];
				break;
		}
		
		return Util.computePlane(p[0], p[1], p[2]);
	}

	// This methods builds the windows on each side of the room. Pay attention because the way it works is
	// quite convoluted.
	void buildWindows(Rectangle room, int story = 0) {
		// We start by clearing our temporary dictionnary of points.
		points.Clear();

		// Then, we build windows for each side of the room
		foreach(Direction side in Enum.GetValues(typeof(Direction))) {
			// Since we iterate on all the values of the enum,
			// this is a possibility that should be taken into account.
			if (side == Direction.NONE) continue;

			// We compute the angle the wall is forming with the horizontal plane, which will be useful
			// for rotating windows so as to correctly match the wall.
			float angle = Util.computeAngle(new Vector2(room.topDispl, wallHeight), new Vector2(room.baseDispl, 0));
			if (angle != 0)angle += (float) (Math.PI/2);
			if (side == Direction.NORTH ||side == Direction.WEST) angle *= -1;
			Vector4 plane = computePlaneFromSide(room, side, story);

			// Then, we build each window we stored in the room
			foreach (Window window in room.getWindows(side)) {
				// By default, all generated windows are facing WEST
				Shape windowShape = ShapeProvider.getShape(window.shape);

				// WEST/SOUTH-oriented windows have their point in the correct counter-clockwise order.
				// EAST/NORTH-oriented windows should go in the reverse order, hence why the list is flipped.
				if (side == Direction.EAST || side == Direction.NORTH) {
					windowShape.points.Reverse();
				}

				// This is a dirty payload ...
				// Since the room is really a two-dimensional object, it did not have a height when
				// it was generated. This is used to give an ifnormation abou the windows's size.
				float size = window.position.y;

				// Then, the center of the window is projected on the wall at the correct location.
				// If it was not, the window would be floating in the air when a wall is sloped.
				Vector3 newCenter = new Vector3(window.position.x, 0, window.position.z);
				if (side == Direction.NORTH || side == Direction.SOUTH) {
					newCenter.y += (wallHeight*(windowShape.center.y+story)) - windowShape.gravityCenter.y;
					newCenter = Util.projectOnPlane(newCenter, plane, Axis.Z);
					newCenter.x += (windowShape.center.x*size) - windowShape.gravityCenter.x;
					newCenter.z += windowShape.winDepth*(((int)side)%2 == 0?1:-1);
				}
				else {
					newCenter.y += (wallHeight*(windowShape.center.y+story)) - windowShape.gravityCenter.y;
					newCenter = Util.projectOnPlane(newCenter, plane, Axis.X);
					newCenter.z += (windowShape.center.x*size) - windowShape.gravityCenter.x;
					newCenter.x += windowShape.winDepth*(((int)side)%2 == 0?1:-1);
				}

				// This method (defined below) actually builds the mesh geometry of a window
				// based on the parameter we just calculated.
				buildWindowAroundCenter(newCenter, windowShape, side, angle, plane);

				// So as to allow for reentrancy, EAST/NORTH-windows are once agin reversed.
				if (side == Direction.EAST || side == Direction.NORTH) {
					windowShape.points.Reverse();
				}
				
			}
		}
	}

	// This method applies a roation to all our points in the temporary dictionnary, to
	// allow for windows to be described once and work on every side of the house.
	void rotatePoints(Vector3 center, Axis axis, double angle) {
		Dictionary<string, Vector3> newPoints = new Dictionary<string, Vector3>();
		
		foreach(string key in points.Keys) {
			newPoints[key] = Util.rotatePoint(points[key], center, angle, axis);
		}
		
		points.Clear();
		foreach(string key in newPoints.Keys) points[key] = newPoints[key];
	}

	// This function makes a triangle appear, pointed to a certain point in space.
	// It was VERY useful when debugging the window-building process.
	void markPoint(Vector3 p, string id) {
		points.Add(id+1, p + new Vector3(0, 0, 0));
		points.Add(id+2, p + new Vector3(0.1f, 1, 0));
		points.Add(id+3, p + new Vector3(-0.1f, 1, 0));
		addTriangle(id+1, id+2, id+3, 2);
		addTriangle(id+2, id+1, id+3, 2);
	}

	// This method may not seem clear at first sight, but it actually makes sense after one or two more readings.
	// It is the method which actually builds the mesh geometry of a window.
	// The direction indicates where is the outside from the window's point of view
	void buildWindowAroundCenter(Vector3 windowPos, Shape shape, Direction side, float angle, Vector4 plane) {
		// If the window is actually empty (it does not contain enough points to form a single triangle),
		// we only build the associated props and exit this method early.
		if (shape.points.Count < 3) {
			foreach(Prop prop in shape.props) {
				buildProp(windowPos, prop, side);
			}
			return;
		}

		// Mandatory cleaning of temporary points
		points.Clear();

		// Those identifier strings will be used to generate all the points of our window.
		// Their name are self-explanatory : Inner/Outer rings are the contour and interior of the window's shape,
		// and Backward versions are the part of the window which is stuck back into the wall. 
		string inr = "InnerRing";
		string our = "OuterRing";
		string bor = "BackwardOuterRing";
		string bir = "BackwardInnerRing";

		float depth = shape.winDepth*(side == Direction.EAST || side == Direction.SOUTH?-1:1);
		// darkpos is used to compute the location of the center point of the window, which will help
		// creating triangles to form an opaque polygon representing the glass.
		float darkPos = 0.8f;
		Axis axis = (side == Direction.NORTH || side == Direction.SOUTH)?Axis.X:Axis.Z;

		// Compute the center of all the points of the shape (a.k.z. gravity center)
		Vector3 windowGravityCenter = new Vector3(0, 0, 0);
		for (int i = 0; i < shape.points.Count; ++i) {
			Vector3 point = new Vector3(windowPos.x, windowPos.y + shape.points[i].y, windowPos.z + shape.points[i].x);
			if (side == Direction.NORTH || side == Direction.SOUTH) {
				point = new Vector3(windowPos.x + shape.points[i].x, windowPos.y + shape.points[i].y, windowPos.z);
			}
			
			windowGravityCenter += point;
		}
		windowGravityCenter /= shape.points.Count;
		// We then add this point ot a list of points which will be used to place some lights
		// inside the windows, to lighten up the mood.
		mWindows.Add(windowGravityCenter);
		
		// Here, we generate all the points of the window according to the shape give by the window's type.
		for (int i = 0; i < shape.points.Count; ++i) {
			Vector3 point = new Vector3(windowPos.x, windowPos.y + shape.points[i].y, windowPos.z + shape.points[i].x);
			Vector3 outerPoint = Util.getPointByPercentage(point, windowGravityCenter, -0.2f);
			Vector3 backwardOuterPoint = new Vector3(outerPoint.x + depth, outerPoint.y, outerPoint.z);
			Vector3 backwardInnerPoint = new Vector3(point.x + depth*darkPos, point.y, point.z);
			
			if (side == Direction.NORTH || side == Direction.SOUTH) {
				point = new Vector3(windowPos.x + shape.points[i].x, windowPos.y + shape.points[i].y, windowPos.z);
				outerPoint = Util.getPointByPercentage(point, windowGravityCenter, -0.2f);
				backwardOuterPoint = new Vector3(outerPoint.x, outerPoint.y, outerPoint.z + depth);
				backwardInnerPoint = new Vector3(point.x, point.y, point.z + depth*darkPos);
			}
			
			points.Add(inr + i, point);
			points.Add(our + i, outerPoint);
			points.Add(bor + i, backwardOuterPoint);
			points.Add(bir + i, backwardInnerPoint);
		}
		
		// We then add the center point
		string cdk = "cdk";
		Vector3 centerDark = new Vector3(windowGravityCenter.x + depth*darkPos, windowGravityCenter.y, windowGravityCenter.z);
		if (side == Direction.NORTH || side == Direction.SOUTH) {
			centerDark = new Vector3(windowGravityCenter.x, windowGravityCenter.y, windowGravityCenter.z + depth*darkPos);
		}
		points.Add(cdk, centerDark);

		// This part of the method rotates the point to fit the (sloped) wall.
		Vector3 rotCen = new Vector3(windowGravityCenter.x+depth, windowGravityCenter.y, windowGravityCenter.z+depth);
		if (side == Direction.NORTH || side == Direction.SOUTH) {
			rotCen = new Vector3(windowGravityCenter.x, windowGravityCenter.y, windowGravityCenter.z+depth);
		}
		rotCen = Util.projectOnPlane(rotCen, plane, Axis.Y);
		
		rotatePoints(rotCen, axis, angle);

		// We then builds the faces from the points which have been created and rotated.
		for (int i = 0; i < shape.points.Count; ++i) {
			addQuadrilateral(our+i, our+(i+1)%shape.points.Count, inr+(i+1)%shape.points.Count, inr+i, 1);
			addQuadrilateral(bor+i, bor+(i+1)%shape.points.Count, our+(i+1)%shape.points.Count, our+i, 1);
			addQuadrilateral(inr+i, inr+(i+1)%shape.points.Count, bir+(i+1)%shape.points.Count, bir+i, 1);
			addTriangle(bir+(i+1)%shape.points.Count, bir+i, cdk, 4);
		}

		// Finally, we build all props associated with this window before exiting the method
		foreach(Prop prop in shape.props) {
			buildProp(windowPos, prop, side);
		}
		
	}
	
	void buildProp(Vector3 windowPos, Prop prop, Direction side) {
		points.Clear();
		Vector3 center = prop.center;
		
		if (side == Direction.WEST || side == Direction.EAST) {
			center = new Vector3(-prop.center.z, prop.center.y, prop.center.x);
		}
		if (side == Direction.WEST || side == Direction.SOUTH) {
			center = new Vector3(center.x, center.y, center.z);
		}
		
		Vector3 propGravityCenter = new Vector3(0, 0, 0);
		for (int i = 0; i < prop.points.Count; ++i) {
			Vector3 point = prop.points[i];
			
			if (side == Direction.WEST || side == Direction.EAST) {
				point = new Vector3(-point.z, point.y, point.x);
			}
			if (side == Direction.WEST || side == Direction.SOUTH) {
				point = new Vector3(-point.x, point.y, -point.z);
			}
			propGravityCenter+= point;
		}
		propGravityCenter /= prop.points.Count;
		
		for (int i = 0; i < prop.points.Count; ++i) {
			Vector3 point = prop.points[i];
			
			if (side == Direction.WEST || side == Direction.EAST) {
				point = new Vector3(-point.z, point.y, point.x);
			}
			if (side == Direction.WEST || side == Direction.SOUTH) {
				point = new Vector3(-point.x, point.y, -point.z);
			}
			
			
			point -= propGravityCenter;
			point += windowPos;
			point += center;
			
			points.Add(""+i, point);
		}

		int colour = (prop.type == PropType.STORE?2:1);
		for (int i = 0; i < prop.faces.Count; ++i) {
			List<int> face = prop.faces[i];
			if (face.Count == 3) addTriangle(""+face[0], ""+face[1], ""+face[2], colour);
			if (face.Count == 4) addQuadrilateral(""+face[0], ""+face[1], ""+face[2], ""+face[3], colour);
		}
	}

	// This method allows for an export of the house's model in OBJ format.
	// Useful if you ever want to use those models outside of Unity.
	void writeAsObj(string fileName) {
		string file = "";

		foreach(Vector3 vertex in vertices) {
			file += "v " + vertex.x + " " + vertex.y + " " + vertex.z + "\n";
		}

		file += "\n";

		file += "# This colour is used for the walls \n";
		file += "tc 0 0 0\n";
		file += "# This colour is used for the windows' border \n";
		file += "tc 0 0 0\n";
		file += "# This colour is used for the roof \n";
		file += "tc 0 0 0\n";
		file += "# This colour is used for ... something, I guess ? \n";
		file += "tc 0 0 0\n";
		file += "# This colour is used for the windows' interior \n";
		file += "tc 0 0 0\n";

		file += "\n";

		for(int i = 0; i < triangles.Count; i += 3) {
			file += "f " + triangles[i] + " " + triangles[i+1] + " " + triangles[i+2] + " " + colours[i/3] + "\n";
		}

		file += "\n";

		System.IO.File.WriteAllText(fileName, file);
	}

	// This is the high-level method that builds an entire house given its firsst layer.
	GameObject buildHouse(Story story) {
		// We clear the window centers list.
		mWindows.Clear();

		// This is the GameObejct that will ultimately be returned
		GameObject house = new GameObject();
		Story currStory = story;
		int storyLevel = 0;

		// We add the required component to make our house capable of rendering its mesh.
		house.AddComponent<MeshFilter> ();
		house.AddComponent<MeshRenderer> ();
		house.AddComponent<MeshCollider> ();
		DestroyImmediate(house.GetComponent<BoxCollider>());

		Mesh mesh = null;

		MeshFilter mf = house.GetComponent<MeshFilter>();
		mf.sharedMesh = new Mesh();
		Mesh meshCopy = Mesh.Instantiate(mf.sharedMesh) as Mesh;
		mf.mesh = meshCopy;
		mesh = meshCopy;

		// This is so you cannot go trhough the house.
		MeshCollider mC = house.GetComponent<MeshCollider>();
		mC.sharedMesh = mf.sharedMesh;

		// Clearing all the temporary list, because we are starting a fresh new building.
		points.Clear();
		vertices.Clear();
		triangles.Clear();
		UVValues.Clear();

		// While we still have a storey to build, we build and and go to the next one.
		while (currStory != null) {
			// We build every room insed the storey.
			// Note that because of the way rooms are generated, they cannot
			// interfere with each other.
			foreach (Rectangle room in currStory.getRooms()) {
				buildRoom(room, storyLevel);
			}
			currStory = currStory.getNext();
			++storyLevel;
		}

		// While dangling points not assigned a colour should not exist anymore,
		// we still check for it in case the algorithm failed somewhere.
		while (vertices.Count > UVValues.Count) {
			UVValues.Add(new Vector2(0, 0));
		}

		// Here, we recompute points according to the anchor point determined
		// by the floors of each room.
		List<Vector3> realVertices = new List<Vector3>();

		anchorPoint.y = 0;
		anchorPoint /= anchorCount;
		anchorPoint.x += dX;
		anchorPoint.y += dY;
		anchorPoint.z += dZ;

		for (int i = 0; i < vertices.Count; ++i) {
			realVertices.Add(vertices[i] - anchorPoint);
		}

		// Then, we decide which windows get to be lighted up.
		int lCount = 0;
		for (int i = 0; i < mWindows.Count; ++i) {
			if (Util.rand.Next(0, 9) > 7.9f && totalLights > 0 && lCount < 3) {
				--totalLights;
				++lCount;
				Vector3 lPosition = mWindows[i] - anchorPoint;
				GameObject lightGameObject = (GameObject) Instantiate(lightWindow, 
				                                 					  lPosition, 
				                                 					  lightWindow.transform.rotation);
				lightGameObject.transform.parent = house.transform;
			}
		}

		// We then assign all points, triangles and colours to the Unity mesh,
		// and let it recalculate the normals of the model. Since we specified
		// our points in the correct order, it will represent our model faithfully.
		mesh.vertices = realVertices.ToArray ();
		mesh.triangles = triangles.ToArray ();
		mesh.uv = UVValues.ToArray ();
		mesh.Optimize ();
		mesh.RecalculateNormals ();

		// We thena assign a particular material to the house, which correspond to
		// its palette colour.
		switch(textureToUse) {
			case PaletteKind.redBeige:
				house.gameObject.renderer.material = baseMaterial;
			break;
			case PaletteKind.purpleLightBrown:
				house.gameObject.renderer.material = baseMaterial2;
			break;
			case PaletteKind.blueWhite:
				house.gameObject.renderer.material = baseMaterial3;
			break;
			case PaletteKind.redBrown:
				house.gameObject.renderer.material = baseMaterial4;
			break;
			case PaletteKind.grayGray:
				house.gameObject.renderer.material = baseMaterial5;
			break;
		}

		// This is yet another dirty payload, passing some informations in the name of the GameObject.
		house.name = anchorPoint.x + " " + anchorPoint.z + " ";
		house.name += "Building #" + buildNum++;

		// Uncomment this line if you ant to export the house as an OBJ file
		//writeAsObj("example.obj");

		return house;
	}
}
