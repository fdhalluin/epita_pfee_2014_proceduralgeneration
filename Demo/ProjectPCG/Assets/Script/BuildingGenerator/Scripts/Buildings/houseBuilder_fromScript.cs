using UnityEngine;
using System.Collections.Generic;
using HouseGen;
using System;

// WARNING: this class is partially defined in another file, houseBuilder.cs
// The method in the current files are used to generate a representation of a building
// as a sequence of Story objects by parsing a script.
public partial class houseBuilder {

	public int count = 0;
	// Links to the three actually implemented props for buildings.
	// This is a dirty hack ; the dictionnary should have been used for that purpose,
	// to load assets at runtime when needed.
	public GameObject tree_1;
	public GameObject bush_1;
	public GameObject table_1;

	// An actually unused dictionnary of all prefabs that can be palced around a building.
	public Dictionary<string, string> propPrefabs;

	// Thuis method is useless as it was replaced by hardcoding the instatiation of prefabs.
	public void initPropPrefabs() {
		propPrefabs = new Dictionary<string, string>();
		propPrefabs["GODUS_TREE"] = "../Resources/Tree_1.prefab";
		propPrefabs["GODUS_BUSH"] = "../Resources/Bush_1.prefab";
	}

	// This method parses the displacement values of a room and returns them as an array.
	List<float> getDisplacementList(string displ) {
		List<float> ans = new List<float>();
		string[] displacements = displ.Split(" ".ToCharArray());
		
		for (int i = 1; i < displacements.Length; ++i) {
			if (displacements[i].Length <= 0) continue;
			ans.Add((float)Double.Parse(""+displacements[i]));
		}
		
		
		return ans;
	}

	// This method parses a line adding pillars to the side of a house, and adds them to the room.
	void processPillars(RoomDescription roomDesc, Rectangle room) {
		for (int i = 0; i < roomDesc.commandsCount[(int)Parser.CommandType.PILLARS]; ++i) {
			string[] pilTokens = roomDesc.properties[Parser.CommandType.PILLARS + "_" + i].Split(' ');
			Direction side = (Direction)Enum.Parse(typeof(Direction), pilTokens[1]);
			int dist = Int32.Parse(pilTokens[2]);
			int startHeight = Int32.Parse(pilTokens[3]);
			int height = Int32.Parse(pilTokens[4]);
			int spacing = Int32.Parse(pilTokens[5]);
			Rectangle.PillarType shape = (Rectangle.PillarType)Enum.Parse(typeof(Rectangle.PillarType), pilTokens[6]);

			room.addPillars(side, dist, spacing, startHeight, height);		
		}
	}

	// This method creates a room from a room description and adds it to the given storey
	Rectangle addRoomFromRoomDescription(string buildStatement, RoomDescription roomDesc, Dictionary<string, Rectangle> rooms, Story floor) {
		string[] buildTokens = buildStatement.Split(' ');
		int xOffset = Int32.Parse(buildTokens[2]);
		int yOffset = Int32.Parse(buildTokens[3]);
		int roomWidth = Int32.Parse(roomDesc.properties["size"].Split(' ')[1]);
		int roomLength = Int32.Parse(roomDesc.properties["size"].Split(' ')[2]);
		int roomHeight = Int32.Parse(roomDesc.properties["height"].Split(' ')[1])-1;
		float roofOffset = Single.Parse(roomDesc.properties["roof"].Split(' ')[1]);
		Rectangle.RoofType roofType = (Rectangle.RoofType)Enum.Parse(typeof(Rectangle.RoofType), roomDesc.properties["roof"].Split(' ')[2]);		
		Rectangle room = new Rectangle(xOffset, yOffset, roomWidth, roomLength, roomHeight);
		room.setDisplacements(getDisplacementList(roomDesc.properties["displacement"]));
		room.roofOffset = roofOffset;
		room.roofType = roofType;
		if (roomDesc.properties["roof"].Split(' ').Length >= 4) {
			room.extendedRoof = (float) Double.Parse(roomDesc.properties["roof"].Split(' ')[3]);
		}
		if (roomDesc.properties["roof"].Split(' ').Length >= 5) {
			room.roofDepth = (float) Double.Parse(roomDesc.properties["roof"].Split(' ')[4]);
		}
		if (roomDesc.properties["roof"].Split(' ').Length >= 6) {
			room.roofDirection = (Direction)Enum.Parse(typeof(Direction), roomDesc.properties["roof"].Split(' ')[5]);
		}

		rooms[roomDesc.name] = room;
		roomDesc.roomsBuffer.Add(room);
		
		Rectangle result = floor.addRoom(room);

		if (result == null) return null;

		processPillars(roomDesc, result);
		return result;	
	}

	// This method is used to recursively grow the rooms up to their specified height,
	// until all rooms of all storeys have been fully constructed.
	void growRooms(Story floor) {
		bool stopGrowing = false;
		Story currFloor = floor;
		while (!stopGrowing) {
			stopGrowing = true;
			if (currFloor.getRooms().Count > 0) {
				if (currFloor.getNext() == null) {
						currFloor = currFloor.generateNextStory("");
					}
					else {
						currFloor = currFloor.getNext();
						currFloor.growRoomsFromPrevious();
					}
					stopGrowing = false;
			}
		}
	}

	// This method flushes all instances rooms instatiated from a single description
	// to the list of finished rooms.
	void flushRoomsBuffer(Dictionary<string, RoomDescription> building) {
		foreach (RoomDescription roomDesc in building.Values) {
			roomDesc.unloadRoomBuffer();
		}
	}

	// This is the main method that takes a script's content as a parameter, and returns the generated
	// house as a GameObject.
	public GameObject initFromScript(string scriptText) {

		Parser p = new Parser();
		// The parser returns us a list of room descriptions, which act as templates for our building
		Dictionary<string, RoomDescription> building = p.parse(scriptText);
		RoomDescription toBuild = building[Parser.roomsToBuild];
		Dictionary<string, Rectangle> rooms = new Dictionary<string, Rectangle>();
		Dictionary<string, RoomDescription> addToBuilding = new Dictionary<string, RoomDescription>();
		anchorPoint = new Vector3(0, 0, 0);
		anchorCount = 0;


		Story floor = new Story(200, 200, "Floor #0");
		
		// First, lay out each room on the first floor
		// These are the room rooted to the ground. All other rooms are on top of those ones.
		foreach (string roomNum in toBuild.properties.Keys) {
			string[] buildTokens = toBuild.properties[roomNum].Split(' ');
			RoomDescription roomDesc = building[buildTokens[1]];
			addRoomFromRoomDescription(toBuild.properties[roomNum], roomDesc, rooms, floor);		
		}
		flushRoomsBuffer(building);

		// Then, while we continue to add new rooms to the building, keep the following loop running
		// to check those new rooms for adjacent ones.
		bool allRoomsAdded = false;
		
		while (!allRoomsAdded) {
			allRoomsAdded = true;

			// We iterate on all room templates
			foreach (string roomName in building.Keys) {
				RoomDescription roomDesc = building[roomName];		

				// For each room instatiated from this template
				foreach(Rectangle room in roomDesc.rooms) {
					// If this rooms already has rooms on its side,
					// it means we already processed it and should not repeat the process
					if (room.getRoomsOnSidesCount() + room.getTowerList().Count > 0) {
						continue;
					}
					// We iterate on all the commands given for the current room's template
					// that result in the addition of a room on one of its side
					for (int i = 0; i < roomDesc.commandsCount[(int)Parser.CommandType.ROOM]; ++i) {
						string[] addTokens = roomDesc.properties[Parser.CommandType.ROOM + "_" + i].Split(' ');

						// This is either for the classical addition of the room,, or the probabilistic one
						if (addTokens[0].Equals("addRoom") || (addTokens[0].Equals("@ddRoom") && Util.rand.Next(1, 10) <= 5)) {
							Direction side = (Direction) Enum.Parse(typeof(Direction), addTokens[1]);
							RoomDescription newRoomDesc = building[addTokens[2]];
							Rectangle.PositionType positionType = (Rectangle.PositionType) Enum.Parse(typeof(Rectangle.PositionType),addTokens[3]);
							int para = Int32.Parse(newRoomDesc.properties["size"].Split(' ')[1]);
							int perp = Int32.Parse(newRoomDesc.properties["size"].Split(' ')[2]);
							int roomHeight = Int32.Parse(newRoomDesc.properties["height"].Split(' ')[1])-1;
							float roofOffset = Single.Parse(newRoomDesc.properties["roof"].Split(' ')[1]);
							Rectangle.RoofType roofType = (Rectangle.RoofType)Enum.Parse(typeof(Rectangle.RoofType), newRoomDesc.properties["roof"].Split(' ')[2]);

							// If the side is either WEST or EAST, reverse the perpendicular and parallel size
							if ((int)side >= (int)Direction.WEST) {
								int temp = para;
								para = perp;
								perp = temp;
							}	

							// We then try to add a room of the specified size to our floor
							// Setting the boolean doNotAdd to true will ensure that we do not
							// actually modify the floor's plan by actually adding the room,
							// as the resulting room may be smaller than the required size
							Rectangle potentialNewRoom = room.addRectangleToSide(side, para, perp, perp, positionType, true);

							// Thus, we do not actually add the new room if it does not respect the size requirement
							if ( (potentialNewRoom.w == para && potentialNewRoom.h == perp)
								|| (potentialNewRoom.w == perp && potentialNewRoom.h == para) ) {
								room.addGeneratedRoomToSide(side, potentialNewRoom);
								addToBuilding[newRoomDesc.name] = newRoomDesc;
								potentialNewRoom.roomHeight = roomHeight;
								potentialNewRoom.roofOffset = roofOffset;
								potentialNewRoom.roofType = roofType;
								newRoomDesc.rooms.Add(potentialNewRoom);
								potentialNewRoom.setDisplacements(getDisplacementList(newRoomDesc.properties["displacement"]));

								// If it does, however, we have to handle its roof properties here
								if (newRoomDesc.properties["roof"].Split(' ').Length >= 4) {
									potentialNewRoom.extendedRoof = (float) Double.Parse(newRoomDesc.properties["roof"].Split(' ')[3]);
								}
								if (newRoomDesc.properties["roof"].Split(' ').Length >= 5) {
									potentialNewRoom.roofDepth = (float) Double.Parse(newRoomDesc.properties["roof"].Split(' ')[4]);
								}
								if (newRoomDesc.properties["roof"].Split(' ').Length >= 6) {
									potentialNewRoom.roofDirection = (Direction)Enum.Parse(typeof(Direction), newRoomDesc.properties["roof"].Split(' ')[5]);
								}

								// And we also account for every room that may potentially be added to its side/top
								allRoomsAdded = false;
							}
							
						}

						// Towerzs can be added to the corners of a room.
						// Although this feature is not used by the generator, it can
						// still be used when manually writing a script
						if (addTokens[0].Equals("addTower")) {
							Direction vertical = (Direction) Enum.Parse(typeof(Direction), addTokens[1]);
							Direction horizontal = (Direction) Enum.Parse(typeof(Direction), addTokens[2]);
							int radius = Int32.Parse(addTokens[3]);
							int height = Int32.Parse(addTokens[4]) - 1;
							
							room.addTower(vertical, horizontal, radius, height);
							// Once again, we also account for every room that may potentially be added to its side/top
							allRoomsAdded = false;
						}
					}
				}
			}
			// We then transfer all the recently-added rooms to the actual rooms list,
			// clearing the temporary buffer.
			foreach (string key in addToBuilding.Keys) {
				building[key] = addToBuilding[key];
			}
			addToBuilding.Clear();
			
			// Following that, we grow all rooms to their actual height
			growRooms(floor);

			// And add the "onTop" rooms over them
			int indCount = 0;
			foreach (string roomName in building.Keys) {
				RoomDescription roomDesc = building[roomName];
				
				for (int i = 0; i < roomDesc.commandsCount[(int)Parser.CommandType.TOP]; ++i) {
					string[] topTokens = roomDesc.properties[Parser.CommandType.TOP + "_" + i].Split(' ');
					RoomDescription newRoomDesc = building[topTokens[1]];
					int xOffset = Int32.Parse(topTokens[2]);
					int yOffset = Int32.Parse(topTokens[3]);
					
					foreach (Rectangle room in roomDesc.rooms) {
					
						if (room.getOnTop().Count > i) continue;
					
						Story targetStory = room.floor.getStoryAtHeight(room.roomHeight+1);
						
						Rectangle newRoom = addRoomFromRoomDescription("build " + newRoomDesc.name + " " + (room.x + xOffset) + " " + (room.y + yOffset), newRoomDesc, rooms, targetStory);				
					
						if (newRoom == null) {
							// This should not happen under normal circumstances/uses of the generator.
							Debug.LogError("A room could not be placed on top of another because of a lack of space");
						}
						else {
							addToBuilding[newRoomDesc.name + "_" + (++indCount)] = newRoomDesc;
							room.getOnTop().Add(newRoom);
							// Once again, we also account for every room that may potentially be added to its side/top
							allRoomsAdded = false;
						}
					}
				
				}
				
			}
			foreach (string key in addToBuilding.Keys) {
				building[key] = addToBuilding[key];
			}
			// We then transfer all the recently-added rooms to the actual rooms list,
			// clearing the temporary buffer.
			addToBuilding.Clear();
			flushRoomsBuffer(building);
			
			// Then, we once again grow all rooms to their actual height
			growRooms(floor);		
		}		
		
		// Finally, we add windows when specified
		foreach (string roomName in building.Keys) {
			RoomDescription roomDesc = building[roomName];

			// This loop handles the commands adding groups of windows
			for (int i = 0; i < roomDesc.commandsCount[(int)Parser.CommandType.WINDOWS]; ++i) {
				string[] winTokens = roomDesc.properties[Parser.CommandType.WINDOWS + "_" + i].Split(' ');
				Direction side = (Direction)Enum.Parse(typeof(Direction), winTokens[1]);
				int storey = Int32.Parse(winTokens[2]);
				int windowSize = Int32.Parse(winTokens[3]);
				int spacing = Int32.Parse(winTokens[4]);
				WindowShape shape = (WindowShape)Enum.Parse(typeof(WindowShape), winTokens[5]);
				
				foreach (Rectangle room in roomDesc.rooms) {
					Rectangle roomAtStory = room.getRoomAtStory(storey);
					if (roomAtStory != null) {
						roomAtStory.addWindows(side, windowSize, spacing, shape);
					}
				}
				
			}

			// This loop handles the commands adding single windows in specific places
			for (int i = 0; i < roomDesc.commandsCount[(int)Parser.CommandType.SINGLE_WINDOW]; ++i) {
				string[] winTokens = roomDesc.properties[Parser.CommandType.SINGLE_WINDOW + "_" + i].Split(' ');
				Direction side = (Direction)Enum.Parse(typeof(Direction), winTokens[1]);
				int storey = Int32.Parse(winTokens[2]);
				float offset = (float) Double.Parse(winTokens[3]);
				WindowShape shape = (WindowShape)Enum.Parse(typeof(WindowShape), winTokens[4]);
				
				foreach (Rectangle room in roomDesc.rooms) {
					room.getRoomAtStory(storey).addSingleWindow(side, offset, shape);					
				}
			}
		}
		
		// We can specify a certain fofset for the generated model with the moveCenter command
		dX = 0;
		dY = 0;
		dZ = 0;
		if (building[Parser.globalRoom].properties.ContainsKey("moveCenter")) {
			dX = (float) Double.Parse(building[Parser.globalRoom].properties["moveCenter"].Split(' ')[1]);
			dY = (float) Double.Parse(building[Parser.globalRoom].properties["moveCenter"].Split(' ')[2]);
			dZ = (float) Double.Parse(building[Parser.globalRoom].properties["moveCenter"].Split(' ')[3]);

		}

		// A simple call to the buildHouse method (defined in the houseBuilder.cs source file)
		// will generate the mesh of our building from the representation we just parsed and built.
		GameObject generatedBuilding = buildHouse(floor);

		int ite = 0;
		// We get teh dirty paylioad stored in the name of the returned object to correctly place
		// props around the building.
		float anchorPointX = Single.Parse (generatedBuilding.name.Split (' ')[0]);
		float anchorPointZ = Single.Parse (generatedBuilding.name.Split (' ')[1]);

		// This loop handles the instatiation and placement of props around and on top of the generated building
		// Note that each of those props is a separate children GameObject, which make badly influence performances.
		while(building[Parser.globalRoom].properties.ContainsKey("prop_"+ite)) {
			string propLine = building[Parser.globalRoom].properties["prop_"+ite];
			++ite;
			string[] propTokens = propLine.Split(' ');

			// Props placemetn can be probabilistic, too.
			if (propTokens[0][0] == '@' && Util.rand.Next (0, 10) < 5) {
				continue;
			}

			float x = Single.Parse(propTokens[1]);
			float z = Single.Parse(propTokens[2]);
			string propType = propTokens[3];

			int gridX = (int) x;
			int gridZ = (int) z;
			float y = 0;
			Story currFloor = floor;

			// If the prop is to be placed on top of a room, we need to get the correct storey
			// to test for the required space.
			while (currFloor != null) {
				if (currFloor.getRoomPlan()[gridX, gridZ] != 0) {
					y += wallHeight;
					currFloor = currFloor.getNext();
				}
				else {
					break;
				}
			}

			// Props should be loaded from the props dictionnary.
			// this is a dirty hack used because of a lack of time.
			GameObject prop = null;
			if (propType.Equals("GODUS_TREE")) {
				prop = (GameObject) Instantiate(tree_1);
			}
			if (propType.Equals("GODUS_BUSH")) {
				prop = (GameObject) Instantiate(bush_1);
			}
			if (propType.Equals ("TABLE")) {
				prop = (GameObject) Instantiate(table_1);
			}
			if (prop == null) {
				Debug.LogError ("Could not load " + propPrefabs[propType]);
				continue;
			}
			// Placement of the prop is done relatively to the anchorPoint received
			// as a dirty payload.
			prop.transform.position = new Vector3(x - anchorPointX, y, z - anchorPointZ);
			prop.transform.parent = generatedBuilding.transform;
		}

		++count;

		// We can also specify a rotation angle for the building inside the script, to allow for easy rotation
		// of previously built models.
		float angleRotation = (float) Double.Parse(building[Parser.globalRoom].properties["rotation"].Split(' ')[1]);

		generatedBuilding.transform.localRotation = generatedBuilding.transform.localRotation * Quaternion.Euler(new Vector3(0, angleRotation, 0));

		return generatedBuilding;
			
	}
	
}
