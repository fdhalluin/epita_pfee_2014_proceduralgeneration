﻿using System;
using System.Collections.Generic;
using System.Text;

public class PerlinNoise {
    static Random mRandom = new Random();
	
	public static float[][] GetPerlinNoise(float[][] iBaseNoise, int iOctaveCount) {
		int lWidth = iBaseNoise.Length;
		int lHeight = iBaseNoise[0].Length;
		float[][][] lSmoothNoise = new float[iOctaveCount][][];
		float lPersistance = 0.45f;
		
		for (int i = 0; i < iOctaveCount; i++) {
			lSmoothNoise[i] = SmoothNoise(iBaseNoise, i);
		}
		
		float[][] lPerlinNoise = GetEmptyArray<float>(lWidth, lHeight);
		
		float lAmplitude = 1.00f;
		float lTotalAmplitude = 0.0f;
		
		for (int lOctave = iOctaveCount - 1; lOctave >= 0; lOctave--) {
			lAmplitude *= lPersistance;
			lTotalAmplitude += lAmplitude;
			
			for (int i = 0; i < lWidth; i++) {
				for (int j = 0; j < lHeight; j++) {
					lPerlinNoise[i][j] += lSmoothNoise[lOctave][i][j] * lAmplitude;;
				}
			}
		}
		
		return lPerlinNoise;
	}
	
	public static float[][] GetPerlinNoise(int iWidth, int iHeight, int iOctaveCount) {
		float[][] lBaseNoise = WhiteNoise(iWidth, iHeight);
		return GetPerlinNoise(lBaseNoise, iOctaveCount);
	}

	private static T[][] GetEmptyArray<T>(int iWidth, int iHeight) {
     	T[][] lImage = new T[iWidth][];

        for (int i = 0; i < iWidth; i++) {
  		     lImage[i] = new T[iHeight];
        }

        return lImage;
    }

	private static float[][] WhiteNoise(int iWidth, int iHeight) {            
        float[][] lNoise = GetEmptyArray<float>(iWidth, iHeight);

		for (int i = 0; i < iWidth; i++) {
			for (int j = 0; j < iHeight; j++) {
                lNoise[i][j] = (float) mRandom.NextDouble() % 1;
            }
		}
        return lNoise;
    }

	private static float[][] SmoothNoise(float[][] iBaseNoise, int iOctave) {
		int lWidth = iBaseNoise.Length;
        int lHeight = iBaseNoise[0].Length;
        float[][] lSmoothNoise = GetEmptyArray<float>(lWidth, lHeight);
        int lSamplePeriod = 1 << iOctave;
        float lSampleFrequency = 1.0f / lSamplePeriod;

        for (int i = 0; i < lWidth; i++) {
            /* Horizontal sampling indices */
			int lSample_i0 = (i / lSamplePeriod) * lSamplePeriod;
            int lSample_i1 = (lSample_i0 + lSamplePeriod) % lWidth;
            float lHorizontal_blend = (i - lSample_i0) * lSampleFrequency;

            for (int j = 0; j < lHeight; j++) {
                /* Vertical sampling indices */
				int lSample_j0 = (j / lSamplePeriod) * lSamplePeriod;
                int lSample_j1 = (lSample_j0 + lSamplePeriod) % lHeight;
                float lVertical_blend = (j - lSample_j0) * lSampleFrequency;

                /* Mix the top two corners */
				float lTop = Interpolate(iBaseNoise[lSample_i0][lSample_j0],
                iBaseNoise[lSample_i1][lSample_j0], lHorizontal_blend);

                /* Mix the bottom two corners */
                float lBottom = Interpolate(iBaseNoise[lSample_i0][lSample_j1],
                iBaseNoise[lSample_i1][lSample_j1], lHorizontal_blend);

                /* End mix */
                lSmoothNoise[i][j] = Interpolate(lTop, lBottom, lVertical_blend);                    
             }
        }
		return lSmoothNoise;
	}

	private static float Interpolate(float iX0, float iX1, float iAlpha) {
		return iX0 * (1 - iAlpha) + iAlpha * iX1;
	}
  }