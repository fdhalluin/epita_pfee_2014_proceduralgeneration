using UnityEngine;
using System.Collections;

public enum EdgeKind {
	minor,
	medium,
	external,
	major
}

public class CityEdge {
	public CityNode mFirstNode { get; internal set; }
	public CityNode mSecondNode { get; internal set; }
	public Vector3 mPosition { get; internal set; }
	public EdgeKind mKind { get; internal set; }
	public bool mThereIsHouse { get; internal set; }
	
	public CityEdge(CityNode iFirstNode, CityNode iSecondNode) {
		mFirstNode = iFirstNode;
		mSecondNode = iSecondNode;
		mPosition = (mFirstNode.mPosition + mSecondNode.mPosition) / 2;
		mKind = EdgeKind.medium;
		mThereIsHouse = true;
	}
}