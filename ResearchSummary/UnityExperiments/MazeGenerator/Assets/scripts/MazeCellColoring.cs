﻿using UnityEngine;
using System.Collections;

public class MazeCellColoring : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision other) {
		if (transform.localScale.y > 1) {
			// Walls are higher than the rest
		}
		else {
			if (renderer.material.color == Color.blue) {
				rigidbody.isKinematic = false;
				rigidbody.useGravity = true;
			}
			else {
				renderer.material.color = Color.cyan;
			}
		}

	}
}
