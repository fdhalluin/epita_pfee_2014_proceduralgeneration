using UnityEngine;
using System.Collections;
using HouseGen;

/// MouseLook rotates the transform based on the mouse delta.
/// Minimum and Maximum values can be used to constrain the possible rotation

/// To make an FPS style character:
/// - Create a capsule.
/// - Add the MouseLook script to the capsule.
///   -> Set the mouse look to use LookX. (You want to only turn character but not tilt it)
/// - Add FPSInputController script to the capsule
///   -> A CharacterMotor and a CharacterController component will be automatically added.

/// - Create a camera. Make the camera a child of the capsule. Reset it's transform.
/// - Add a MouseLook script to the camera.
///   -> Set the mouse look to use LookY. (You want the camera to tilt up and down like a head. The character already turns.)
[AddComponentMenu("Camera-Control/Mouse Look")]
public class MouseLook : MonoBehaviour {

	public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	public RotationAxes axes = RotationAxes.MouseXAndY;
	public float sensitivityX = 15F;
	public float sensitivityY = 15F;

	public float minimumX = -360F;
	public float maximumX = 360F;

	public float minimumY = -60F;
	public float maximumY = 60F;

	float rotationY = 0F;

	bool activated = true;

	string script = "";
	string scriptName = "";

	Vector2 scrollPosition = new Vector2(0, 0);

	public GameObject builder;


	void Update ()
	{
		if (Input.GetKeyDown("escape")) activated = !activated;

		if (!activated) return;

		if (axes == RotationAxes.MouseXAndY)
		{
			float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;
			
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
		}
		else if (axes == RotationAxes.MouseX)
		{
			transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
		}
		else
		{
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);
			
			transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
		}
	}
	
	void Start ()
	{
		// Make the rigid body not change rotation
		if (rigidbody)
			rigidbody.freezeRotation = true;

		foreach (GameObject lObject in GameObject.FindObjectsOfType(typeof(GameObject))) {
			if (lObject.name == "Fontain(Clone)") {
				this.gameObject.transform.position = lObject.transform.position;
				this.gameObject.transform.position += new Vector3(0, 1.5f, 7);
				this.gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
			} else {
				if (lObject.GetComponent<BoxCollider>() != null) {
					Destroy(lObject.GetComponent<BoxCollider>());
				}
			}
		}

		scriptName = "simpleHouse";
		//script = System.IO.File.ReadAllText(folder + scriptName + ".txt");
	}

	void OnGUI() {
		if (activated) return;

		GUI.Box(new Rect(10, 10, Screen.width - 20, Screen.height - 20), "");
		GUI.Label(new Rect(20, 18, 300, 20), "Script name: ");
		scriptName = GUI.TextField(new Rect(100, 20, 100, 20), scriptName);
		GUILayout.BeginArea(new Rect(20, 50, Screen.width - 40, Screen.height - 70));
		scrollPosition = GUILayout.BeginScrollView(scrollPosition);
		script = GUILayout.TextArea(script);
		GUILayout.EndScrollView();
		GUILayout.EndArea();

		if (GUI.Button(new Rect(205, 19, 50, 20), "Load")) {
			//script = System.IO.File.ReadAllText(folder + scriptName + ".txt");
		}
		if (GUI.Button(new Rect(260, 19, 50, 20), "Save")) {
			//System.IO.File.WriteAllText(folder + scriptName + ".txt", script);
		}
		if (GUI.Button(new Rect(315, 19, 68, 20), "Compile")) {
			((houseBuilder)builder.GetComponent(typeof(houseBuilder))).initFromScript(script);
		}

	}
}