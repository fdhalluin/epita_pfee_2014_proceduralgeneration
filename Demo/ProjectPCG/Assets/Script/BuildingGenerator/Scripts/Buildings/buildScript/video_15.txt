
Room_10:
size ?(5, 8) ?(4, 7)
height 4
build Room_10 50 50
addRoom NORTH Door_11 RANDOM
onTop Room_12 0 0
displacement 0 -0.2 -0.2 -0.2 0.2 
windows NORTH 0 2 2 SQUARE
windows SOUTH 0 3 3 SQUARE
windows WEST 0 3 1 SQUARE
windows EAST 0 1 2 SQUARE
windows NORTH 1 2 2 SQUARE
windows SOUTH 1 2 3 SQUARE
windows WEST 1 1 3 SQUARE
windows EAST 1 2 2 SQUARE
windows NORTH 2 1 3 SQUARE
windows SOUTH 2 3 3 SQUARE
windows WEST 2 1 1 SQUARE
windows EAST 2 2 3 SQUARE
windows NORTH 3 2 3 SQUARE
windows SOUTH 3 3 3 SQUARE
windows WEST 3 3 2 BALCONEY
windows EAST 3 2 3 SQUARE
roof 0 SLOPE 0.9 0.3

Room_12:
size ?(2, 5) ?(2, 5)
height 1
displacement 0 0.4 
windows NORTH 0 2 2 SQUARE
windows SOUTH 0 3 1 SQUARE
windows WEST 0 3 3 SQUARE
windows EAST 0 2 1 SQUARE
roof 1 POINT 0.4





Door_11:
size ?(3, 6) ?(1, 4)
height 1
addWindow NORTH 0 1 DOOR
displacement 
roof 0 SLOPE 0.7 0.4 NORTH


