﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Perlin : MonoBehaviour {

	public class Point
	{
		public float height;
		public float time;
	}

	public List<Point> points = new List<Point>();
	public float scale = 0.01f;
	public float offset = 0f;
	public float timeScale = 1f;
	public float heightScale = 1f;

	// Use this for initialization
	void Start () {
	
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		float now = Time.time;
		for (int i = 0; i < points.Count - 1; ++i)
		{
			var prev = points[i];
			var next = points[i + 1];
			var prevPos = new Vector3((prev.time - now) * timeScale, prev.height * heightScale, 0);
			var nextPos = new Vector3((next.time - now) * timeScale, next.height * heightScale, 0);
			Gizmos.DrawLine(prevPos, nextPos);
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		points.Add(new Point()
	    {
			height = Mathf.PerlinNoise(Time.time * scale + offset, 0f),
			time = Time.time,
		});
	}
}
