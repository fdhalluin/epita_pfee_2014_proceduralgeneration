﻿using System;
using CoherentNoise;
using CoherentNoise.Generation;
using CoherentNoise.Generation.Displacement;
using CoherentNoise.Generation.Fractal;
using CoherentNoise.Generation.Modification;
using CoherentNoise.Generation.Patterns;
using CoherentNoise.Texturing;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;
using UnityEditor;


/*
 * Editor window for the terrain
 */

public class TerrainEditor : EditorWindow 
{
	private TerrainGenerator terrain;

	// Settings for the terrain
	private TerrainSettings terSet = TerrainSettings.Instance;
	private Generator m_Generator;
	private int intFieldLength = 1000;
	private int intFieldWidth = 1000;
	private bool discreteBool = false;
	private bool CenterBool = false;
	private int hSliderDisDiv = 40;
	private int hSliderDisMult = 40;

	private float hSliderWaterLevel = 0.0f;


	// Noise 1
	private int selectionGridInt = 0;
	private string[] selectionStrings = {"PinkNoise", "BillowNoise", "RidgeNoise"};

	private int hSliderOctave = 5;
	private float hSliderFreq = 0.46f;
	private float hSliderMult = 1f;
	private int intFieldSeed = 42;
	private float hSliderMod = 1f;

	private int toolbarInt = 0;
	private string[] toolbarStrings = {"Nothing", "Bias", "Gain"};
	
	// Blend 2 noises
	private bool BlendBool = false;

	// Noise 2
	private int selectionGridInt2 = 0;

	private int hSliderOctave2 = 5;
	private float hSliderFreq2 = 0.46f;
	private float hSliderMult2 = 1f;
	private int intFieldSeed2 = 42;
	private float hSliderMod2 = 1f;

	private int toolbarInt2 = 0;

	// Custom Noises
	private bool TerrainBool = true;
	private int selectionTerrainInt = 3;
	private string[] selectionTerrainStrings = {"Random1", "Random2", "Random3", "Terrain1", "Terrain2", "Saved Terrain"};
	private float hSliderMultTerrain = 0.5f;
	private float hSliderDilatTerrain = 1f;
	private bool SnowBool = false;
	private int intFieldSeedCust = 42;

	private Generator CustGenerator;


	//Tree
	private float hSliderDensTree = 1f;




	[MenuItem ("Town/Terrain Editor")]
	
	static void Init () {
		TerrainEditor window = (TerrainEditor) EditorWindow.GetWindow(typeof (TerrainEditor));
	}



	void OnGUI () {

		EditorGUILayout.Separator();
		TerrainBool = EditorGUILayout.Toggle ("Custom Terrain", TerrainBool);
		EditorGUILayout.Separator();
		discreteBool = EditorGUILayout.Toggle ("Discrete Terrain", discreteBool);
		CenterBool = EditorGUILayout.Toggle ("Center Terrain", CenterBool);
		EditorGUILayout.Separator();


		if (discreteBool)
		{
			hSliderDisDiv = EditorGUILayout.IntSlider ("Discrete Div:", hSliderDisDiv, hSliderDisMult, 300);
			hSliderDisMult = EditorGUILayout.IntSlider ("Discrete Mult:", hSliderDisMult, 1, hSliderDisDiv);
		}
		
		if (!TerrainBool)
		{
		//	GUI.Box (new Rect (10, 10, 550, 350), "Test Terrain");
		
			BlendBool = EditorGUILayout.Toggle ("Blend 2 noises", BlendBool);

			GUILayout.BeginHorizontal();
			GUILayout.BeginVertical();
			CreateGuiNoise1();
			GUILayout.EndVertical();

			if (BlendBool)
			{
				GUILayout.BeginVertical();
				CreateGUINoise2();
				GUILayout.EndVertical();
			}
			GUILayout.EndHorizontal();

			CreateGuiSize ();
			CreateGuiTrees ();
			CreateGuiWater ();

			if(GUILayout.Button("Generate"))
			{
				if (intFieldSeed < 0)
					intFieldSeed = 42;
				
				if (intFieldSeed2 < 0)
					intFieldSeed2 = 42;
				
				GenerateNoise1(intFieldSeed);
				
				if (BlendBool)
					GenerateNoise2(intFieldSeed2);

				GenerateTerrain();
			}
		}
		else
			GenerateCustomNoise();
		
	}

	private void CreateGuiWater()
	{
		EditorGUILayout.Separator();
		hSliderWaterLevel = EditorGUILayout.Slider ("Water Level:", hSliderWaterLevel, 0f, 1.0f);

		GUILayout.BeginHorizontal();
		if (GUILayout.Button ("Add Water")) 
		{
			terrain = FindObjectOfType<TerrainGenerator>();
			terrain.AddWater(hSliderWaterLevel);
		}
		
		if (GUILayout.Button ("Delete Water")) 
		{
			terrain = FindObjectOfType<TerrainGenerator>();
			terrain.ClearWater();
		}
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button ("Add Lake")) 
		{
			terrain = FindObjectOfType<TerrainGenerator>();
			terrain.AddLake();
		}
		
		if (GUILayout.Button ("Delete Lake")) 
		{
			terrain = FindObjectOfType<TerrainGenerator>();
			terrain.ClearLake();
		}
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		if (GUILayout.Button ("Add River")) 
		{
			terrain = FindObjectOfType<TerrainGenerator>();
			terrain.AddRiver(hSliderWaterLevel);
		}
		
		if (GUILayout.Button ("Delete River")) 
		{
			terrain = FindObjectOfType<TerrainGenerator>();
			terrain.ClearRiver();
		}
		GUILayout.EndHorizontal();
	}

	// GUI for trees
	private void CreateGuiTrees()
	{
		EditorGUILayout.Separator();
		hSliderDensTree = EditorGUILayout.Slider ("Density Trees:", hSliderDensTree, 0.0f, 1.0f);

		GUILayout.BeginHorizontal();
		if (GUILayout.Button ("Add Trees")) 
		{
			terrain = FindObjectOfType<TerrainGenerator>();
			terrain.AddTrees(hSliderDensTree, (int)Random.Range(0, 99999999));
		}

		if (GUILayout.Button ("Delete Trees")) 
		{
			terrain = FindObjectOfType<TerrainGenerator>();
			terrain.ClearTrees();
		}
		GUILayout.EndHorizontal();
	}

	/*
	 * Generate a terrain with seletected parameter
	 */
	private void GenerateTerrain()
	{
		if (intFieldWidth < 500)
			intFieldWidth = 500;
		
		if (intFieldWidth > 5000)
			intFieldWidth = 5000;

		if (intFieldLength < 500)
			intFieldLength = 500;

		if (intFieldLength > 5000)
			intFieldLength = 5000;

		terSet.setWidth (intFieldWidth);
		terSet.setLength (intFieldLength);
		terSet.setGenerator (m_Generator);
		terSet.setDiscreteTerrain (discreteBool);
		terSet.setDisMult(hSliderDisMult);
		terSet.setDisDiv(hSliderDisDiv);
		terSet.setWaterLevel (hSliderWaterLevel);
		terSet.setCenterTerrain (CenterBool);
		terSet.setDensTrees(hSliderDensTree);
		if (TerrainBool)
			terSet.setSnowTerrain (SnowBool);

		terrain = FindObjectOfType<TerrainGenerator>();
		terrain.CreateTerrain();		
	}

	/*
	 * GUI for the size of the terrain
	 */
	private void CreateGuiSize()
	{
		EditorGUILayout.Separator();
		intFieldWidth = EditorGUILayout.IntField(
		                               "Terrain Width:", 
		                               intFieldWidth);
		intFieldLength = EditorGUILayout.IntField(
		                                     "Terrain Length:", 
		                                     intFieldLength);

		if (intFieldWidth < 500)
			intFieldWidth = 500;
		
		if (intFieldWidth > 5000)
			intFieldWidth = 5000;

		if (intFieldLength < 500)
			intFieldLength = 500;
		
		if (intFieldLength > 5000)
			intFieldLength = 5000;
	}

	/*
	 * GUI for noise1
	 */
	private void CreateGuiNoise1()
	{
		EditorGUILayout.Separator();
		selectionGridInt = GUILayout.SelectionGrid (selectionGridInt, selectionStrings, 3);

		EditorGUILayout.Separator();
		hSliderOctave = EditorGUILayout.IntSlider ("Octave:", hSliderOctave, 1, 10);
	//	hSliderOctave = GUI.HorizontalSlider (new Rect (25, 120, 100, 30), hSliderOctave, 0.0f, 10.0f);
		if (selectionGridInt != 2)
			hSliderFreq = EditorGUILayout.Slider ("Persistence:", hSliderFreq, 0.0f, 1.0f);
		hSliderMult = EditorGUILayout.Slider ("Multiplication:", hSliderMult, 0.0f, 1.0f);

		EditorGUILayout.Separator();
		toolbarInt = GUILayout.Toolbar (toolbarInt, toolbarStrings);
		hSliderMod = EditorGUILayout.Slider ("Modification:", hSliderMod, 0f, 1.0f);

		EditorGUILayout.Separator();
		intFieldSeed = EditorGUILayout.IntField ( "Seed:", intFieldSeed);

		if (intFieldSeed <= 0)
			intFieldSeed = 0;

	}


	/*
	 *  GUI for noise2 if blend 2 noises is selected
	 */
	private void CreateGUINoise2()
	{
		EditorGUILayout.Separator();
		selectionGridInt2 = GUILayout.SelectionGrid (selectionGridInt2, selectionStrings, 3);

		EditorGUILayout.Separator();
		hSliderOctave2 = EditorGUILayout.IntSlider ("Octave: ", hSliderOctave2, 1, 10);
		if (selectionGridInt2 != 2)
			hSliderFreq2 = EditorGUILayout.Slider ("Persistence: ", hSliderFreq2, 0.0f, 1.0f);
		hSliderMult2 = EditorGUILayout.Slider ("Multiplication: ", hSliderMult2, 0.0f, 1.0f);

		EditorGUILayout.Separator();
		toolbarInt2 = GUILayout.Toolbar (toolbarInt2, toolbarStrings);
		hSliderMod2 = EditorGUILayout.Slider ( "Modification:", hSliderMod2, 0f, 1.0f);

		EditorGUILayout.Separator();
		intFieldSeed2 = EditorGUILayout.IntField ("Seed:", intFieldSeed2);

		if (intFieldSeed2 <= 0)
			intFieldSeed2 = 0;
	}

	/*
	 * Create Generator for noise1 with selected parameters
	 */
	private void GenerateNoise1(int seed1)
	{
		Generator generator1;
		
		if (selectionGridInt == 0)
		{
			var noise1 = new PinkNoise(seed1)
			{
				OctaveCount = hSliderOctave,
				Persistence = hSliderFreq
			};
			generator1 = noise1;
		}
		else if (selectionGridInt == 1)
		{
			var noise2 = new BillowNoise(seed1)
			{
				OctaveCount = hSliderOctave,
				Persistence = hSliderFreq
			};
			generator1 = noise2;
		}
		else
		{
			var noise3 = new RidgeNoise(seed1)
			{
				OctaveCount = hSliderOctave
			};
			//noise3.Frequency = (int) hSliderFreq;
			generator1 = noise3;
		}
		
		m_Generator = generator1 * hSliderMult;
		
		if (toolbarInt == 1)
			m_Generator = new Bias(m_Generator, hSliderMod);
		else if (toolbarInt == 2)
			m_Generator = new Gain(m_Generator, hSliderMod);


		terSet.setGenerator (m_Generator);
	}

	/*
	 * Create Generator with selected parameters
	 */

	private void GenerateNoise2(int seed2)
	{
		Generator generator2;
		Generator weight;
		
		if (selectionGridInt2 == 0)
		{
			var noise1 = new PinkNoise(seed2)
			{
				OctaveCount = hSliderOctave2,
				Persistence = hSliderFreq2
			};
			generator2 = noise1;
		}
		else if (selectionGridInt2 == 1)
		{
			var noise2 = new BillowNoise(seed2)
			{
				OctaveCount = hSliderOctave2,
				Persistence = hSliderFreq2
			};
			generator2 = noise2;
		}
		else
		{
			var noise3 = new RidgeNoise(seed2)
			{
				OctaveCount = hSliderOctave2
			};
			//noise3.Frequency = (int) hSliderFreq;
			generator2 = noise3;
		}
		generator2 *= hSliderMult2;
		
		if (toolbarInt2 == 1)
			generator2 = new Bias(generator2, hSliderMod2);
		else if (toolbarInt2 == 2)
			generator2 = new Gain(generator2, hSliderMod2);
		
		weight = new Cache(new PinkNoise(346546)
		                   {
			Frequency = 0.5f,
			OctaveCount = 3
		}.ScaleShift(0.5f, 0.5f));
		
		m_Generator = m_Generator.Blend(generator2, weight).ScaleShift(0.5f, 0.5f);

		terSet.setGenerator (m_Generator);
	}

	/*
	 * GUI for Custom noise
	 */

	private void GenerateCustomNoise()
	{
//		GUI.Box (new Rect (10, 10, 270, 350), "Test Terrain");
		EditorGUILayout.Separator();
		m_Generator = CustGenerator;

		if (selectionTerrainInt == 0 || selectionTerrainInt == 1 || selectionTerrainInt == 2)
		{
			intFieldSeedCust = EditorGUILayout.IntField ( "Seed:", intFieldSeedCust);
			
			if (intFieldSeed <= 0)
				intFieldSeed = 0;
		}
		selectionTerrainInt = GUILayout.SelectionGrid (selectionTerrainInt, selectionTerrainStrings, 3);

		EditorGUILayout.Separator();
		if(GUILayout.Button("Save Terrain")) 
		{
			terSet.setPersoGenerator(terSet.getGenerator());
		}
		EditorGUILayout.Separator();
		hSliderMultTerrain = EditorGUILayout.Slider ("Multiplication:", hSliderMultTerrain, 0.0f, 1.0f);

		hSliderDilatTerrain = EditorGUILayout.Slider ("Dilatation:", hSliderDilatTerrain, 1.0f, 2.0f);
		
		if(GUILayout.Button("Modif Size"))
		{
			m_Generator *= hSliderMultTerrain;
			terSet.setGenerator (m_Generator);
			terSet.setDilatTerrain(hSliderDilatTerrain);
			
			GenerateTerrain();
		}
		EditorGUILayout.Separator();
		SnowBool = EditorGUILayout.Toggle ("Snow Terrain", SnowBool);

		CreateGuiSize ();
		CreateGuiTrees ();
		CreateGuiWater ();

		EditorGUILayout.Separator();
		if(GUILayout.Button("Generate"))
		{
			CityGenerator citygenerator = FindObjectOfType<CityGenerator>();
			citygenerator.ClearAll();

			if (selectionTerrainInt == 0)
			{
				CreateRandomNoise1();
			}
			else if (selectionTerrainInt == 1)
			{
				CreateRandomNoise2();
			}
			else if (selectionTerrainInt == 2)
			{
				CreateRandomNoise3();
			}
			else if (selectionTerrainInt == 3)
			{
				CreateCustomNoise1();
			}
			else if (selectionTerrainInt == 4)
			{
				CreateCustomNoise2();
			}
			else
			{
				m_Generator = terSet.getPersoGenerator();
			}
			
			CustGenerator = m_Generator;
			m_Generator *= hSliderMultTerrain;
			terSet.setGenerator (m_Generator);
			terSet.setDilatTerrain(hSliderDilatTerrain);

			GenerateTerrain();
		}
	}

	/*
	 * Parameters for custom noise1
	 */
	private void CreateCustomNoise1()
	{
		var noise = new BillowNoise(45645)
		{
			OctaveCount = 5,
			Persistence = 0.44f
		};
		
		var noise2 = new PinkNoise(8)
		{
			OctaveCount = 5,
			Persistence = 0.43f
		};
		
		Generator weight = new Cache(new PinkNoise(346546)
		                             {
			Frequency = 0.5f,
			OctaveCount = 3
		}.ScaleShift(0.5f, 0.5f));
		
		m_Generator = noise.Blend(noise2, weight).ScaleShift(0.5f, 0.5f);
		terSet.setGenerator (m_Generator);
	}


	
	/*
	 * Parameters for custom noise2
	 */
	private void CreateCustomNoise2()
	{
		var noise = new BillowNoise(125)
		{
			OctaveCount = 5,
			Persistence = 0.44f
		};

		var noise2 = new PinkNoise(112)
		{
			OctaveCount = 5,
			Persistence = 0.43f
		};

		Generator weight = new Cache(new PinkNoise(346546)
		{
			Frequency = 0.5f,
			OctaveCount = 3
		}.ScaleShift(0.5f, 0.5f));
		
		m_Generator = noise.Blend(noise2, weight).ScaleShift(0.5f, 0.5f);
		terSet.setGenerator (m_Generator);
	}

	private void CreateRandomNoise1()
	{
	/*	var noise = new Bias(
			new PinkNoise((int)Random.Range(0, 99999999))
			{
			OctaveCount = 5,
			Persistence = 0.4f
		}, 0.4f);

*/

		var noise = new PinkNoise(intFieldSeedCust)
			{
			OctaveCount = 6,
			Persistence = 0.48f
		} * 0.4f;
		var noise2 = new Bias(noise, 0.4f);
		m_Generator = noise2;
		terSet.setGenerator (m_Generator);
	}

	private void CreateRandomNoise2()
	{
		Generator noise = new RidgeNoise(intFieldSeedCust)
		{
			OctaveCount = 5,
		};
		
		Generator noise2 = new PinkNoise(intFieldSeedCust)
		{
			OctaveCount = 5,
			Persistence = 0.2f
		};
		
		Generator weight = new Cache(new PinkNoise(346546)
		                             {
			Frequency = 0.5f,
			OctaveCount = 3
		}.ScaleShift(0.5f, 0.5f));

		noise = noise * 0.5f;
		noise2 = noise2 * 0.2f;

		m_Generator = noise.Blend(noise2, weight).ScaleShift(0.5f, 0.5f);
		terSet.setGenerator (m_Generator);
	}

	private void CreateRandomNoise3()
	{
		Generator noise = new BillowNoise(intFieldSeedCust)
		{
			OctaveCount = 7,
			Persistence = 0.44f
		};
		
		Generator noise2 = new PinkNoise(intFieldSeedCust)
		{
			OctaveCount = 6,
			Persistence = 0.43f
		};
		
		Generator weight = new Cache(new PinkNoise(346546)
		                             {
			Frequency = 0.5f,
			OctaveCount = 3
		}.ScaleShift(0.5f, 0.5f));
		
		m_Generator = noise.Blend(noise2, weight).ScaleShift(0.5f, 0.5f);
		terSet.setGenerator (m_Generator);
	}


}