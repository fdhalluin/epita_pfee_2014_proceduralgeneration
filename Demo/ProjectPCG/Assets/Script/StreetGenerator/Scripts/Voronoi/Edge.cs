using System;
using System.Collections.Generic;

namespace graph
{
	public class Edge
	{
		internal readonly graph.Point first;

		internal readonly graph.Point second;

		public Edge(graph.Point first, graph.Point second) : base()
		{
			this.first = first;
			this.second = second;
		}

		public virtual bool equals(graph.Edge arg0)
		{
			graph.Edge toEdge = arg0;
			if (toEdge.first == null || toEdge.second == null)
			{
				return false;
			}
			if (toEdge.first.x == first.x && toEdge.first.y == first.y && toEdge.second.x == 
				second.x && toEdge.second.y == second.y)
			{
				return true;
			}
			if (toEdge.second.x == first.x && toEdge.second.y == first.y && toEdge.first.x ==
				 second.x && toEdge.first.y == second.y)
			{
				return true;
			}
			return false;
		}

		public virtual graph.Point getFirst()
		{
			return first;
		}

		public virtual graph.Point getSecond()
		{
			return second;
		}

		public static HashSet<graph.Edge> toEdges(System.Collections.Generic.ICollection
			<graph.Point> c)
		{
			HashSet<graph.Edge> ans = new HashSet<graph.Edge>();
			if (c.Count <= 0)
			{
				return ans;
			}
			IEnumerator<graph.Point> ite = c.GetEnumerator();
			// TODO: ite.MoveNext();
			graph.Point first = ite.Current;
			graph.Point temp1 = first;
			graph.Point temp2 = null;
			while (ite.MoveNext())
			{
				temp2 = ite.Current;
				ans.Add(new graph.Edge(temp1, temp2));
				temp1 = temp2;
			}
			if (ans.Count > 1)
			{
				ans.Add(new graph.Edge(temp2, first));
			}
			return ans;
		}

		public static HashSet<graph.Edge> toEdges(graph.VoronoiPolygon p)
		{
			HashSet<graph.Point> points = new HashSet<graph.Point>();
			for (int i = 0; i < p.npoints; ++i)
			{
				points.Add(new graph.Point(p.xpoints[i], p.ypoints[i]));
			}
			return graph.Edge.toEdges(points);
		}
	}
}
