Place video and demo binaries (zipped) in this folder.
Fill in authors.txt/licence.txt and this file.


Demo:

You'll probably code with MonoDevelop or Visual.  I'd recommend Visual, but you may need to setup the project files by hand (you can use UnityVS, but AFAIK it isn't free).
Coding style is up to you, but pick one and stick to it.
Learn to use Unity in-game GUI, editor windows and Gizmos before you start coding, it'll save you a lot of time debugging.  Check the demo template for a very simple sample.



State of the art:

Put screenshots directly in the folder, number and name each one correctly (e.g. 01_WatchDogs_Crowd), use appropriate resolution and compression.  Use e.g. the windows snipping tool.
Make a doc file with links to youtube videos/blog posts/articles/papers, number each reference.
Classify references like a table of contents.  Only title for each section, no need to develop each point (same as what you got for the research topic, just add links and new sections):



Epita reports:

Copy reports as they are after you turned them in for Epita, no need to organize except file names.



Research summary:

Start a doc file.  Write down ideas for your demo.  No need to organize this now.
You can make a google doc file if it's easier to collaborate, if so, add a shortcut to it in the folder for now, later add the hard copy.



Git usage:

Do not add unity runtime files to git repo (check .gitignore in Demo/)
Write proper commit notes (one sentence, no need for file list, use English imperative, e.g. "Add references on PCG in Spelunky", "Fix infinite loop in offline city generation").