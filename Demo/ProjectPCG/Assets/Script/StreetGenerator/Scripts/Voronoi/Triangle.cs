using System;
using System.Collections.Generic;

namespace graph
{
	[System.Serializable]
	public class Triangle : IComparable<Triangle>
	{
		internal graph.Point p1;

		internal graph.Point p2;

		internal graph.Point p3;

		internal HashSet<graph.Point> points;

		internal HashSet<graph.Edge> edges;

		internal HashSet<graph.Triangle> neighbours;

		internal graph.Point circumcenter;

		public int npoints = 3;

		public float[] xpoints = new float[3];
		public float[] ypoints = new float[3];

		public Triangle(graph.Point p1, graph.Point p2, graph.Point p3) : base()
		{
			this.p1 = p1;
			this.p2 = p2;
			this.p3 = p3;
			points = new HashSet<graph.Point>();
			points.Add(p1);
			points.Add(p2);
			points.Add(p3);

			xpoints[0] = p1.x;
			xpoints[1] = p2.x;
			xpoints[2] = p3.x;
			ypoints[0] = p1.y;
			ypoints[1] = p2.y;
			ypoints[2] = p3.y;

			edges = new HashSet<graph.Edge>();
			edges.Add(new graph.Edge(p1, p2));
			edges.Add(new graph.Edge(p2, p3));
			edges.Add(new graph.Edge(p3, p1));
			neighbours = new HashSet<graph.Triangle>();
		}

		public Triangle(graph.Point p1, graph.Edge e) : this(p1, e.first, e.second)
		{
		}

		public bool contains(graph.Point p) {
			int hits = 0;
			
			float lastx = xpoints[npoints - 1];
			float lasty = ypoints[npoints - 1];
			float curx, cury;
			
			for (int i = 0; i < npoints; lastx = curx, lasty = cury, i++) {
				curx = xpoints[i];
				cury = ypoints[i];
				
				if (cury == lasty) {
					continue;
				}
				
				float leftx;
				if (curx < lastx) {
					if (p.x >= lastx) {
						continue;
					}
					leftx = curx;
				} else {
					if (p.x >= curx) {
						continue;
					}
					leftx = lastx;
				}
				
				double test1, test2;
				if (cury < lasty) {
					if (p.y < cury || p.y >= lasty) {
						continue;
					}
					if (p.x < leftx) {
						hits++;
						continue;
					}
					test1 = p.x - curx;
					test2 = p.y - cury;
				} else {
					if (p.y < lasty || p.y >= cury) {
						continue;
					}
					if (p.x < leftx) {
						hits++;
						continue;
					}
					test1 = p.x - lastx;
					test2 = p.y - lasty;
				}
				
				if (test1 < (test2 / (lasty - cury) * (lastx - curx))) {
					hits++;
				}
			}
			
			bool result = ((hits & 1) != 0);

			return result;
		}

		private float two(float n)
		{
			return n * n;
		}

		/// <summary>
		/// Computes the center of the circumcircle and returns it
		/// (see http://en.wikipedia.org/wiki/Circumscribed_circle#Cartesian_coordinates
		/// for the formula)
		/// </summary>
		/// <returns>a \a Point representing the cirumcenter of the triangle</returns>
		public virtual graph.Point getCircumCenter()
		{
			if (circumcenter != null)
			{
				return circumcenter;
			}
			double s1 = two(p1.x) + two(p1.y);
			double s2 = two(p2.x) + two(p2.y);
			double s3 = two(p3.x) + two(p3.y);
			double i1 = p3.x - p2.x;
			double i2 = p1.x - p3.x;
			double i3 = p2.x - p1.x;
			double d1 = p2.y - p3.y;
			double d2 = p3.y - p1.y;
			double d3 = p1.y - p2.y;
			double d = p1.x * d1 + p2.x * d2 + p3.x * d3;
			d *= 2;
			double ox = s1 * d1 + s2 * d2 + s3 * d3;
			ox /= d;
			double oy = s1 * i1 + s2 * i2 + s3 * i3;
			oy /= d;
			circumcenter = new graph.Point((int)ox, (int)oy);
			return circumcenter;
		}

		public virtual HashSet<graph.Point> getPoints()
		{
			return points;
		}

		public virtual HashSet<graph.Edge> getEdges()
		{
			return edges;
		}

		public virtual bool equals(graph.Triangle obj)
		{
			return edges.Equals(obj.getEdges());
		}

		public virtual bool hasEdge(graph.Edge e)
		{
			foreach (graph.Edge m in edges)
			{
				if (m.equals(e))
				{
					return true;
				}
			}
			return false;
		}

		public virtual graph.Edge getOpposingEdge(graph.Point p)
		{
			foreach (graph.Edge ans in edges)
			{
				if (!ans.first.Equals(p) && !ans.second.Equals(p))
				{
					return ans;
				}
			}
			return null;
		}

		public virtual bool isInCircumcircle(graph.Point p)
		{
			graph.Point cc = getCircumCenter();
			return cc.distance(p) <= cc.distance(p1);
		}

		public virtual graph.Point closestPoint(graph.Point p)
		{
			double d1 = p1.distance(p);
			double d2 = p2.distance(p);
			double d3 = p3.distance(p);
			if (d1 <= d2 && d1 <= d3)
			{
				return p1;
			}
			if (d2 <= d3)
			{
				return p2;
			}
			return p3;
		}

		public virtual void addNeighbour(graph.Triangle t)
		{
			neighbours.Add(t);
		}

		public virtual void removeNeighbour(graph.Triangle t)
		{
			neighbours.Remove(t);
		}

		public virtual HashSet<graph.Triangle> getNeighbours()
		{
			return neighbours;
		}

		public virtual bool hasPoint(graph.Point p)
		{
			return p.Equals(p1) || p.Equals(p2) || p.Equals(p3);
		}

		public virtual graph.Point getCenter()
		{
			graph.Point ans = new graph.Point();
			ans.x += p1.x;
			ans.x += p2.x;
			ans.x += p3.x;
			ans.y += p1.y;
			ans.y += p2.y;
			ans.y += p3.y;
			ans.x /= 3;
			ans.y /= 3;
			return ans;
		}

		public int CompareTo(graph.Triangle argo) {
			return compareTo(argo);
		}

		public virtual int compareTo(graph.Triangle arg0)
		{
			if (!(arg0 is graph.Triangle))
			{
				return -1;
			}
			graph.Triangle casted = (graph.Triangle)arg0;
			graph.Point p = casted.getCenter();
			graph.Point m = getCenter();
			if (m.x.CompareTo(p.x) == 0)
			{
				return m.y.CompareTo(p.y);
			}
			return m.x.CompareTo(p.x);
		}
	}
}
