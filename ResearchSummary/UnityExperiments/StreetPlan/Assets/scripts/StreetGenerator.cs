﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public enum StreetDirection {
	up,
	down,
	left,
	right
}

public class Quarter {
	public List<LBranch> mStreets = new List<LBranch>();
	public LBranch mLastStreet;
	public StreetDirection mStreetDirection;
	public GameObject mParentObject;

	public Quarter(StreetDirection iStreetDirection, GameObject iParentObject) {
		mStreetDirection = iStreetDirection;
		mParentObject = iParentObject;
	}
}

public class LBranch {
	public Vector3 mPosition;
	public Vector3 mRotation;
	
	public LBranch() {}
	
	public LBranch(Vector3 iPosition, Vector3 iRotation) {
		this.mPosition = iPosition;
		this.mRotation = iRotation;
	}
}

public class StreetGenerator {
	public GameObject mHouseHolder;

	public float mStartAngle = 90f;
	public StreetDirection mDirection;

	private int mIteration = 4;
	private float mAngle = 90;
	private float mLength = 0.06f;
	private float mOrganic = 0.9993f;

	private string mLCity = "";
	private List<char> mVariables = new List<char>();
	private List<string> mRules = new List<string>();
	private List<LBranch> mList = new List<LBranch>();
	private List<GameObject> mGameObjects = new List<GameObject>();
	private Dictionary<GameObject, Pair<Vector3, Vector3>> mStreets = new Dictionary<GameObject, Pair<Vector3, Vector3>>();
	private List<Quarter> mQuarters = new List<Quarter>();

	public float mMinXObject = 0.0f;
	public float mMaxXObject = 0.0f;
	public float mMinZObject = 0.0f;
	public float mMaxZObject = 0.0f;
	
	public void Generate(Vector3 iPosition, StreetDirection iDirection) {
		mVariables.Clear();
		mVariables.Add('F');
		mVariables.Add('[');
		mVariables.Add(']');
		mVariables.Add('+');
		mVariables.Add('-');
		mVariables.Add('&');
		mVariables.Add('^');
		mVariables.Add('<');
		mVariables.Add('>');
		mVariables.Add('|');

		Direction(iDirection);

		GameObject lRootStreet = new GameObject();
		lRootStreet.name = "Quarter " + iDirection.ToString();
		mGameObjects.Add(lRootStreet);
		
		GameObject lObject = GameObject.Find("Quarters");
		
		if (lObject != null) {
			lRootStreet.transform.parent = lObject.transform;
		}

		init(mRules);
		LBranch lRootBranch = new LBranch(iPosition, new Vector3(0, 0, 310));
		Quarter lQuarter = new Quarter(mDirection, lObject);
		mQuarters.Add(lQuarter);
		constructLSystem(lQuarter, lRootBranch, lRootStreet);
	}

	public void Increase() {
		foreach (Quarter lQuarter in mQuarters) {
			Direction(lQuarter.mStreetDirection);
			init(mRules);
			constructLSystem(lQuarter, lQuarter.mStreets[(int)Random.Range(0f, (float)lQuarter.mStreets.Count - 1)], lQuarter.mParentObject);
		}
	}

	private void Direction(StreetDirection iDirection) {
		mDirection = iDirection;
		switch (mDirection) {
		case StreetDirection.up:
			mRules = GetSmallVillageUpRules();
			break;
		case StreetDirection.down:
			mRules = GetSmallVillageDownRules();
			break;
		case StreetDirection.left:
			mRules = GetSmallVillageLeftRules();
			break;
		case StreetDirection.right:
			mRules = GetSmallVillageRightRules();
			break;
		}
	}

	private void constructLSystem(Quarter iQuarter, LBranch iRootBranch, GameObject iParent) {
		mList.Add(iRootBranch);

		for (int i = 0; i < mLCity.Length; ++i) {
			LBranch lBranch = new LBranch();
			lBranch.mPosition = mList[mList.Count - 1].mPosition;
			lBranch.mRotation = mList[mList.Count - 1].mRotation;
			iQuarter.mStreets.Add(lBranch);

			Vector3 lCurrentPosition = (mList[mList.Count - 1].mPosition + mList[mList.Count - 1].mRotation * mLength + mList[mList.Count - 1].mPosition) / 2;

			if (lCurrentPosition.x > mMinXObject + 50
			    && lCurrentPosition.x < mMaxXObject
			    && lCurrentPosition.z > mMinZObject + 50	
			    && lCurrentPosition.z < mMaxZObject) {
				break;
			}

			if (Physics.Raycast(lCurrentPosition, lCurrentPosition, 2)
			    || Physics.Raycast(mList[mList.Count - 1].mRotation * mLength + mList[mList.Count - 1].mPosition, mList[mList.Count - 1].mRotation * mLength + mList[mList.Count - 1].mPosition, 2)) {
				break;
			}

//			while (i < mLCity.Length - 1
//			       && Physics.Raycast(lCurrentPosition, lCurrentPosition, 10)
//			       && Physics.Raycast(mList[mList.Count - 1].mRotation * mLength + mList[mList.Count - 1].mPosition, mList[mList.Count - 1].mRotation * mLength + mList[mList.Count - 1].mPosition, 10)) {
//				++i;
//			}

			if (mLCity[i] == '[') {
				mList.Add(lBranch);
			}
			else if (mLCity[i] == ']') {
				if (mList.Count > 0) {
//					mList.RemoveAt(mList.Count - 1);
				}
			}
			else if (mLCity[i] == '+') {
				mList[mList.Count - 1].mRotation = Quaternion.AngleAxis(mAngle, Vector3.up) * mList[mList.Count - 1].mRotation;
			}
			else if (mLCity[i] == '-') {
				mList[mList.Count - 1].mRotation = Quaternion.AngleAxis(-mAngle, Vector3.forward) * mList[mList.Count - 1].mRotation;
			}
			else if (mLCity[i] == '&') {
				mList[mList.Count - 1].mRotation = Quaternion.AngleAxis(mAngle, Vector3.up) * mList[mList.Count - 1].mRotation;
			}
			else if (mLCity[i] == '^') {
				mList[mList.Count - 1].mRotation = Quaternion.AngleAxis(-mAngle, Vector3.forward) * mList[mList.Count - 1].mRotation;
			}
			else if (mLCity[i] == '<') {
				mList[mList.Count - 1].mRotation = Quaternion.AngleAxis(mAngle, Vector3.up) * mList[mList.Count - 1].mRotation;
			}
			else if (mLCity[i] == '>') {
				mList[mList.Count - 1].mRotation = Quaternion.AngleAxis(-mAngle, Vector3.forward) * mList[mList.Count - 1].mRotation;
			}
			else if (mLCity[i] == '|') {
//				UpdateAngle();
				GameObject lStreet = new GameObject();
				lStreet.transform.parent = iParent.transform;
				lStreet.AddComponent<BoxCollider>();
				BoxCollider lCollider = lStreet.GetComponent<BoxCollider>();
				lCollider.size = new Vector3(10, 10, 10);
				lCollider.isTrigger = false;

				if (i >= 1) {
					lStreet.name = "Street " + mLCity[i - 1];
				} else {
					lStreet.name = "First Street";
				}
				
//				if (Random.Range(0.0f, 1.0f) > 0.90f) {
//					ProcessCurve(lStreet);
//				} else {
					ProcessStraight(lStreet);
//				}

//				UpdateArea(lStreet);
			}
			else if (mLCity[i] == 'F') {
//				UpdateAngle();
				GameObject lStreet = new GameObject();
				lStreet.transform.parent = iParent.transform;
				lStreet.AddComponent<BoxCollider>();
				BoxCollider lCollider = lStreet.GetComponent<BoxCollider>();
				lCollider.size = new Vector3(6,6,6);
				lCollider.isTrigger = false;
				if (i >= 1) {
					lStreet.name = "Street " + mLCity[i - 1];
				} else {
					lStreet.name = "First Street";
				}
				ProcessStraight(lStreet);
//				UpdateArea(lStreet);
			} 
		}
	}
	
	private void init(List<string> iRules) {
		mLCity = "F";

		for (int i = 0; i < mIteration; ++i) {
			int lLength = mLCity.Length;
			string lNewString = "";
			
			for (int j = 0; j < lLength; ++j) {
				for (int k = 0; k < mVariables.Count; ++k) {
					if (mLCity[j] == mVariables[k]) {
						lNewString += iRules[k];
						break;
					}
				}
			}
			mLCity = lNewString;
		}
	}

	private List<string> GetSmallVillageUpRules() {
		mAngle = 90;
		mIteration = 3;
		mOrganic = 0.999f;
		List<string> lRules = new List<string>();
		lRules.Add("F[<F]^");
		lRules.Add("[");
		lRules.Add("]");
		lRules.Add("+F<>F");
		lRules.Add("-F");
		lRules.Add("&-F");
		lRules.Add("^+-F");
		lRules.Add("<^");
		lRules.Add(">");
		lRules.Add("|");
		return lRules;
	}

	private List<string> GetSmallVillageDownRules() {
		mAngle = 90;
		mIteration = 3;
		mOrganic = 0.999f;
		List<string> lRules = new List<string>();
		lRules.Add("F+[&+F][>+F]");
		lRules.Add("[+F]");
		lRules.Add("]");
		lRules.Add("+");
		lRules.Add("-[&F]");
		lRules.Add("&");
		lRules.Add("^");
		lRules.Add("<[+F]");
		lRules.Add(">");
		lRules.Add("|");
		return lRules;
	}

	private List<string> GetSmallVillageRightRules() {
		mAngle = 90;
		mIteration = 3;
		mOrganic = 0.999f;
		List<string> lRules = new List<string>();
		lRules.Add("F[&F]");
		lRules.Add("[F[+F][^F]");
		lRules.Add("]");
		lRules.Add("+");
		lRules.Add("-[&F]");
		lRules.Add("&");
		lRules.Add("^");
		lRules.Add("<");
		lRules.Add(">");
		lRules.Add("|");
		return lRules;
	}

	private List<string> GetSmallVillageLeftRules() {
		mAngle = 90;
		mIteration = 3;
		mOrganic = 0.999f;
		List<string> lRules = new List<string>();
		lRules.Add("F[&--F][>-F]");
		lRules.Add("[");
		lRules.Add("]");
		lRules.Add("+[>F]");
		lRules.Add("-[&F]");
		lRules.Add("&[+F]");
		lRules.Add("^[<F]");
		lRules.Add("<[&F]");
		lRules.Add(">");
		lRules.Add("|");
		return lRules;
	}

	private List<string> GetVillageRules() {
		mAngle = 90;
		mIteration = 4;
		mOrganic = 0.99993f;
		List<string> lRules = new List<string>();
		lRules.Add("F^[&+F]^[->F]F[&F]");
		lRules.Add("[");
		lRules.Add("]");
		lRules.Add("+");
		lRules.Add("-[&+F][>-F]^^");
		lRules.Add("&");
		lRules.Add("^[&^F][+^F]");
		lRules.Add("<");
		lRules.Add(">");
		lRules.Add("|");
		return lRules;
	}

	private List<string> GetCityRules() {
		mAngle = 90;
		mIteration = 4;
		mOrganic = 0.99998f;
		List<string> lRules = new List<string>();
		lRules.Add("F[&+F]-[F+F]|[F+F]|[-&F&F]");
		lRules.Add("[F");
		lRules.Add("]&F+F&F+F");
		lRules.Add("+F-[F+F]");
		lRules.Add("-[-F]+F+F");
		lRules.Add("&[|F]");
		lRules.Add("^[-F+F-F|F]");
		lRules.Add("<[-&F&F]F");
		lRules.Add(">F");
		lRules.Add("|F[&F]FF");
		return lRules;
	}

	private List<string> GetExtendedCityRules() {
		mAngle = 90;
		mIteration = 4;
		mOrganic = 0.99998f;
		List<string> lRules = new List<string>();
		lRules.Add("F[&+F]-[F+F]|[F+F]|[-&F&F]");
		lRules.Add("[F");
		lRules.Add("]&F+F");
		lRules.Add("+F");
		lRules.Add("-[-F]+F+F");
		lRules.Add("&[|F]");
		lRules.Add("^[-F+F-F+F]");
		lRules.Add("<[-&F&F]FFFFFF");
		lRules.Add(">F");
		lRules.Add("|F[&F]FFFF");
		return lRules;
	}

	public void ClearAll() {
		foreach (KeyValuePair<GameObject, Pair<Vector3, Vector3>> lEntry in mStreets) {
			GameObject lObject = lEntry.Key;
			GameObject.Destroy(lObject);
		}

		foreach (GameObject lObject in mGameObjects) {
			GameObject.Destroy(lObject);
		}

		mGameObjects.Clear();
		mStreets.Clear();

		mMinXObject = 0.0f;
		mMaxXObject = 0.0f;
		mMinZObject = 0.0f;
		mMaxZObject = 0.0f;
	}

	private void UpdateAngle() {
		if (Random.Range(0.0F, 1.0F) >= mOrganic) {
			mAngle = Random.Range(10.0F, 100.0F);
		} else {
			mAngle = 90;
		}
	}

	private void ProcessStraight(GameObject iStreet) {
		iStreet.AddComponent<LineRenderer>();
		LineRenderer lRenderer = iStreet.GetComponent<LineRenderer>();
		lRenderer.SetWidth(0.1f, 0.1f);
		lRenderer.SetVertexCount(2);

		Vector3 lFirstPosition = mList[mList.Count - 1].mPosition;
		Vector3 lSecondPosition = mList[mList.Count - 1].mPosition + mList[mList.Count - 1].mRotation * mLength;
		
		lFirstPosition.y = Terrain.activeTerrain.SampleHeight(lFirstPosition) + 1;
		lSecondPosition.y = Terrain.activeTerrain.SampleHeight(lSecondPosition) + 1;
		
		lRenderer.SetPosition(0, lFirstPosition);
		lRenderer.SetPosition(1, lSecondPosition);
		lRenderer.material = new Material(Shader.Find("Particles/Additive"));
		
		mList[mList.Count - 1].mPosition = mList[mList.Count - 1].mPosition + mList[mList.Count - 1].mRotation * mLength;
		
		bool lFind = false;
		iStreet.transform.position = (lFirstPosition + lSecondPosition) / 2;
		
		Vector3 lPosition = iStreet.transform.position;
		int lX = (int)lPosition.x;
		int lZ = (int)lPosition.z;
		
		Vector3 lNewSecondPosition = lSecondPosition;
		
		foreach (KeyValuePair<GameObject, Pair<Vector3, Vector3>> lEntry in mStreets) {
			GameObject lObject = lEntry.Key;
			if ((int)lObject.transform.position.x == lX
			    && (int)lObject.transform.position.z == lZ) {
				lFind = true;
				break;
			}
			
			Vector3 lFirst = lEntry.Value.First;
			Vector3 lSecond = lEntry.Value.Second;
			
			if (((lFirstPosition.z > lFirst.z && lFirstPosition.z > lSecond.z
			      && lSecondPosition.z < lFirst.z && lSecondPosition.z < lSecond.z)
			     
			     || (lFirstPosition.z < lFirst.z && lFirstPosition.z < lSecond.z
			    && lSecondPosition.z > lFirst.z && lSecondPosition.z > lSecond.z))
			    
			    && lSecondPosition.x > lFirst.x && lSecondPosition.x < lSecond.x) {
				
				lNewSecondPosition = (lFirst + lSecond) / 2;
				lRenderer.SetPosition(1, lNewSecondPosition);
			}
		}
		
		if (!lFind) {
			mStreets.Add(iStreet, new Pair<Vector3, Vector3>(lFirstPosition, lNewSecondPosition));
		} else {
			GameObject.Destroy(iStreet);
		}
	}

	private void ProcessCurve(GameObject iStreet) {
		int lMaxVertex = 34;

		if (Random.Range(0.0f, 1.0f) > 0.72) {
			lMaxVertex = 34;
		} else {
			lMaxVertex = 19;
		}

		iStreet.AddComponent<LineRenderer>();
		LineRenderer lRenderer = iStreet.GetComponent<LineRenderer>();
		lRenderer.SetWidth(0.1f, 0.1f);
		lRenderer.SetVertexCount(lMaxVertex);
		lRenderer.material = new Material(Shader.Find("Particles/Additive"));

		Vector3 lFirstPosition = mList[mList.Count - 1].mPosition;
		Vector3 lLastPosition = mList[mList.Count - 1].mPosition + mList[mList.Count - 1].mRotation * mLength;
		lFirstPosition.y = Terrain.activeTerrain.SampleHeight(lFirstPosition) + 1;
		lLastPosition.y = Terrain.activeTerrain.SampleHeight(lLastPosition) + 1;

		iStreet.transform.position = (lFirstPosition + lLastPosition) / 2;

		lRenderer.SetPosition(0, lFirstPosition);

		float x = lFirstPosition.x;
		float z = lFirstPosition.z;
		float r = Random.Range(1.0f, 6.0f);

		float theta;
		if (r < 2.0f) {
			 theta = 0.0f;
		} else if (r >= 2.0f && r <= 4.0f) {
			theta = 3.1415f / 2;
		} else {
			theta = 3.1415f;
		}

		for (int j = 1; j < lMaxVertex; j++) {
			x = r * Mathf.Cos(theta) + lFirstPosition.x;
			z = r * Mathf.Sin(theta) + lFirstPosition.z;
			
			Vector3 pos = new Vector3(x, Terrain.activeTerrain.SampleHeight(new Vector3(x, 0, z)) + 1, z);
			lRenderer.SetPosition(j, pos);
			theta += 0.1f;
		}

		lRenderer.SetPosition(lMaxVertex - 1, lFirstPosition);
		mList[mList.Count - 1].mPosition = mList[mList.Count - 1].mPosition + mList[mList.Count - 1].mRotation * mLength;

		Vector3 lPosition = iStreet.transform.position;
		int lX = (int)lPosition.x;
		int lZ = (int)lPosition.z;
		bool lFind = false;

		foreach (KeyValuePair<GameObject, Pair<Vector3, Vector3>> lEntry in mStreets) {
			GameObject lObject = lEntry.Key;
			if ((int)lObject.transform.position.x == lX
			    && (int)lObject.transform.position.z == lZ) {
				lFind = true;
				break;
			}
		}

		if (!lFind) {
			mStreets.Add(iStreet, new Pair<Vector3, Vector3>(lFirstPosition, lLastPosition));
		} else {
			GameObject.Destroy(iStreet);
		}
	}

//	private void UpdateArea(GameObject iObject) {
//		if (iObject.transform.position.x < mMinXObject) {
//			mMinXObject = iObject.transform.position.x;
//		}
//
//		if (iObject.transform.position.x > mMaxXObject) {
//			mMaxXObject = iObject.transform.position.x;
//		}
//
//		if (iObject.transform.position.z < mMinZObject) {
//			mMinZObject = iObject.transform.position.z;
//		}
//
//		if (iObject.transform.position.z > mMaxZObject) {
//			mMaxZObject = iObject.transform.position.z;
//		}
//	}
}