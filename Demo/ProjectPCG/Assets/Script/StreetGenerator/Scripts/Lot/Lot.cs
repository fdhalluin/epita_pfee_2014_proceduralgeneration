using System;
using System.Collections.Generic;
using UnityEngine;
using HouseGen;

namespace LotGeneration {
	public enum LotType {CITY_HALL = 0, CHURCH = 1, TOWER = 2, RESIDENTIAL = 3, SKYSCRAPER = 4, STAIRHOUSE = 5, MINIHOUSE = 6, RESTAURANT = 7, SHOP = 8};

	public class Lot : IComparer<Lot>, IIndexedObject {
		public static readonly Lot Comparer = new Lot(0, 0, 0, 0);

		public PaletteKind color;
		public float x;
		public float y;
		public float length;
		public float width;
		public float interestingScore;
		public Vector3 lotCenter;
		public Vector3 firstPoint;
		public Vector3 secondPoint;
		public bool hasBeenAssimilated = false;

		public int Index { get; set; }

		public static int count = 0;

		public Lot (float x, float y, float l, float w)
		{
			++count;
			this.x = x;
			this.y = y;
			this.length = l;
			this.width = w;
			this.interestingScore = -1;
		}

		public GameObject draw(Material baseMaterial) {
			float height = 3;
			Vector3 center = new Vector3(x, 0, y);
			//center = lotCenter;
			List<Vector3> points = new List<Vector3>();
			List<int> triangles = new List<int>();
			List<Vector2> UVValues = new List<Vector2>();

			GameObject house = new GameObject();
			house.AddComponent<MeshFilter> ();
			house.AddComponent<MeshRenderer> ();
			house.AddComponent<MeshCollider> ();
			
			Mesh mesh = house.GetComponent<MeshFilter> ().mesh;

			points.Add(new Vector3(0, height, 0));
			points.Add(new Vector3(width, height, 0));
			points.Add(new Vector3(width, height, length));
			points.Add(new Vector3(0, height, length));

			triangles.Add (2);
			triangles.Add (1);
			triangles.Add (0);

			triangles.Add (2);
			triangles.Add (0);
			triangles.Add (3);

			while (points.Count > UVValues.Count) {
				UVValues.Add(new Vector2(0.4f, 0.5f));
			}
			
			mesh.vertices = points.ToArray();
			mesh.triangles = triangles.ToArray();
			mesh.uv = UVValues.ToArray ();
			mesh.Optimize ();
			mesh.RecalculateNormals ();
			house.gameObject.renderer.material = baseMaterial;
			house.transform.localScale = new Vector3(0.9f, 1, 0.9f);
			house.transform.position = center;
			return house;
		}

		public int Compare(Lot x, Lot y) {
			if (x.interestingScore > y.interestingScore)
				return -1;
			else if (x.interestingScore < y.interestingScore)
				return 1;
			return 0;
		}
	}

	public class LotMesh {
		public Lot[,] mesh;
		public int width;
		public int length;
		public float x;
		public float y;
		public float lotSize;
		public PaletteKind color;

		public LotMesh(int w, int l) {
			this.width = w;
			this.length = l;
			mesh = new Lot[w, l];
		}

		public bool getRandChance(int size) {
			int roll = Util.rand.Next(0, 1000);
			switch (size) {
			case 2:
				return roll < 270;
			case 3:
				return roll < 30;
			case 4:
				return roll < 10;
			default:
				return roll < 5;
			}
		}

		public LotMesh doAssimilate(Settings settings) {
			for (int i = 3; i > 1; --i) {
				assimilate(settings, i);
				assimilate(settings, i);
				assimilate(settings, i);
			}
			assimilateSmallLots(settings);
			return this;
		}

		public void assimilateSmallLots(Settings settings) {
			for (int i = 0; i < width-1; ++i) {
				for (int j = 0; j < length-1; ++j) {
					if (mesh[i, j] != null && !mesh[i, j].hasBeenAssimilated) {
						Vector3 lotCenter = new Vector3(mesh[i, j].x + mesh[i, j].width / 2,
						                                settings.mCityCenter.y,
						                                mesh[i, j].y + mesh[i, j].length / 2);
						float distToCenter = Vector3.Distance(lotCenter, settings.mCityCenter);
						mesh[i, j].interestingScore = 1 * 10000 - distToCenter;
						mesh[i, j].lotCenter = lotCenter;
						mesh[i, j].hasBeenAssimilated = true;
						//
						mesh[i, j].color = this.color;
						//
						settings.mInterestingLots.Push(mesh[i, j]);
					}
				}
			}
		}

		// Will try to merge lots into bigger (square) lots
		public LotMesh assimilate(Settings settings, int numMerged = 2) {
			for (int i = 0; i < width-1; ++i) {
				for (int j = 0; j < length-1; ++j) {
					bool merge = true;
					for (int k = 0; k < numMerged; ++k) {
						for (int l = 0; l < numMerged; ++l) {
							if (mesh[i+k,j+l] == null
							    	|| mesh[i+k,j+l].length != mesh[i,j].length
							    	|| mesh[i+k,j+l].width != mesh[i,j].width) {
								merge = false;
								break;
							}
						}
						if (!merge) break;
					}

					merge = merge && getRandChance(numMerged);

					if (merge) {
						for (int k = 0; k < numMerged; ++k) {
							for (int l = 0; l < numMerged; ++l) {
								if (k == 0 && l == 0) {
									mesh[i, j].width *= numMerged;
									mesh[i, j].length *= numMerged;
									Vector3 lotCenter = new Vector3(mesh[i, j].x + mesh[i, j].width / 2,
									                                settings.mCityCenter.y,
									                                mesh[i, j].y + mesh[i, j].length / 2);
									float distToCenter = Vector3.Distance(lotCenter, settings.mCityCenter);
									mesh[i, j].interestingScore = numMerged * 10000 - distToCenter;
									mesh[i, j].lotCenter = lotCenter;
									mesh[i, j].hasBeenAssimilated = true;
									//
									mesh[i, j].color = this.color;
									//
									settings.mInterestingLots.Push(mesh[i, j]);
									continue;
								}
								mesh[i + k,j + l] = null;
							}
						}
					}
				}
			}
			return this;
		}
	}
}