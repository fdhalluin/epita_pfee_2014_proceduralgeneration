﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using ScriptGen;
using HouseGen;


namespace Editor
{
public class BuildingWindow : EditorWindow {

		string script = "";
		string scriptName = "sphinx";
		
		Vector2 scrollPosition = new Vector2(0, 0);
		
		public houseBuilder builder;

		GameObject lastBuild = null;

		bool destroyLastBuild = true;
		
		string folder = "Assets/BuildingGenerator/Scripts/Buildings/buildScript/";

		ScriptGenerator scriptGenerator;

		public BuildingWindow() {
			scriptGenerator = new ScriptGenerator();
		}

		[MenuItem ("Town/Building Editor")]
		static void Init () {
			BuildingWindow window = (BuildingWindow)EditorWindow.GetWindow (typeof (BuildingWindow));
		}

		string videoPrefix = "video_";
		private int videoCount = 0;

		void OnGUI() {
//			GUI.Box(new Rect(10, 10, Screen.width - 20, Screen.height - 20), "");
			GUI.Label(new Rect(20, 18, 300, 20), "Script name: ");
			scriptName = GUI.TextField(new Rect(100, 20, 100, 20), scriptName);
			destroyLastBuild = GUI.Toggle(new Rect(20, 45, 200, 20), destroyLastBuild, "Destroy last compiled building");
			GUILayout.BeginArea(new Rect(20, 65, Screen.width - 40, Screen.height - 70));
			scrollPosition = GUILayout.BeginScrollView(scrollPosition);
			script = GUILayout.TextArea(script);
			GUILayout.EndScrollView();
			GUILayout.EndArea();
			
			if (GUI.Button(new Rect(205, 19, 50, 20), "Load")) {
				script = System.IO.File.ReadAllText(folder + scriptName + ".txt");
			}
			if (GUI.Button(new Rect(260, 19, 50, 20), "Save")) {
				System.IO.File.WriteAllText(folder + scriptName + ".txt", script);
			}
			if (GUI.Button(new Rect(315, 19, 68, 20), "Compile")) {
				builder = FindObjectOfType<houseBuilder>();

				if (destroyLastBuild && lastBuild != null) {
					DestroyImmediate(lastBuild);
				}

				lastBuild = builder.initFromScript(script);
			}
			if (GUI.Button(new Rect(220, 45, 68, 20), "Generate")) {
				scriptGenerator.height = 5;
				scriptGenerator.maxStorey = 3;
				scriptGenerator.maxWidth = 9;
				scriptGenerator.maxHeight = 9;

				script = scriptGenerator.getScript(BaseShape.RECT);
			}
			if (GUI.Button(new Rect(290, 45, 68, 20), "Video Next")) {
				script = System.IO.File.ReadAllText(folder + videoPrefix + videoCount++ + ".txt");

				builder = FindObjectOfType<houseBuilder>();
				
				if (destroyLastBuild && lastBuild != null) {
					DestroyImmediate(lastBuild);
				}
				
				lastBuild = builder.initFromScript(script);
			}
			if (GUI.Button(new Rect(360, 45, 44, 20), "Reset")) {
				videoCount = 0;
				if (destroyLastBuild && lastBuild != null) {
					DestroyImmediate(lastBuild);
				}
			}
			
		}

}

}
