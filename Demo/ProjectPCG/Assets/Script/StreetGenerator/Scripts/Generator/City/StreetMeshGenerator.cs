﻿using UnityEngine;
using System.Collections.Generic;
using HouseGen;

public class StreetMeshGenerator : MonoBehaviour {

	List<Vector3> vertices;
	List<int> triangles;
	List<Vector2> UVValues;
	Dictionary<string, Vector3> points;

	int totalColors = 5;

	public StreetMeshGenerator() {
		vertices = new List<Vector3> ();
		triangles = new List<int> ();
		UVValues = new List<Vector2> ();
		points = new Dictionary<string, Vector3> ();
	}

	public void addPoint(string name, Vector3 coord) {
		points.Add(name, coord);
	}

	public Vector3 getPoint(string name) {
		return points[name];
	}

	public void Clear() {
		points.Clear();
		vertices.Clear();
		triangles.Clear();
		UVValues.Clear();
	}

	public void DumpPoints() {
		string log = "";

		foreach(string name in points.Keys) {
			log += name + "\t" + points[name] + "\n";
		}

		Debug.Log (log);
	}

	public void addTriangle(string pointName1, string pointName2, string pointName3, int color, bool twoFaces = false) {
		// We add + 0.01f to avoid a strange behaviour with the UVMapping
		Vector2 colorVect = new Vector2((float)color / totalColors + 0.01f, 0.5f);
		int point1 = vertices.Count;
		vertices.Add(points[pointName1]);
		UVValues.Add(colorVect);
		int point2 = vertices.Count;
		vertices.Add(points[pointName2]);
		UVValues.Add(colorVect);
		int point3 = vertices.Count;
		vertices.Add(points[pointName3]);
		UVValues.Add(colorVect);
		
		triangles.Add (point1);
		triangles.Add (point2);
		triangles.Add (point3);
		
		if (twoFaces) {
			triangles.Add (point1);
			triangles.Add (point3);
			triangles.Add (point2);
		}
	}
	
	public void addQuadrilateral(string pointName1, string pointName2, string pointName3, string pointName4, int color, bool twoFaces = false) {
		addTriangle (pointName2, pointName1, pointName4, color, twoFaces);
		addTriangle (pointName2, pointName4, pointName3, color, twoFaces);
	}
	public void addQuadrilateral(string baseName, int color, bool twoFaces = false) {
		addQuadrilateral(baseName+1, baseName+2, baseName+3, baseName+4, color, twoFaces);
	}


	public GameObject createGameObject() {
		GameObject house = new GameObject();
		house.AddComponent<MeshFilter> ();
		house.AddComponent<MeshRenderer> ();
		house.AddComponent<MeshCollider> ();

		Mesh mesh = null;
		
		MeshFilter mf = house.GetComponent<MeshFilter>();
		mf.sharedMesh = new Mesh();
		Mesh meshCopy = Mesh.Instantiate(mf.sharedMesh) as Mesh;
		mf.mesh = meshCopy;
		mesh = meshCopy;

		mesh.vertices = vertices.ToArray ();
		mesh.triangles = triangles.ToArray ();
		mesh.uv = UVValues.ToArray ();
		mesh.Optimize ();
		mesh.RecalculateNormals ();

		house.name = "ROADS";
		house.transform.position = new Vector3(0, 5, 0);

		GameObject buildingsGenerator = GameObject.Find("BuildingsGenerator");
		houseBuilder builder = ((houseBuilder)buildingsGenerator.GetComponent(typeof(houseBuilder)));

		house.renderer.material = builder.baseMaterial3;

		return house;
	}

}
