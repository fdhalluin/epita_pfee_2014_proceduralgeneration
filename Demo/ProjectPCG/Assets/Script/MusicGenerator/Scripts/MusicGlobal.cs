﻿
using UnityEngine;
using System.Collections;

public enum MusicKind {
	empty,
	walk,
	countryside,
	colibri,
	classic,
	futur,
	melancholia
};

public enum KEY {
	C,
	Db,
	D,
	Eb,
	E,
	F,
	Gb,
	G,
	Ab,
	A,
	Bb,
	B
};

public class MusicGlobal {
	public static MusicKind music_kind = MusicKind.empty;
	public static int channel_count = 5;
	public static int max_channel_count = 10;
	public static float tempo = 90f;
	public static AudioClip lead;
	public static AudioClip backing;
	public static KEY key = 0;
	public static int seed = 1053;

	public static void setDefaultSettings() {
		MusicGlobal.tempo = 90f;
		MusicGlobal.lead = null;
		MusicGlobal.backing = null;
		MusicGlobal.key = 0;
		MusicGlobal.seed = 1053;
	}

	public static void setFirstSettings() {
		MusicGlobal.tempo = 100f;
		MusicGlobal.lead = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/piano-electric-reed-octave0.wav");
		MusicGlobal.backing = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/piano-electric-tine-octave2.wav");
		MusicGlobal.key = 0;
		MusicGlobal.seed = 42;
	}

	public static void setSecondSettings() {
		MusicGlobal.tempo = 120f;
		MusicGlobal.lead = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/banjo-C.wav");
		MusicGlobal.backing = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/ukelele damped buzz pluck bright hi.wav");
		MusicGlobal.key = 0;
		MusicGlobal.seed = 8061963;
	}

	public static void setThirdSettings() {
		MusicGlobal.tempo = 120f;
		MusicGlobal.lead = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/melodic/marimba2.wav");
		MusicGlobal.backing = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/woodblock.wav");
		MusicGlobal.key = 0;
		MusicGlobal.seed = 123456789;
	}

	public static void setFourthSettings() {
		MusicGlobal.tempo = 120f;
		MusicGlobal.lead = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/harpsichord-octave0.wav");
		MusicGlobal.backing = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/bass-ac-C-octave0.wav");
		MusicGlobal.key = 0;
		MusicGlobal.seed = 312;
	}

	public static void setFifthSettings() {
		MusicGlobal.tempo = 120f;
		MusicGlobal.lead = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/piano-FM-octave2.wav");
		MusicGlobal.backing = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/piano-FM-octave0.wav");
		MusicGlobal.key = 0;
		MusicGlobal.seed = 684;
	}

	public static void setSixthSettings() {
		MusicGlobal.tempo = 120f;
		MusicGlobal.lead = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/piano-electric-tine-octave2.wav");
		MusicGlobal.backing = Resources.LoadAssetAtPath<AudioClip>("Assets/Script/MusicGenerator/Resources/samples/piano-electric-tine-octave0.wav");
		MusicGlobal.key = 0;
		MusicGlobal.seed = 964853;
	}
}
