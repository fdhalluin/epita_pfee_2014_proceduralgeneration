using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using LotGeneration;
using System.IO;

public class VillageEditor : EditorWindow {
	private CityGenerator mCityGenerator;
	private CityKind mCityKind = CityKind.village;
	private Settings mSettings = null;
	private Vector2 lScrollViewVector = Vector2.zero;

	[MenuItem ("Town/Village Editor")]

	public static void Init () {
		VillageEditor window = (VillageEditor) EditorWindow.GetWindow(typeof (VillageEditor));
	}
	
	public void OnGUI () {
		lScrollViewVector =  EditorGUILayout.BeginScrollView(lScrollViewVector, GUILayout.Width(position.width), GUILayout.Height(position.height));

		EditorGUILayout.LabelField("General settings : ", EditorStyles.boldLabel);
		mCityKind = (CityKind)EditorGUILayout.EnumPopup("Theme", mCityKind);

		switch (mCityKind) {
		case CityKind.village:
			mSettings = VillageSettings.getInstance();
			break;
		case CityKind.city:
			mSettings = CitySettings.getInstance();
			break;
		case CityKind.manhattan:
			mSettings = ManhatthanSettings.getInstance();
			break;
		case CityKind.greekVillage:
			mSettings = GreekSettings.getInstance();
			break;
		}

		mSettings.mSkyKind = (SkyKind)EditorGUILayout.EnumPopup("Day time", mSettings.mSkyKind);
		mSettings.mSeed = EditorGUILayout.IntSlider("Seed", mSettings.mSeed, 1, 300);
		mSettings.mRandom = EditorGUILayout.Toggle("Random seed", mSettings.mRandom);

		EditorGUILayout.Separator();
		EditorGUILayout.LabelField("Village settings : ", EditorStyles.boldLabel);
		mSettings.mCitySize = EditorGUILayout.Slider("Size of the city", mSettings.mCitySize, 100, 800);
		mSettings.mNbNodes = EditorGUILayout.IntSlider("Organicity", mSettings.mNbNodes, 10, 250);
		mSettings.mLloydRelaxation = EditorGUILayout.IntSlider("Uniformization", mSettings.mLloydRelaxation, 0, 5);
		mSettings.mMaxHeightDiff = EditorGUILayout.IntSlider("Max diff elevation", mSettings.mMaxHeightDiff, 1, 25);
		mSettings.mLotDepth = EditorGUILayout.IntSlider("Lot depth", (int)mSettings.mLotDepth, 1, 50);
		mSettings.mNbFields = EditorGUILayout.IntSlider("Fields", (int)mSettings.mNbFields, 0, 5);

		EditorGUILayout.Separator();
		EditorGUILayout.LabelField("Props settings : ", EditorStyles.boldLabel);
		EditorGUILayout.BeginHorizontal();
		mSettings.mHaveCityPlace = EditorGUILayout.Toggle("Center Place", mSettings.mHaveCityPlace);
		mSettings.mHaveCarts = EditorGUILayout.Toggle("Carts", mSettings.mHaveCarts);
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.BeginHorizontal();
		mSettings.mHaveStreetLights = EditorGUILayout.Toggle("Street lights", mSettings.mHaveStreetLights);
		mSettings.mHavePhones = EditorGUILayout.Toggle("Phones", mSettings.mHavePhones);
		EditorGUILayout.EndHorizontal();
		mSettings.mHavePylones = EditorGUILayout.Toggle("Pylones", mSettings.mHavePylones);
		mSettings.mDistanceExtremityPylone = EditorGUILayout.IntSlider("Distance b-e pylones", (int)mSettings.mDistanceExtremityPylone, 800, 1350);
		mSettings.mDistanceBetweenPylone = EditorGUILayout.IntSlider("Distance between pylones", (int)mSettings.mDistanceBetweenPylone, 10, 17);


		EditorGUILayout.Separator();
		EditorGUILayout.LabelField("Building settings : ", EditorStyles.boldLabel);
		mSettings.mBuildingKind = (BuildingKind)EditorGUILayout.EnumPopup("Buildings", mSettings.mBuildingKind);

		switch (mSettings.mBuildingKind) {
		case BuildingKind.house:
			mSettings.GetHouseLots();
			break;
		case BuildingKind.building:
			mSettings.GetBuildingLots();
			break;
		case BuildingKind.cuteHouse:
			mSettings.GetCuteHouseLots();
			break;
		}
		mSettings.mNbLights = EditorGUILayout.IntSlider("Nb lights", mSettings.mNbLights, 0, 1000);
		mSettings.mNbDifferentHouses = EditorGUILayout.IntSlider("Different houses", mSettings.mNbDifferentHouses, 10, 500);
		mSettings.mFirstPaletteKind = (PaletteKind)EditorGUILayout.EnumPopup("Primary color", mSettings.mFirstPaletteKind);
		mSettings.mSecondPaletteKind = (PaletteKind)EditorGUILayout.EnumPopup("Second color", mSettings.mSecondPaletteKind);
		mSettings.mThirdPaletteKind = (PaletteKind)EditorGUILayout.EnumPopup("Minor color", mSettings.mThirdPaletteKind);
		EditorGUILayout.BeginHorizontal();
		mSettings.mLowEndModels = EditorGUILayout.Toggle("Low-end models", mSettings.mLowEndModels);
		mSettings.mPopulated = EditorGUILayout.Toggle("Increase houses nb", mSettings.mPopulated);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Separator();
		EditorGUILayout.LabelField("Street settings : ", EditorStyles.boldLabel);
		mSettings.mSmooth = EditorGUILayout.IntSlider("Noise streets ", mSettings.mSmooth, 0, 10);
		mSettings.mSmoothMainRoad = EditorGUILayout.IntSlider("Smooth main ", mSettings.mSmoothMainRoad, 0, 7);
		mSettings.mMinDistanceNodes = EditorGUILayout.IntSlider("Min distance city nodes", mSettings.mMinDistanceNodes, 50, 200);
		mSettings.mMinLongStreet = EditorGUILayout.IntSlider("Min length streets", mSettings.mMinLongStreet, 0, 20);

		EditorGUILayout.Separator();
		EditorGUILayout.LabelField("Connection settings : ", EditorStyles.boldLabel);
		mSettings.mNbOutsideNodes = EditorGUILayout.IntSlider("Max outside connections", mSettings.mNbOutsideNodes, 0, 5);
		mSettings.mMinDistanceOusideNodes = EditorGUILayout.IntSlider("Min distance connections", mSettings.mMinDistanceOusideNodes, 300, 1200);
		mSettings.mStep = 10 / mSettings.mRoadPrecision;

		EditorGUILayout.Separator();

		EditorGUILayout.BeginHorizontal();
		 
		if (GUILayout.Button("Generate"))
		{
			mCityGenerator = FindObjectOfType<CityGenerator>();
			mCityGenerator.ClearAll();
			mCityGenerator.GenerateCity(mSettings);
		}

		if (GUILayout.Button("Build"))
		{
			mCityGenerator = FindObjectOfType<CityGenerator>();
			houseBuilder.lowEndModels = mSettings.mLowEndModels;
			mCityGenerator.BuildHouses();

			TerrainData lData = Terrain.activeTerrain.terrainData;
			float[,,] lAlphaMap = lData.GetAlphamaps(0, 0, lData.alphamapWidth, lData.alphamapHeight);
			
			TerrainSettings lTerrainSetting = TerrainSettings.Instance;
			lTerrainSetting.setAlphaMap(lAlphaMap);
			
			mCityGenerator.ApplyTexture();
			mCityGenerator.PreRemoveBadTrees();
			mCityGenerator.RemoveBadHouses();

		}

		if (GUILayout.Button("Clear")) {
			mCityGenerator = FindObjectOfType<CityGenerator>();
			mCityGenerator.ClearAll();
			TerrainSettings lTerrainSetting = TerrainSettings.Instance;
			TerrainData lData = Terrain.activeTerrain.terrainData;
			if (lTerrainSetting.getAlphaMap() != null) {
				lData.SetAlphamaps(0, 0, lTerrainSetting.getAlphaMap());
			}
		}

		if (GUILayout.Button("Update light"))
		{
			mCityGenerator = FindObjectOfType<CityGenerator>();
			mCityGenerator.UpdateSky();
		}

		EditorGUILayout.EndHorizontal();
		EditorGUILayout.EndScrollView();
	}
}