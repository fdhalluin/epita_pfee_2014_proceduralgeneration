using System;
using System.Collections.Generic;
using UnityEngine;

namespace HouseGen {

enum Axis {X=0, Y=1, Z=2};

class Util {
	public static System.Random rand = new System.Random();
	
	public static Direction reverse(Direction d) {
		switch (d) {
		case Direction.NORTH: return Direction.SOUTH;
		case Direction.SOUTH: return Direction.NORTH;
		case Direction.WEST: return Direction.EAST;
		case Direction.EAST: return Direction.WEST;
		}

		return Direction.NONE;
		}

	public static Vector3 getPointByPercentage(Vector3 p1, Vector3 p2, float percentage) {
			Vector3 p = new Vector3(p1.x + (p2.x - p1.x)*percentage,
			                        p1.y + (p2.y - p1.y)*percentage,
			                        p1.z + (p2.z - p1.z)*percentage);

			return p;
	}
	
	public static Vector3 rotatePoint(Vector3 target, Vector3 center, double angle, Axis axis) {
		double x=target.x;
		double y=target.y;
		double z=target.z;
		
		switch (axis) {
			case Axis.X:
				x=target.x;
				y=(target.z-center.z)*Math.Sin(angle) + (target.y-center.y)*Math.Cos(angle) + center.y;
				z=(target.z-center.z)*Math.Cos(angle) - (target.y-center.y)*Math.Sin(angle) + center.z;
				break;
			case Axis.Y:
				x=(target.x-center.x)*Math.Cos(angle) - (target.z-center.z)*Math.Sin(angle) + center.x;
				y=target.y;
				z=(target.x-center.x)*Math.Sin(angle) + (target.z-center.z)*Math.Cos(angle) + center.z;
				break;
			case Axis.Z:
				x=(target.x-center.x)*Math.Cos(angle) - (target.y-center.y)*Math.Sin(angle) + center.x;
				y=(target.x-center.x)*Math.Sin(angle) + (target.y-center.y)*Math.Cos(angle) + center.y;
				z=target.z;
				break;
		}
		
		return new Vector3((float)x, (float)y, (float)z);		
	}	
	
	public static float computeAngle(Vector2 p1, Vector2 p2) {
		if (p1.x == p2.x) return 0;
		Vector2 c = new Vector2(p2.x-p1.x, p2.y-p1.y);
		
		return (float) Math.Atan((double)c.y/c.x) + (p2.x<p1.x?(float)Math.PI:0);
	}
	
	public static Vector4 computePlane(Vector3 p1, Vector3 p2, Vector3 p3) {
		Vector3 P = p2-p1;
		Vector3 Q = p3-p1;
		Vector3 coeffs = Vector3.Cross(P, Q);
		float d = -coeffs.x*p1.x - coeffs.y*p1.y - coeffs.z*p1.z;
	
		return new Vector4(coeffs.x, coeffs.y, coeffs.z, d);
	}
	
	}

}