
Room_20:
size 2 5
height 3
build Room_20 50 50
addRoom NORTH Door_21 RANDOM
displacement 0 0 0 0.1 
windows NORTH 0 1 3 SQUARE
windows SOUTH 0 1 2 SQUARE
windows WEST 0 3 2 SQUARE
windows EAST 0 2 2 SQUARE
windows NORTH 1 1 3 SQUARE
windows SOUTH 1 1 2 SQUARE
windows WEST 1 3 3 SQUARE
windows EAST 1 1 2 SQUARE
windows NORTH 2 1 0 SQUARE
windows SOUTH 2 1 1 SQUARE
windows WEST 2 3 0 SQUARE
windows EAST 2 2 2 SQUARE
roof 1 POINT 0.4



Door_21:
size 3 1
height 1
addWindow NORTH 0 1 DOOR
displacement 0 0 
windows NORTH 0 2 0 SQUARE
windows SOUTH 0 2 1 SQUARE
windows WEST 0 1 2 SQUARE
windows EAST 0 1 1 SQUARE
roof 1 SLOPE 0.7 0.4 NORTH


