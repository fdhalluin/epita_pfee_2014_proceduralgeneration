﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Graph<N> where N : ANode {

	public List<N> mNodes { get; set; }

	public Graph() {
		mNodes = new List<N>();
	}

	/* Add a new node in the graph */
	public void addNode(N iNode) {
		mNodes.Add(iNode);
	}

	/* Get the shortest path between the sourcenode and the destination node */
	public List<N> getDijkstra(ANode iSourceNode, ANode iDestinationNode) {
		reset();
		computePaths(iSourceNode);
		return shortestPathTo(iSourceNode, iDestinationNode);
	}

	/* Compute paths from the source node */
	private void computePaths(ANode iSourceNode) {
		iSourceNode.mMinDistance = 0;
		PriorityQueue<ANode> lNodeQueue = new PriorityQueue<ANode>(ANode.Comparer);
		lNodeQueue.Push(iSourceNode);
		
		while (lNodeQueue.Count != 0) {
			ANode lCurrentNode = lNodeQueue.Pop();
			foreach (KeyValuePair<ANode, int> lEntry in lCurrentNode.getAdjacentNodes()) {
				ANode lAdjacentNode = lEntry.Key;
				int lWeight = lEntry.Value;
				int lDistanceNode = lCurrentNode.mMinDistance + lWeight;
				if (lDistanceNode < lAdjacentNode.mMinDistance) {
					lNodeQueue.Remove(lAdjacentNode);
					lAdjacentNode.mMinDistance = lDistanceNode;
					lAdjacentNode.mPrievousNode = lCurrentNode;
					lNodeQueue.Push(lAdjacentNode);
				}
			}
		}
	}

	/* Get the path */
	private List<N> shortestPathTo(ANode iSourceNode, ANode iDestinationNode) {
		List<N> lPath = new List<N>();
		for (ANode lNode = iDestinationNode; lNode != null; lNode = lNode.mPrievousNode) {
			lPath.Add((N)lNode);
		}
		lPath.Reverse();
		return lPath;
	}

	/* Reset weights */
	private void reset() {
		foreach (N lNode in mNodes) {
			lNode.mMinDistance = 9999999;
			lNode.mPrievousNode = null;
		}
	}
}
