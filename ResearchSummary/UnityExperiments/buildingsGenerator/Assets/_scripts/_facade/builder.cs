﻿using UnityEngine;
using System.Collections;

public class builder : MonoBehaviour {
	
	public Texture2D bricks;
	public Texture2D window;
	public Texture2D pillar;
	public Texture2D store1;
	public Texture2D store2;
	public Texture2D store3;
	public Texture2D border;
	public int myVar;

	Texture2D blockText;
	GameObject building;

	static int textureHeight = 32;
	static int textureWidth = 32;

	public int textureBlockWidth;
	public int textureBlockHeight;

	IBuilder iBuilder;

	// Use this for initialization
	void Start () {
		iBuilder = new SimpleBuilder ();
		init ();
	}


	void init() {
		blockText = new Texture2D (textureBlockWidth*textureWidth,
		                           textureBlockHeight*textureHeight,
		                           TextureFormat.ARGB32,
		                           false);
		blockText.filterMode = FilterMode.Point;
		string desc = iBuilder.getVerticalDescription(textureBlockHeight-1);

		Debug.Log (desc);

		for (int i = desc.Length - 1; i >= 0; --i) {
			string story = iBuilder.getStory(desc[i], textureBlockWidth);
			Debug.Log (story);
			readTexturePlacementFromString (story, 0, i);
		}



		blockText.Apply ();	
		building = GameObject.CreatePrimitive (PrimitiveType.Cube);
		building.gameObject.renderer.material.mainTexture = blockText;
	}

	void readTexturePlacementFromString(string format, int x, int y) {
		for (int i = 0; i < format.Length; ++i) {
			Texture2D tempText = getText(format[i]);
			blockText.SetPixels( (x+i)*textureWidth, y*textureHeight,
			                    textureWidth, textureHeight,
			                    tempText.GetPixels(0, 0, textureWidth, textureHeight));
		}
	}

	Texture2D getText(char c) {
		switch (c) {
		case 'b' : return bricks;
		case 'w' : return window;
		case 'p' : return pillar;
		case '1' : return store1;
		case '2' : return store2;
		case '3' : return store3;
		case 'B' : return border;
		}

		return bricks;
	}

}
